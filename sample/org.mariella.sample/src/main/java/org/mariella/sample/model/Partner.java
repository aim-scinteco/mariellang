package org.mariella.sample.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

@javax.persistence.Entity
@Table(name="PARTNER")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(
	name="TYPE",
	discriminatorType=DiscriminatorType.STRING
)

public class Partner extends Entity {
	private String alias;
	private List<Partner> collaborators = new TrackedList<Partner>(propertyChangeSupport, "collaborators");
	
@Column(name="ALIAS")	
public String getAlias() {
	return alias;
}

public void setAlias(String alias) {
	propertyChangeSupport.firePropertyChange("alias", this.alias, this.alias = alias);
}

@ManyToMany
@JoinTable(
	name="COLLABORATORS",
	joinColumns=@JoinColumn(name="PARTNER_ID", referencedColumnName="ID"),
	inverseJoinColumns=@JoinColumn(name="COLLABORATOR_ID", referencedColumnName="ID")
)

public List<Partner> getCollaborators() {
	return collaborators;
}
	
}
