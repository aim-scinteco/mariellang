package org.mariella.sample.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="RESOURCE")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("File")
public class File extends Resource {
	private long size;

@Column(name="SIZE")
public long getSize() {
	return size;
}

public void setSize(long size) {
	propertyChangeSupport.firePropertyChange("size", this.size, this.size = size);
}

}
