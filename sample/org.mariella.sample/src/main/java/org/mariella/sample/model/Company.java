package org.mariella.sample.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="COMPANY")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue("C")
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Company extends Partner {
	private String name;

@Column(name="NAME")
public String getName() {
	return name;
}

public void setName(String name) {
	propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
}

}	
