package org.mariella.sample.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

@javax.persistence.Entity
@Table(name="RESOURCE")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="TYPE",
	discriminatorType=DiscriminatorType.STRING
)
public class Resource extends Entity {
	private String name;
	private Timestamp lastModified;
	private Folder parent;
	private TrackedList<Resource> children = new TrackedList<>(propertyChangeSupport, "children");
	
@Column(name="NAME")	
public String getName() {
	return name;
}

public void setName(String name) {
	propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
}

@ManyToOne
@JoinColumn(name="PARENT_ID", referencedColumnName="ID")
public Folder getParent() {
	return parent;
}

@OneToMany(mappedBy="parent")
public TrackedList<Resource> getChildren() {
	return children;
}

public void setParent(Folder parent) {
	propertyChangeSupport.firePropertyChange("parent", this.parent, this.parent = parent);
}

@Column(name="LAST_MODIFIED")
public Timestamp getLastModified() {
	return lastModified;
}

public void setLastModified(Timestamp lastModified) {
	propertyChangeSupport.firePropertyChange("lastModified", this.lastModified, this.lastModified = lastModified);
}
	
}
