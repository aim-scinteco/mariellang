package org.mariella.sample.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.mariella.persistence.database.BaseUUIDConverter;
import org.mariella.persistence.database.UUIDConverter;

public class H2UUIDConverter extends BaseUUIDConverter implements UUIDConverter {
	public static final String CONVERTER_NAME = "H2UUID";
	public static final H2UUIDConverter Singleton = new H2UUIDConverter();
	
	@Override
	public void setObject(PreparedStatement ps, int index, UUID value) throws SQLException {
		if (value == null) {
			ps.setNull(index, Types.BINARY);
		} else {
			ps.setObject(index, value, Types.BINARY);
		}
	}
	
	@Override
	public UUID getObject(ResultSet rs, int index) throws SQLException {
		return (UUID) rs.getObject(index);
	}

	@Override
	public UUID getObject(ResultSet rs, String column) throws SQLException {
		return (UUID) rs.getObject(column);
	}
	
	@Override
	public void printSql(StringBuilder b, UUID value) {
		if (value == null) {
			b.append("null");
		} else {
			b.append(toString(value));
		}
	}
}
