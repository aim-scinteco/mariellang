package org.mariella.sample.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.mariella.persistence.annotations.Converter;
import org.mariella.sample.persistence.H2UUIDConverter;

@MappedSuperclass
public class Entity {
	private UUID id;

	@Transient
	protected final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	public Entity() {
		this(UUID.randomUUID());
	}
	
	public Entity(UUID id) {
		this.id = id;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	@Id
	@Column(name="ID")
	@Converter(name = H2UUIDConverter.CONVERTER_NAME)
//	@GeneratedValue(strategy=GenerationType.AUTO)
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		propertyChangeSupport.firePropertyChange("id", this.id, this.id = id);
	}
	
}
