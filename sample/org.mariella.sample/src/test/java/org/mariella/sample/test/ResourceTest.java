package org.mariella.sample.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Timestamp;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.mariella.persistence.persistor.ClusterDescription;
import org.mariella.persistence.query.BinaryCondition;
import org.mariella.persistence.query.Expression;
import org.mariella.persistence.query.Literal;
import org.mariella.persistence.query.QueryBuilder;
import org.mariella.persistence.query.QueryExecutor;
import org.mariella.persistence.query.StringLiteral;
import org.mariella.persistence.query.TableReference;
import org.mariella.sample.model.File;
import org.mariella.sample.model.Folder;
import org.mariella.sample.model.Resource;
import org.mariella.sample.persistence.H2UUIDConverter;

class ResourceTest extends AbstractSimpleTest {

@Test
public void test() throws Exception {
	create();
	load();
	queryDiscriminator();
}

private void create() throws Exception {
	logger.info("inserting");

	createModificationTracker();
	Folder folder = new Folder();
	folder.setId(UUID.randomUUID());
	modificationTracker.addNewParticipant(folder);
	folder.setName("root");
	folder.setLastModified(new Timestamp(System.currentTimeMillis()));
	
	File file = new File();
	file.setId(UUID.randomUUID());
	modificationTracker.addNewParticipant(file);
	file.setParent(folder);
	assertEquals(folder.getChildren().size(), 1);
	assertTrue(folder.getChildren().get(0) == file);
	file.setName("test.txt");
	file.setLastModified(new Timestamp(System.currentTimeMillis()));
	file.setSize(5);

	persist();

	logger.info("updating");
	
	file.setSize(7);
	persist();
}

private void load() throws Exception {
	logger.info("loading");
	QueryBuilder queryBuilder;
	QueryExecutor queryExecutor;
	
	queryBuilder = new QueryBuilder(environment.getSchemaMapping());
	queryBuilder.join(getClassDescription(Resource.class), "root");
	queryBuilder.addSelectItem("root.id");
	queryBuilder.and(BinaryCondition.eq(queryBuilder.createColumnReference("root.name"), new StringLiteral("test.txt")));
	
	queryExecutor = new QueryExecutor(queryBuilder, createDatabaseAccess());
	UUID id = (UUID)queryExecutor.queryforObject();
	assertNotNull(id);


	ClusterDescription cd;
	Resource r;
	File file;
	
	// load cluster
	logger.info("loading cluster");
	createModificationTracker();
	cd = new ClusterDescription(getClassDescription(Resource.class), "root", "root.parent");
	r = loadById(cd, id, false);
	assertTrue(r instanceof File);
	file = (File)r;
	assertEquals(file.getSize(), 7);
	assertTrue(file.getParent() != null);
	assertTrue(file.getParent().getChildren().isEmpty());

	// load cluster
	logger.info("loading cluster");
	createModificationTracker();
	cd = new ClusterDescription(getClassDescription(Resource.class), "root", "root.parent", "root.parent.children");
	r = loadById(cd, id, false);
	assertTrue(r instanceof File);
	file = (File)r;
	assertEquals(file.getSize(), 7);
	assertTrue(file.getParent() != null);
	assertTrue(!file.getParent().getChildren().isEmpty());
}


private void queryDiscriminator() throws Exception {
	logger.info("query descriminator");
	QueryBuilder queryBuilder;
	QueryExecutor queryExecutor;

	queryBuilder = new QueryBuilder(environment.getSchemaMapping());
	queryBuilder.join(getClassDescription(Resource.class), "root");
	queryBuilder.addSelectItem("root.id");
	queryBuilder.and(BinaryCondition.eq(queryBuilder.createColumnReference("root.name"), new StringLiteral("test.txt")));
	
	queryExecutor = new QueryExecutor(queryBuilder, createDatabaseAccess());
	UUID id = (UUID)queryExecutor.queryforObject();
	assertNotNull(id);

	QueryBuilder discriminatorQueryBuilder = new QueryBuilder(environment.getSchemaMapping());
	TableReference tr = discriminatorQueryBuilder.join(getClassDescription(Resource.class), "root");
	discriminatorQueryBuilder.addSelectItem(
		new Expression() {
			@Override
			public void printSql(StringBuilder b) {
				b.append(tr.getAlias() + ".TYPE");
			}
		}
	);
	discriminatorQueryBuilder.and(BinaryCondition.eq(queryBuilder.createColumnReference("root.id"), new Literal<UUID>(H2UUIDConverter.Singleton, id)));
	
	queryExecutor = new QueryExecutor(discriminatorQueryBuilder, createDatabaseAccess());
	String discriminator = (String)queryExecutor.queryforObject();
	assertEquals(discriminator, "File");
}

}
