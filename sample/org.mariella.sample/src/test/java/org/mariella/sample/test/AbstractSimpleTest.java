package org.mariella.sample.test;

import java.io.InputStreamReader;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.Driver;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mariella.oxygen.runtime.core.OxyConnectionProvider;
import org.mariella.oxygen.runtime.impl.OxyJ2SEConnectionProvider;
import org.mariella.oxygen.runtime.impl.StandaloneEnvironment;
import org.mariella.persistence.annotations.mapping_builder.PersistenceBuilder;
import org.mariella.sample.persistence.H2UUIDConverter;

public class AbstractSimpleTest extends AbstractTest {
	public static final Logger logger = LogManager.getLogger(SimpleTest.class.getName());
	
@BeforeEach
public void beforeEach() throws SQLException {
	DriverManager.registerDriver(new Driver());

	connection = createConnectionProvider().getConnection();
	RunScript.execute(connection, new InputStreamReader(SimpleTest.class.getResourceAsStream("/create.sql")));
	connection.commit();

	environment = new StandaloneEnvironment() {
		@Override
		protected void configurePersistenceBuilder(PersistenceBuilder persistenceBuilder) {
			super.configurePersistenceBuilder(persistenceBuilder);
			persistenceBuilder.getConverterRegistry().registerConverter(H2UUIDConverter.CONVERTER_NAME, H2UUIDConverter.Singleton);
		}
	};
	environment.setConnectionProvider(createConnectionProvider());
	environment.initialize("sample/h2", new HashMap<>());

	connection = createConnectionProvider().getConnection();
}

@AfterEach
public void afterEach() throws SQLException {
	connection.close();
	connection = null;
	environment = null;
}

protected OxyConnectionProvider createConnectionProvider() {
	return new OxyJ2SEConnectionProvider("jdbc:h2:~/test", "sa", "");
}

}
