package org.mariella.sample.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.Driver;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mariella.oxygen.runtime.core.OxyConnectionProvider;
import org.mariella.oxygen.runtime.impl.OxyJ2SEConnectionProvider;
import org.mariella.oxygen.runtime.impl.StandaloneEnvironment;
import org.mariella.persistence.annotations.mapping_builder.PersistenceBuilder;
import org.mariella.persistence.persistor.ClusterDescription;
import org.mariella.persistence.query.BinaryCondition;
import org.mariella.persistence.query.Expression;
import org.mariella.persistence.query.Literal;
import org.mariella.persistence.query.QueryBuilder;
import org.mariella.persistence.query.QueryExecutor;
import org.mariella.persistence.query.StringLiteral;
import org.mariella.persistence.query.TableReference;
import org.mariella.sample.model.Company;
import org.mariella.sample.model.Partner;
import org.mariella.sample.model.Person;
import org.mariella.sample.persistence.H2UUIDConverter;

class SimpleTest extends AbstractSimpleTest {

@Test
public void test() throws Exception {
	create();
	load();
}

private void create() throws Exception {
	logger.info("inserting");

	createModificationTracker();
	Person p = new Person();
	p.setId(UUID.randomUUID());
	modificationTracker.addNewParticipant(p);
	p.setAlias("hs");
	p.setFirstName("Hug");
	p.setLastName("Schlonz");

	Company c = new Company();
	c.setId(UUID.randomUUID());
	modificationTracker.addNewParticipant(c);
	c.setAlias("Bellaflor");
	c.setName("Bellaflora Blumen GmbH & Co KG");
	
	p.getCollaborators().add(c);
	
	persist();

	logger.info("updating");
	
	p.setFirstName("Hugo");
	c.setAlias("Bellaflora");
	persist();
}

private void load() throws Exception {
	// Query Hugo by firstName
	// This query generates too many joins (company join is useless) -> SecondaryTableJoinBuilder
	logger.info("loading");
	QueryBuilder queryBuilder;
	QueryExecutor queryExecutor;
	
	createModificationTracker();
	queryBuilder = new QueryBuilder(environment.getSchemaMapping());
	queryBuilder.join(getClassDescription(Partner.class), "root");
	queryBuilder.addSelectItem("root.id");
	queryBuilder.and(BinaryCondition.eq(queryBuilder.createColumnReference("root.firstName"), new StringLiteral("Hugo")));
	
	queryExecutor = new QueryExecutor(queryBuilder, createDatabaseAccess());
	UUID id = (UUID)queryExecutor.queryforObject();
	assertNotNull(id);


	// load cluster
	logger.info("loading cluster");
	ClusterDescription cd = new ClusterDescription(getClassDescription(Partner.class), "root", "root.collaborators");
	Person hugo = loadById(cd, id, false);
	assertNotNull(hugo);
	assertEquals(hugo.getFirstName(), "Hugo");
	assertEquals(hugo.getCollaborators().size(), 1);
	Company c = (Company)hugo.getCollaborators().get(0);
	assertTrue(c.getName().startsWith("Bellaflora"));
}


// this fails because it is challenging to determine the alias of the discriminator´s TableReference 
private void problem() throws Exception {
	logger.info("loading");
	QueryBuilder queryBuilder;
	QueryExecutor queryExecutor;
	
	createModificationTracker();
	queryBuilder = new QueryBuilder(environment.getSchemaMapping());
	queryBuilder.join(getClassDescription(Partner.class), "root");
	queryBuilder.addSelectItem("root.id");
	queryBuilder.and(BinaryCondition.eq(queryBuilder.createColumnReference("root.firstName"), new StringLiteral("Hugo")));
	
	queryExecutor = new QueryExecutor(queryBuilder, createDatabaseAccess());
	UUID id = (UUID)queryExecutor.queryforObject();
	assertNotNull(id);

	// query discrimiator of Hugo
	QueryBuilder discriminatorQueryBuilder = new QueryBuilder(environment.getSchemaMapping());
	TableReference tr = discriminatorQueryBuilder.join(getClassDescription(Partner.class), "root");
	discriminatorQueryBuilder.addSelectItem(
		new Expression() {
			@Override
			public void printSql(StringBuilder b) {
				b.append(tr.getAlias() + ".TYPE");
			}
		}
	);
	discriminatorQueryBuilder.and(BinaryCondition.eq(queryBuilder.createColumnReference("root.id"), new Literal<UUID>(H2UUIDConverter.Singleton, id)));
	
	queryExecutor = new QueryExecutor(discriminatorQueryBuilder, createDatabaseAccess());
	String discriminator = (String)queryExecutor.queryforObject();
	assertEquals(discriminator, "P");
}

}
