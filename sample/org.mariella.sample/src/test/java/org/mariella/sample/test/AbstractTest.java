package org.mariella.sample.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.mariella.oxygen.runtime.impl.StandaloneEnvironment;
import org.mariella.persistence.loader.ClusterLoader;
import org.mariella.persistence.loader.ClusterLoaderConditionProvider;
import org.mariella.persistence.loader.ClusterLoaderConditionProviderImpl;
import org.mariella.persistence.loader.LoaderContext;
import org.mariella.persistence.mapping.ClassMapping;
import org.mariella.persistence.mapping.ColumnMapping;
import org.mariella.persistence.mapping.RelationshipPropertyMapping;
import org.mariella.persistence.persistor.ClusterDescription;
import org.mariella.persistence.persistor.ConnectionDatabaseAccess;
import org.mariella.persistence.persistor.DatabaseAccess;
import org.mariella.persistence.persistor.Persistor;
import org.mariella.persistence.query.BinaryCondition;
import org.mariella.persistence.query.Expression;
import org.mariella.persistence.query.InCondition;
import org.mariella.persistence.query.JoinBuilder;
import org.mariella.persistence.query.QueryBuilder;
import org.mariella.persistence.query.TableReference;
import org.mariella.persistence.runtime.ModificationTracker;
import org.mariella.persistence.runtime.ModificationTrackerImpl;
import org.mariella.persistence.runtime.RIListener;
import org.mariella.persistence.schema.ClassDescription;
import org.mariella.sample.persistence.H2UUIDConverter;

public class AbstractTest {
	public static class InClusterLoaderConditionProvider extends ClusterLoaderConditionProviderImpl {
		private final String idPropertyPath;
		private final Collection<UUID> ids;

		public InClusterLoaderConditionProvider(Collection<UUID> ids) {
			this(ids, null);
		}
		
		public InClusterLoaderConditionProvider(Collection<UUID> ids, String idPropertyPath) {
			this.ids = ids;
			this.idPropertyPath = idPropertyPath != null ? idPropertyPath : "root.id";
		}
		
		@Override
		public void pathExpressionJoined(QueryBuilder queryBuilder, String pathExpression, ClassMapping classMapping, TableReference tableReference) {
			List<Expression> inExpressions = new ArrayList<>(ids.size());
			H2UUIDConverter guidConverter = H2UUIDConverter.Singleton;
			for (Object id : ids) {
				inExpressions.add(guidConverter.createLiteral(id));
			}
			queryBuilder.and(
				new InCondition(queryBuilder.createColumnReference(idPropertyPath), inExpressions)
			);		
		}
		
		@Override
		public String[] getConditionPathExpressions() {
			return new String[] { "root" };
		}		
	}	
	
	protected StandaloneEnvironment environment;
	protected ModificationTracker modificationTracker;
	protected Connection connection;

protected void createModificationTracker() {
	ModificationTrackerImpl m = new ModificationTrackerImpl(environment.getSchemaMapping().getSchemaDescription());
	m.addPersistentListener(new RIListener(m));
	modificationTracker = m;
}
	
public void persist() throws SQLException {
	Persistor persistor = new Persistor(environment.getSchemaMapping(), new ConnectionDatabaseAccess(connection), modificationTracker);
	persistor.persist();
	connection.commit();
}
	
public DatabaseAccess createDatabaseAccess() {
	return new ConnectionDatabaseAccess(connection);
}

@SuppressWarnings("unchecked")
public <T> T loadById(final ClusterDescription cd, final Object identity, final boolean isUpdate) {
	ClusterLoaderConditionProvider cp = new ClusterLoaderConditionProvider() {
		@Override
		public String[] getConditionPathExpressions() {
			return new String[] { "root" };
		}
		@Override
		public void aboutToJoinRelationship(QueryBuilder queryBuilder, String pathExpression, RelationshipPropertyMapping rpm, JoinBuilder joinBuilder) {
		}
		public void pathExpressionJoined(QueryBuilder queryBuilder, String pathExpression, final ClassMapping classMapping, TableReference tableReference) {
			if(pathExpression.equals("root")) {
				for(final ColumnMapping columnMapping : classMapping.getPrimaryKey().getColumnMappings()) {
					Expression condition = BinaryCondition.eq(
						tableReference.createColumnReference(columnMapping.getReadColumn()),
						columnMapping.getReadColumn().getConverter().createLiteral(identity)
					);
					queryBuilder.and(condition);
				}
			}
		}
	};
	List<Object> objects = load(cd, isUpdate, cp);
	return objects != null && !objects.isEmpty() ? (T) objects.get(0) : null;
}

@SuppressWarnings("unchecked")
public <T> T loadById(Class<T> entityClass, boolean isUpdate, Object id, String... clusterExpressions) {
	if (clusterExpressions == null || clusterExpressions.length == 0) {
		clusterExpressions = new String[] { "root" };
	}
	return (T) loadById(new ClusterDescription(getClassDescription(entityClass), clusterExpressions), id, isUpdate);
}


public <T> List<T> loadByIds(Class<T> entityClass, boolean isUpdate, final Collection<UUID> ids, String... pathExpressions) {
	return load(new ClusterDescription(getClassDescription(entityClass), pathExpressions), isUpdate, new InClusterLoaderConditionProvider(ids));
}

@SuppressWarnings("unchecked")
protected <T> List<T> load(final ClusterDescription cd, final boolean isUpdate, final ClusterLoaderConditionProvider conditionProvider) {
	ClusterLoader clusterLoader = new ClusterLoader(environment.getSchemaMapping(), cd);
	LoaderContext loaderContext = new LoaderContext(modificationTracker);
	loaderContext.setUpdate(isUpdate);
	return (List<T>)clusterLoader.load(new ConnectionDatabaseAccess(connection), loaderContext, conditionProvider);
}


protected ClassDescription getClassDescription(Class<?> clazz) {
	return environment.getSchemaMapping().getSchemaDescription().getClassDescription(clazz.getName());
}

}
