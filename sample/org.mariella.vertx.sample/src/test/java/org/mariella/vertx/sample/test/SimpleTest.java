package org.mariella.vertx.sample.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.jdbcclient.JDBCConnectOptions;
import io.vertx.jdbcclient.JDBCPool;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.Tuple;


@ExtendWith(VertxExtension.class)
class SimpleTest {
	
	private SqlClient sqlClient;
	
	@BeforeEach
	void setupBeforeEach(VertxTestContext ctx, Vertx vertx) {
		sqlClient = createSqlClient(vertx);
		vertx.fileSystem().readFile("create.sql")
			.compose(createCommands -> sqlClient.preparedQuery(createCommands.toString()).execute())
			.onSuccess(r -> ctx.completeNow())
			.onFailure(cause -> ctx.failNow(cause));
	}
	
	@Test
	void test(VertxTestContext ctx, Vertx vertx) throws Exception {
		
		/* given */
		UUID id = UUID.randomUUID();
		
		/* when (insert and select) */
		insertCustomer(id)
			.compose(rowSet -> selectCustomers())
		
			/* then exactly one record is found and the id matches */
			.onComplete(ctx.succeeding(rowSet -> ctx.verify(() -> {
				assertEquals(1, rowSet.size());
				assertEquals(id, rowSet.iterator().next().getUUID(0));
				ctx.completeNow();
			})));
	}
	
/*	
	Folder folder = load("root", "root.children")
	folder.setlastModified(...)
	commit

	-----
	
	connection ->
	objectcontext
	executeAndBuildObjects("root") -> future
	executeAndBuildObjects("root.children") -> future
	SELECT RESOURCE.TYPE, RESOURCE.ID, RESOURCE1.NAME, RESOURCE1.LAST_MODIFIED, RESOURCE1.ID FROM RESOURCE RESOURCE LEFT OUTER JOIN RESOURCE RESOURCE1 ON RESOURCE1.TYPE IN ('Folder') AND RESOURCE.PARENT_ID = RESOURCE1.ID WHERE RESOURCE.ID = 'BBDD26F013D348A0A1662F21E60FB6F6'
	where resource1.id in (asdlfjslöadfjölasjdf)
	
	
	return future (waitforfutures)
	
	connection ondemand
	
*/
			
	protected Future<RowSet<Row>> insertCustomer(UUID id) {
		return sqlClient.preparedQuery("INSERT INTO customer (id) VALUES ($1)").execute(Tuple.of(id));
	}
	
	protected Future<RowSet<Row>> selectCustomers() {
		return sqlClient.preparedQuery("SELECT * FROM customer").execute();
	}

	protected SqlClient createSqlClient(Vertx vertx) {
		return JDBCPool.pool(vertx,
				// configure the connection
				new JDBCConnectOptions()
						.setJdbcUrl("jdbc:h2:./target/test")
						.setUser("sa")
						.setPassword(""),
				// configure the pool
				new PoolOptions().setMaxSize(16));
	}

}
