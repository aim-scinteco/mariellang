package org.mariella.persistence.h2;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mariella.persistence.database.Column;
import org.mariella.persistence.database.Converter;
import org.mariella.persistence.database.SingleRowPreparedStatementBuilder;
import org.mariella.persistence.mapping.JoinedClassMapping;
import org.mariella.persistence.mapping.PrimaryKeyJoinColumn;
import org.mariella.persistence.persistor.ObjectPersistor;
import org.mariella.persistence.persistor.PersistenceStatementsManager;
import org.mariella.persistence.persistor.PersistenceStatementsManager.PersistenceStatement;
import org.mariella.persistence.persistor.Row;
import org.mariella.persistence.runtime.MariellaPersistence;

public class H2JoinedUpsertStatementBuilder extends SingleRowPreparedStatementBuilder {
	private interface BuildCallback {
		public void columnValue(StringBuilder b, Column column);
	}

	private final ObjectPersistor objectPersistor;
	private final JoinedClassMapping classMapping;

	public H2JoinedUpsertStatementBuilder(ObjectPersistor objectPersistor, JoinedClassMapping classMapping, Row row) {
		super(row);
		this.objectPersistor = objectPersistor;
		this.classMapping = classMapping;
	}
	
	@Override
	public void initialize() {
		super.initialize();
		for(PrimaryKeyJoinColumn primaryKeyJoinColumn : classMapping.getPrimaryKeyJoinColumns().getPrimaryKeyJoinColumns()) {
			Object value = objectPersistor.getModifiableAccessor().getValue(objectPersistor.getModificationInfo().getObject(), primaryKeyJoinColumn.getPrimaryKeyProperty());
			getRow().setProperty(primaryKeyJoinColumn.getJoinTableColumn(), value);
		}
	}

	
	@Override
	public void execute(PersistenceStatementsManager psManager) {
		final List<Column> columns = new ArrayList<Column>();
		String sql = buildSqlString(
			new BuildCallback() {
				@Override
				public void columnValue(StringBuilder b, Column column) {
					b.append(column.getConverter() == null ? "?" : column.getConverter().createParameter());
					columns.add(column);
				}
			}
		);
		
		try {
			PersistenceStatement ps = psManager.prepareStatement(row.getTable().getName(), false, sql);
			int index = 1;
			for (Column column : columns) {
				column.setObject(ps.getPreparedStatement(), index++, row.getProperty(column));
			}
			ps.execute(getSqlDebugString());
		} catch(SQLException e) {
			MariellaPersistence.logger.atError().withThrowable(e).log("Failed to execute upsert statement: {}", sql);
			throw new RuntimeException("Failed to execute upsert statement");
		}	
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public String getSqlDebugString() {
		return buildSqlString(
			new BuildCallback() {
				@Override
				@SuppressWarnings("rawtypes")
				public void columnValue(StringBuilder b, Column column) {
					Converter conv = column.getConverter();
					b.append(conv.toString(row.getProperty(column)));
				}
			}
		);
	}
	
	private String buildSqlString(BuildCallback buildCallback) {
		boolean first;

		List<Column> requiredNotSetColumns = new ArrayList<>();
		StringBuilder key = new StringBuilder();
		key.append(" KEY (");
		first = true;
		for(PrimaryKeyJoinColumn primaryKeyJoinColumn : classMapping.getPrimaryKeyJoinColumns().getPrimaryKeyJoinColumns()) {
			if(first) first = false; else key.append(", ");
			key.append(primaryKeyJoinColumn.getJoinTableColumn().getName());
			if(!row.getSetColumns().contains(primaryKeyJoinColumn.getJoinTableColumn())) {
				requiredNotSetColumns.add(primaryKeyJoinColumn.getJoinTableColumn());
			}
		}
		key.append(")");

		StringBuilder b = new StringBuilder();
		b.append("MERGE INTO "+ row.getTable().getName());
		b.append(" (");
		first = true;
		for(Column column : row.getSetColumns()) {
			if(first) first = false; else b.append(", ");
			b.append(column.getName());
		}
		for(Column column : requiredNotSetColumns) {
			if(first) first = false; else b.append(", ");
			b.append(column.getName());
		}
		b.append(") ");
		
		b.append(key);
		
		b.append(" VALUES (");
		first = true;
		for(Column column : row.getSetColumns()) {
			if(first) first = false; else b.append(", ");
			buildCallback.columnValue(b, column);
		}
		
		for(Column column : requiredNotSetColumns) {
			if(first) first = false; else b.append(", ");
			buildCallback.columnValue(b, column);	
		}
		
		b.append(")");
			
		return b.toString();
	}
	
}
