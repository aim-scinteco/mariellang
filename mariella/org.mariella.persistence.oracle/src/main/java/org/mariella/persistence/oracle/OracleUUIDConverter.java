package org.mariella.persistence.oracle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.mariella.persistence.database.BaseUUIDConverter;
import org.mariella.persistence.database.UUIDConverter;

public class OracleUUIDConverter extends BaseUUIDConverter implements UUIDConverter {

	@Override
	public void setObject(PreparedStatement ps, int index, UUID value) throws SQLException {
		if (value == null) {
			ps.setNull(index, Types.VARBINARY);
		} else {
			ps.setBytes(index, toBytes(value));
		}
	}
	
	@Override
	public UUID getObject(ResultSet rs, int index) throws SQLException {
		byte[] bytes = rs.getBytes(index);
		return bytes != null ? toUUID(bytes) : null;
	}

	@Override
	public UUID getObject(ResultSet rs, String column) throws SQLException {
		return toUUID(rs.getBytes(column));
	}
	
	private static UUID toUUID(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		if (bytes.length != 16) {
			throw new IllegalArgumentException();
		}
		long high = 0;
		for (int i = 0; i < 8; i++) {
			high = high * 256 + ((long) bytes[i] & 0xffL);
		}
		long low = 0;
		for (int i = 8; i < 16; i++) {
			low = low * 256 + ((long) bytes[i] & 0xffL);
		}
		return new UUID(high, low);
	}
	
	private static byte[] toBytes(UUID uuid) {
		if (uuid == null) return null;
		byte[] result = new byte[16];
		long high = uuid.getMostSignificantBits();
		for (int i = 7; i >= 0; i--) {
			result[i] = (byte) (high & 0xffL);
			high >>= 8;
		}
		long low = uuid.getLeastSignificantBits();
		for (int i = 15; i >= 8; i--) {
			result[i] = (byte) (low & 0xffL);
			low >>= 8;
		}
		return result;
	}

	@Override
	public void printSql(StringBuilder b, UUID value) {
		if (value == null) {
			b.append("null");
		} else {
			b.append("hextoraw(").append(toString(value)).append(")");
		}
	}
}
