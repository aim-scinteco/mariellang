package org.mariella.persistence.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.mariella.persistence.persistor.PersistenceStatementsManager;
import org.mariella.persistence.persistor.PersistenceStatementsManager.PersistenceStatement;
import org.mariella.persistence.runtime.MariellaPersistence;
import org.mariella.persistence.persistor.Row;


public class InsertStatementBuilder extends SingleRowPreparedStatementBuilder {

public InsertStatementBuilder(Row row) {
	super(row);
}

@Override
public void execute(PersistenceStatementsManager psManager) {
	String sql = getInsertString();
	try {
		PersistenceStatement ps = psManager.prepareStatement(row.getTable().getName(), false, sql);
		setParameters(ps.getPreparedStatement());
		ps.execute(getSqlDebugString());
	} catch(SQLException e) {
		MariellaPersistence.logger.atError().withThrowable(e).log("Failed to execute insert statement: {}", sql);
		throw new RuntimeException("Failed to execute insert statement");
	}
}

protected String getInsertString() {
	StringBuilder b = new StringBuilder();
	b.append("INSERT INTO ");
	b.append(row.getTable().getName());
	b.append(" (");
	boolean first = true;
	for(Column column : row.getSetColumns()) {
		if(first) first = false;
		else b.append(", ");
		b.append(column.getName());
	}
	b.append(") VALUES (");
	first = true;
	for(Column column : row.getSetColumns()) {
		if(first) first = false;
		else b.append(", ");
		b.append(column.getConverter() == null ? "?" : column.getConverter().createParameter());
	}
	b.append(")");
	return b.toString();
}

protected void setParameters(PreparedStatement ps) throws SQLException {
	int index = 1;
	for(Column column : row.getSetColumns()) {
		column.setObject(ps, index, row.getProperty(column));
		index++;
	}
}

@SuppressWarnings("unchecked")
@Override
public String getSqlDebugString() {
	StringBuilder b = new StringBuilder();
	b.append("INSERT INTO ");
	b.append(row.getTable().getName());
	b.append(" (");
	boolean first = true;
	for(Column column : row.getSetColumns()) {
		if(first) first = false;
		else b.append(", ");
		b.append(column.getName());
	}
	b.append(") VALUES (");
	first = true;
	for(Column column : row.getSetColumns()) {
		if(first) first = false;
		else b.append(", ");
		@SuppressWarnings("rawtypes")
		Converter conv = column.getConverter();
		b.append(conv.toString(row.getProperty(column)));
	}
	b.append(")");
	return b.toString();
}

}
