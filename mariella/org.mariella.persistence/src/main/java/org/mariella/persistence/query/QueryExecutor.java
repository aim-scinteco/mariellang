package org.mariella.persistence.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mariella.persistence.database.ConnectionCallback;
import org.mariella.persistence.database.Converter;
import org.mariella.persistence.persistor.DatabaseAccess;
import org.mariella.persistence.runtime.MariellaPersistence;

public class QueryExecutor {
	private final QueryBuilder queryBuilder;
	private final DatabaseAccess databaseAccess;
	
public QueryExecutor(QueryBuilder queryBuilder,DatabaseAccess databaseAccess) {
	this.queryBuilder = queryBuilder;
	this.databaseAccess = databaseAccess;
}

protected void setParameters(PreparedStatement ps) throws SQLException {
}

public Object getObject(ResultSet rs, int index) throws SQLException {
	Expression e = queryBuilder.getSubSelect().getSelectClause().getSelectItems().get(index - 1);
	if(e instanceof ScalarExpression) {
		Converter<?> converter = ((ScalarExpression)e).getConverter();
		if(converter != null) {
			return converter.getObject(rs, index);
		}
	}
	return rs.getObject(index);
}

public Object queryforObject() throws SQLException {
	List<Object[]> result = queryForObjects();
	if(result.size() != 1) {
		throw new RuntimeException("Expected a single result. Actual result size is " + result.size());
	}
	if(result.get(0).length != 1) {
		throw new RuntimeException("The result row must contain a single value. The actual result row contains " + result.get(0).length + " values");
	}
	return result.get(0)[0];
}

public List<Object[]> queryForObjects() throws SQLException {
	final List<Object[]> result = new ArrayList<>();
	query(new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Object[] values = new Object[queryBuilder.getSubSelect().getSelectClause().getSelectItems().size()];
				for(int i = 0; i<values.length; i++) {
					values[i] = getObject(rs, i+1);
				}
				result.add(values);
			}
		}
	);
	return result;
}

public void query(RowCallbackHandler rowCallbackHandler) throws SQLException {
	databaseAccess.doInConnection(
		new ConnectionCallback() {
			@Override
			public Object doInConnection(Connection connection) throws SQLException {
				StringBuilder b = new StringBuilder();
				queryBuilder.getSubSelect().printSql(b);
				MariellaPersistence.logger.debug(b.toString());
				PreparedStatement ps = connection.prepareStatement(b.toString());
				try {
					setParameters(ps);
					ResultSet rs = ps.executeQuery();
					try {
						while(rs.next()) {
							rowCallbackHandler.processRow(rs);
						}
					} finally {
						rs.close();
					}
				} finally {
					ps.close();
				}
				return null;
			}
		}
	);
}



}
