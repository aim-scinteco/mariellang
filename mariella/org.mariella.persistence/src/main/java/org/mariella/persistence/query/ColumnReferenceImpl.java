package org.mariella.persistence.query;


import org.mariella.persistence.database.Column;
import org.mariella.persistence.database.Converter;

public class ColumnReferenceImpl implements ColumnReference {
	private Column column;
	private Expression tableReference;
	
public ColumnReferenceImpl(Expression tableReference, Column column) {
	super();
	this.tableReference = tableReference;
	this.column = column;
}
	
public Column getColumn() {
	return column;
}

public void setColumn(Column column) {
	this.column = column;
}

@Override
@SuppressWarnings("unchecked")
public <T> Converter<T> getConverter() {
	return (Converter<T>)column.getConverter();
}

public Expression getTableReference() {
	return tableReference;
}

public void setTableReference(Expression tableReference) {
	this.tableReference = tableReference;
}

public void printSql(StringBuilder b) {
	tableReference.printSql(b);
	b.append('.');
	b.append(column.getName());
}

}
