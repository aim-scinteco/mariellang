package org.mariella.persistence.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import org.mariella.persistence.query.Literal;

public abstract class BaseUUIDConverter implements UUIDConverter {
	protected class UUIDLiteral extends Literal<UUID> {
		public UUIDLiteral(UUID value) {
			super(BaseUUIDConverter.this, value);
		}
		
		@Override
		public void printSql(StringBuilder b) {
			BaseUUIDConverter.this.printSql(b, value);
		}
	}
	
	@Override
	public String toString(UUID uuid) {
		return uuid == null ? "null" : String.format("'%s'", uuid.toString().replace("-", "").toUpperCase());
	}
	
	@Override
	public final void setObject(PreparedStatement ps, int index, int type, UUID value) throws SQLException {
		setObject(ps, index, value);
	}
	
	@Override
	public String createParameter() {
		return "?";
	}
	
	@Override
	public Literal<UUID> createLiteral(Object value) {
		return new UUIDLiteral((UUID) value);
	}

	@Override
	public Literal<UUID> createDummy() {
		return new UUIDLiteral(UUID.randomUUID());
	}
}
