package org.mariella.persistence.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mariella.persistence.query.Literal;


public class DateToStringConverter implements Converter<Timestamp> {
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private static final DateToStringConverter INSTANCE = new DateToStringConverter();
	private static final Logger LOGGER = LogManager
			.getLogger(DateToStringConverter.class);
	
	private static class TimestampLiteral extends Literal<Timestamp> {
		public TimestampLiteral(Timestamp value) {
			super(DateToStringConverter.getInstance(), value);
		}
	}
	
	public static DateToStringConverter getInstance() {
		return INSTANCE;
	}
	
	private DateToStringConverter() {
		
	}
	
	@Override
	public void setObject(PreparedStatement ps, int index, int type, Timestamp value) throws SQLException {
		ps.setString(index, value != null ? FORMAT.format(value) : null);
	}

	@Override
	public Timestamp getObject(ResultSet rs, int index) throws SQLException {
		String s = rs.getString(index);
		if (s == null || s.isEmpty()) {
			return null;
		}
		try {
			return new Timestamp(FORMAT.parse(s).getTime());
		} catch (ParseException e) {
			LOGGER.warn("Invalid date string: {}", s);
			return null;
		}
	}

	@Override
	public String createParameter() {
		return "?";
	}

	@Override
	public Literal<Timestamp> createLiteral(Object value) {
		return new TimestampLiteral((Timestamp) value);
	}

	@Override
	public Literal<Timestamp> createDummy() {
		return createLiteral(new Timestamp(0));
	}

	@Override
	public String toString(Timestamp value) {
		if (value == null) {
			return "NULL";
		} else {
			return String.format("DATE '%s'", FORMAT.format(value));
		}
	}
}
