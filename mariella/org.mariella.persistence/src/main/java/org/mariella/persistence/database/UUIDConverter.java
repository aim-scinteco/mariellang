package org.mariella.persistence.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public interface UUIDConverter extends Converter<UUID> {
	void setObject(PreparedStatement stmt, int paramterIndex, UUID value) throws SQLException;
	UUID getObject(ResultSet rs, String columnName) throws SQLException;
	void printSql(StringBuilder b, UUID value);
}
