package org.mariella.persistence.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.mariella.persistence.query.Literal;


public interface Converter<T> {
	void setObject(PreparedStatement ps, int index, int type, T value) throws SQLException;
	T getObject(ResultSet rs, int index) throws SQLException;
	String createParameter();
	Literal<T> createLiteral(Object value);
	Literal<T> createDummy();
	String toString(T value);
}
