package org.mariella.oxygen.runtime.impl;

import java.util.Map;

import javax.persistence.PersistenceException;
import javax.sql.DataSource;

import org.mariella.oxygen.basic_impl.DefaultClassResolver;
import org.mariella.oxygen.runtime.core.OxyConnectionProvider;
import org.mariella.persistence.annotations.processing.ClassLoaderPersistenceUnitParser;
import org.mariella.persistence.annotations.processing.OxyUnitInfoBuilder;
import org.mariella.persistence.annotations.processing.PersistenceUnitParser;
import org.mariella.persistence.mapping.OxyUnitInfo;

public class StandaloneEnvironment extends EnvironmentImpl {

	private OxyConnectionProvider connectionProvider;
	private OxyEntityTransactionFactory entityTransactionFactory;

	public StandaloneEnvironment() {
		super();
	}


	@Override
	public void initialize(String emName, Map<?, ?> properties) {
		try {
			this.properties = properties;
			PersistenceUnitParser parser = new ClassLoaderPersistenceUnitParser(getClass().getClassLoader());
			OxyUnitInfoBuilder builder = new OxyUnitInfoBuilder(parser);
			builder.build();

			for (OxyUnitInfo oxyUnitInfo : builder.getOxyUnitInfos()) {
				if (oxyUnitInfo.getPersistenceUnitName().equals(emName)) {
					this.oxyUnitInfo = oxyUnitInfo;
				}
			}
			if (this.oxyUnitInfo == null) {
				throw new IllegalStateException("Could not find any META-INF/persistence.xml having name " + emName);
			}

			persistenceClassResolver = new DefaultClassResolver(getClass().getClassLoader());

			createSchemaMapping();
		} catch (Throwable t) {
			throw new PersistenceException(t);
		}
	}

	public OxyConnectionProvider createConnectionProvider() {
		return connectionProvider;
	}

	public void setConnectionProvider(OxyConnectionProvider connectionProvider) {
		this.connectionProvider = connectionProvider;
	}

	public OxyEntityTransactionFactory createEntityTransactionFactory() {
		return entityTransactionFactory;
	}

	public void setEntityTransactionFactory(OxyEntityTransactionFactory entityTransactionFactory) {
		this.entityTransactionFactory = entityTransactionFactory;
	}

	public void setDataSource(DataSource dataSource) {
		setConnectionProvider(new OxyDataSourceConnectionProvider(dataSource));
	}
}
