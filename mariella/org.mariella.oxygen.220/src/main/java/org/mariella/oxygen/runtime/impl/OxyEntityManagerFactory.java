package org.mariella.oxygen.runtime.impl;


import java.util.Collections;
import java.util.Map;

import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;

/**
 * @author aim
 */
public class OxyEntityManagerFactory implements EntityManagerFactory {
	private boolean isOpen = true;
	private Environment environment;
	
public OxyEntityManagerFactory(String emName, Map<?, ?> properties, Environment environment) {
	super();
	this.environment = environment;
}

public void close() {
	isOpen = false;
}

public EntityManager createEntityManager() {
	checkOpen();
	return createEntityManager(Collections.EMPTY_MAP);
}

public EntityManager createEntityManager(Map properties) {
	checkOpen();
	try {
		return new OxyServerEntityManagerImpl(
			environment.createConnectionProvider(), 
			environment.getSchemaMapping(), 
			environment.createEntityTransactionFactory(), 
			environment.getPersistenceClassResolver()
		);
	} catch(RuntimeException e) {
		throw e;
	} catch (Exception e) {
		throw new PersistenceException(e);
	}
}

public boolean isOpen() {
	return isOpen;
}

private void checkOpen() {
	if(!isOpen) {
		throw new IllegalStateException("EntityManagerFactory is not open!");
	}
}

@Override
public <T> void addNamedEntityGraph(String arg0, EntityGraph<T> arg1) {
	throw new UnsupportedOperationException();
}

@Override
public void addNamedQuery(String arg0, Query arg1) {
	throw new UnsupportedOperationException();
}

@Override
public EntityManager createEntityManager(SynchronizationType arg0) {
	throw new UnsupportedOperationException();
}

@Override
public EntityManager createEntityManager(SynchronizationType arg0, Map arg1) {
	throw new UnsupportedOperationException();
}

@Override
public Cache getCache() {
	throw new UnsupportedOperationException();
}

@Override
public CriteriaBuilder getCriteriaBuilder() {
	throw new UnsupportedOperationException();
}

@Override
public Metamodel getMetamodel() {
	throw new UnsupportedOperationException();
}

@Override
public PersistenceUnitUtil getPersistenceUnitUtil() {
	throw new UnsupportedOperationException();
}

@Override
public Map<String, Object> getProperties() {
	throw new UnsupportedOperationException();
}

@Override
public <T> T unwrap(Class<T> arg0) {
	throw new UnsupportedOperationException();
}


}
