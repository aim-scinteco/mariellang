package org.mariella.persistence.annotations.processing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.mariella.persistence.annotations.processing.ClasspathBrowser.Entry;
import org.mariella.persistence.mapping.OxyUnitInfo;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.google.common.reflect.ClassPath;

public class ClassLoaderPersistenceUnitParser implements PersistenceUnitParser {
	private ClassLoader classLoader = null;
	private List<OxyUnitInfo> oxyUnitInfos = new ArrayList<OxyUnitInfo>();

	
	public ClassLoaderPersistenceUnitParser(ClassLoader classLoader) {
		super();
		this.classLoader = classLoader;
	}
	
	@Override
	public Class<?> loadClass(Entry entry, String className) throws ClassNotFoundException {
		return classLoader.loadClass(className);
	}
	
	protected List<String> getPersistentClassNames(OxyUnitInfo unitInfo) {
		String s = (String)unitInfo.getProperties().get("org.mariella.persistence.packages");
		if(s == null) {
			throw new RuntimeException("Property 'org.mariella.persistence.packages' is missing in persistence.xml");
		}
		List<String> packageNames = new ArrayList<String>();
		StringTokenizer tokenizer = new StringTokenizer(s, ",");
		while(tokenizer.hasMoreTokens()) {
			String packageName = tokenizer.nextToken().trim().toLowerCase();
			packageNames.add(packageName);
		}	
		try {
			return ClassPath.from(classLoader)
				      .getAllClasses()
				      .stream()
				      .filter(clazz -> packageNames.contains(clazz.getPackageName()))
				      .map(clazz -> clazz.getName())
				      .collect(Collectors.toList());
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Entry> readEntries(OxyUnitInfo oxyUnitInfo) throws Exception {
		
		List<Entry> entries = new ArrayList<Entry>();
		
		getPersistentClassNames(oxyUnitInfo).forEach(cn -> {
			Entry entry = new Entry();
			InputStream is = classLoader.getResourceAsStream(cn);
			entry.setInputStream(is);
			entry.setName(cn);
			entries.add(entry);
		});
		return entries;
	}


	@Override
	public void parsePersistenceUnits() throws Exception {
		URL url = getPersistenceXmlUrl();
		InputStream persistenceXmlIs = url.openStream();
		if (persistenceXmlIs == null)
			throw new Exception("Could not find " + url.toString());

		PersistenceXmlHandler handler = new PersistenceXmlHandler();
		File file = new File(urlDecode(url.getFile()));
		File rootFile = file.getParentFile().getParentFile();
		URL rootUrl;
		if (rootFile.getPath().endsWith("BOOT-INF")) {
			// spring-boot
			rootFile = new File(rootFile.getPath() + "/lib");
			rootUrl = url;
		} else {
			rootUrl = new URL("file", null, rootFile.getPath());
		}

		XMLReader reader = XMLReaderFactory.createXMLReader();
		reader.setContentHandler(handler);
		reader.parse(new InputSource(persistenceXmlIs));

		List<OxyUnitInfo> oxyUnitInfos = handler.getOxyUnitInfos();
		for (OxyUnitInfo unitInfo : oxyUnitInfos) {
			unitInfo.setPersistenceUnitRootUrl(rootUrl);
		}

		this.oxyUnitInfos.addAll(oxyUnitInfos);
	}

	
	private URL getPersistenceXmlUrl() throws Exception {
		String arg = System.getProperty("persistence.xml");
		if (arg != null) {
			File persistenceLocation = new File(arg);
			return persistenceLocation.toURI().toURL();
		}
		
		URL url = classLoader.getResource("META-INF/persistence.xml");
		if (url == null) {
			throw new Exception("No persistence.xml found.");
		}
		return url;
	}

	@Override
	public List<OxyUnitInfo> getOxyUnitInfos() {
		return oxyUnitInfos;
	}

	private String urlDecode(String file) throws UnsupportedEncodingException {
		String decoded = URLDecoder.decode(file, "UTF-8");
		return decoded;
	}

}
