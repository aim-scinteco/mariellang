package com.scinteco.improve.node.aspects.biostat;

import java.util.List;

import com.scinteco.improve.nodes.RNodeAspectData;

public class BiostatAspectData extends RNodeAspectData<BiostatAspect> {

	public BiostatAspectData(BiostatAspect aspect) {
		super(aspect);
	}
	
    private List<String> owners;
    private List<String> collaborators;
    private boolean isProject;
    private String projectVisibility;

    public List<String> getOwners() {
        return owners;
    }
    
    public void setOwners(List<String> owners) {
        this.owners = owners;
    }
    
    public List<String> getCollaborators() {
        return collaborators;
    }
    
    public void setCollaborators(List<String> collaborators) {
        this.collaborators = collaborators;
    }

	public boolean isProject() {
		return isProject;
	}

	public void setProject(boolean isProject) {
		this.isProject = isProject;
	}

	public String getProjectVisibility() {
		return projectVisibility;
	}

	public void setProjectVisibility(String projectVisibility) {
		this.projectVisibility = projectVisibility;
	}


}
