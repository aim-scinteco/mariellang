package com.scinteco.improve.nodes;

public abstract class AceAspect extends RNodeAspect {

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
	}	

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new AceQueryExtender(this, query));
	}

}
