package com.scinteco.improve.node.aspects.analysis;

import java.util.List;

import com.scinteco.improve.nodes.RNodeAspectData;

public class StepAspectData extends RNodeAspectData<StepAspect> {

	private boolean keyStep;
	private boolean baseModel;
	private boolean fullModel;
	private boolean finalModel;
	private List<String> toolCategories;
	private List<String> tools;
	
	public StepAspectData(StepAspect aspect) {
		super(aspect);
	}
	
	public boolean isKeyStep() {
		return keyStep;
	}
	
	public void setKeyStep(boolean keyStep) {
		this.keyStep = keyStep;
	}
	
	public boolean isBaseModel() {
		return baseModel;
	}
	
	public void setBaseModel(boolean baseModel) {
		this.baseModel = baseModel;
	}
	
	public boolean isFullModel() {
		return fullModel;
	}
	
	public void setFullModel(boolean fullModel) {
		this.fullModel = fullModel;
	}
	
	public boolean isFinalModel() {
		return finalModel;
	}
	
	public void setFinalModel(boolean finalModel) {
		this.finalModel = finalModel;
	}
	
	public List<String> getToolCategories() {
		return toolCategories;
	}
	
	public void setToolCategories(List<String> toolCategories) {
		this.toolCategories = toolCategories;
	}
	
	public List<String> getTools() {
		return tools;
	}
	
	public void setTools(List<String> tools) {
		this.tools = tools;
	}
	
}
