package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.AllowedUsersAspect;
import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

import at.rufus.resources.api.model.GroupTree;

public class PublishAspect extends RNodeAspect {

    private final AllowedUsersAspect allowedUsersAspect;
    
    public PublishAspect(AllowedUsersAspect allowedUsersAspect) {
        this.allowedUsersAspect = allowedUsersAspect;
    }
    
    GroupTree getGroupTree() {
        return allowedUsersAspect.getGroupTree();
    }
    
    @Override
    public String getKey() {
        return "publish";
    }

    @Override
    public void initializeRNode(RNode node) throws ClassNotFoundException {
        node.addExtension(this, new PublishAspectData(this));
    }
    
    @Override
    public void addQueryExtenders(RNodeQuery query) {
        query.getExtenders().add(new PublishQueryExtender(this, query));
    }
}
