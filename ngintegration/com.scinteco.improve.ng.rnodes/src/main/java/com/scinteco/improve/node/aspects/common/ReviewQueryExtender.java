package com.scinteco.improve.node.aspects.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;


public class ReviewQueryExtender extends RNodeQueryExtender<ReviewAspect> {

	public ReviewQueryExtender(ReviewAspect aspect, RNodeQuery query) {
		super(aspect, query);
	}

	@Override
	public boolean hasCommonTableExpressions() {
		return true;
	}

	@Override
	public void addCommonTableExpressions() {
		query.getQuery().append(
				" reviewdata (id, review_status, entry_status) as (" +
						"  select n.id, r.status, re.status" +
						"  from nodes n" +
						"  inner join review_entry re on re.resource_node = n.resource_node" +
						"  inner join review r on r.id = re.review" +
						" )"
				);
	}

	@Override
	public void select() {
		query.getQuery().append(
				" union all select rd.id, '" + IResourcesIndexConstants.FIELD_REVIEW_STATUS + "', rd.review_status, null, null, null, null from reviewdata rd" +
						" union all select rd.id, '" + IResourcesIndexConstants.FIELD_VALIDATION_STATUS + "', rd.entry_status, null, null, null, null from reviewdata rd" 
				);
	}

	private ReviewAspectData getAspectData(RNode node) {
		return (ReviewAspectData)node.getAspectData(aspect); 
	}

	@Override
	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
		if (IResourcesIndexConstants.FIELD_REVIEW_STATUS.equals(attributeName)) {
			getAspectData(node).setReviewStatus(rs.getString(COL_VALUE_TEXT));
		} else if (IResourcesIndexConstants.FIELD_VALIDATION_STATUS.equals(attributeName)) {
			getAspectData(node).setValidationStatus(rs.getString(COL_VALUE_TEXT));
		}	
	}

}
