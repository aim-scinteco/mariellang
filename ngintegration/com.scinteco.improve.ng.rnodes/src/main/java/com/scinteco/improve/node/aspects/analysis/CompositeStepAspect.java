package com.scinteco.improve.node.aspects.analysis;

import com.scinteco.improve.nodes.RNodeAspect;

public class CompositeStepAspect extends RNodeAspect {
	public static final String RESOURCE_TYPE = "compositeStep";

	public final static CompositeStepAspect Singleton = new CompositeStepAspect();
	
	CompositeStepAspect() {
	}

	@Override
	public String getKey() {
		return RESOURCE_TYPE;
	}
	
}
