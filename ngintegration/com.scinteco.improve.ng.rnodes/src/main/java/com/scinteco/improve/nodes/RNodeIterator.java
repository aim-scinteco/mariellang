package com.scinteco.improve.nodes;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class RNodeIterator <T extends RNode>{
	public Comparator<T> CompareByIdFoldersFirst = new Comparator<T>() {
		@Override
		public int compare(T r1, T r2) {
			if(r1.isFolder() != r2.isFolder()) {
				return r1.isFolder() ? -1 : 1;
			} else {
				return r1.getId().compareTo(r2.getId());
			}
		}
	};
	public Comparator<T> CompareByIdFoldersLast= new Comparator<T>() {
		@Override
		public int compare(T r1, T r2) {
			if(r1.isFolder() != r2.isFolder()) {
				return r1.isFolder() ? 1 : -1;
			} else {
				return r1.getId().compareTo(r2.getId());
			}
		}
	};


	private class ChildrenCache {
		private Map<String, T[]> childrenMap = new HashMap<>();
		private Map<T, T> parentMap = new HashMap<>();

		@SuppressWarnings("unchecked")
		public T[] getChildren(T resource) {
			T[] children = childrenMap.get(resource.getId());
			if(children == null) {
				children = (T[])resource.listChildren();
				if(children != null) {
					if(childComparator != null) {
						Arrays.sort(children, childComparator);
					}
					childrenMap.put(resource.getId(), children);
					for(T child : children) {
						parentMap.put(child, resource);
					}
				};
			}
			return children;	
		}

		public T getParent(T resource) {
			T parent = parentMap.get(resource);
			if(parent == null) {
				throw new IllegalStateException("Unable to determine parent for " + resource.getId());
			}
			return parent;
		}

		public void remove(T resource) {
			T[] children = childrenMap.get(resource.getId());
			if(children != null) {
				for(RNode child : children) {
					parentMap.remove(child);
				}
			}
			childrenMap.remove(resource.getId());
		}
	}

	private T root;

	private T currentParent;
	private T[] currentChildren;
	private int currentChildIndex = -1;
	private volatile boolean atEnd = false;

	public boolean isAtEnd() {
		return atEnd;
	}

	// the children cache is used to ensure, that Resource.listChildren() is only called once during traversal
	// whenever a folder is "left" during traversal, the children of this folder are "uncached"
	private ChildrenCache childrenCache;

	private boolean skipChildren = false;

	private Comparator<T> childComparator = CompareByIdFoldersFirst;

	public RNodeIterator(T root) {
		this(root, null, null);
	}

	public RNodeIterator(T root, T current) {
		this(root, current, (ChildrenCache)null);
	}

	public RNodeIterator(T root, RNodeIterator<T> parentIterator) {
		this(root, null, parentIterator.childrenCache);
	}

	protected RNodeIterator(T root, T current, ChildrenCache cache) {
		this.root = root;
		childrenCache = new ChildrenCache();
		if(cache != null) {
			T[] cachedChildren = cache.getChildren(root);
			if(cachedChildren != null) {
				childrenCache.childrenMap.put(root.getId(), cachedChildren);
				for(T child : cachedChildren) {
					childrenCache.parentMap.put(child, root);
				}
			}
		} else {
			if (current != null) {
				T x = current;
				while (x != null) {
					T parent = restoreParent(x);
					childrenCache.parentMap.put(x, parent);
					x = parent;
				}
			}
		}

		if(current != null) {
			setCurrent(current);
		} 
	}

	protected T restoreParent(T x) {
		throw new UnsupportedOperationException();
	}

	private void setCurrent(T current) {
		if(current.equals(root)) {
			throw new IllegalArgumentException();
		} 
		currentParent = childrenCache.getParent(current);
		currentChildren = childrenCache.getChildren(currentParent);
		for(int i=0; i<currentChildren.length; i++) {
			if(currentChildren[i].equals(current)) {
				currentChildIndex = i;
			}
		}
		if(currentChildIndex == -1) {
			throw new IllegalStateException();
		}
		if (currentParent.equals(root) && currentChildIndex == currentChildren.length-1) {
			atEnd = true;
		}
	}

	public T next() {
		if(atEnd) {
			return null;
		} else {
			iterate(true);
			return atEnd ? null : currentChildren[currentChildIndex];
		}
	}

	public Comparator<T> getChildComparator() {
		return childComparator;
	}

	public void setChildComparator(Comparator<T> childComparator) {
		this.childComparator = childComparator;
	}

	public boolean isSkipChildren() {
		return skipChildren;
	}

	public void setSkipChildren(boolean skipChildren) {
		this.skipChildren = skipChildren;
	}

	private void iterate(boolean visitChildren) {
		if(currentChildren == null) {
			// root
			currentParent = root;
			currentChildren = childrenCache.getChildren(root);
			if(currentChildren.length == 0) {
				currentChildren = null;
				atEnd = true;
			} else {
				currentChildIndex = 0;
				atEnd = false;
			}
		} else {
			T resource = currentChildren[currentChildIndex];
			if(!skipChildren && visitChildren && resource.isFolder()) {
				T[] children = childrenCache.getChildren(resource);
				if(children.length > 0) {
					currentParent = resource;
					currentChildren = children;
					currentChildIndex = 0;
					return;
				} 
			}
			currentChildIndex++;
			if(currentChildIndex == currentChildren.length) {
				if(currentParent.equals(root)) {
					childrenCache.remove(root);
					currentParent = null;
					currentChildren = null;
					currentChildIndex = -1;
					atEnd = true;
				} else {
					childrenCache.remove(currentParent);
					setCurrent(currentParent);
					iterate(false);
				}
			}
		}
	}


}
