package com.scinteco.improve.nodes;


public class RNodeAspectData <T extends RNodeAspect> implements IRNodeExt {
	protected final T aspect;

	public RNodeAspectData(T aspect) {
		this.aspect = aspect;
	}

	public T getAspect() {
		return aspect;
	}

	public void initialize(RNode node) {

	}

}
