package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

public class RemarksAspect extends RNodeAspect {
	public static final RemarksAspect Singleton = new RemarksAspect();

	RemarksAspect() {
	}
	
	@Override
	public String getKey() {
		return "remarks";
	}	

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, new RemarksAspectData(this));
	}	

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new RemarksQueryExtender(this, query));
	}

}
