package com.scinteco.improve.node.aspects.analysis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;


public class StepQueryExtender extends RNodeQueryExtender<StepAspect> {

	public StepQueryExtender(StepAspect aspect, RNodeQuery query) {
		super(aspect, query);
	}

	@Override
	public boolean hasCommonTableExpressions() {
		return true;
	}

	@Override
	public void addCommonTableExpressions() {
		query.getQuery().append(
				" steps (id, step_id, key_step, base_model, full_model, final_model) as (" +
						"  select" + 
						"   n.id,"+ 
						"   s.id,"+ 
						"   s.key_step," + 
						"   s.base_model," + 
						"   s.full_model," +
						"   s.final_model" +
						"  from nodes n" + 
						"  join step s on s.id = n.resource_node" +
						" )," +
						" processes (id, tool, tool_category) as (" +
						"  select" + 
						"   s.id," + 			
						"   t.name,"+ 
						"   tc.name" + 
						"  from steps s" + 
						"  join process p on p.step = s.step_id" +
						"  join tool t on t.id = p.tool" +
						"  join tool_category tc on tc.id = t.category" +		
						"  where p.deleted = 0" +
						" )"
				);
	}

	@Override
	public void select() {
		query.getQuery().append(
				" union all select s.id, '" + StepAspect.FIELD_MODEL_KEY_STEP + "', null, null, s.key_step, null, null from steps s" +
						" union all select s.id, '" + StepAspect.FIELD_MODEL_BASE + "', null, null, s.base_model, null, null from steps s" +
						" union all select s.id, '" + StepAspect.FIELD_MODEL_FULL + "', null, null, s.full_model, null, null from steps s" +
						" union all select s.id, '" + StepAspect.FIELD_MODEL_FINAL + "', null, null, s.final_model, null, null from steps s" +
						" union all select p.id, '" + StepAspect.FIELD_TOOL_CATEGORIES + "', p.tool_category, null, null, null, null from processes p" +
						" union all select p.id, '" + StepAspect.FIELD_TOOLS + "', p.tool, null, null, null, null from processes p"
				);
	}

	@Override
	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
		StepAspectData stepNode = (StepAspectData)node.getAspectData(aspect);
		if (StepAspect.FIELD_MODEL_KEY_STEP.equals(attributeName)) {
			stepNode.setKeyStep(rs.getInt(COL_VALUE_NUMBER) == 1);
		} else if (StepAspect.FIELD_MODEL_BASE.equals(attributeName)) {
			stepNode.setBaseModel(rs.getInt(COL_VALUE_NUMBER) == 1);
		} else if (StepAspect.FIELD_MODEL_FULL.equals(attributeName)) {
			stepNode.setFullModel(rs.getInt(COL_VALUE_NUMBER) == 1);
		} else if (StepAspect.FIELD_MODEL_FINAL.equals(attributeName)) {
			stepNode.setFinalModel(rs.getInt(COL_VALUE_NUMBER) == 1);
		} else if (StepAspect.FIELD_TOOL_CATEGORIES.equals(attributeName)) {
			String toolCategory = rs.getString(COL_VALUE_TEXT);
			List<String> toolCategories = stepNode.getToolCategories();
			if (toolCategories == null) {
				toolCategories = new ArrayList<String>(3);
				stepNode.setToolCategories(toolCategories);
			}
			if (!toolCategories.contains(toolCategory)) {
				toolCategories.add(toolCategory);
			}
		} else if (StepAspect.FIELD_TOOLS.equals(attributeName)) {
			String tool = rs.getString(COL_VALUE_TEXT);
			List<String> tools = stepNode.getTools();
			if (tools == null) {
				tools = new ArrayList<String>(3);
				stepNode.setTools(tools);
			}
			if (!tools.contains(tool)) {
				tools.add(tool);
			}
		}
	}

}
