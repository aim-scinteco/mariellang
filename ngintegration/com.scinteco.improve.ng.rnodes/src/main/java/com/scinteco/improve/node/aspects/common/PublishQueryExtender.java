package com.scinteco.improve.node.aspects.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;

import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;

public class PublishQueryExtender extends RNodeQueryExtender<PublishAspect> {
    public static final String ATTR_PUBLISH_ACE = "publish";

    public PublishQueryExtender(PublishAspect aspect, RNodeQuery query) {
        super(aspect, query);
    }
    
    @Override
    public boolean hasCommonTableExpressions() {
        return false;
    }
    
    @Override
    public void select() {
        query.getQuery().append(" union all select n.id, '" + ATTR_PUBLISH_ACE + "', rights, null, order_nr, app_member, null from nodes n join access_control_entry ace on ace.resource_node = n.resource_node and ace.rights_area = 2");
    }
    
    @Override
    public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
        if (ATTR_PUBLISH_ACE.equals(attributeName)) {
            PublishAspectData ad = (PublishAspectData) node.getAspectData(getAspect());
            String rights = rs.getString(COL_VALUE_TEXT);
            if (rights.startsWith("AA")) {
                GUID memberId = GuidConverter.Singleton.getObject(rs, COL_VALUE_GUID);
                ad.addMemberId(memberId);
            }
        }
    }
}
