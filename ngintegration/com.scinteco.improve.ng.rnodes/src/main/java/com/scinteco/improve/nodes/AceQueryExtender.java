package com.scinteco.improve.nodes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import at.rufus.base.api.model.GuidConverter;

public class AceQueryExtender extends RNodeQueryExtender<AceAspect> {
	public static final String ATTR_ACCESS_CONTROL_ENTRIES = "ace";

	public AceQueryExtender(AceAspect aspect, RNodeQuery query) {
		super(aspect, query);
	}

	@Override
	public boolean hasCommonTableExpressions() {
		return false;
	}

	@Override	
	public void select() {
		query.getQuery().append(" union all select n.id, '" + ATTR_ACCESS_CONTROL_ENTRIES + "', case when ace.inherit = 1 then 'i' else ' ' end || rights || to_char(rights_area, '9'), null, order_nr, app_member, null from nodes n join access_control_entry ace on ace.resource_node = n.resource_node");
	}

	@Override
	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
		if (ATTR_ACCESS_CONTROL_ENTRIES.equals(attributeName)) {
			AceAspectData<?> ad = (AceAspectData<?>)node.getAspectData(aspect);
			List<AceElement> aces = ad.getAces();
			if (aces == null) {
				aces = new ArrayList<AceElement>(5);
				ad.setAces(aces);
			}
			AceElement ace = new AceElement();
			ace.setMemberId(GuidConverter.Singleton.getObject(rs, COL_VALUE_GUID));
			ace.setOrderNr(rs.getInt(COL_VALUE_NUMBER));
			String rights = rs.getString(COL_VALUE_TEXT);
			ace.setInherit(rights.charAt(0) == 'i');
			ace.setRights(rights.substring(1, rights.length()-2));
			ace.setRightsArea(Integer.valueOf(rights.substring(rights.length()-1)));
			aces.add(ace);
		} 	
	}

}
