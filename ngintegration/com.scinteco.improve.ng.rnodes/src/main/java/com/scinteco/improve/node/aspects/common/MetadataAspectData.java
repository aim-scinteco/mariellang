package com.scinteco.improve.node.aspects.common;

import java.util.ArrayList;
import java.util.List;

import com.scinteco.improve.nodes.MdElement;
import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspectData;

public class MetadataAspectData extends RNodeAspectData<MetadataAspect> {
	private List<MdElement> metadata;
	private List<MdElement> resolvedMetadata;

	public MetadataAspectData(MetadataAspect aspect) {
		super(aspect);
	}

	public List<MdElement> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<MdElement> metadata) {
		this.metadata = metadata;
	}

	public List<MdElement> getResolvedMetadata() {
		return resolvedMetadata;
	}

	private MetadataAspectData getAspectData(RNode node) {
		return (MetadataAspectData)node.getAspectData(aspect);
	}

	@Override
	public void initialize(RNode node) {
		super.initialize(node);

		resolvedMetadata = new ArrayList<>();
		RNode current = node;
		while (current != null && !current.isRepositoryNode()) {
			List<MdElement> metadata = getAspectData(current).getMetadata();
			if (metadata != null) {
				for (MdElement element : metadata) {
					if (current == node|| element.isInheritToChilds()) {
						resolvedMetadata.add(element);
					}
				}
			}
			current = current.getParent();
		}
	}

}
