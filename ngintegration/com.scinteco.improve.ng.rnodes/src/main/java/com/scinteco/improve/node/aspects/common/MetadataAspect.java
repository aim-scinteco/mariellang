package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

public class MetadataAspect extends RNodeAspect {
	public static final MetadataAspect Singleton = new MetadataAspect();

	MetadataAspect() {
	}
	
	@Override
	public String getKey() {
		return "metadata";
	}	

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, new MetadataAspectData(this));
	}	

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new MetadataQueryExtender(this, query));
	}

}
