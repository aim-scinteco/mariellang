package com.scinteco.improve.node.aspects.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.scinteco.improve.nodes.MdElement;
import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;

import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;
import at.rufus.resources.api.model.MetadataStrategy;
import at.rufus.resources.api.model.MetadataType;

public class MetadataQueryExtender extends RNodeQueryExtender<MetadataAspect> {
	public static final String ATTR_METADATA = "metadata";

	public MetadataQueryExtender(MetadataAspect aspect, RNodeQuery query) {
		super(aspect, query);
	}

	@Override
	public boolean hasCommonTableExpressions() {
		return true;
	}

	@Override
	public void addCommonTableExpressions() {
		query.getQuery().append(
				" mdata (id, desc_id, desc_name, inherit, type, text_value, num_value, date_value, lov_value_id, lov_value_text, rcm_id) as (" +
						"  select" +
						"   n.id," +
						"   d.id," +
						"   d.name," +
						"   case when d.strategy = '" + MetadataStrategy.Inherit + "' then 1 else 0 end," +
						"   d.type," +
						"   m.text_value," +
						"   m.num_value," +
						"   m.date_value," +
						"   l.id," +
						"   l.text," +
						"   rcm.id" +
						"  from nodes n" +
						"  join metadata m on m.resource_node = n.resource_node" +
						"  join metadata_desc d on d.id = m.metadata_desc" +
						"  left join metadata_category c on c.id = d.category " +
						"  left join metadata_lov l on l.id = m.lov_value and c.id = l.category" +
						"  left join resource_character_metadata rcm on rcm.resource_character = n.resource_character and rcm.metadata_desc = d.id" +
						"  where m.revision_from_time <= ? and m.revision_to_time > ?", query.getContext().getRevisionTime(), query.getContext().getRevisionTime()).append(
								"  and m.deleted = 0" +
										" )"
								);
	}	

	@Override
	public void select() {
		query.getQuery().append(
				" union all select md.id, '" + ATTR_METADATA + "', case when md.type = '" + MetadataType.LOV + "' then md.lov_value_text else md.text_value end, md.date_value, md.num_value, md.lov_value_id," + 
						" case when md.rcm_id is null then md.desc_id || '\t' || md.desc_name || '\t' || md.inherit || '\t' || md.type else md.desc_id || '\t' || md.desc_name || '\t' || md.inherit || '\t' || md.type || '\t' || md.rcm_id end from mdata md"
				);
	}

	@Override	
	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {	
		if (ATTR_METADATA.equals(attributeName)) {
			MetadataAspectData ad = (MetadataAspectData)node.getAspectData(aspect);
			String propsAsText = rs.getString(COL_PROPERTIES);
			if (propsAsText != null) {
				String[] properties = propsAsText.split("\t");
				if (properties.length >= 4 && properties.length <= 5) {
					List<MdElement> metadata = ad.getMetadata();
					if (metadata == null) {
						metadata = new ArrayList<MdElement>(3);
						ad.setMetadata(metadata);
					}
					int propIdx = 0;
					MdElement mdElement = new MdElement();
					mdElement.setDescriptorId(GUID.fromHexString(properties[propIdx++]));
					mdElement.setDescriptorName(properties[propIdx++]);
					mdElement.setInheritToChilds(Integer.valueOf(properties[propIdx++]).intValue() == 1);
					MetadataType type = MetadataType.valueOf(properties[propIdx++]);
					mdElement.setType(type);
					if (properties.length > propIdx) {
						mdElement.setResourceCharacterId(GUID.fromHexString(properties[propIdx++]));
					}
					if (type == MetadataType.TEXT || type == MetadataType.URL) {
						mdElement.setTextValue(rs.getString(COL_VALUE_TEXT));
					} else if (type == MetadataType.DATE) {
						mdElement.setDateValue(rs.getTimestamp(COL_VALUE_TIMESTAMP));
					} else if (type == MetadataType.NUMBER) {
						mdElement.setNumValue(rs.getBigDecimal(COL_VALUE_NUMBER));
					} else if (type == MetadataType.LOV) {
						mdElement.setLovValueId(GuidConverter.Singleton.getObject(rs, COL_VALUE_GUID));
						mdElement.setLovValueText(rs.getString(COL_VALUE_TEXT));
					}
					metadata.add(mdElement);
				}
			}
		}
	}

}