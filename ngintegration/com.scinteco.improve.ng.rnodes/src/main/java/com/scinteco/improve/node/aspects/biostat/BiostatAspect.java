package com.scinteco.improve.node.aspects.biostat;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

import at.rufus.base.api.model.BiostatConstants;
import at.rufus.base.api.model.GUID;

public class BiostatAspect extends RNodeAspect {
	public final static GUID PROJECTS_FOLDER_ID = GUID.fromHexString("6DC6F49AD9104E0995662D113B5004C9");
	
	public final static BiostatAspect Singleton = new BiostatAspect();
	
	private static class MembersCacheLoader extends CacheLoader<GUID, List<String>> {
		private final Function<GUID, GUID> membersFunction;


		MembersCacheLoader(Function<GUID, GUID> membersFunction) {
			this.membersFunction = membersFunction;
		}

		@Override
		public List<String> load(GUID key) throws Exception {
			GUID id = membersFunction.apply(key);
			return id == null? Collections.emptyList() : Collections.singletonList(id.toString());
		}
	}

	private LoadingCache<GUID, List<String>> ownersMap; 
	private LoadingCache<GUID, List<String>> collaboratorsMap;

	private BiostatAspect() {
	}
	
	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, new BiostatAspectData(this));
	}

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new BiostatQueryExtender(this, query));
	}

    @Override
    public String getKey() {
    	return "biostat";
    }

    GUID getProjectResourceId(RNode node) {
        RNode currentNode = node;
        while (currentNode != null) {
            if (isOwnedResource(currentNode)) {
                return currentNode.getResourceId();
            }
            currentNode = currentNode.getParent();
        }
        return null;
    }
    
    private boolean isOwnedResource(RNode node) {
        return node.getParent() != null && BiostatConstants.PROJECTS_FOLDER_ID.equals(node.getParent().getResourceId());
    }
    

	public LoadingCache<GUID, List<String>> getOwnersMap() {
		initializeMaps();
		return ownersMap;
	}

	public LoadingCache<GUID, List<String>> getCollaboratorsMap() {
		initializeMaps();
		return collaboratorsMap;
	}

	private void initializeMaps() {
		if (this.ownersMap == null) {
//			SbProjectService projectService = SbServices.getService(SbProjectService.class);
//			this.ownersMap = CacheBuilder.newBuilder()
//					.maximumSize(100)
//		            .build(new MembersCacheLoader((id) -> projectService.getOwnersGroupId(id))); 
//			this.collaboratorsMap = CacheBuilder.newBuilder()
//					.maximumSize(100)
//		            .build(new MembersCacheLoader((id) -> projectService.getCollaboratorsGroupId(id)));
		}
	}

	public static boolean isProject(GUID resourceId) {
		return resourceId.equals(PROJECTS_FOLDER_ID);
	}
    
}
