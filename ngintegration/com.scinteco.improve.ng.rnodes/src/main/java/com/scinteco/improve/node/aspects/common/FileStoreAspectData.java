package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNodeAspectData;

public class FileStoreAspectData extends RNodeAspectData<FileStoreAspect> {
	private String fileStorePath;

	public FileStoreAspectData(FileStoreAspect aspect) {
		super(aspect);
	}

	public String getFileStorePath() {
		return fileStorePath;
	}

	public void setFileStorePath(String fileStorePath) {
		this.fileStorePath = fileStorePath;
	}

}
