package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

public class FileStoreAspect extends RNodeAspect {
	public static final FileStoreAspect Singleton = new FileStoreAspect();

	FileStoreAspect() {
	}
	
	@Override
	public String getKey() {
		return "fileStore";
	}	

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, new FileStoreAspectData(this));
	}	

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new FileStoreQueryExtender(this, query));
	}

}
