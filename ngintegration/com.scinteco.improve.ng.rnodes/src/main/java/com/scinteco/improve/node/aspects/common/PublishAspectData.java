package com.scinteco.improve.node.aspects.common;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.scinteco.improve.nodes.RNodeAspectData;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.GroupTree;

public class PublishAspectData extends RNodeAspectData<PublishAspect> {

    private Set<GUID> memberIds = new HashSet<>();
    
    public PublishAspectData(PublishAspect aspect) {
        super(aspect);
    }

    void addMemberId(GUID memberId) {
        memberIds.add(memberId);
    }
    
    public List<String> getUserIds() {
        return toUserStringIds(memberIds);
    }
    
    private List<String> toUserStringIds(Collection<GUID> memberIds) {
        if (memberIds == null) {
            return Collections.emptyList();
        }
        GroupTree groupTree = aspect.getGroupTree();
        Set<GUID> users = new HashSet<>();
        for (GUID memberId: memberIds) {
            Set<GUID> groupUsers = groupTree.resolveGroupToUsers(memberId);
            if (groupUsers == null) {
                users.add(memberId);
            } else {
                users.addAll(groupUsers);
            }
        }
        return GUID.toStringIds(users);
    }
}
