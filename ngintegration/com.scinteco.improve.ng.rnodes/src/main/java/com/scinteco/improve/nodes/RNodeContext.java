package com.scinteco.improve.nodes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import at.rufus.base.api.common.DbType;
import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;
import at.rufus.persistence.query.IResourceType;
import at.rufus.persistence.query.Query;

public abstract class RNodeContext {
	protected final Timestamp revisionTime;
	protected List<RNodeAspect> aspects = new ArrayList<>();

	public RNodeContext(Timestamp revisionTime) {
		this.revisionTime = revisionTime;
	}
	
	public List<RNode> listChildren(RNode node) {
		ChildrenQuery query = new ChildrenQuery(this, node, aspects);
		return query.query();
	}

	public Timestamp getRevisionTime() {
		return revisionTime;
	}
	
	public RNode createRepositoryNode() {
		RNode repositoryNode = new RNode(this, Collections.emptyList());
		repositoryNode.setName("_REPOSITORY_");
		return repositoryNode;
	}


	public RNode createNode(GUID resourceId) {
		GUID[] hierarchyIds = queryResourceHierarchy(resourceId);
		if (hierarchyIds != null) {
			// load children from repository root and exclude already processed children (by name) level by level
			RNode currentNode = createRepositoryNode();
			for (int level = 0; level < hierarchyIds.length; level++) {
				// TODO: the hierarchy to the parent was always loaded with all aces, regardless of context.isUpdateRights(). This implementation loads the aces but does not initialize the rights
				GUID childResourceId = hierarchyIds[level];

				Optional<RNode> childNode = loadChildOfParent(currentNode, childResourceId);
				if (!childNode.isPresent()) {
					return null;
				}
				childNode.get().setParent(currentNode);
				childNode.get().initialize();
				currentNode = childNode.get();
			}
			return currentNode;
		}
		return null;
	}
	

	private GUID[] queryResourceHierarchy(GUID resourceId) {
		Query query = new Query(
				"with recursive rq(resource_node, parent) as (" +
						" select rnv.resource_node, rnv.parent" + 
						"  from resource_node_version rnv" + 
						"  where rnv.resource_node = guidlit(?) and rnv.revision_from_time <= ? and rnv.revision_to_time > ?", resourceId, revisionTime, revisionTime).append(
								" union all" +  
										" select rnv.resource_node, rnv.parent" +
										"  from resource_node_version rnv" +
										"  join rq on rnv.resource_node = rq.parent" +
										"  where rnv.revision_from_time <= ? and rnv.revision_to_time > ?", revisionTime, revisionTime).append(
												") select resource_node from rq"
												);
		List<GUID> resourceIds = getJdbcTemplate().query(query.getSql(), new RowMapper<GUID>() {
			@Override
			public GUID mapRow(ResultSet rs, int rowNum) throws SQLException {
				return GuidConverter.Singleton.getObject(rs, 1);
			}
		}, query.getArgs());
		if (resourceIds.isEmpty()) {
			return null;
		}
		GUID[] result = new GUID[resourceIds.size()];
		int i = 0;
		for (GUID nextResourceId : resourceIds) {
			result[result.length - 1 - i] = nextResourceId;
			i++;
		}
		return result;
	}


	private Optional<RNode> loadChildOfParent(RNode node, GUID childResourceId) {
		ChildOfParentQuery query = new ChildOfParentQuery(this, node, childResourceId, aspects);

		ArrayList<RNode> childNodes = query.query();
		if (childNodes.size() != 1) {
			return Optional.empty();
		}
		return Optional.of(childNodes.get(0));
	}

	public abstract IResourceType getResourceType(String nodeType);

	public DbType getDbType() {
		return DbType.postgres;
	}
	
	public abstract JdbcTemplate getJdbcTemplate();
	
	public boolean includeDeleted() {
		return false;
	}

	protected String getExcludedTypes() {
		return null;
	}
}
