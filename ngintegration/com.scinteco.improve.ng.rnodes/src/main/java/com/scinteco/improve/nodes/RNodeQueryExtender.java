package com.scinteco.improve.nodes;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class RNodeQueryExtender <T extends RNodeAspect>{
	public static final int COL_RESOURCE_VERSION_ID = 1;
	public static final int COL_ATTRIBUTE_NAME = 2;

	public static final int COL_VALUE_TEXT = 3;
	public static final int COL_VALUE_TIMESTAMP = 4;
	public static final int COL_VALUE_NUMBER = 5;
	public static final int COL_VALUE_GUID = 6;
	public static final int COL_PROPERTIES = 7;

	protected final T aspect;
	protected final RNodeQuery query;

	public RNodeQueryExtender(T aspect, RNodeQuery query) {
		this.aspect = aspect;
		this.query = query;
	}

	public RNodeAspect getAspect() {
		return aspect;
	}

	public RNodeQuery getQuery() {
		return query;
	}

	public void select() {
	}

	public void addCommonTableExpressions() {
	}

	public RNodeQuery.NodeQuerySelectItem[] getNodeQuerySelectItems() {
		return null;
	}

	public abstract boolean hasCommonTableExpressions();

	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {

	}

}
