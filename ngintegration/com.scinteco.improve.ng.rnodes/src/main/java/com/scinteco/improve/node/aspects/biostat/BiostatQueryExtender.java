package com.scinteco.improve.node.aspects.biostat;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;

import at.rufus.base.api.model.GUID;

public class BiostatQueryExtender extends RNodeQueryExtender<BiostatAspect> {
    private static final String ATTR_SWB_RESOURCE_ID = "biostat_resource_id";
	
    public BiostatQueryExtender(BiostatAspect aspect, RNodeQuery query) {
    	super(aspect, query);
    }
    
    @Override
    public boolean hasCommonTableExpressions() {
    	return false;
    }
    
    @Override
    public void select() {
    	query.getQuery().append(
    	" union all select n.id, '" + ATTR_SWB_RESOURCE_ID + "', null, null, null, n.resource_node, null from nodes n"
    	);
    }
    
    @Override
    public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
        if (ATTR_SWB_RESOURCE_ID.equals(attributeName)) {
            GUID ownerId = ((BiostatAspect)getAspect()).getProjectResourceId(node);
            if (ownerId != null) {
            	BiostatAspectData data = (BiostatAspectData)node.getAspectData(BiostatAspect.Singleton);
                data.setOwners(BiostatAspect.Singleton.getOwnersMap().getUnchecked(ownerId));
                data.setCollaborators(BiostatAspect.Singleton.getCollaboratorsMap().getUnchecked(ownerId));
            }
            if (node.getParent() != null && !node.getParent().isRepositoryNode() && BiostatAspect.isProject(node.getParent().getResourceId())) {
            	BiostatAspectData data = (BiostatAspectData)node.getAspectData(BiostatAspect.Singleton);
            	data.setProject(true);
//        		IAppContext appContext = new AppContext(null, ResourceLovs.ACTOR_REPOSITORY);
//            	ProjectRights rights = projectService.getProjectRights(appContext, ownerId);
//                data.setProjectVisibility(getProjectVisibilityKeyword(rights));
            }
        }
    }
    
//    public static String getProjectVisibilityKeyword(ProjectRights projectRights) {
//		switch(projectRights) {
//		case PRIVILEGED_HIDDEN:
//			return "privileged hidden";
//		case PRIVILEGED_VISIBLE:
//			return "privileged visible";
//		case PUBLIC:
//			return "public";
//		}
//		throw new IllegalArgumentException();
//	}

}
