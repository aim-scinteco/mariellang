package com.scinteco.improve.node.aspects.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;

import at.rufus.base.api.model.GuidConverter;

public class DetailsQueryExtender extends RNodeQueryExtender<DetailsAspect> {
	public static final RNodeQuery.NodeQuerySelectItem[] NodeQuerySelectItems = new RNodeQuery.NodeQuerySelectItem[] {
		new RNodeQuery.NodeQuerySelectItem("repository_id", "rn.repository_id"),
		new RNodeQuery.NodeQuerySelectItem("entity_id", "rn.entity_id"),
		new RNodeQuery.NodeQuerySelectItem("entity_version_id", "rnv.entity_version_id"),
		new RNodeQuery.NodeQuerySelectItem("created_at", "rnr.created_at"),
		new RNodeQuery.NodeQuerySelectItem("created_by", "rnr.created_by"),
		new RNodeQuery.NodeQuerySelectItem("modified_at", "rnvr.created_at"),
		new RNodeQuery.NodeQuerySelectItem("modified_by", "rnvr.created_by"),
		new RNodeQuery.NodeQuerySelectItem("views", "rn.touch_count"),
		new RNodeQuery.NodeQuerySelectItem("usages", "rn.usage_count"),
		new RNodeQuery.NodeQuerySelectItem("node_comment", "rn.node_comment"),
		new RNodeQuery.NodeQuerySelectItem("description", "rn.description"),
		new RNodeQuery.NodeQuerySelectItem("rationale", "rn.rationale"),
		new RNodeQuery.NodeQuerySelectItem("resource_character", "rn.resource_character")
	};
	
	
public DetailsQueryExtender(DetailsAspect aspect, RNodeQuery query) {
	super(aspect, query);
}

@Override
public boolean hasCommonTableExpressions() {
	return false;
}

@Override
public RNodeQuery.NodeQuerySelectItem[] getNodeQuerySelectItems() {
	return NodeQuerySelectItems; 
}

@Override
public void select() {
	query.getQuery().append(
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_REPOSITORY_ID + "', null, null, null, n.repository_id, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_ENTITY_ID + "', n.entity_id, null, null, null, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_ENTITY_VERSION_ID + "', n.entity_version_id, null, null, null, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_CREATED_AT + "', null, n.created_at, null, null, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_CREATED_BY_ID + "', null, null, null, n.created_by, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_LAST_MODIFIED_AT + "', null, n.modified_at, null, null, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_LAST_MODIFIED_BY_ID + "', null, null, null, n.modified_by, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_VIEWS + "', null, null, n.views, null, null from nodes n" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_USAGES + "', null, null, n.usages, null, null from nodes n" +	
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_COMMENT + "', n.node_comment, null, null, null, null from nodes n where n.node_comment is not null" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_DESCRIPTION + "', n.description, null, null, null, null from nodes n where n.description is not null" +
		" union all select n.id, '" + IResourcesIndexConstants.FIELD_RATIONALE + "', n.rationale, null, null, null, null from nodes n where n.rationale is not null" 
	);
}

@Override
public void processAttributes(ResultSet rs, RNode n, String attributeName) throws SQLException {
	DetailsAspectData node = (DetailsAspectData)n.getAspectData(aspect);
	if (IResourcesIndexConstants.FIELD_REPOSITORY_ID.equals(attributeName)) {
        node.setRepositoryId(GuidConverter.Singleton.getObject(rs, COL_VALUE_GUID));
    } else if (IResourcesIndexConstants.FIELD_ENTITY_ID.equals(attributeName)) {
        node.setEntityId(rs.getString(COL_VALUE_TEXT));
    } else if (IResourcesIndexConstants.FIELD_ENTITY_VERSION_ID.equals(attributeName)) {
        node.setEntityVersionId(rs.getString(COL_VALUE_TEXT));
    } else if (IResourcesIndexConstants.FIELD_CREATED_AT.equals(attributeName)) {
        node.setCreatedAt(rs.getTimestamp(COL_VALUE_TIMESTAMP));
    } else if (IResourcesIndexConstants.FIELD_CREATED_BY_ID.equals(attributeName)) {
        node.setCreatedById(GuidConverter.Singleton.getObject(rs, COL_VALUE_GUID));
    } else if (IResourcesIndexConstants.FIELD_LAST_MODIFIED_AT.equals(attributeName)) {
        node.setLastModifiedByAt(rs.getTimestamp(COL_VALUE_TIMESTAMP));
    } else if (IResourcesIndexConstants.FIELD_LAST_MODIFIED_BY_ID.equals(attributeName)) {
        node.setLastModifiedById(GuidConverter.Singleton.getObject(rs, COL_VALUE_GUID));
    } else if (IResourcesIndexConstants.FIELD_VIEWS.equals(attributeName)) {
        node.setNumberOfViews(rs.getInt(COL_VALUE_NUMBER));
    } else if (IResourcesIndexConstants.FIELD_USAGES.equals(attributeName)) {
        node.setNumberOfUsages(rs.getInt(COL_VALUE_NUMBER));
    } else if (IResourcesIndexConstants.FIELD_COMMENT.equals(attributeName)) {
        node.setComment(rs.getString(COL_VALUE_TEXT));
    } else if (IResourcesIndexConstants.FIELD_DESCRIPTION.equals(attributeName)) {
        node.setDescription(rs.getString(COL_VALUE_TEXT));
    } else if (IResourcesIndexConstants.FIELD_RATIONALE.equals(attributeName)) {
        node.setRationale(rs.getString(COL_VALUE_TEXT));
    } 
}

}
