package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

public class DetailsAspect extends RNodeAspect {
	public static final DetailsAspect Singleton = new DetailsAspect();

	DetailsAspect() {
	}
	
	@Override
	public String getKey() {
		return "basic";
	}	

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, new DetailsAspectData(this));
	}	

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new DetailsQueryExtender(this, query));

	}

}
