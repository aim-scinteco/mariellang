package com.scinteco.improve.node.aspects.analysis;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

import at.rufus.analysis.api.model.Step;


public class StepAspect extends RNodeAspect {
	public static final StepAspect Singleton = new StepAspect();

	public static final String RESOURCE_TYPE_STEP = "step";
	public static final String FIELD_TOOL_CATEGORIES = "toolCategories";
	public static final String FIELD_TOOLS = "tools";
	public static final String FIELD_MODEL_KEY_STEP = "modelKeyStep";
	public static final String FIELD_MODEL_BASE = "modelBase";
	public static final String FIELD_MODEL_FULL = "modelFull";
	public static final String FIELD_MODEL_FINAL = "modelFinal";
	public static final String[] FIELDS_TO_INDEX = new String[] {
			FIELD_TOOL_CATEGORIES,
			FIELD_TOOLS,
			FIELD_MODEL_KEY_STEP,
			FIELD_MODEL_BASE,
			FIELD_MODEL_FULL,
			FIELD_MODEL_FINAL
		};
	
	StepAspect() {
	}
	
	@Override
	public String getKey() {
		return "step";
	}

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		if (node.getResourceType().getResourceClass() == Step.class) {
			node.addExtension(this, new StepAspectData(this));
		}
	}

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new StepQueryExtender(this, query));
	}

}
