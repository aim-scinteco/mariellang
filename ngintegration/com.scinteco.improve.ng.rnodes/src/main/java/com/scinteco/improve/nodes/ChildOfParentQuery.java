package com.scinteco.improve.nodes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowCallbackHandler;

import at.rufus.base.api.common.DbType;
import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;
import at.rufus.persistence.query.Query;
import at.rufus.resources.api.model.RepositoryEntityTypes;

public class ChildOfParentQuery extends RNodeQuery {
	final RNode parent;
	final GUID parentResourceId;
	final GUID childResourceId;

	public ChildOfParentQuery(RNodeContext context, RNode parent, GUID childResourceId, List<RNodeAspect> aspects) {
		super(context, aspects);
		this.parent = parent;
		parentResourceId = parent != null ? parent.getResourceId() : null;
		this.childResourceId = childResourceId;
	}

	@Override
	public RNode getParentNode() {
		return parent;
	}

	protected Query buildQuery() {
		query = new Query("with nodes (id, resource_node, node_type, name, revision_id"); 
		// append the extenders select items to the cte column definition list
		for(RNodeQueryExtender<?> extender : extenders) {
			if(extender.getNodeQuerySelectItems() != null) {
				for(NodeQuerySelectItem si : extender.getNodeQuerySelectItems()) {
					query.append(", " + si.alias);
				}
			}
		}
		query.append(") as (");

		query.append("select rnv.id, rnv.resource_node, rnv.node_type, rnv.name, rnv.revision_from_id");
		// append the extenders select items to the cte select list
		for(RNodeQueryExtender<?> extender : extenders) {
			if(extender.getNodeQuerySelectItems() != null) {
				for(NodeQuerySelectItem si : extender.getNodeQuerySelectItems()) {
					query.append(", " + si.selectExpression);
				}
			}
		}

		query.append(		
				"  from resource_node_version rnv" +
						"  join revision rnvr on rnvr.id = rnv.revision_from_id" +			
						"  join resource_node rn on rn.id = rnv.resource_node" +
						"  join revision rnr on rnr.id = rn.revision_id" +
						"  where "
				);

		if (parentResourceId != null) {
			query.append("rnv.parent = guidlit(?)", parentResourceId);
		} else {
			query.append("rnv.parent is null");
		};
		if (childResourceId != null) {
			query.append("  and rnv.resource_node = guidlit(?)", childResourceId);
		}
		// oder
		// query.getQuery().appendIn("nv.id", resourceIds);

		query.append(
					"  and rnv.revision_from_time <= ? and rnv.revision_to_time > ?", getContext().getRevisionTime(), getContext().getRevisionTime()).append(
							 (!getContext().includeDeleted() ? "and rnv.deleted <= 0 " : "") +
									(excludedTypes == null ? "" : "  and rnv.node_type not in (" + excludedTypes + ")") +
									" )"
							);

		for(RNodeQueryExtender<?> extender : getExtenders()) {
			if(extender.hasCommonTableExpressions()) {
				query.append(", ");
				extender.addCommonTableExpressions();
			}
		}

		// resources attributes
		query.append(
				" select n.id as rv_id, '" + ATTR_RESOURCE_ID + "' as attr_name, null as text_value, " + 
						(getContext().getDbType() == DbType.postgres ? "cast(null as timestamp)" : "null") + " as time_value, " +
						(getContext().getDbType() == DbType.postgres ? "cast(null as numeric)" : "null") + " as num_value, " + 
						"n.resource_node as id_value, " +
						"null as attr_props from nodes n" +
						" union all select n.id, '" + ATTR_NODE_TYPE + "', n.node_type , null, null, null, null from nodes n" +  
						" union all select n.id, '" + ATTR_NAME + "', n.name , null, null, null, null from nodes n" +
						" union all select n.id, '" + ATTR_REVISION_ID + "', null, null, null, n.revision_id, null from nodes n" + 			
						" union all select n.id, '" + ATTR_HAS_CHILDREN + "', null, null, 1, null, null from nodes n where n.node_type not in ('" + RepositoryEntityTypes.FILE_VERSION + "') and exists (" +
						"  select 1 from resource_node_version rnv where rnv.parent=n.resource_node and rnv.revision_from_time <= ? and rnv.revision_to_time > ? and rnv.deleted <= 0 " + (excludedTypes == null ? "" : "and rnv.node_type not in (" + excludedTypes + ")"), getContext().getRevisionTime(), getContext().getRevisionTime()).append(
								" )"
								);

		for(RNodeQueryExtender<?> extender : getExtenders()) {
			extender.select();
		}

		return query;
	}
	public ArrayList<RNode> query() {
		buildQuery();

		final Map<GUID, RNode> childNodeMap = new HashMap<GUID, RNode>();
		getContext().getJdbcTemplate().query(query.getSql(), new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				GUID resourceVersionId = GuidConverter.Singleton.getObject(rs, COL_RESOURCE_VERSION_ID);
				String attributeName = rs.getString(COL_ATTRIBUTE_NAME);
				RNode node = childNodeMap.get(resourceVersionId);
				if (node == null) {
					node = new RNode(getContext(), getAspects());
					node.setParent(parent);
					node.setResourceVersionId(resourceVersionId);
					childNodeMap.put(resourceVersionId, node);
				}
				processAttributes(rs, node, attributeName);
			}
		}, query.getArgs());
		ArrayList<RNode> childNodes = new ArrayList<RNode>(childNodeMap.values());
		for (RNode childNode : childNodes) {
			childNode.initialize();
		}
		return childNodes;
	}


}
