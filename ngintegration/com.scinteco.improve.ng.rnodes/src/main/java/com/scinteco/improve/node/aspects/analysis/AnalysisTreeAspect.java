package com.scinteco.improve.node.aspects.analysis;

import com.scinteco.improve.nodes.RNodeAspect;

public class AnalysisTreeAspect extends RNodeAspect {
	public static final String RESOURCE_TYPE = "analysisTree";
	
	public final static AnalysisTreeAspect Singleton = new AnalysisTreeAspect();
	
	AnalysisTreeAspect() {
	}

	@Override
	public String getKey() {
		return RESOURCE_TYPE;
	}

}
