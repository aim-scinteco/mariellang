package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNodeAspectData;

import at.rufus.resources.api.model.ReviewEntryStatus;
import at.rufus.resources.api.model.ReviewStatus;

public class ReviewAspectData extends RNodeAspectData<ReviewAspect> {
	private ReviewStatus reviewStatus;
	private ReviewEntryStatus validationStatus;

	public ReviewAspectData(ReviewAspect aspect) {
		super(aspect);
	}

	public ReviewStatus getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus == null ? null : ReviewStatus.valueOf(reviewStatus);
	}

	public ReviewEntryStatus getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus == null ? null : ReviewEntryStatus.valueOf(validationStatus);
	}

}
