package com.scinteco.improve.node.aspects.common;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeAspect;
import com.scinteco.improve.nodes.RNodeQuery;

public class ReviewAspect extends RNodeAspect {
	public static final ReviewAspect Singleton = new ReviewAspect();

	ReviewAspect() {
	}
	
	@Override
	public String getKey() {
		return "review";
	}	

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, new ReviewAspectData(this));
	}

	@Override
	public void addQueryExtenders(RNodeQuery query) {
		query.getExtenders().add(new ReviewQueryExtender(this, query));
	}

}
