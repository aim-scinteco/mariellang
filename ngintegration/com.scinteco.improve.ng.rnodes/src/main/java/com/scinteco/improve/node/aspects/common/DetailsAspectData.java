package com.scinteco.improve.node.aspects.common;

import java.sql.Timestamp;

import com.scinteco.improve.nodes.RNodeAspectData;

import at.rufus.base.api.model.GUID;

public class DetailsAspectData extends RNodeAspectData<DetailsAspect> {
	private GUID repositoryId;
	private String entityId;
	private String entityVersionId;
	private Timestamp createdAt;
	private GUID createdById;
	private Timestamp lastModifiedByAt;
	private GUID lastModifiedById;
	private int numberOfViews;
	private int numberOfUsages;
	private String comment;
	private String description;
	private String rationale;

	public DetailsAspectData(DetailsAspect aspect) {
		super(aspect);
	}

	public GUID getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(GUID repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityVersionId() {
		return entityVersionId;
	}

	public void setEntityVersionId(String entityVersionId) {
		this.entityVersionId = entityVersionId;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public GUID getCreatedById() {
		return createdById;
	}

	public void setCreatedById(GUID createdById) {
		this.createdById = createdById;
	}

	public Timestamp getLastModifiedByAt() {
		return lastModifiedByAt;
	}

	public void setLastModifiedByAt(Timestamp lastModifiedByAt) {
		this.lastModifiedByAt = lastModifiedByAt;
	}

	public GUID getLastModifiedById() {
		return lastModifiedById;
	}
	public void setLastModifiedById(GUID lastModifiedById) {
		this.lastModifiedById = lastModifiedById;
	}
	public int getNumberOfViews() {
		return numberOfViews;
	}
	public void setNumberOfViews(int numberOfViews) {
		this.numberOfViews = numberOfViews;
	}
	public int getNumberOfUsages() {
		return numberOfUsages;
	}
	public void setNumberOfUsages(int numberOfUsages) {
		this.numberOfUsages = numberOfUsages;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getRationale() {
		return rationale;
	}

	public void setRationale(String rationale) {
		this.rationale = rationale;
	}

}
