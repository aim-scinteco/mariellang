package com.scinteco.improve.node.aspects.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;


public class RemarksQueryExtender extends RNodeQueryExtender<RemarksAspect> {
	public static final String ATTR_FILE_STORE_PATH = "fsPath";

	public RemarksQueryExtender(RemarksAspect aspect, RNodeQuery query) {
		super(aspect, query);
	}

	@Override
	public boolean hasCommonTableExpressions() {
		return true;
	}

	@Override
	public void addCommonTableExpressions() {
		query.getQuery().append(
				" rcomments (id, text) as (" +
						"  select" +
						"   n.id," +
						"   rc.text" +
						"  from nodes n" +
						"  join resource_comment rc on rc.resource_node = n.resource_node and rc.revision_time <= ?", query.getContext().getRevisionTime()).append(
								" )" 
								);
	}

	@Override
	public void select() {
		query.getQuery().append(
				" union all select rc.id, '" + IResourcesIndexConstants.FIELD_REMARKS + "', rc.text, null, null, null, null from rcomments rc"  
				);
	}

	@Override
	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
		if (IResourcesIndexConstants.FIELD_REMARKS.equals(attributeName)) {
			RemarksAspectData ad = (RemarksAspectData)node.getAspectData(aspect);
			String remark = rs.getString(COL_VALUE_TEXT);
			List<String> remarks = ad.getRemarks();
			if (remarks == null) {
				remarks = new ArrayList<String>(3);
				ad.setRemarks(remarks);
			}
			remarks.add(remark);
		}
	}

}
