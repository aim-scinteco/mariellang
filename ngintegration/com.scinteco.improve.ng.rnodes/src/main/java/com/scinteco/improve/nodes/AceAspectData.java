package com.scinteco.improve.nodes;

import java.util.List;

public class AceAspectData<T extends AceAspect> extends RNodeAspectData<T> {
	private List<AceElement> aces;

	public AceAspectData(T aspect) {
		super(aspect);
	}

	public List<AceElement> getAces() {
		return aces;
	}

	public void setAces(List<AceElement> aces) {
		this.aces = aces;
	}

}
