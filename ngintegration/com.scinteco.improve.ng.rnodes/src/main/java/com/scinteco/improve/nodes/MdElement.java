package com.scinteco.improve.nodes;

import java.math.BigDecimal;
import java.sql.Timestamp;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.MetadataType;

public class MdElement {

	private GUID descriptorId;
	private String descriptorName;
	private MetadataType type;
	private String textValue;
	private BigDecimal numValue;
	private Timestamp dateValue;
	private GUID lovValueId;
	private String lovValueText;
	private GUID resourceCharacterId;
	private boolean inheritToChilds;
	
	public GUID getDescriptorId() {
		return descriptorId;
	}
	
	public void setDescriptorId(GUID descriptorId) {
		this.descriptorId = descriptorId;
	}
	
	public String getDescriptorName() {
		return descriptorName;
	}
	
	public void setDescriptorName(String descriptorName) {
		this.descriptorName = descriptorName;
	}
	
	public MetadataType getType() {
		return type;
	}
	
	public void setType(MetadataType type) {
		this.type = type;
	}
	
	public String getTextValue() {
		return textValue;
	}
	
	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	
	public BigDecimal getNumValue() {
		return numValue;
	}
	
	public void setNumValue(BigDecimal numValue) {
		this.numValue = numValue;
	}
	
	public Timestamp getDateValue() {
		return dateValue;
	}
	
	public void setDateValue(Timestamp dateValue) {
		this.dateValue = dateValue;
	}
	
	public GUID getLovValueId() {
		return lovValueId;
	}
	
	public void setLovValueId(GUID lovValueId) {
		this.lovValueId = lovValueId;
	}
	
	public String getLovValueText() {
		return lovValueText;
	}
	
	public void setLovValueText(String lovValueText) {
		this.lovValueText = lovValueText;
	}
	
	public GUID getResourceCharacterId() {
		return resourceCharacterId;
	}
	
	public void setResourceCharacterId(GUID resourceCharacterId) {
		this.resourceCharacterId = resourceCharacterId;
	}
	
	public boolean isInheritToChilds() {
		return inheritToChilds;
	}
	
	public void setInheritToChilds(boolean inheritToChilds) {
		this.inheritToChilds = inheritToChilds;
	}	
	
}
