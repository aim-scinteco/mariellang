package com.scinteco.improve.node.aspects.discussions;

import com.scinteco.improve.nodes.RNodeAspect;

public class DiscussionAspect extends RNodeAspect {
	public static final DiscussionAspect Singleton = new DiscussionAspect();
	
	DiscussionAspect() {
	}
	
	@Override
	public String getKey() {
		return "discussion";
	}
	
}
