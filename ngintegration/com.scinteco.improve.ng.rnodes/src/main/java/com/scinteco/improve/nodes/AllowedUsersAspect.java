package com.scinteco.improve.nodes;

import java.util.Map;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.member.MemberNode;
import at.rufus.resources.api.model.GroupTree;

public abstract class AllowedUsersAspect extends AceAspect {
	public final static String KEY = "indexerAce";

	protected GroupTree groupTree;
	protected Map<GUID, MemberNode> users;

	public AllowedUsersAspect() {
		rebuild();
	}

	public abstract void rebuild();

	@Override
	public String getKey() {
		return KEY;
	}

	public GroupTree getGroupTree() {
		return groupTree;
	}

	@Override
	public void initializeRNode(RNode node) throws ClassNotFoundException {
		node.addExtension(this, buidAspectData(node));
	}

	protected abstract IRNodeExt buidAspectData(RNode node);

	public MemberNode getMemberNode(GUID guid) {
		MemberNode m = users.get(guid);
		if(m == null) {
			throw new IllegalArgumentException("MemberNode with ID " + guid.toString() + " not found.");
		}
		return m;
	}

}