package com.scinteco.improve.nodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.GroupTree;
import at.rufus.resources.api.model.IAccessControlEntry;

public class AllowedUsersAspectData extends AceAspectData<AllowedUsersAspect> {
	public static final String RIGHT_USERS_ALL = "_all_";
	
	@SuppressWarnings("serial")
	static class MemberNotLoadedException extends RuntimeException {
	}
	
	private RNode node;

	private final boolean isContainer;
	private Set<GUID> visibleUserIds;
	private Set<GUID> readUserIds;

	public AllowedUsersAspectData(AllowedUsersAspect aspect, boolean isContainer) {
		super(aspect);
		this.isContainer = isContainer;
	}	

	@Override
	public void initialize(RNode node) {
		this.node = node;
		if (getAces() != null && getAces().size() > 1) {
			getAces().sort(AceElement.ORDER_NR_COMPARATOR);
		}
		try {
			buildUserRights();
		} catch (MemberNotLoadedException e) {
			getAspect().rebuild();
			buildUserRights();
		}
	}

	static enum Statement {
		Allow,
		Deny,
		NoStatement;

		static Statement getStatement(String rights, int right) {
			right--;
			if (right < rights.length()) {
				char r = rights.charAt(right);
				switch (r) {
				case 'A': return Allow;
				case 'D': return Deny;
				case ' ': return NoStatement;
				default: throw new IllegalArgumentException();
				}
			}
			return NoStatement;
		}
	}

	static class AceNode {
		final RNode node;
		final AceElement basic;
		final Set<GUID> userIds;

		AceNode(RNode node, AceElement basic, GroupTree groupTree) {
			this.node = node;
			this.basic = basic;
			this.userIds = groupTree.resolveGroupToUsers(basic.getMemberId());
		}

		Statement getStatement(int right) {
			return Statement.getStatement(basic.getRights(), right);
		}

		public boolean isInherit() {
			return basic.isInherit();
		}

		public Statement getStatement(GUID userId, int right) {
			if (userIds == null) {
				throw new MemberNotLoadedException();
			}
			if (!userIds.contains(userId))
				return Statement.NoStatement;
			return getStatement(right);
		}
	}

	private List<AceNode> aceNodes; 
	private List<AceNode> aceNodesWithParents; 


	private void buildUserRights() {
		try {
			// build a flat list of aces including all parent aces
			buildAceNodes();

			Set<GUID> allUserIds = ((AllowedUsersAspect)aspect).getGroupTree().getAllUsers();
			//	System.out.println("All user ids: " + allUserIds);
			Set<GUID> readUserIds = new HashSet<GUID>();
			Set<GUID> fullVisibleUserIds = new HashSet<GUID>();

			for (GUID userId : allUserIds) {
				//		System.out.println(userId);
				boolean visible = hasBasicRight(userId, IAccessControlEntry.RIGHT_VISIBLE, true);
				if (visible && isTraversableFor(userId)) {
					fullVisibleUserIds.add(userId);
				}
				boolean read = hasBasicRight(userId, IAccessControlEntry.RIGHT_READ_CONTENT, true);
				if (read) {
					readUserIds.add(userId);
				}
			}

			this.readUserIds = readUserIds.size() == allUserIds.size() ? null : readUserIds;
			this.visibleUserIds = fullVisibleUserIds.size() == allUserIds.size() ? null : fullVisibleUserIds;
		} catch (MemberNotLoadedException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private boolean hasBasicRight(GUID userId, int right, boolean inherited) {
		List<AceNode> aces = inherited ? aceNodesWithParents : aceNodes;
		for (AceNode ace : aces) {
			if (ace.node == node|| ace.isInherit()) {
				Statement visibleSt = ace.getStatement(userId, right);
				if (visibleSt != Statement.NoStatement) {
					return visibleSt == Statement.Allow;
				}
			}
		}
		// no statement found, use default values
		switch (right) {
		case IAccessControlEntry.RIGHT_VISIBLE:
		case IAccessControlEntry.RIGHT_READ_CONTENT:
		case IAccessControlEntry.RIGHT_TRAVERSE_CONTAINER:
		case IAccessControlEntry.RIGHT_TRAVERSE_LEAF:
			return true;
		default: 
			throw new IllegalArgumentException();
		}
	}

	private boolean isTraversableFor(GUID userId) {
		if (node.getParent().isRepositoryNode())
			return true; // repository node

		if (isContainerResource()) {
			if (!getAspectData(node.getParent()).hasBasicRight(userId, IAccessControlEntry.RIGHT_TRAVERSE_CONTAINER, true)) {
				return false;
			}
		} else {
			if (!getAspectData(node.getParent()).hasBasicRight(userId, IAccessControlEntry.RIGHT_TRAVERSE_LEAF, true) 
					|| (node.getParent().getParent() != null && !node.getParent().getParent().isRepositoryNode() && !getAspectData(node.getParent().getParent()).hasBasicRight(userId, IAccessControlEntry.RIGHT_TRAVERSE_CONTAINER, true))) {
				return false;
			}
		}

		return true;
	}

	private AllowedUsersAspectData getAspectData(RNode node) {
		return (AllowedUsersAspectData)node.getExtension(aspect);
	}

	private void buildAceNodes() {
		aceNodes = new ArrayList<>();
		aceNodesWithParents = new ArrayList<>();
		RNode curNode = node;
		do {
			List<AceElement> elements = getAspectData(curNode).getAces();
			if (elements != null) {
				for (AceElement e : elements) {
					AceNode ace = new AceNode(curNode, e, ((AllowedUsersAspect)aspect).getGroupTree());
					if (curNode == node) {
						aceNodes.add(ace);
					}
					aceNodesWithParents.add(ace);
				}
			}
			curNode = curNode.getParent();
		} while (curNode.getResourceType() != null);

	}

	public List<String> getVisibleRightUsers() {
		if (visibleUserIds == null)
			return Arrays.asList(RIGHT_USERS_ALL);
		return GUID.toStringIds(visibleUserIds);	
	}

	public  List<String> getReadRightUsers() {
		if (readUserIds == null)
			return Arrays.asList(RIGHT_USERS_ALL);
		return GUID.toStringIds(readUserIds);	
	}

	public boolean isContainerResource() {
		return isContainer;
	}

}
