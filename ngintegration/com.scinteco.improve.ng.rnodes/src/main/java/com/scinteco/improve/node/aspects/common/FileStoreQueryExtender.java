package com.scinteco.improve.node.aspects.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.scinteco.improve.nodes.RNode;
import com.scinteco.improve.nodes.RNodeQuery;
import com.scinteco.improve.nodes.RNodeQueryExtender;

public class FileStoreQueryExtender extends RNodeQueryExtender<FileStoreAspect> {
	public static final String ATTR_FILE_STORE_PATH = "fsPath";

	public FileStoreQueryExtender(FileStoreAspect aspect, RNodeQuery query) {
		super(aspect, query);
	}

	@Override
	public boolean hasCommonTableExpressions() {
		return true;
	}

	@Override
	public void addCommonTableExpressions() {
		query.getQuery().append(
				" files (id, file_store_path) as (" +
						"  select" + 
						"   n.id,"+ 
						"   fv.file_store_path" + 
						"  from nodes n" + 
						"  join file_version fv on fv.id = n.id" +
						"  where fv.file_store_path is not null" +
						" )"			
				);
	}

	@Override
	public void select() {
		query.getQuery().append(
				" union all select f.id, '" + ATTR_FILE_STORE_PATH + "', f.file_store_path, null, null, null, null from files f"
				);
	}

	@Override
	public void processAttributes(ResultSet rs, RNode node, String attributeName) throws SQLException {
		if (ATTR_FILE_STORE_PATH.equals(attributeName)) {
			FileStoreAspectData ad = (FileStoreAspectData)node.getAspectData(aspect);
			ad.setFileStorePath(rs.getString(COL_VALUE_TEXT));
		}	
	}

}
