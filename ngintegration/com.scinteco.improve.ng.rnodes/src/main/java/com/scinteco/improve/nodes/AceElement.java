package com.scinteco.improve.nodes;

import java.util.Comparator;

import at.rufus.base.api.model.GUID;

public class AceElement {
	
	public static final Comparator<AceElement> ORDER_NR_COMPARATOR = new Comparator<AceElement>() {
		@Override
		public int compare(AceElement o1, AceElement o2) {
			return Integer.compare(o1.orderNr, o2.orderNr);
		}
	};

	private GUID memberId;
	private String rights;
	private boolean inherit;
	private int orderNr;
	private int rightsArea;
	
	public GUID getMemberId() {
		return memberId;
	}
	
	public void setMemberId(GUID memberId) {
		this.memberId = memberId;
	}
	
	public String getRights() {
		return rights;
	}
	
	public void setRights(String rights) {
		this.rights = rights;
	}
	
	public boolean isInherit() {
		return inherit;
	}
	
	public void setInherit(boolean inherit) {
		this.inherit = inherit;
	}
	
	public int getOrderNr() {
		return orderNr;
	}
	
	public void setOrderNr(int orderNr) {
		this.orderNr = orderNr;
	}

	public int getRightsArea() {
		return rightsArea;
	}

	public void setRightsArea(int rightsArea) {
		this.rightsArea = rightsArea;
	}
	
}
