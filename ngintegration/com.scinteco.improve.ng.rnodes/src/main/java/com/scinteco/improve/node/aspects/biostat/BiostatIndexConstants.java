package com.scinteco.improve.node.aspects.biostat;

public interface BiostatIndexConstants {
    String FIELD_OWNERS = "owners";
    String FIELD_COLLABORATORS = "collaborators";
	String FIELD_IS_PROJECT = "isProject";
	String FIELD_PROJECT_VISIBILITY = "projectVisibility";
    
    
    String[] FIELDS_TO_INDEX = new String[] {
            FIELD_OWNERS,
            FIELD_COLLABORATORS,
            FIELD_IS_PROJECT,
            FIELD_PROJECT_VISIBILITY
    };
}
