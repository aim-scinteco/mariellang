package com.scinteco.improve.node.aspects.common;

import java.util.List;

import com.scinteco.improve.nodes.RNodeAspectData;

public class RemarksAspectData extends RNodeAspectData<RemarksAspect> {
	private List<String> remarks;

	public RemarksAspectData(RemarksAspect aspect) {
		super(aspect);
	}

	public List<String> getRemarks() {
		return remarks;
	}

	public void setRemarks(List<String> remarks) {
		this.remarks = remarks;
	}

}
