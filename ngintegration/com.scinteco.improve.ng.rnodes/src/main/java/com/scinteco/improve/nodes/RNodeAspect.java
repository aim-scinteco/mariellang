package com.scinteco.improve.nodes;

public abstract class RNodeAspect {
	protected int extIndex;

	protected RNodeAspect() {
	}

	public abstract String getKey();

	public void initializeRNode(RNode node) throws ClassNotFoundException {
	}

	public void addQueryExtenders(RNodeQuery query) {
	}

}
