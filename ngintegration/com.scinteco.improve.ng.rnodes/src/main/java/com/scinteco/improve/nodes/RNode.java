package com.scinteco.improve.nodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.GUID;
import at.rufus.persistence.query.IResourceType;


public class RNode {
	public static final Comparator<RNode> NAME_COMPARATOR = new Comparator<RNode>() {
		@Override
		public int compare(RNode o1, RNode o2) {
			return CmpUtil.cmp(o1.getName(), o2.getName());
		}
	};

	protected final RNodeContext context;
	private final List<RNodeAspect> aspects;

	private RNode parent;
	private final Map<String, IRNodeExt> extensionMap = new HashMap<String, IRNodeExt>();
	private final List<IRNodeExt> extensions = new ArrayList<IRNodeExt>();

	private GUID resourceId;
	private GUID resourceVersionId;
	private IResourceType resourceType;
	private String name;
	private GUID revisionId;
	private boolean hasChildren;

	public RNode(RNodeContext context, List<RNodeAspect> aspects) {
		this.context = context;
		this.aspects = aspects;
	}

	public RNodeContext getContext() {
		return context;
	}

	public List<RNodeAspect> getAspects() {
		return aspects;
	}

	public RNode getParent() {
		return parent;
	}

	public void setParent(RNode parent) {
		this.parent = parent;
	}

	public GUID getResourceId() {
		return resourceId;
	}

	public void setResourceId(GUID resourceId) {
		this.resourceId = resourceId;
	}

	public GUID getResourceVersionId() {
		return resourceVersionId;
	}

	public void setResourceVersionId(GUID resourceVersionId) {
		this.resourceVersionId = resourceVersionId;
	}

	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}

	public IResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(IResourceType resourceType) {
		this.resourceType = resourceType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GUID getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(GUID revisionId) {
		this.revisionId = revisionId;
	}

	public void addExtension(RNodeAspect aspect, IRNodeExt extension) {
		extensions.add(extension);
		extensionMap.put(aspect.getKey(), extension);
	}

	public List<IRNodeExt> getExtensions() {
		return extensions;
	}

	public IRNodeExt getExtension(RNodeAspect aspect) {
		return extensionMap.get(aspect.getKey());
	}

	public RNodeAspectData<?> getAspectData(RNodeAspect aspect) {
		IRNodeExt ext = getExtension(aspect);
		return ext instanceof RNodeAspectData ? (RNodeAspectData<?>)ext : null;
	}

	public RNodeAspectData<?> getAspectData(String key) {
		IRNodeExt ext = extensionMap.get(key);
		return ext instanceof RNodeAspectData ? (RNodeAspectData<?>)ext : null;
	}

	public void initialize() {
		for(IRNodeExt ext : extensions) {
			if(ext instanceof RNodeAspectData) {
				((RNodeAspectData<?>)ext).initialize(this); 
			}
		}
	}

	public String getId() {
		return resourceId == null ? "ROOT" : resourceId.toString();
	}

	public boolean isFolder() {
		return hasChildren;
	}

	public RNode[] listChildren() {
		List<RNode> list = context.listChildren(this);
		if(list != null) {
			Collections.sort(list, RNode.NAME_COMPARATOR);
			return list.toArray(new RNode[list.size()]);
		} else {
			return new RNode[0];
		}
	}

	public boolean isRepositoryNode() {
		return resourceId == null;
	}

	public RNode getRepositoryNode() {
		RNode node = this;
		while (node != null && !node.isRepositoryNode()) {
			node = node.getParent();
		}
		return node;
	}

	public String buildResourcePath(boolean absolute) {
		String path = "/";
		RNode node = this;
		while ((node = node.getParent()) != null && !node.isRepositoryNode()) {
			path = "/" + node.getName() + path;
		}
		if (absolute) {
			path += getName();
		}
		return path;
	}

	@Override
	public String toString() {
		return String.format("%s [%s]", name, resourceType);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RNode)) {
			return false;
		}
		if (isRepositoryNode()) {
			return ((RNode)obj).isRepositoryNode();
		}

		return resourceId.equals(((RNode)obj).resourceId);
	}

	@Override
	public int hashCode() {
		if (isRepositoryNode()) {
			return name.hashCode();
		}
		return resourceId.hashCode();
	}

}
