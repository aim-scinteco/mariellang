package at.rufus.resources.api.member;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class MemberTreeNode implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private final GUID id;
	private final MemberNode member;
	private final List<MemberTreeNode> parents = new ArrayList<MemberTreeNode>();
	private final List<MemberTreeNode> children = new ArrayList<MemberTreeNode>();

	public MemberTreeNode(GUID id, MemberNode memberNode) {
		this.id = id;
		this.member = memberNode;
	}

	public GUID getId() {
		return id;
	}

	public List<MemberTreeNode> getParents() {
		return parents;
	}

	public List<MemberTreeNode> getChildren() {
		return children;
	}

	public MemberNode getMember() {
		return member;
	}

}
