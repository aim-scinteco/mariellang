package at.rufus.resources.api.member;

import java.io.Serializable;

import at.rufus.base.api.license.UserSerialNumber;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;
import at.rufus.resources.api.model.UserRole;
import at.rufus.resources.api.model.UserStatus;

public class MemberNode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Class<? extends Member> memberClass;
	private GUID memberId;
	private String displayName;
	private String email;

	private GUID userVersionId;
	private String userName;
	private UserRole role;
	private UserStatus status;
	private boolean hasParents;
	private boolean hasChildren;
	
	private String systemGroupId;
	private String licenseKey;
	private transient UserSerialNumber serialNumber;
	
	public MemberNode() {
		super();
	};
	
	
	public Class<? extends Member> getMemberClass() {
		return memberClass;
	}
	public void setMemberClass(Class<? extends Member> memberClass) {
		this.memberClass = memberClass;
	}
	public GUID getMemberId() {
		return memberId;
	}
	public void setMemberId(GUID memberId) {
		this.memberId = memberId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getEmail() {
        return email;
    }
	
	public void setEmail(String email) {
        this.email = email;
    }
	
	public GUID getUserVersionId() {
		return userVersionId;
	}
	public void setUserVersionId(GUID userVersionId) {
		this.userVersionId = userVersionId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public UserRole getRole() {
		return role;
	}
	public void setRole(UserRole role) {
		this.role = role;
	}
	public UserStatus getStatus() {
		return status;
	}
	public void setStatus(UserStatus status) {
		this.status = status;
	}
	public String getLicenseKey() {
		return licenseKey;
	}
	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}
	
	public boolean isHasChildren() {
		return hasChildren;
	}


	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}


	public String getSystemGroupId() {
		return systemGroupId;
	}


	public void setSystemGroupId(String systemGroupId) {
		this.systemGroupId = systemGroupId;
	}
		

	@Override
	public int hashCode() {
		return memberId == null ? 0 : memberId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MemberNode) {
			MemberNode other = (MemberNode) obj;
			return memberId == other.memberId || (memberId != null && memberId.equals(other.memberId));
		}
		return false;
	}


	public boolean isHasParents() {
		return hasParents;
	}


	public void setHasParents(boolean hasParents) {
		this.hasParents = hasParents;
	}
	
	public UserSerialNumber getSerialNumber() {
		if (serialNumber == null && licenseKey != null) {
			serialNumber = new UserSerialNumber();
			if (!serialNumber.set(licenseKey)) {
				serialNumber = null;
			}
		}
		return serialNumber;
	}
}
