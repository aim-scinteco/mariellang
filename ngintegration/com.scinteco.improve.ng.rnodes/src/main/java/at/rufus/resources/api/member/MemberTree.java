package at.rufus.resources.api.member;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class MemberTree implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private final List<MemberTreeNode> rootNodes = new ArrayList<MemberTreeNode>(3);
	
	public List<MemberTreeNode> getRootNodes() {
		return rootNodes;
	}
	
	public MemberTreeNode findMemberTreeNode(GUID memberId) {
		for (MemberTreeNode memberTreeNode : rootNodes) {
			 MemberTreeNode foundMember = findMemberTo(memberId, memberTreeNode);
			 if (foundMember != null) {
				 return foundMember;
			 }
		}
		return null;
	}
	
	private MemberTreeNode findMemberTo(GUID memberId, MemberTreeNode node) {
		if (node.getId() != null && node.getId().equals(memberId)) {
			return node;
		}
		MemberTreeNode result = null;
		for (MemberTreeNode memberTreeNode : node.getChildren()) {
			if (result != null) {
				break;
			}
			GUID id = memberTreeNode.getId();
			if (id != null && id.equals(memberId)) {
				// got it
				result =  memberTreeNode;
			}else{
				// search for children
				result = findMemberTo(memberId, memberTreeNode);
			}
		}
		return result;
	}
	
}
