package com.scinteco.improve.ng.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.List;

import org.mariella.oxygen.runtime.core.OxyConnectionProvider;
import org.mariella.oxygen.runtime.impl.OxyJ2SEConnectionProvider;
import org.mariella.persistence.database.Column;
import org.mariella.persistence.database.Converter;
import org.mariella.persistence.database.Table;
import org.mariella.persistence.database.TimestampConverter;
import org.mariella.persistence.loader.ClusterLoader;
import org.mariella.persistence.loader.ClusterLoaderConditionProvider;
import org.mariella.persistence.loader.LoaderContext;
import org.mariella.persistence.loader.ModifiableFactoryImpl;
import org.mariella.persistence.mapping.ClassMapping;
import org.mariella.persistence.mapping.ColumnMapping;
import org.mariella.persistence.mapping.IBatchStrategy;
import org.mariella.persistence.mapping.RelationshipPropertyMapping;
import org.mariella.persistence.mapping.SchemaMapping;
import org.mariella.persistence.persistor.ClusterDescription;
import org.mariella.persistence.persistor.ConnectionDatabaseAccess;
import org.mariella.persistence.persistor.DatabaseAccess;
import org.mariella.persistence.persistor.Persistor;
import org.mariella.persistence.query.BinaryCondition;
import org.mariella.persistence.query.Expression;
import org.mariella.persistence.query.JoinBuilder;
import org.mariella.persistence.query.QueryBuilder;
import org.mariella.persistence.query.TableReference;
import org.mariella.persistence.runtime.ModificationTracker;
import org.mariella.persistence.runtime.ModificationTrackerImpl;
import org.mariella.persistence.schema.ClassDescription;

import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;
import at.rufus.persistence.api.RufusPersistence;
import at.rufus.resources.api.model.Revision;
import at.rufus.resources.api.model.UserVersion;

public class Test {
	public static void main(String[] args) throws Exception {
		Test test = new Test();
	}
	
	private RufusPersistence persistence;
	private OxyConnectionProvider connectionProvider;

	public Test() throws Exception {
		connectionProvider = new OxyJ2SEConnectionProvider("jdbc:postgresql://localhost/improve_dev_local", "postgres", "sc1nt3c0");
		persistence = new RufusPersistence(connectionProvider); // connection will be managed (closed) by persistence
		persistence.getEnvironment().getSchemaMapping();

		connectionProvider = new OxyJ2SEConnectionProvider("jdbc:postgresql://localhost/improve_dev_local", "postgres", "sc1nt3c0");
		loadAdmin();
		connectionProvider.close();
	}
	
	private void loadAdmin() throws Exception {
		Connection connection = connectionProvider.getConnection();
		PreparedStatement ps;
		ResultSet rs;
		
		ps = connection.prepareStatement("select id from app_user_version where username = 'admin' and revision_to_time = ?");
		TimestampConverter.Singleton.setObject(ps, 1, Types.TIMESTAMP, Revision.LATEST_TIME);
		rs = ps.executeQuery();
		rs.next();
		GUID id = GuidConverter.Singleton.getObject(rs, 1);
		ModificationTracker mt = new ModificationTrackerImpl(persistence.getEnvironment().getSchemaMapping().getSchemaDescription());
		UserVersion userVersion = loadById(mt, UserVersion.class, false, id, "root", "root.member", "root.member.parents");
		new Object();
	}

	private SchemaMapping getSchemaMapping() {
		return persistence.getEnvironment().getSchemaMapping();
	}
	
	public void saveChanges(ModificationTracker modificationTracker) {
		saveChanges(modificationTracker, null);
	}
	
	private DatabaseAccess getDatabaseAccess() {
		return new ConnectionDatabaseAccess(connectionProvider.getConnection());
	}
	
	public ClassDescription getClassDescription(Class<?> entityClass) {
		return getSchemaMapping().getSchemaDescription().getClassDescription(entityClass.getName());
	}
	
	@SuppressWarnings("unchecked")
	public <T> T loadById(ModificationTracker modificationTracker, Class<T> entityClass, boolean isUpdate, Object id, String... clusterExpressions) {
		if (clusterExpressions == null || clusterExpressions.length == 0) {
			clusterExpressions = new String[] { "root" };
		}
		return (T) loadById(modificationTracker, new ClusterDescription(getClassDescription(entityClass), clusterExpressions), id, isUpdate);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T loadById(ModificationTracker modificationTracker, final ClusterDescription cd, final Object identity, final boolean isUpdate) {
		ClusterLoaderConditionProvider cp = new ClusterLoaderConditionProvider() {
			@Override
			public String[] getConditionPathExpressions() {
				return new String[] { "root" };
			}
			@Override
			public void aboutToJoinRelationship(QueryBuilder queryBuilder, String pathExpression, RelationshipPropertyMapping rpm, JoinBuilder joinBuilder) {
			}
			public void pathExpressionJoined(QueryBuilder queryBuilder, String pathExpression, final ClassMapping classMapping, TableReference tableReference) {
				if(pathExpression.equals("root")) {
					for(final ColumnMapping columnMapping : classMapping.getPrimaryKey().getColumnMappings()) {
						Expression condition = BinaryCondition.eq(
							tableReference.createColumnReference(columnMapping.getReadColumn()),
							columnMapping.getReadColumn().getConverter().createLiteral(identity)
						);
						queryBuilder.and(condition);
					}
				}
			}
		};
		List<Object> objects = load(modificationTracker, getDatabaseAccess(), cd, isUpdate, cp);
		return objects != null && !objects.isEmpty() ? (T) objects.get(0) : null;
	}
	
	public <T> List<T> load(ModificationTracker modificationTracker, Class<T> entityClass, final boolean isUpdate, final ClusterLoaderConditionProvider conditionProvider, String... pathExpressions) {
		ClusterDescription cd = new ClusterDescription(getClassDescription(entityClass), pathExpressions);
		return load(modificationTracker, entityClass, cd, isUpdate, conditionProvider);
	}

	public <T> List<T> load(ModificationTracker modificationTracker, Class<T> entityClass, final ClusterDescription cd, final boolean isUpdate, final ClusterLoaderConditionProvider conditionProvider) {
		return load(modificationTracker, getDatabaseAccess(), cd, isUpdate, conditionProvider);
	}
	
	public <T> List<T> load(ModificationTracker modificationTracker, Class<T> entityClass, boolean isUpdate, String... pathExpressions) {
		ClusterDescription cd = new ClusterDescription(getClassDescription(entityClass), pathExpressions);
		ClusterLoaderConditionProvider cp = new ClusterLoaderConditionProvider() {
			@Override
			public void pathExpressionJoined(QueryBuilder queryBuilder, String pathExpression, ClassMapping classMapping,TableReference tableReference) {
			}
			
			@Override
			public void aboutToJoinRelationship(QueryBuilder queryBuilder, String pathExpression, RelationshipPropertyMapping rpm, JoinBuilder joinBuilder) {
			}
			
			@Override
			public String[] getConditionPathExpressions() {
				return new String[0];
			}
		};
		return load(modificationTracker, entityClass, cd, isUpdate, cp);
	}

	@SuppressWarnings("unchecked")
	private <T> List<T> load(ModificationTracker modificationTracker, final DatabaseAccess databaseAccess, final ClusterDescription cd, final boolean isUpdate, final ClusterLoaderConditionProvider conditionProvider) {
		ClusterLoader clusterLoader = new ClusterLoader(getSchemaMapping(), cd);
		LoaderContext loaderContext = new LoaderContext(modificationTracker, new ModifiableFactoryImpl());
		loaderContext.setUpdate(isUpdate);
		return (List<T>)clusterLoader.load(databaseAccess, loaderContext, conditionProvider);
	}

	/**
	 * @param batched Can be used to overwrite the classes returned by the {@link SchemaMapping#getDefaultBatchStrategy()}.
	 */
	public void saveChanges(ModificationTracker modificationTracker, IBatchStrategy batchStrategy) {
		Persistor persistor = new Persistor(getSchemaMapping(), getDatabaseAccess(), modificationTracker);
		if (batchStrategy != null) {
			persistor.setBatchStrategy(batchStrategy);
		}
		persistor.persist();
	}

	public <T> void deleteEntities(Class<T> entityClass, Object... identities) {
		ClassMapping cm = getSchemaMapping().getClassMapping(entityClass.getName());
		if(cm == null) {
			throw new IllegalArgumentException("Entity class '" + entityClass + "' is unkown.");
		}
		Table table = cm.getPrimaryUpdateTable();
		ColumnMapping columnMapping = cm.getPrimaryKey().getColumnMappings()[0];
		Column primaryKeyColumn = columnMapping.getUpdateColumn();
		Converter<?> converter = primaryKeyColumn.getConverter();
		int i = 0;
		while (i < identities.length) {
			StringBuilder sb = new StringBuilder("DELETE from ").append(table.getName()).append(" WHERE ").append(primaryKeyColumn.getName()).append(" IN (");
			int chunkStart = i;
			int j = 0;
			while (i < identities.length && j < 1000) {
				if (j > 0) {
					sb.append(",");
				}
				if (converter != null) {
					converter.createLiteral(identities[i]).printSql(sb);
				} else {
					sb.append("?");
				}
				i++;
				j++;
			}
			sb.append(")");
			if (converter != null) {
				// connectionProvider.getConnection().update(sb.toString());
				throw new UnsupportedOperationException();
			} else {
				Object[] identityChunk;
				if (identities.length <= 1000) {
					identityChunk = identities;
				} else {
					identityChunk = new Object[j];
					System.arraycopy(identities, chunkStart, identityChunk, 0, identityChunk.length);
				}
				// connectionProvider.getConnection().update(sb.toString(), identityChunk);
				throw new UnsupportedOperationException();
			}
		}
	}
}

