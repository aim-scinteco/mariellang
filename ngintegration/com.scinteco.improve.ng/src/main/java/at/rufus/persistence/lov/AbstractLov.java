package at.rufus.persistence.lov;

import javax.persistence.MappedSuperclass;

import at.rufus.base.api.common.ObjectUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@MappedSuperclass
public class AbstractLov extends Entity {
	private static final long serialVersionUID = 1L;

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj.getClass() == getClass()) {
			AbstractLov other = (AbstractLov) obj;
			return ObjectUtil.equals(getId(), other.getId());
		}
		return super.equals(obj);
	}
	
	public boolean equalsId(GUID id) {
		GUID myId = getId();
		return myId != null && id != null && (myId == id || myId.equals(id));
	}
	
	@Override
	public int hashCode() {
		GUID id = getId();
		return id == null ? 0 : id.hashCode();
	}
	
}
