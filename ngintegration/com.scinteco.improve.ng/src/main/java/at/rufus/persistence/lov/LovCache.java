package at.rufus.persistence.lov;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mariella.persistence.runtime.ModificationTracker;
import org.mariella.persistence.runtime.ModificationTrackerImpl;

import at.rufus.base.api.model.AbstractLov;
import at.rufus.base.api.model.GUID;
import at.rufus.persistence.api.RufusPersistence;

public class LovCache<T extends AbstractLov> {
	
	private Class<T> lovClass;
	private String[] pathExpressions;
	private Map<GUID, T> values = new HashMap<>();
	
	LovCache(Class<T> lovClass, String... pathExpressions) {
		this.lovClass = lovClass;
		this.pathExpressions = pathExpressions;
	}
	
	public void load(RufusPersistence persistence) {
		ModificationTracker modificationTracker = new ModificationTrackerImpl(persistence.getEnvironment().getSchemaMapping().getSchemaDescription());
		List<T> values = persistence.load(modificationTracker, lovClass, false, pathExpressions);
		for (T value : values) {
			this.values.put(value.getId(), value);
		}
		modificationTracker.detachAll();
	}
	
	public T getValue(GUID id) {
		return id == null ? null : values.get(id);
	}
	
	public List<T> getAllValues() {
		return new ArrayList<>(values.values());
	}
	
}