package at.rufus.persistence.api;

import java.util.HashMap;
import java.util.List;

import org.mariella.oxygen.runtime.core.OxyConnectionProvider;
import org.mariella.persistence.postgres.PostgresUUIDConverter;
import org.mariella.persistence.runtime.ModificationTracker;

import at.rufus.base.api.model.GuidConverter;


public class RufusPersistence {
	private RufusEnvironment environment;

	public RufusPersistence(OxyConnectionProvider connectionProvider) { 
		GuidConverter.Singleton.setUUIDConverter(new PostgresUUIDConverter());
		environment = new RufusEnvironment(this);
		environment.setConnectionProvider(connectionProvider);
		environment.initialize("improve/postgres", new HashMap<>());
	}

	public RufusEnvironment getEnvironment() {
		return environment;
	}

	public <T> List<T> load(ModificationTracker modificationTracker, Class<T> entityClass, boolean isUpdate, String... pathExpressions) {
		throw new UnsupportedOperationException();
	}
}
