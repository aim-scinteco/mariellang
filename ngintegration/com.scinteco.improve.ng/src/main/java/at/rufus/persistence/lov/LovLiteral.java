package at.rufus.persistence.lov;

import org.mariella.persistence.database.Converter;
import org.mariella.persistence.query.Literal;

import at.rufus.base.api.model.AbstractLov;

public class LovLiteral<T extends AbstractLov> extends Literal<T> {
	
	public LovLiteral(Converter<T> converter, T value) {
		super(converter, value);
	}

	@Override
	public void printSql(StringBuilder b) {
		b.append(value == null ? "is null" : "guidlit("+converter.toString(value)+")");
	}
	
}