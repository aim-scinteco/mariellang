package at.rufus.persistence.lov;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import at.rufus.base.api.model.AbstractLov;
import at.rufus.persistence.api.RufusPersistence;

public class LovManager {

	public class MemoizingSupplier<T> implements Supplier<T>, Serializable {
		private static final long serialVersionUID = 1L;
		
		private final Supplier<T> delegate;
		private transient volatile boolean initialized;
		// "value" does not need to be volatile; visibility piggy-backs
		// on volatile read of "initialized".
		private transient T value;

		public MemoizingSupplier(Supplier<T> delegate) {
			this.delegate = delegate;
		}

		@Override
		public T get() {
			// A 2-field variant of Double Checked Locking.
			if (!initialized) {
				synchronized (this) {
					if (!initialized) {
						T t = delegate.get();
						value = t;
						initialized = true;
						return t;
					}
				}
			}
			return value;
		}

		@Override
		public String toString() {
			return "Suppliers.memoize(" + (initialized ? "<supplier that returned " + value + ">" : delegate) + ")";
		}
	}
		
	private RufusPersistence persistence;
	
	@SuppressWarnings("rawtypes")
	private final Map<Class<?>, Supplier<LovCache>> cache = new HashMap<>();
	
	public LovManager(RufusPersistence persistence) {
		this.persistence = persistence;
	}
	
	public RufusPersistence getPersistence() {
		return persistence;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T extends AbstractLov> LovCache<T> getLovCache(Class<T> lovClass) {
		Supplier<LovCache> supplier = null;
		synchronized (cache) {
			supplier = (Supplier<LovCache>) cache.get(lovClass);
			if (supplier == null) {
				supplier = new MemoizingSupplier<>(() -> {
					LovCache<T> lovCache = new LovCache<>(lovClass);
					lovCache.load(persistence);
					return lovCache;
				});
				cache.put(lovClass, supplier);
			}
		}
		return supplier.get();
	}

}
