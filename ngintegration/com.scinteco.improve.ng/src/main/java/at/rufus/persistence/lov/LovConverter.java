package at.rufus.persistence.lov;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.mariella.persistence.database.Converter;
import org.mariella.persistence.query.Literal;

import at.rufus.base.api.model.AbstractLov;
import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;

public class LovConverter<T extends AbstractLov> implements Converter<T> {
	
	private final Class<T> lovClass;
	private final LovManager lovManager;
	
	public LovConverter(LovManager lovManager, Class<T> lovClass) {
		this.lovManager = lovManager;
		this.lovClass = lovClass;
	}

	public Class<T> getLovClass() {
		return lovClass;
	}

	@Override
	public void setObject(PreparedStatement ps, int index, int type, T value) throws SQLException {
		GuidConverter.Singleton.setObject(ps, index, type, value == null ? null : value.getId());
	}

	@Override
	public T getObject(ResultSet rs, int index) throws SQLException {
		GUID id = GuidConverter.Singleton.getObject(rs, index);
		if (id == null) {
			return null;
		}
		return lovManager.getLovCache(lovClass).getValue(id);
	}

	@Override
	public String createParameter() {
		return "guidlit(?)";
	}

	@SuppressWarnings("unchecked")
	@Override
	public Literal<T> createLiteral(Object value) {
		return new LovLiteral<T>(this, (T) value);
	}

	@Override
	public Literal<T> createDummy() {
		return createLiteral(new GUID());
	}

	@Override
	public String toString(T value) {
		return value == null || value.getId() == null ? "null" : "'" + value.getId().toString() + "'";
	}

}
