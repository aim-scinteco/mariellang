package at.rufus.persistence.api;

import java.sql.Types;

import org.mariella.oxygen.runtime.impl.StandaloneEnvironment;
import org.mariella.persistence.annotations.mapping_builder.ConverterRegistryImpl;
import org.mariella.persistence.annotations.mapping_builder.DatabaseColumnInfo;
import org.mariella.persistence.annotations.mapping_builder.DatabaseTableInfo;
import org.mariella.persistence.annotations.mapping_builder.PersistenceBuilder;
import org.mariella.persistence.database.ByteArrayConverter;
import org.mariella.persistence.database.Converter;
import org.mariella.persistence.postgres.PostgresUUIDConverter;
import org.mariella.persistence.schema.ScalarPropertyDescription;

import at.rufus.base.api.model.GUID;
import at.rufus.base.api.model.GuidConverter;
import at.rufus.persistence.lov.AbstractLov;
import at.rufus.persistence.lov.LovConverter;
import at.rufus.persistence.lov.LovManager;
import at.rufus.resources.api.model.Actor;
import at.rufus.resources.api.model.AuditTrailOperation;
import at.rufus.resources.api.model.ParameterLov;
import at.rufus.resources.api.model.ParameterRole;
import at.rufus.resources.api.model.RelationTypeLov;
import at.rufus.resources.api.model.SystemLayer;

public class RufusEnvironment extends StandaloneEnvironment {
	private final LovManager lovManager;

public RufusEnvironment(RufusPersistence persistence) {
	super();
	this.lovManager = new LovManager(persistence);
}

public LovManager getLovManager() {
	return lovManager;
}

@Override
protected void configurePersistenceBuilder(PersistenceBuilder persistenceBuilder) {
	GuidConverter.Singleton.setUUIDConverter(new PostgresUUIDConverter());
	super.configurePersistenceBuilder(persistenceBuilder);
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.VARBINARY, GUID.class, new ConverterRegistryImpl.ConverterFactory() {
		@Override
		public Converter<?> createConverter(ScalarPropertyDescription propertyDescription, DatabaseTableInfo tableInfo, DatabaseColumnInfo columnInfo) {
			return GuidConverter.Singleton;
		}
	});
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.VARBINARY, GUID.class, new ConverterRegistryImpl.ConverterFactoryImpl(GuidConverter.Singleton));
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.BINARY, GUID.class, new ConverterRegistryImpl.ConverterFactoryImpl(GuidConverter.Singleton));
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.OTHER, GUID.class, new ConverterRegistryImpl.ConverterFactoryImpl(GuidConverter.Singleton));
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.VARBINARY, byte[].class, new ConverterRegistryImpl.ConverterFactory() {
		@Override
		public Converter<?> createConverter(ScalarPropertyDescription propertyDescription, DatabaseTableInfo tableInfo, DatabaseColumnInfo columnInfo) {
			return ByteArrayConverter.Singleton;
		}
	});
	
	registerLovConverter(persistenceBuilder, new LovConverter<SystemLayer>(lovManager, SystemLayer.class));
	registerLovConverter(persistenceBuilder, new LovConverter<Actor>(lovManager, Actor.class));
	registerLovConverter(persistenceBuilder, new LovConverter<AuditTrailOperation>(lovManager, AuditTrailOperation.class));
	registerLovConverter(persistenceBuilder, new LovConverter<ParameterRole>(lovManager, ParameterRole.class));
	registerLovConverter(persistenceBuilder, new LovConverter<ParameterLov>(lovManager, ParameterLov.class));
	registerLovConverter(persistenceBuilder, new LovConverter<RelationTypeLov>(lovManager, RelationTypeLov.class));

}

public static <T extends AbstractLov> void registerLovConverter(PersistenceBuilder persistenceBuilder, LovConverter<?> converter) {
	ConverterRegistryImpl.ConverterFactory factory = new ConverterRegistryImpl.ConverterFactoryImpl(converter);
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.VARBINARY, converter.getLovClass(), factory);
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.BINARY, converter.getLovClass(), factory);
	persistenceBuilder.getConverterRegistry().registerConverterFactory(Types.OTHER, converter.getLovClass(), factory);
}

}
