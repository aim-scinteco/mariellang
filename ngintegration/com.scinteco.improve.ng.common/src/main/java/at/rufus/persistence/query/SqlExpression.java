package at.rufus.persistence.query;

import org.mariella.persistence.query.Expression;

public class SqlExpression implements Expression {

	private String sql;
	
	public SqlExpression(String sql) {
		this.sql = sql;
	}
	
	@Override
	public void printSql(StringBuilder b) {
		b.append(sql);
	}

}
