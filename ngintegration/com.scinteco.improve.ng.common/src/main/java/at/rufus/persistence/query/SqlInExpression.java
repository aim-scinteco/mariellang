package at.rufus.persistence.query;

import org.mariella.persistence.query.ColumnReference;
import org.mariella.persistence.query.Expression;

public class SqlInExpression implements Expression {

	private ColumnReference colRef;
	private String subselect;
	
	public SqlInExpression(ColumnReference colRef, String subselect) {
		this.colRef = colRef;
		this.subselect = subselect;
	}
	
	@Override
	public void printSql(StringBuilder b) {
		colRef.printSql(b);
		b.append(" in (");
		b.append(subselect);
		b.append(")");
	}

}
