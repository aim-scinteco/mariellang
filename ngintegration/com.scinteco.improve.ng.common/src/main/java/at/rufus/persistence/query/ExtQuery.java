package at.rufus.persistence.query;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class ExtQuery {
	
	private StringBuilder withClause;
	private StringBuilder resultAttributes;
	private StringBuilder fromClause;
	private StringBuilder whereClause;
	private StringBuilder appendix;
	private List<Object> withArgs = new ArrayList<Object>(5);
	private List<Object> resultAttributeArgs = new ArrayList<Object>(5);
	private List<Object> fromArgs = new ArrayList<Object>(5);
	private List<Object> whereArgs = new ArrayList<Object>(5);
	private List<Object> appendixArgs = new ArrayList<Object>(5);

	public ExtQuery() {
		this(null, null, null, null, null);
	}
	
	public ExtQuery(String withClause, String resultAttributes, String fromClause, String whereClause, String appendix) {
		this.withClause = new StringBuilder(withClause == null ? "" : withClause);
		this.resultAttributes = new StringBuilder(resultAttributes == null ? "" : resultAttributes);
		this.fromClause = new StringBuilder(fromClause == null ? "" : fromClause);
		this.whereClause = new StringBuilder(whereClause == null ? "" : whereClause);
		this.appendix = new StringBuilder(appendix == null ? "" : appendix);
	}
	
	private void append(StringBuilder sql, List<Object> args, String sqlToAppend, Object... argsToAppend) {
		sql.append(sqlToAppend);
		for (Object arg : argsToAppend) {
			args.add(arg);
		}
	}

	public void appendWithClause(String sql, Object... args) {
		append(withClause, withArgs, sql, args);
	}

	public void appendResultAttributes(String sql, Object... args) {
		sql = sql.trim();
		if (resultAttributes.length() > 0 && sql.length() > 0 && !resultAttributes.toString().endsWith(",") && !sql.startsWith(",")) {
			append(resultAttributes, resultAttributeArgs, ", ");
		}
		append(resultAttributes, resultAttributeArgs, sql, args);
	}
	
	public void appendFromClause(String sql, Object... args) {
		append(fromClause, fromArgs, sql, args);
	}
	
	public void appendWhereClause(String sql, Object... args) {
		append(whereClause, whereArgs, sql, args);
	}
	
	public void appendAppendix(String sql, Object... args) {
		append(appendix, appendixArgs, sql, args);
	}
	
	public void appendWhereClauseIn(String attribute, Collection<?> values) {
		boolean useVariables = true;
		int i = 0;
		whereClause.append(" ("+attribute+" in (");
		for (Object value : values) {
			if (i == 1000) {
				whereClause.append(") or "+attribute+" in (");
				i = 0;
			} else if (i > 0) {
				whereClause.append(',');
			}
			if (value instanceof GUID) {
				whereClause.append("guidlit(");
			}
			if (useVariables) {
				whereClause.append("?");
				whereArgs.add(value != null && value.getClass().isEnum() ? value.toString() : value);
			} else {
				boolean quote = value instanceof GUID || value instanceof String || (value != null && value.getClass().isEnum());
				if (quote) {
					whereClause.append("'");
				}
				whereClause.append(value.toString());
				if (quote) {
					whereClause.append("'");
				}
			}
			if (value instanceof GUID) {
				whereClause.append(")");
			}
			i++;
		}
		whereClause.append("))");
	}
	
	public String getSql() {
		return 
			withClause +
			" select " +
			resultAttributes +
			" from " +
			fromClause +
			(whereClause.length() == 0 ? "" : " where "+whereClause) +
			" " +
			appendix;
	}
	
	@Override
	public String toString() {
		return getSql();
	}
	
	public String toDebugSql() {
		SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS XXX");
		String s = getSql();
		for (Object o : getArgs()) {
			if (o instanceof String) {
				s = s.replaceFirst("\\?", "'"+o.toString()+"'");
			} else if (o instanceof GUID) {
				s = s.replaceFirst("\\?", "'"+o+"'");
			} else if (o instanceof byte[]) {
				s = s.replaceFirst("\\?", "'"+new GUID((byte[]) o)+"'");
			} else if (o instanceof Timestamp) {
				s = s.replaceFirst("\\?", "TIMESTAMP '"+tf.format(o)+"'");
			} else if (o instanceof Date) {
				s = s.replaceFirst("\\?", "DATE '"+tf.format(o)+"'");
			} else {
				s = s.replaceFirst("\\?", o.toString());
			}
		}
		return s;
	}
	
	public Object[] getArgs() {
		List<Object> args = new ArrayList<>(withArgs.size()+resultAttributeArgs.size()+fromArgs.size()+whereArgs.size()+appendixArgs.size());
		args.addAll(withArgs);
		args.addAll(resultAttributeArgs);
		args.addAll(fromArgs);
		args.addAll(whereArgs);
		args.addAll(appendixArgs);
		return args.toArray();
	}
	
}
