package at.rufus.persistence.query;

import java.util.Collection;

public interface InClauseValues<E> extends Collection<E> {

	boolean isDirty();

	void synchronize();

}
