package at.rufus.persistence.query;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class Query {
	
	private StringBuilder sql = new StringBuilder();
	private List<Object> args = new ArrayList<Object>(5);
	
	private static String IN_CLAUSE_MESSAGE = " [in clause: temporay table(s) not synchronized]";
	private List<InClauseValues<?>> inClauseValues = new ArrayList<InClauseValues<?>>(); 

	public Query() {
	}
	
	public Query(String sql, Object... args) {
		append(sql, args);
	}
	
	public Query append(String sql, Object... args) {
		this.sql.append(sql);
		for (Object arg : args) {
			this.args.add(arg);
		}
		return this;
	}
	
	public Query append(Query subQuery) {
		if (subQuery != null) {
			sql.append(subQuery.getSql());
			args.addAll(subQuery.args);
		}
		return this;
	}
	
	public Query append(String sql1, Query subQuery, String sql2) {
		sql.append(sql1);
		sql.append(subQuery.getSql());
		sql.append(sql2);
		args.addAll(subQuery.args);
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> Query appendIn(String attribute, T... values) {
		return appendIn(attribute, Arrays.asList(values));
	}
	
	public Query appendIn(String attribute, Collection<?> values) {
	    return appendIn(attribute, false, values);
	}
	
	public Query appendNotIn(String attribute, Collection<?> values) {
        return appendIn(attribute, true, values);
    }
	
	private Query appendIn(String attribute, boolean negative, Collection<?> values) {
		boolean useVariables = true;
		int i = 0;
		sql.append(" (" + attribute + (negative ? " not in (" : " in ("));
		for (Object value : values) {
			if (i == 1000) {
				if (negative) {
					sql.append(") and " + attribute + " not in (" );
				} else {
					sql.append(") or " + attribute +  " in (");
				}
				i = 0;
			} else if (i > 0) {
				sql.append(',');
			}
			if (value instanceof GUID) {
				sql.append("guidlit(");
			}
			if (useVariables) {
				sql.append("?");
				args.add(value != null && value.getClass().isEnum() ? value.toString() : value);
			} else {
				boolean quote = value instanceof GUID || value instanceof String || (value != null && value.getClass().isEnum());
				if (quote) {
					sql.append("'");
				}
				sql.append(value.toString());
				if (quote) {
					sql.append("'");
				}
			}
			if (value instanceof GUID) {
				sql.append(")");
			}
			i++;
		}
		sql.append("))");
		return this;
	}
	
	public String getSelect(String resultAttributes) {
		return "select " + resultAttributes + " from " + getSql();
	}
	
	public String getSelectForExecute(String resultAttributes) {
		prepareInClauses();
		return getSelect(resultAttributes);
	}
	
	public String getSql() {
		return sql.toString();
	}
	
	public String getSqlForExecute() {
		prepareInClauses();
		return sql.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder(sql.toString());
		if (hasDirtyInClauses())
			b.append(IN_CLAUSE_MESSAGE);
		return b.toString();
	}
	
	public Object[] getArgs() {
		return args.toArray();
	}
	
	public String toDebugSql() {
		SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS XXX");
		String s = sql.toString();
		for (Object o : args) {
			if (o instanceof String) {
				s = s.replaceFirst("\\?", "'"+o.toString()+"'");
			} else if (o instanceof GUID) {
				s = s.replaceFirst("\\?", "'"+o+"'");
			} else if (o instanceof byte[]) {
				s = s.replaceFirst("\\?", "'"+new GUID((byte[]) o)+"'");
			} else if (o instanceof Timestamp) {
				s = s.replaceFirst("\\?", "TIMESTAMP '"+tf.format(o)+"'");
			} else if (o instanceof Date) {
				s = s.replaceFirst("\\?", "DATE '"+tf.format(o)+"'");
			} else {
				s = s.replaceFirst("\\?", o.toString());
			}
		}
		if (hasDirtyInClauses())
			s += IN_CLAUSE_MESSAGE;
		return s;
	}
	
	public List<InClauseValues<?>> getInClauseValues() {
		return inClauseValues;
	}
	
	private void prepareInClauses() {
		for (InClauseValues<?> values : inClauseValues) {
			values.synchronize();
		}
	}
	
	private boolean hasDirtyInClauses() {
		for (InClauseValues<?> values : getInClauseValues()) {
			if (values.isDirty())
				return true;
		}
		return false;
	}
	
}
