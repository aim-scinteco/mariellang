package at.rufus.persistence.query;

public final class SqlUtil {
	
	public static String patternToLike(String pattern, String escapeChar) {
		return pattern.replace("_", escapeChar+"_").replace("%", escapeChar+"%").replace("*", "%");
	}

	public static String patternToLike(String pattern) {
		return patternToLike(pattern, "\\");
	}
	
}
