package at.rufus.persistence.query;

import at.rufus.resources.api.model.Resource;
import at.rufus.resources.api.model.ResourceVersion;

public interface IResourceType {

	Class<? extends Resource> getResourceClass();
	
	Class<? extends ResourceVersion> getResourceVersionClass();

	String getName();
	
	String getIndexType();

}
