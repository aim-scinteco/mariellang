package at.rufus.persistence.query;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Transaction {
	
	private static final Logger LOGGER = LogManager.getLogger(Transaction.class);
	
	public static interface IStatementExecutor {
		void execute(Connection connection) throws SQLException;
	}

	public static void commit(DataSource dataSource, IStatementExecutor executor) throws SQLException {
		Connection connection = dataSource.getConnection();
		try {
			connection.setAutoCommit(false);
			executor.execute(connection);
			connection.commit();
		} catch (SQLException ex) {
			LOGGER.log(Level.ERROR, "Commit error.", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.log(Level.ERROR, "Rollback error.", e);
			}
			throw ex;
		} finally {
			try {
				connection.close(); 
			} catch (SQLException ex) {
				LOGGER.log(Level.ERROR, "Connection close error.", ex);
			}
		}
	}
	
}
