package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;

public interface ISearchCriteriaExt extends Serializable {

	String getType();
	
}
