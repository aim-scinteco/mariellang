package at.rufus.resources.api.elasticsearch.indexer;

public interface IResourcesIndexConstants {

	public static final String DOC_TYPE_RESOURCE = "resource";
	
	public static final String ANALYZER_STD = "std_analyser";
	public static final String ANALYZER_WHITESPACE = "ws_analyser";
	public static final String ANALYZER_REVERSE = "reverse_analyser";
	public static final String ANALYZER_DATE = "date_analyser";
	public static final String ANALYZER_NUM = "num_analyser";
	public static final String NORMALIZER_KEYWORD = "kw_normalizer";
	
	public static final String FIELD_STD = "std";
	public static final String FIELD_NORM = "norm";
	public static final String FIELD_RAW = "raw";
	public static final String FIELD_REVERSE = "reverse";
	
	public static final String RESOURCE_TYPE_FOLDER = "folder";
	public static final String RESOURCE_TYPE_FILE = "file";
	public static final String RESOURCE_TYPE_REVIEW = "review";
	
	public static final String FIELD_TYPE = "type";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_REPOSITORY_ID = "repositoryId";
	public static final String FIELD_ENTITY_ID = "entityId";
	public static final String FIELD_ENTITY_VERSION_ID = "entityVersionId";
	public static final String FIELD_REVISION_ID = "revisionId";
	public static final String FIELD_PATH = "path";
	public static final String FIELD_CREATED_BY = "createdBy";
	public static final String FIELD_CREATED_BY_ID = "createdById";
	public static final String FIELD_CREATED_AT = "createdAt";
	public static final String FIELD_LAST_MODIFIED_BY = "lastModifiedBy";
	public static final String FIELD_LAST_MODIFIED_BY_ID = "lastModifiedById";
	public static final String FIELD_LAST_MODIFIED_AT = "lastModifiedAt";
	public static final String FIELD_VISIBLE_RIGHT_USERS = "visibleRightUsers";
	public static final String FIELD_READ_RIGHT_USERS = "readRightUsers";
	public static final String FIELD_VIEWS = "views";
	public static final String FIELD_USAGES = "usages";
	public static final String FIELD_COMMENT = "comment";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_RATIONALE = "rationale";
	public static final String FIELD_REMARKS = "remarks";
	public static final String FIELD_VALIDATION_STATUS = "validationStatus";
	public static final String FIELD_REVIEW_STATUS = "reviewStatus";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_CONTENT_PENDING = "contentPending";
	public static final String FIELD_CONTENT_SKIPPED = "skipContent";
	public static final String FIELD_FILE_STORE_PATH = "fileStorePath";
	public static final String FIELD_ATTACHMENT_CONTENT = "attachment.content";
	public static final String FIELD_ATTACHMENT_CONTENT_LEN = "attachment.content_length";
	public static final String FIELD_ATTACHMENT_CONTENT_TYPE = "attachment.content_type";
	
	public static final String[] FIELDS_TO_INDEX = new String[] {
		FIELD_TYPE,
		FIELD_NAME,
		FIELD_REPOSITORY_ID,
		FIELD_ENTITY_ID,
		FIELD_ENTITY_VERSION_ID,
		FIELD_REVISION_ID,
		FIELD_PATH,
		FIELD_CREATED_BY,
		FIELD_CREATED_BY_ID,
		FIELD_CREATED_AT,
		FIELD_LAST_MODIFIED_BY,
		FIELD_LAST_MODIFIED_BY_ID,
		FIELD_LAST_MODIFIED_AT,
		FIELD_VISIBLE_RIGHT_USERS,
		FIELD_READ_RIGHT_USERS,
		FIELD_VIEWS,
		FIELD_USAGES,
		FIELD_COMMENT,
		FIELD_DESCRIPTION,
		FIELD_RATIONALE,
		FIELD_REMARKS,
		FIELD_VALIDATION_STATUS,
		FIELD_REVIEW_STATUS,
		FIELD_CONTENT,
		FIELD_FILE_STORE_PATH,
		FIELD_CONTENT_PENDING
	};
	
	public static final String NESTED_TYPE_METADATA = "metadata";
	public static final String FIELD_METADATA_DESC_ID = "mdDescId";
	public static final String FIELD_METADATA_NAME = "mdName";
	public static final String FIELD_METADATA_TYPE = "mdType";
	public static final String FIELD_METADATA_TEXT_VALUE = "mdTextValue";
	public static final String FIELD_METADATA_URL_VALUE = "mdUrlValue";
	public static final String FIELD_METADATA_NUM_VALUE = "mdNumValue";
	public static final String FIELD_METADATA_DATE_VALUE = "mdDateValue";
	public static final String FIELD_METADATA_LOV_ID_VALUE = "mdLovIdValue";
	public static final String FIELD_METADATA_LOV_TEXT_VALUE = "mdLovTextValue";
	
	public static final String[] NESTED_TYPES_TO_INDEX = new String[] {
		NESTED_TYPE_METADATA
	};
	
	public static String metadata(String attribute) {
		return NESTED_TYPE_METADATA + "." + attribute;
	}
	
	public static String raw(String attribute) {
		return attribute + "." + FIELD_RAW;
	}
	
	public static String norm(String attribute) {
		return attribute + "." + FIELD_NORM;
	}
	
	public static String std(String attribute) {
		return attribute + "." + FIELD_STD;
	}
	
	public static String reverse(String attribute) {
        return attribute + "." + FIELD_REVERSE;
    }
}
