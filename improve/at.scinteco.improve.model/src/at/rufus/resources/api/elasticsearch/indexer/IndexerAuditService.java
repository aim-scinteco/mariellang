package at.rufus.resources.api.elasticsearch.indexer;

import java.io.IOException;

import at.rufus.resources.api.model.IndexerFailure;

public interface IndexerAuditService {
    <T> T getFilesWithoutContent(ListCollector<IndexedResource, T> collector) throws IOException;
    
    <T> T getResourcesInFolder(String folder, ListCollector<IndexedResource, T> collector) throws IOException;
    
    IndexedResource get(String id) throws IOException;
    
    <T> T getFailures(ListCollector<IndexerFailure, T> collector);
}
