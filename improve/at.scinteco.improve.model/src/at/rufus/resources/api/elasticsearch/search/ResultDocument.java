package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResultDocument implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String id;
	private final String type;
	private List<ResultField> fields;
	private List<ResultHighlightField> highlightFields;
	private long version;
	private float score;
	
	public ResultDocument(String id, String type) {
		this.id = id;
		this.type = type;
	}
	
	public String getId() {
		return id;
	}
	
	public String getType() {
		return type;
	}
	
	public ResultField[] getFields() {
		return fields().toArray(new ResultField[fields().size()]);
	}
	
	public void setFields(ResultField[] fields) {
		this.fields = Arrays.asList(fields);
	}
	
	private List<ResultField> fields() {
		if (fields == null) {
			fields = new ArrayList<ResultField>(5);
		}
		return fields;
	}
	
	public void addField(ResultField field) {
		fields().add(field);
	}
	
	public ResultField getField(String name) {
		for (ResultField field : fields()) {
			if (name.equals(field.getName())) {
				return field;
			}
		}
		return null;
	}
	
	public ResultHighlightField[] getHighlightFields() {
		return highlightFields().toArray(new ResultHighlightField[highlightFields().size()]);
	}
	
	public void setHighlightFields(ResultHighlightField[] highlightFields) {
		this.highlightFields = Arrays.asList(highlightFields);
	}
	
	private List<ResultHighlightField> highlightFields() {
		if (highlightFields == null) {
			highlightFields = new ArrayList<ResultHighlightField>(5);
		}
		return highlightFields;
	}
	
	public void addHighlightField(ResultHighlightField highlightField) {
		highlightFields().add(highlightField);
	}
	
	public ResultHighlightField getHighlightField(String name) {
		for (ResultHighlightField field : highlightFields()) {
			if (name.equals(field.getName())) {
				return field;
			}
		}
		return null;
	}
	
	public void setVersion(long version) {
		this.version = version;
	}
	
	public long getVersion() {
		return version;
	}
	
	public float getScore() {
		return score;
	}
	
	public void setScore(float score) {
		this.score = score;
	}

}
