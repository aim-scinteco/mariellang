package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;

public class ResultField implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String label;
	private Object value;
	
	public ResultField(String name) {
		this(name, name, null);
	}
	
	public ResultField(String name, String label) {
		this(name, label, null);
	}
	
	public ResultField(String name, String label, Object value) {
		this.name = name;
		this.label = label;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLabel() {
		return label;
	}

	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}

}
