package at.rufus.resources.api.elasticsearch.search;

public enum SearchOperator {

	Equals("Equals", "="), 
	Greater("Greater", ">"), 
	EqualsGreater("Equals or Greater", ">="), 
	Lower("Lower", "<"), 
	EqualsLower("Equals or Lower", "<=");
	
	private final String label;
	private final String code;
	
	private SearchOperator(String label, String code) {
		this.label = label;
		this.code = code;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getCode() {
		return code;
	}
	
	public static String[] getCodes() {
		SearchOperator[] operators = values();
		String[] codes = new String[operators.length];
		for (int i = 0; i < operators.length; i++) {
			codes[i] = operators[i].getCode();
		}
		return codes;
	}
	
}
