package at.rufus.resources.api.elasticsearch.indexer;

public enum ReindexType {
    REINDEX,
	REPAIR,
	REPAIR_DATA_ONLY
}
