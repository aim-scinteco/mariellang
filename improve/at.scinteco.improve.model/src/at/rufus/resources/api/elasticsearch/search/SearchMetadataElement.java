package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;

import at.rufus.resources.api.model.MetadataType;

public class SearchMetadataElement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private MetadataType type;
	private SearchOperator operator;
	private String name;
	private Object value;

	public MetadataType getType() {
		return type;
	}

	public void setType(MetadataType type) {
		this.type = type;
	}

	public SearchOperator getOperator() {
		return operator;
	}

	public void setOperator(SearchOperator operator) {
		this.operator = operator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
