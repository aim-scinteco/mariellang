package at.rufus.resources.api.elasticsearch.indexer;

import at.rufus.base.api.model.GUID;

public interface IIndexer {
    String DEFAULT_INDEX_NAME =  "improve-index";
    String DEFAULT_INDEX_ALIAS = "improve";
    
    
	void startup() throws IndexException;
	
	void shutdown() throws IndexException;
	
	boolean isStarted();
	
	void startIndexing() throws IndexException;
	
	void stopIndexing() throws IndexException;
	
	boolean isIndexing();
	
	void startFileReindexing() throws IndexException;
	
	void stopFileReindexing() throws IndexException;
	
	boolean isFileReindexing();
	
	void startReindexing(ReindexType type, GUID[] rootResourceIds) throws IndexException;
	
	void resumeReindexing(ReindexType type, GUID[] rootResourceIds) throws IndexException;

	void stopReindexing() throws IndexException;
	
	boolean isReindexing();
	
	IIndexerStatus getStatus(boolean details);
	
	
}
