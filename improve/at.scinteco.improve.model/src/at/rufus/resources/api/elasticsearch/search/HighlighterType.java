package at.rufus.resources.api.elasticsearch.search;

public enum HighlighterType {
    PLAIN("plain"),
    UNIFIED("unified");
    
    private String esName;
    
    private HighlighterType(String esName) {
        this.esName = esName;
    }
    
    public String getEsName() {
        return esName;
    }
}
