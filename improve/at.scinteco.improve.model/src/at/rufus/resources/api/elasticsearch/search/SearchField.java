package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;

public class SearchField implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String name;
	private final String label;
	
	public SearchField(String name, String label) {
		this.name = name;
		this.label = label;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLabel() {
		return label;
	}
	
	public static SearchField getFieldByName(String name, SearchField... fields) {
		for (SearchField field : fields) {
			if (name.equals(field.getName())) {
				return field;
			}
		}
		return null;
	}
	
	public static SearchField getFieldByLabel(String label, SearchField... fields) {
		for (SearchField field : fields) {
			if (label.equals(field.getLabel())) {
				return field;
			}
		}
		return null;
	}
	
}
