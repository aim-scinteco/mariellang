package at.rufus.resources.api.elasticsearch.indexer;

public enum ReindexingStatus {

	None,
	ReindexingAll,
	ReindexingData
	
}
