package at.rufus.resources.api.elasticsearch.indexer;

import at.rufus.resources.api.model.SearchIndex;

public interface IIndexerStatus {
	
	boolean isUpAndRunning();
	
	IndexingStatus getIndexingStatus();
	
	ReindexingStatus getReindexingStatus();
	
	boolean isReindexingFiles();
	
	SearchIndex getIndexDetails();
	
}
