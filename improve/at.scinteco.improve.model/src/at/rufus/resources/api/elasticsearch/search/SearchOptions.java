package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class SearchOptions implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final int DEFAULT_HL_FRAGMENT_SIZE = 100;
	public static final String DEFAULT_HL_PREFIX = "<em>";
	public static final String DEFAULT_HL_POSTFIX = "</em>";
	public static final int DEFAULT_FETCH_SIZE = 10;
	public static final int MAX_SEARCH_ROWS = 10000;
	
	private int fetchOffset = 0;
	private int fetchSize = DEFAULT_FETCH_SIZE;
	private String[] hlPreTags = null;
	private String[] hlPostTags = null;
	private int hlFragmentSize = DEFAULT_HL_FRAGMENT_SIZE;
	private HighlighterType hlType = HighlighterType.UNIFIED;
	
	private final List<SortField> sortFields = new LinkedList<SortField>();

	public SearchOptions() {
		super();
	}
	
	public List<SortField> getSortFields() {
		return sortFields;
	}
	
	public int getFetchOffset() {
		return fetchOffset;
	}

	public void setFetchOffset(int fetchOffset) {
		this.fetchOffset = fetchOffset;
	}

	public int getFetchSize() {
		return fetchSize;
	}

	public void setFetchSize(int fetchSize) {
		this.fetchSize = fetchSize;
	}
	
	/**
	 * First page has index 1!
	 */
	public void setPage(int pageIdx) {
		int offset = Math.max(0, fetchSize * (pageIdx - 1));
		setFetchOffset(offset);
	}
	
	public String[] getHlPreTags() {
		return hlPreTags;
	}
	
	public void setHlPreTags(String... hlPreTags) {
		this.hlPreTags = hlPreTags;
	}
	
	public String[] getHlPostTags() {
		return hlPostTags;
	}
	
	public void setHlPostTags(String... hlPostTags) {
		this.hlPostTags = hlPostTags;
	}
	
	public int getHlFragmentSize() {
        return hlFragmentSize;
    }
	
	public void setHlFragmentSize(int hlFragmentSize) {
        this.hlFragmentSize = hlFragmentSize;
    }
	
	public HighlighterType getHlType() {
        return hlType;
    }
	
	public void setHlType(HighlighterType hlType) {
        this.hlType = hlType;
    }
}
