package at.rufus.resources.api.elasticsearch.search;

public class SearchException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public SearchException(String message) {
		super(message);
	}
	
	public SearchException(Throwable cause) {
		super(cause);
	}
	
	public SearchException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
