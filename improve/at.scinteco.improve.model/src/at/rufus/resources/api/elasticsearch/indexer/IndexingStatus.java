package at.rufus.resources.api.elasticsearch.indexer;

public enum IndexingStatus {
	
	None,
	Indexing

}
