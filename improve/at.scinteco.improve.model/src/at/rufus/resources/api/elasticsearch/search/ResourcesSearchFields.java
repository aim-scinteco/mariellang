package at.rufus.resources.api.elasticsearch.search;

import at.rufus.resources.api.elasticsearch.indexer.IResourcesIndexConstants;

public final class ResourcesSearchFields {

	public static final SearchField FIELD_NAME = new SearchField(IResourcesIndexConstants.FIELD_NAME, "Name");
	public static final SearchField FIELD_COMMENT = new SearchField(IResourcesIndexConstants.FIELD_COMMENT, "Comment");
	public static final SearchField FIELD_CONTENT = new SearchField(IResourcesIndexConstants.FIELD_ATTACHMENT_CONTENT, "Content");
	
	public static final SearchField[] SEARCH_FIELDS = new SearchField[] {
		FIELD_NAME, 
		FIELD_COMMENT,
		FIELD_CONTENT
	};
	
}
