package at.rufus.resources.api.elasticsearch.indexer;

import at.rufus.resources.api.model.MetadataType;

public enum AttributeType {
    TEXT(false), 
    SHORT_TEXT(false), 
    KEYWORD(false), 
    STRICT_KEYWORD(false),
    DATETIME(true), 
    DATE(true), 
    BOOLEAN(false), 
    LONG(true), 
    DOUBLE(true), 
    ACCESS(false), 
    USER(false), 
    LOV(false);
    
    private boolean comparable;
    
    private AttributeType(boolean comparable) {
        this.comparable = comparable;
    }
    
    public boolean isComparable() {
        return comparable;
    }
    
    public static AttributeType fromMetadataType(MetadataType type) {
        if (type == null) {
            return AttributeType.TEXT;
        }
        switch (type) {
        case LOV: return AttributeType.LOV;
        case TEXT: return AttributeType.TEXT;
        case URL: return AttributeType.KEYWORD;
        case DATE: return AttributeType.DATE;
        case NUMBER: return AttributeType.DOUBLE;
        default: throw new IllegalStateException("Unknown metdata type " + type);
        }
    }
}
