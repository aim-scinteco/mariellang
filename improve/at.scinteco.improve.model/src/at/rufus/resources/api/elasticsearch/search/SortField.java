package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;

public class SortField implements Serializable {
	private static final long serialVersionUID = 1L;

	public static enum MissingStrategy { First, Last, None }; 
	
	public static enum SortOrder { ASC, DESC };
	
	private String fieldName;
	private String type;
	private String script;
	private SortOrder sortOrder = SortOrder.ASC;
	private MissingStrategy missingStrategy = MissingStrategy.Last;
	
	public SortField() {
	}
	
	public SortField(String fieldName) {
		this(fieldName, SortOrder.ASC);
	}
	
	public SortField(String fieldName, SortOrder sortOrder) {
		this.fieldName = fieldName;
		this.sortOrder = sortOrder;
		this.script = null;
		this.type = null;
	}
	
	public SortField(String script, String type) {
		this(script, type, SortOrder.ASC);
	}
	
	public SortField(String script, String type, SortOrder sortOrder) {
		this.script = script;
		this.type = type;
		this.sortOrder = sortOrder;
		this.fieldName = null;
	}
	
	public String getScript() {
		return script;
	}
	
	public String getType() {
		return type;
	}
	
	public String getFieldName() {
		return fieldName;
	}
	
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	
	public MissingStrategy getMissingStrategy() {
		return missingStrategy;
	}
	
	public void setMissingStrategy(MissingStrategy missingStrategy) {
		this.missingStrategy = missingStrategy;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
}
