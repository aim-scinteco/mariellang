package at.rufus.resources.api.elasticsearch.indexer;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface ListCollector<T, R> {
    Supplier<R> seed();
    
    BiConsumer<T, R> accumulator();
    
    Consumer<R> finisher();
}
