package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResultHighlightField implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String name;
	private final String label;
	private List<String> fragments;
	
	public ResultHighlightField(String name) {
		this.name = name;
		this.label = name;
	}
	
	public ResultHighlightField(String name, String label) {
		this.name = name;
		this.label = label;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setFragments(String[] fragments) {
		this.fragments = Arrays.asList(fragments);
	}

	public String[] getFragments() {
		return fragments().toArray(new String[fragments().size()]);
	}
	
	private List<String> fragments() {
		if (fragments == null) {
			fragments = new ArrayList<String>(5);
		}
		return fragments;
	}
	
	public void addFragment(String fragment) {
		fragments().add(fragment);
	}
	
}
