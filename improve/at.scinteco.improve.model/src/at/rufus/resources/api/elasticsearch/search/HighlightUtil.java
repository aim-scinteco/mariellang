package at.rufus.resources.api.elasticsearch.search;

import java.util.Collection;

public final class HighlightUtil {

	public static class HlRegion {
		
		private int offset;
		private int length;
		
		private HlRegion() {
		}
		
		private HlRegion(int offset, int length) {
			this.offset = offset;
			this.length = length;
		}
		
		public int getOffset() {
			return offset;
		}
		
		public int getLength() {
			return length;
		}
		
	}
	
	public static String decodeHighlightSnippet(String hlSnippet, String hlPrefix, String hlPostifx, Collection<HlRegion> hlRegions) {
		StringBuilder decoded = new StringBuilder();
		HlRegion hlRegion = null;
		String pattern = hlPrefix;
		int idxLast = 0, idxNext, i = 0, patternSize = 0;
		while((idxNext = hlSnippet.indexOf(pattern, idxLast)) != -1) {
			decoded.append(hlSnippet.substring(idxLast, idxNext));
			idxLast = idxNext + pattern.length();
			if(i % 2 == 0) {
				hlRegion = new HlRegion();
				hlRegion.offset = idxNext - patternSize;
				patternSize += pattern.length();
				pattern = hlPostifx;
			} else {
				hlRegion.length = idxNext - hlRegion.offset - patternSize;
				hlRegions.add(hlRegion);
				patternSize += pattern.length();
				pattern = hlPrefix;
			}
			i++;
		}
		decoded.append(hlSnippet.substring(idxLast));
		return decoded.toString();
	}
	
	public static String decodeHighlightSnippet(String hlSnippet, Collection<HlRegion> hlRegions) {
		return decodeHighlightSnippet(hlSnippet, SearchOptions.DEFAULT_HL_PREFIX, SearchOptions.DEFAULT_HL_POSTFIX, hlRegions);
	}
	
}
