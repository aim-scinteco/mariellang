package at.rufus.resources.api.elasticsearch.search;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * @author anageler
 *
 */
public final class SearchUtil {
	
	private static final DateFormat UTC;
	
	static {
		UTC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		UTC.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	
	public static final boolean isEmpty(String s) {
		return s == null || s.length() == 0 || s.trim().length() == 0;
	}
	
	public static final boolean notEmpty(String s) {
		return !isEmpty(s);
	}
	
	public static final boolean isEmpty(String[] ss) {
		if (ss == null || ss.length == 0)
			return true;
		
		boolean empty = true;
		for (int i=0; i<ss.length && empty; i++) {
			if (notEmpty(ss[i]))
				empty = false;
		}
		return empty;
	}
	
	public static final boolean notEmpty(String[] ss) {
		return ! isEmpty(ss);
	}
	
	public static final String getDateUTCFormatted(Date date) {
		return UTC.format(date);
	}
	
	public static final String getDateFromUTCFormatted(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		
		return UTC.format(gc.getTime());
	}
	
	public static final String getDateToUTCFormatted(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		gc.set(Calendar.MILLISECOND, 999);
		
		return UTC.format(gc.getTime());
	}
	
	public static final String getSeparatedString(String[] tokens, String sep) {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i < tokens.length; i++) {
			if (i > 0)
				sb.append(",");
			sb.append(tokens[i]);
		}
		return sb.toString();
	}
	
	public static final String[] toStringArray(Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof String[]) {
			return (String[]) value;
		} else if (value.getClass().isArray()) {
			Object[] array = (Object[]) value;
			String[] val = new String[array.length];
			for(int i = 0; i < array.length; i++) {
				val[i] = array == null ? null : array.toString();
			}
			return val;
		} else if (value instanceof Collection<?>) {
			Collection<?> collection = (Collection<?>) value;
			String[] val = new String[collection.size()];
			int i = 0;
			for(Object element : collection) {
				val[i++] = element == null ? null : element.toString();
			}
			return val;
		}
		return new String[] { value.toString() };
	}
	
}
