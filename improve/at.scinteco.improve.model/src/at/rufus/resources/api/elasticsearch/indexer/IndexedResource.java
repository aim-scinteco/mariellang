package at.rufus.resources.api.elasticsearch.indexer;

public class IndexedResource {
    private final String id;
    private final String type;
    private final String name;
    private final String path;
    private final Boolean contentSkipped;
    
    public IndexedResource(String id, String type, String name, String path, Boolean contentSkipped) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.path = path;
        this.contentSkipped = contentSkipped;
    }
    
    public String getId() {
        return id;
    }
    
    public String getType() {
        return type;
    }
    
    public String getName() {
        return name;
    }
    
    public String getPath() {
        return path;
    }
    
    public Boolean isContentSkipped() {
        return contentSkipped;
    }
}
