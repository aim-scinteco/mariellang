package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchResult implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long totalHits;
	private List<ResultDocument> documents;
	
	public SearchResult() {
		this(0);
	}
	
	public SearchResult(long totalHits) {
		this.totalHits = totalHits;
	}

	public ResultDocument[] getDocuments() {
		return documents().toArray(new ResultDocument[documents().size()]);
	}
	
	protected List<ResultDocument> documents() {
		if (documents == null)
			documents = new ArrayList<ResultDocument>();
		return documents;
	}

	public long getTotalHits() {
		return totalHits;
	}
	
	public void setTotalHits(long totalHits) {
		this.totalHits = totalHits;
	}
	
	public void addDocument(ResultDocument resultDocument) {
		documents().add(resultDocument);
	}
	
	public void setDocuments(ResultDocument[] resultDocuments) {
		documents = Arrays.asList(resultDocuments);
	}

}
