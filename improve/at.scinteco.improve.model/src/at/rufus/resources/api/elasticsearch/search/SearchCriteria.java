package at.rufus.resources.api.elasticsearch.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class SearchCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static class SearchElement implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private final String type;
		private final String[] fields;
		
		public SearchElement(String type, String[] fields) {
			this.type = type;
			this.fields = fields;
		}
		
		public String getType() {
			return type;
		}
		
		public String[] getFields() {
			return fields;
		}
		
		@Override
		public String toString() {
			String text = type;
			if (fields != null && fields.length > 0) {
				text += " (";
				for (int i = 0; i < fields.length; i++) {
					text += fields[i];
					if (i < fields.length - 1) {
						text += ",";
					}
				}
				text += ")";
			}
			return text;
		}
		
	}
	
	private transient String repository;
	
	 // search criteria name (may be null)
	private String name;
	
	// search for documents containing 'phrase'
	private String phrase;
	
	// search for documents that are children of 'path' 
	private String path;
	
	private String createdBy;
	private String lastModifiedBy;
	
	// search for documents that were created between 'createdFrom' and 'createdTo' 
	private Date createdFrom;
	private Date createdTo;
	
	// search for documents that were last modified between 'modifiedFrom' and 'modifiedTo'
	private Date modifiedFrom;
	private Date modifiedTo;
	
	// field name values
	private List<String> fileNames;
	
	// search for documents that are either granted to 'grantedUser' or any of 'grantedGroups' 
	private String grantedUser;
	private List<String> grantedGroups;
	
	// elements to search the phrase
	private final List<SearchElement> searchElements = new LinkedList<SearchElement>();
	
	// metadata to search for (independent from phrase)
	private final List<SearchMetadataElement> metadataElements = new LinkedList<SearchMetadataElement>();

	// criteria extensions per type
	private final List<ISearchCriteriaExt> extensions = new LinkedList<ISearchCriteriaExt>();
	
	public SearchCriteria() {
		this (null);
	}
	
	public SearchCriteria(String phrase) {
		setPhrase(phrase);
	}
	
	public SearchCriteria(String repository, String phrase) {
		setRepository(repository);
		setPhrase(phrase);
	}	
	
	public void setRepository(String repository) {
		this.repository = repository;
	}
	
	public String getRepository() {
		return repository;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPhrase() {
		return phrase;
	}
	
	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public Date getCreatedFrom() {
		return createdFrom;
	}
	
	public void setCreatedFrom(Date createdFrom) {
		this.createdFrom = createdFrom;
	}
	
	public Date getCreatedTo() {
		return createdTo;
	}
	
	public void setCreatedTo(Date createdTo) {
		this.createdTo = createdTo;
	}
	
	public Date getModifiedFrom() {
		return modifiedFrom;
	}
	
	public void setModifiedFrom(Date modifiedFrom) {
		this.modifiedFrom = modifiedFrom;
	}
	
	public Date getModifiedTo() {
		return modifiedTo;
	}
	
	public void setModifiedTo(Date modifiedTo) {
		this.modifiedTo = modifiedTo;
	}
	
	public List<String> getFileNames() {
		if (fileNames == null) {
			fileNames = new ArrayList<String>();
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public String getGrantedUser() {
		return grantedUser;
	}
	
	public void setGrantedUser(String grantedUser) {
		this.grantedUser = grantedUser;
	}

	public String[] getGrantedGroups() {
		return groups().toArray(new String[groups().size()]);
	}
	
	protected List<String> groups() {
		if (grantedGroups == null)
			grantedGroups = new ArrayList<String>();
		return grantedGroups;
	}
	
	public void addGrantedGroup(String grantedGroup) {
		groups().add(grantedGroup);
	}
	
	public void setGrantedGroups(String[] grantedGroups) {
		this.grantedGroups = Arrays.asList(grantedGroups);
	}
	
	public List<SearchElement> getSearchElements() {
		return searchElements;
	}
	
	public List<SearchMetadataElement> getMetadataElements() {
		return metadataElements;
	}
	
	public List<ISearchCriteriaExt> getExtensions() {
		return extensions;
	}
	
	public void adoptCriteria(SearchCriteria criteria) {
		repository = criteria.repository;
		name = criteria.name;
		phrase = criteria.phrase;
		path = criteria.path;
		createdBy = criteria.createdBy;
		lastModifiedBy = criteria.lastModifiedBy;
		createdFrom = criteria.createdFrom;
		createdTo = criteria.createdTo;
		modifiedFrom = criteria.modifiedFrom;
		modifiedTo = criteria.modifiedTo;
		fileNames = criteria.fileNames;
		grantedUser = criteria.grantedUser;
		grantedGroups = criteria.grantedGroups;
		searchElements.clear();
		searchElements.addAll(criteria.getSearchElements());
		metadataElements.clear();
		metadataElements.addAll(criteria.getMetadataElements());
		extensions.clear();
		extensions.addAll(criteria.getExtensions());
	}
	
}
