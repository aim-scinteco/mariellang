package at.rufus.resources.api.elasticsearch.indexer;

public interface IndexField {
    String getName();
    AttributeType getType();
    boolean isStored();
    
    static IndexField attr(String name, AttributeType type) {
        return new DefaultIndexField(name, type, true);
    }
    
    static IndexField attr(String name, AttributeType type, boolean stored) {
        return new DefaultIndexField(name, type, stored);
    }
    
    static class DefaultIndexField implements IndexField {
        private final String name;
        private final AttributeType type;
        private final boolean stored;

        DefaultIndexField(String name, AttributeType type, boolean stored) {
            this.name = name;
            this.type = type;
            this.stored = stored;
        }
        
        @Override
        public String getName() {
            return name;
        }

        @Override
        public AttributeType getType() {
            return type;
        }

        @Override
        public boolean isStored() {
            return stored;
        }
    }
}
