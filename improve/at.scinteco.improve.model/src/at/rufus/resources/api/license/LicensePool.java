package at.rufus.resources.api.license;

import java.io.Serializable;
import java.util.Date;

public class LicensePool implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int numberOfLicenses;
	private int numberOfFreeLicenses;
	private int type;
	private Date validFrom;
	private Date validThrough;

	public int getNumberOfLicenses() {
		return numberOfLicenses;
	}
	
	public void setNumberOfLicenses(int numberOfLicenses) {
		this.numberOfLicenses = numberOfLicenses;
	}
	
	public int getNumberOfFreeLicenses() {
		return numberOfFreeLicenses;
	}
	
	public void setNumberOfFreeLicenses(int numberOfFreeLicenses) {
		this.numberOfFreeLicenses = numberOfFreeLicenses;
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public Date getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	
	public Date getValidThrough() {
		return validThrough;
	}
	
	public void setValidThrough(Date validThrough) {
		this.validThrough = validThrough;
	}

}
