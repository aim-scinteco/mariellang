package at.rufus.resources.api.license;

import java.io.Serializable;
import java.util.Date;

public class LicenseActivationInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int numberOfUserLicensesCreated;
	private Date validFrom;
	private Date validThrough;
	
	public int getNumberOfUserLicensesCreated() {
		return numberOfUserLicensesCreated;
	}
	
	public void setNumberOfUserLicensesCreated(int numberOfUserLicensesCreated) {
		this.numberOfUserLicensesCreated = numberOfUserLicensesCreated;
	}
	
	public Date getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	
	public Date getValidThrough() {
		return validThrough;
	}
	
	public void setValidThrough(Date validThrough) {
		this.validThrough = validThrough;
	}

}
