package at.rufus.resources.api.license;

import java.io.Serializable;
import java.util.Date;

public class RevalidatedLicense implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String repositoryId;
	private ServerLicenseDetail serverLicenseDetail;
	
	private String newTempUserLicenseKey;
	private boolean noLicense;
	private Date licenseBeginsAt;
	private Date licenseExpiredAt;
	private boolean licenseUnassignedFromUser;
	private int incompatibleLicenseType = -1;
	
	/**
	 * The temp date of a foreign license (not managed by the current server) expired.  
	 */
	private Date tempLicenseExpiredAt;
	
	private boolean currentTempLicenseManaged;

	public String getNewTempUserLicenseKey() {
		return newTempUserLicenseKey;
	}
	
	public void setNewTempUserLicenseKey(String newTempUserLicenseKey) {
		this.newTempUserLicenseKey = newTempUserLicenseKey;
	}

	public boolean isNoLicense() {
		return noLicense;
	}
	
	public void setNoLicense(boolean noLicense) {
		this.noLicense = noLicense;
	}
	
	public Date getLicenseBeginsAt() {
		return licenseBeginsAt;
	}
	
	public void setLicenseBeginsAt(Date licenseBeginsAt) {
		this.licenseBeginsAt = licenseBeginsAt;
	}
	
	public void setLicenseExpiredAt(Date licenseExpiredAt) {
		this.licenseExpiredAt = licenseExpiredAt;
	}
	
	public Date getLicenseExpiredAt() {
		return licenseExpiredAt;
	}
	
	public int getIncompatibleLicenseType() {
		return incompatibleLicenseType;
	}
	
	public void setIncompatibleLicenseType(int incompatibleLicenseType) {
		this.incompatibleLicenseType = incompatibleLicenseType;
	}

	public Date getTempLicenseExpiredAt() {
		return tempLicenseExpiredAt;
	}
	
	public void setTempLicenseExpiredAt(Date tempLicenseExpiredAt) {
		this.tempLicenseExpiredAt = tempLicenseExpiredAt;
	}
	
	public boolean isLicenseUnassignedFromUser() {
		return licenseUnassignedFromUser;
	}
	
	public void setLicenseUnassignedFromUser(boolean licenseUnassignedFromUser) {
		this.licenseUnassignedFromUser = licenseUnassignedFromUser;
	}
	
	public ServerLicenseDetail getServerLicenseDetail() {
		return serverLicenseDetail;
	}

	public void setServerLicenseDetail(ServerLicenseDetail serverLicenseDetail) {
		this.serverLicenseDetail = serverLicenseDetail;
	}

	public String getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}
	
	public void setCurrentTempLicenseManaged(boolean currentTempLicenseManaged) {
		this.currentTempLicenseManaged = currentTempLicenseManaged;
	}
	
	public boolean isCurrentTempLicenseManaged() {
		return currentTempLicenseManaged;
	}

}
