package at.rufus.resources.api.license;

import java.io.Serializable;

public class ServerLicenseDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int numberOfNoYetValidUserLicenses;
	private int numberOfValidUserLicenses;
	private int numberOfBoundUserLicenses;
	private int numberOfCurrentFreeLicenses;
	
	public int getNumberOfNoYetValidUserLicenses() {
		return numberOfNoYetValidUserLicenses;
	}
	public void setNumberOfNoYetValidUserLicenses(int numberOfNoYetValidUserLicenses) {
		this.numberOfNoYetValidUserLicenses = numberOfNoYetValidUserLicenses;
	}
	public int getNumberOfValidUserLicenses() {
		return numberOfValidUserLicenses;
	}
	public void setNumberOfValidUserLicenses(int numberOfValidUserLicenses) {
		this.numberOfValidUserLicenses = numberOfValidUserLicenses;
	}
	public int getNumberOfBoundUserLicenses() {
		return numberOfBoundUserLicenses;
	}
	public void setNumberOfBoundUserLicenses(int numberOfBoundUserLicenses) {
		this.numberOfBoundUserLicenses = numberOfBoundUserLicenses;
	}
	public void setNumberOfCurrentFreeLicenses(int numberOfCurrentFreeLicenses) {
		this.numberOfCurrentFreeLicenses = numberOfCurrentFreeLicenses;
	}
	public int getNumberOfCurrentFreeLicenses() {
		return numberOfCurrentFreeLicenses;
	}
	
}
