package at.rufus.resources.api.metadata;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.MetadataStrategy;
import at.rufus.resources.api.model.MetadataType;

public class MetadataElement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private GUID resourceId;
	private GUID descriptorId;
	private String scope;
	private String descriptorName;
	private String descriptorLabel;
	private boolean descriptorInitial;
	private MetadataStrategy descriptorStrategy;
	private MetadataType type;
	private GUID categoryId;
	private String categoryName;
	private GUID metadataId;
	private GUID metadataHistoryId;
	private BigDecimal numValue;
	private String textValue;
	private Timestamp dateValue;
	private GUID lovId;
	private String lovText;
	private boolean deleted;
	private GUID resourceCharacterId;
	private boolean inheritedFromParent;	// the metadata is inherited form a parent resource to the resource queried
	private boolean mandatory;
	
	public GUID getResourceId() {
		return resourceId;
	}
	public void setResourceId(GUID resourceId) {
		this.resourceId = resourceId;
	}
	public GUID getDescriptorId() {
		return descriptorId;
	}
	public void setDescriptorId(GUID descriptorId) {
		this.descriptorId = descriptorId;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getDescriptorName() {
		return descriptorName;
	}
	public void setDescriptorName(String descriptorName) {
		this.descriptorName = descriptorName;
	}
	public String getDescriptorLabel() {
		return descriptorLabel;
	}
	public void setDescriptorLabel(String descriptorLabel) {
		this.descriptorLabel = descriptorLabel;
	}
	public boolean isDescriptorInitial() {
		return descriptorInitial;
	}
	public void setDescriptorInitial(boolean descriptorInitial) {
		this.descriptorInitial = descriptorInitial;
	}
	public MetadataStrategy getDescriptorStrategy() {
		return descriptorStrategy;
	}
	public void setDescriptorStrategy(MetadataStrategy descriptorStrategy) {
		this.descriptorStrategy = descriptorStrategy;
	}
	public MetadataType getType() {
		return type;
	}
	public void setType(MetadataType type) {
		this.type = type;
	}
	public GUID getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(GUID categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public GUID getMetadataId() {
		return metadataId;
	}
	public void setMetadataId(GUID metadataId) {
		this.metadataId = metadataId;
	}
	public GUID getMetadataHistoryId() {
		return metadataHistoryId;
	}
	public void setMetadataHistoryId(GUID metadataHistoryId) {
		this.metadataHistoryId = metadataHistoryId;
	}
	public BigDecimal getNumValue() {
		return numValue;
	}
	public void setNumValue(BigDecimal numValue) {
		this.numValue = numValue;
	}
	public String getTextValue() {
		return textValue;
	}
	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	public Timestamp getDateValue() {
		return dateValue;
	}
	public void setDateValue(Timestamp dateValue) {
		this.dateValue = dateValue;
	}
	public GUID getLovId() {
		return lovId;
	}
	public void setLovId(GUID lovId) {
		this.lovId = lovId;
	}
	public String getLovText() {
		return lovText;
	}
	public void setLovText(String lovText) {
		this.lovText = lovText;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public GUID getResourceCharacterId() {
		return resourceCharacterId;
	}
	public void setResourceCharacterId(GUID resourceCharacterId) {
		this.resourceCharacterId = resourceCharacterId;
	}
	public boolean isInheritedFromParent() {
		return inheritedFromParent;
	}
	public void setInheritedFromParent(boolean inherited) {
		this.inheritedFromParent = inherited;
	}
	public boolean isInheritToChilds() {
		return descriptorStrategy == MetadataStrategy.Inherit;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	public Object getValue(MetadataType type) {
		switch (type) {
		case TEXT: return getTextValue();
		case URL: return getTextValue();
		case NUMBER: return getNumValue();
		case DATE: return getDateValue();
		case LOV: return getLovId();
		}
		return null;
	}
	
	public void setValue(MetadataType type, Object value) {
		switch (type) {
		case TEXT: setTextValue((String) value); break;
		case URL: setTextValue((String) value); break;
		case NUMBER: setNumValue((BigDecimal) value); break;
		case DATE: setDateValue((Timestamp) value); break;
		case LOV: setLovId((GUID) value); break;
		}
	}
	
	public void clearValue() {
		textValue = null;
		numValue = null;
		dateValue = null;
		lovId = null;
		lovText = null;
	}
	
}
