package at.rufus.resources.api.metadata;

import java.io.Serializable;

import at.rufus.base.api.model.GUID;

public class TagElement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private GUID id;
	private String name;
	private GUID resourceId;
	private String scope;
	private boolean deleted;
	
	public GUID getId() {
		return id;
	}
	public void setId(GUID id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public GUID getResourceId() {
		return resourceId;
	}
	public void setResourceId(GUID resourceId) {
		this.resourceId = resourceId;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
