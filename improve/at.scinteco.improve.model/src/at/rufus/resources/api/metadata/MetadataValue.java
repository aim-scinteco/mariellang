package at.rufus.resources.api.metadata;

import java.io.Serializable;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.MetadataType;

public class MetadataValue implements Serializable {
	private static final long serialVersionUID = 1L;

	private GUID historyId;
	private GUID descriptorId;
	private String descriptorName;
	private MetadataType type;
	private Object value;
	
	public GUID getHistoryId() {
		return historyId;
	}
	
	public void setHistoryId(GUID historyId) {
		this.historyId = historyId;
	}
	
	public GUID getDescriptorId() {
		return descriptorId;
	}
	
	public void setDescriptorId(GUID descriptorId) {
		this.descriptorId = descriptorId;
	}
	
	public String getDescriptorName() {
		return descriptorName;
	}
	
	public void setDescriptorName(String descriptorName) {
		this.descriptorName = descriptorName;
	}
	
	public MetadataType getType() {
		return type;
	}
	
	public void setType(MetadataType type) {
		this.type = type;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
}
