package at.rufus.resources.api.report.json;

public interface IResourceReportConstants {

	public static final String PARAM_INCLUDE_COMMENTS = "report.inclComments";
	public static final String PARAM_INCLUDE_METADATA = "report.inclMetadata";
	
}
