package at.rufus.base.api.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.mariella.persistence.database.Converter;
import org.mariella.persistence.database.UUIDConverter;
import org.mariella.persistence.query.Literal;


public class GuidConverter implements Converter<GUID> {
	
	private class GuidLiteral extends Literal<GUID> {

		public GuidLiteral(Converter<GUID> converter, GUID value) {
			super(GuidConverter.this, value);
		}
		
		@Override
		public void printSql(StringBuilder b) {
			uuidConverter.printSql(b, toUUID(value));
		}
	}
	
	private UUIDConverter uuidConverter;
	
	public static final GuidConverter Singleton = new GuidConverter();

	
	private GuidConverter() {
		
	}
	
	public void setUUIDConverter(UUIDConverter uuidConverter) {
		this.uuidConverter = uuidConverter;
	}
	
	@Override
	public String toString(GUID value) {
		return value == null ? "null" : "'" + value.toString() + "'";
	}

	@Override
	public String createParameter() {
		return uuidConverter.createParameter();
	}

	@Override
	public Literal<GUID> createLiteral(Object value) {
		return new GuidLiteral(this, (GUID) value);
	}
	
	@Override
	public Literal<GUID> createDummy() {
		return new GuidLiteral(this, new GUID());
	}

	@Override
	public void setObject(PreparedStatement ps, int index, int type, GUID value) throws SQLException {
		uuidConverter.setObject(ps, index, toUUID(value));
	}

	public void setObject(PreparedStatement ps, int index,GUID value) throws SQLException {
		uuidConverter.setObject(ps, index, toUUID(value));
	}

	@Override
	public GUID getObject(ResultSet rs, int index) throws SQLException {
		UUID uuid = uuidConverter.getObject(rs, index);
		return uuid != null ? new GUID(uuid) : null;
	}

	public GUID getObject(ResultSet rs, String columnName) throws SQLException {
		UUID uuid = uuidConverter.getObject(rs, columnName);
		return uuid != null ? new GUID(uuid) : null;
	}
	
	private static UUID toUUID(GUID guid) {
		return guid != null ? guid.getUUID() : null;
	}
}
