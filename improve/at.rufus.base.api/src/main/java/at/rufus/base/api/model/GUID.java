package at.rufus.base.api.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;


public class GUID implements Comparable<GUID>, Serializable, Externalizable {
	private static final long serialVersionUID = 4020773490308101874L;
	private UUID uuid;

	public static GUID of(UUID uuid) {
		return new GUID(uuid);
	}
	
	public static GUID fromHexString(String hex) {
		if (hex.length() == 32) {
			hex = hex.toLowerCase();
			StringBuilder sb = new StringBuilder()
				.append(hex, 0, 8)
				.append('-')
				.append(hex, 8, 12)
				.append('-')
				.append(hex, 12, 16)
				.append('-')
				.append(hex, 16, 20)
				.append('-')
				.append(hex, 20, 32);
			return new GUID(UUID.fromString(sb.toString()));
		} else if (hex.length() == 36) {
			return new GUID(UUID.fromString(hex.toLowerCase()));
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public GUID(UUID uuid) {
		this.uuid = uuid;
	}
	
	public GUID() {
		uuid = UUID.randomUUID();
	}
	
	public GUID(String name) {
		uuid = UUID.nameUUIDFromBytes(name.getBytes());
	}
	
	public GUID(byte[] bytes) {
		if (bytes == null || bytes.length != 16) {
			throw new IllegalArgumentException();
		}
		long high = 0;
		for (int i = 0; i < 8; i++) {
			high = high * 256 + ((long) bytes[i] & 0xffL);
		}
		long low = 0;
		for (int i = 8; i < 16; i++) {
			low = low * 256 + ((long) bytes[i] & 0xffL);
		}
		uuid = new UUID(high, low);
	}

	public boolean equals(Object obj) {
		if (obj instanceof GUID) {
			GUID guid = (GUID) obj;
			return uuid.equals(guid.uuid);
		}
		return false;
	}

	public int hashCode() {
		return uuid.hashCode();
	}

	public String toString() {
		return uuid.toString().replace("-", "").toUpperCase();
	}
	
	public UUID getUUID() {
		return uuid;
	}
	
	public byte[] toBytes() {
		return toBytes(uuid);
	}
	
	public static byte[] toBytes(UUID uuid) {
		if (uuid == null) return null;
		byte[] result = new byte[16];
		long high = uuid.getMostSignificantBits();
		for (int i = 7; i >= 0; i--) {
			result[i] = (byte) (high & 0xffL);
			high >>= 8;
		}
		long low = uuid.getLeastSignificantBits();
		for (int i = 15; i >= 8; i--) {
			result[i] = (byte) (low & 0xffL);
			low >>= 8;
		}
		return result;
	}
	
	public static List<String> toStringIds(Collection<GUID> ids) {
		List<String> sids = new ArrayList<String>(ids.size());
		for (GUID id : ids) {
			sids.add(id.toString());
		}
		return sids;
	}

	@Override
	public int compareTo(GUID guid) {
		return getUUID().compareTo(guid.getUUID());
	}
	
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		String hex = ((String)in.readObject()).toLowerCase();
		StringBuilder sb = new StringBuilder()
				.append(hex, 0, 8)
				.append('-')
				.append(hex, 8, 12)
				.append('-')
				.append(hex, 12, 16)
				.append('-')
				.append(hex, 16, 20)
				.append('-')
				.append(hex, 20, 32);
		this.uuid = UUID.fromString(sb.toString());
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(toString());
	}
}
