package at.rufus.base.api.common;

import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class StringUtil {
    
	private StringUtil() {

	}
	
	public static String toString(String s) {
		return s == null ? "" : s;
	}
	
	public static String toSingleLine(String s) {
		return s == null ? "" : s.replace("\r", "").replace("\n", " ");
	}
	
	public static String toString(Collection<String> elements, String delimiter) {
		StringBuilder sb = new StringBuilder();
		for (String s : elements) {
			if (s != null) {
				if (sb.length() > 0) {
					sb.append(delimiter);
				}
				sb.append(s);
			}
		}
		return sb.toString();
	}
	
	public static String toString(String[] array, String delimiter) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			if (i >= 1) {
				sb.append(delimiter);
			}
			if (array[i] != null) {
				sb.append(array[i]);
			}
		}
		return sb.toString();
	}
	
	public static String toQuotedString(Collection<String> strings, int maxElements) {
		return toQuotedString(strings, "'", maxElements);
	}
	
	public static String toQuotedString(Collection<String> strings, String quote, int maxElements) {
		int i = 0;
		StringBuilder sb = new StringBuilder();
		for (String s : strings) {
			if (i == maxElements) {
				break;
			}
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(quote);
			sb.append(s);
			sb.append(quote);
			i++;
		}
		if (strings.size() > maxElements) {
			sb.append(", ...");
		}
		return sb.toString();
	}
	
	public static String[] split(String s, boolean trimElements, boolean returnEmptyElements, String... delimiters) {
		List<String> elements = new ArrayList<String>();
		int start = 0;
		int i = 0;
		while (i < s.length()) {
			for (String delimiter : delimiters) {
				if (i+delimiter.length() <= s.length() && s.regionMatches(i, delimiter, 0, delimiter.length())) {
					String element = s.substring(start, i);
					if (trimElements) {
						element = element.trim();
					}
					if (returnEmptyElements || !element.isEmpty()) {
						elements.add(element);
					}
					start = i+delimiter.length();
					i = start-1;
					break;
				}
			}
			i++;
		}
		if (start < s.length()) {
			String element = s.substring(start);
			if (trimElements) {
				element = element.trim();
			}
			if (returnEmptyElements || !element.isEmpty()) {
				elements.add(element);
			}
		}
		return elements.toArray(new String[elements.size()]);
	}
	
	public static String[] merge(String[] array1, String[] array2) {
		if (array1.length == 0) {
			return array2;
		} else if (array2.length == 0) {
			return array1;
		} else {
			String[] array = new String[array1.length+array2.length];
			System.arraycopy(array1, 0, array, 0, array1.length);
			System.arraycopy(array2, 0, array, array1.length, array2.length);
			return array;
		}
	}
	
	public static boolean equals(String s1, String s2) {
		return s1 == s2 || (s1 != null && s1.equals(s2));
	}
	
	public static String removeControlChars(String s) {
		if (s == null) {
			return null;
		}
		StringBuffer sb = null;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (Character.isISOControl(c)) {
				if (sb == null) {
					sb = new StringBuffer(s.length());
					sb.append(s.substring(0, i));
				}
			} else {
				if (sb != null) {
					sb.append(c);
				}
			}
		}
		return sb == null ? s : sb.toString();
	}
	
	public static boolean isNullOrBlank(String s) {
		if (s == null) {
			return true;
		}
		StringCharacterIterator it = new StringCharacterIterator(s);
		for (char ch = it.first(); ch != StringCharacterIterator.DONE; ch = it.next()) {
			if (!Character.isWhitespace(ch)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNullOrEmpty(String s) {
		return s == null || s.isEmpty();
	}
	
	public static String trimToSize(String s, int size) {
		if (s == null) {
			return null;
		} else if (s.length() <= size) {
			return s;
		} else {
			return s.substring(0, size);
		}
	}
	
	public static boolean isWeakPassword(String password) {
		if (password.length() < 8) { 
			return true;
		}
		boolean hasLetters = false;
		boolean hasDigits = false;
		for (int i = 0; i < password.length(); i++) {
			if (!hasLetters && Character.isAlphabetic(password.charAt(i))) {
				hasLetters = true;
			}
			if (!hasDigits && Character.isDigit(password.charAt(i))) {
				hasDigits = true;
			}
			if (hasDigits && hasLetters) {
				return false;
			}
		}
		return true;
	}
	
	public static int toInt(String s, int defaultValue) {
		if (s == null || s.isEmpty()) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException ex) {
			return defaultValue;
		}
	}

	public static long toLong(String s, long defaultValue) {
		if (s == null || s.isEmpty()) {
			return defaultValue;
		}
		try {
			return Long.parseLong(s);
		} catch (NumberFormatException ex) {
			return defaultValue;
		}
	}
	
	public static String toCamelCase(String... strings) {
		if (strings == null || strings.length == 0) {
			return null;
		} else if (strings.length == 1) {
			return strings[0];
		} else {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < strings.length; i++) {
				if (strings[i] != null && !strings[i].isEmpty()) {
					if (sb.length() == 0) {
						sb.append(strings[i].toLowerCase());
					} else {
						sb.append(Character.toUpperCase(strings[i].charAt(0)));
						sb.append(strings[i].substring(1).toLowerCase());
					}
				}
			}
			return sb.toString();
		}
	}
	
	public static boolean endsWithIgnoreCase(String text, String end) {
		if (text == null || end == null) {
			throw new NullPointerException();
		} else if (end.length() > text.length()) {
			return false;
		}
		return text.regionMatches(true, text.length()-end.length(), end, 0, end.length());
	}
	
	private final static char[] hexCode = "0123456789ABCDEF".toCharArray();
	public static String toHexString(byte[] data) {
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
	}
	
	public static String replaceChars(String text, char[] charsToReplace, char replacementChar) {
		StringBuilder sb = new StringBuilder(text);
		for (int i = 0; i < sb.length(); i++) {
			for (int j = 0; j < charsToReplace.length; j++) {
				if (text.charAt(i) == charsToReplace[j]) {
					sb.setCharAt(i, replacementChar);
					break;
				}
			}
		}
		return sb.toString();
	}


	public static String escapeControlCharsInFilename(String fileName) {
		if (fileName.contains("^")) {
			fileName = fileName.replace("^", "^^");
		}
		if (fileName.contains("(") && !fileName.contains(" (")) {
			fileName = fileName.replace("(", "^(");
		} 
		if (fileName.contains("&")) {
			fileName = fileName.replace("&", "^&");
		}
		if (fileName.contains("=") || fileName.contains(";")) {
			fileName = "\"" + fileName;
		}
		return fileName;
	}
}
