package at.rufus.base.api.model;

public class PredecessorVersionsNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PredecessorVersionsNotFoundException() {
	}

	public PredecessorVersionsNotFoundException(String message) {
		super(message);
	}

	public PredecessorVersionsNotFoundException(Throwable cause) {
		super(cause);
	}

	public PredecessorVersionsNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public PredecessorVersionsNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
