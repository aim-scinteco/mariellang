package at.rufus.base.api.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class Entity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String ID = "id";

	private GUID id;
	
	@Transient
	protected final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	public Entity() {
		this(new GUID());
	}
	
	public Entity(GUID id) {
		this.id = id;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	@Id
	@Column(name="ID")
//	@GeneratedValue(strategy=GenerationType.AUTO)
	public GUID getId() {
		return id;
	}
	
	public void setId(GUID id) {
		propertyChangeSupport.firePropertyChange(ID, this.id, this.id = id);
	}
	
	public boolean equalsId(Entity entity) {
		return (id != null) && ((this == entity) || (entity != null && id.equals(entity.id)));
	}
	
	public static boolean equalsId(Entity e1, Entity e2) {
		if (e1 != null) {
			return e1.equalsId(e2);
		} else {
			return e2 == null;
		}
	}
	
	public static <T extends Entity> T findEntity(Collection<T> entities, GUID id) {
		for(T entity : entities) {
			if(id.equals(entity.getId())) {
				return entity;
			}
		}
		return null;
	}
	
	public static List<GUID> toIds(Collection<? extends Entity> entities) {
		List<GUID> ids = new ArrayList<GUID>(entities.size());
		for (Entity entity : entities) {
			ids.add(entity.getId());
		}
		return ids;
	}
	
	public static <T extends Entity> Map<GUID, T> toIdMap(Collection<T> entities) {
		Map<GUID, T> map = new HashMap<GUID, T>();
		for (T entity : entities) {
			map.put(entity.getId(), entity);
		}
		return map;
	}
	
	@Transient
	public String getDiscriminator() {
		return null;
	}
}
