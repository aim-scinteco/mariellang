package at.rufus.base.api.model;

public interface BiostatConstants {
	String APPLICATION_NAME = "Biostat";
	
	//preferences
	public static final String PREFERENCES_NODE_REPOSITORY = "repositoryNode";
	public static final String PREFERENCES_CURRENT_REPOSITORY = "currentRepository";
	
	public static final String PREFERENCES_NODE_PROJECT_CONTROLLER = "projectNode";
	public static final String PREFERENCES_CURRENT_PROJECT = "currentProjectId.";
	
	public static final String PREFERENCES_NODE_FILTER = "filterNode";
	public static final String PREFERENCES_CURRENT_PROJECTFILTER = "currentProjectFilterId.";
	public static final String PREFERENCES_CURRENT_LIBRARYFILTER = "currentLibraryFilterId.";
	public static final String PREFERENCES_CURRENT_FAVORITESFILTER = "currentFavoritesFilterId.";
	public static final String PREFERENCES_CURRENT_RESOURCESFILTER = "currentResourcesFilterId.";
	
	//users and groups
    public static final String SWB_ADMIN_GROUP = "Biostat Administrators";
    public static  final String GLOBAL_PROJECT_GROUP_NAME = "Biostat";
    public static  final String PROJECT_GROUP_PREFIX = "";
    public static  final String OWNER_GROUP_PREFIX = "OWN_";
    public static  final String COLLABORATOR_GROUP_PREFIX = "COL_";	
    public static  final String READONLY_GROUP_PREFIX = "RDO_";	
    
    //filter, query
    public final String FILTER_EXTENSION = ".biostatfilter";
    public final String QUERY_EXTENSION = ".biostatquery";
    
    public final String METADATA_IMPROVE_CLIENT_SCOPE = "Improve Client";
    
    public final GUID SWB_FOLDER_ID = GUID.fromHexString("719C3EDAF30A4E5290573A6AB4145F7F");
    public final GUID PROJECTS_FOLDER_ID = GUID.fromHexString("6DC6F49AD9104E0995662D113B5004C9");
    public final GUID LIBRARIES_FOLDER_ID = GUID.fromHexString("DFA4807F027B4A859125D5672C0BF52E");
    public final GUID QUERIES_FOLDER_ID = GUID.fromHexString("EF48DB23BD3C467BA832C56F8250130D");
    public final GUID PROJECTFILTERS_FOLDER_ID = GUID.fromHexString("BA27130C75C14B22A6C183FB65893996");
    public final GUID LIBRARYFILTERS_FOLDER_ID = GUID.fromHexString("B5E31A4EA045478E873F053D0E7E15A4");
    public final GUID FAVORITES_FOLDER_ID = GUID.fromHexString("262FA08147BC40CC944D70D4629264DC");
    public final GUID FAVORITESFILTERS_FOLDER_ID = GUID.fromHexString("CA5E5B1E90D94F17B40921F4A1E0D19A");
    public final GUID RESOURCESFILTERS_FOLDER_ID = GUID.fromHexString("59AC0037E097474599949D7CDD6899D4");
    
    public final String METADATA_SYSTEM_SCOPE = "System";
    public final String METADATA_PUBLISHED = "Published";
    
    public final GUID METADATA_PROJECTSTATE_CATEGORY_ID = GUID.fromHexString("A4E098D2058E4B02ADD65AB6E39C1F9A");
    public final GUID METADATA_PROJECTSTATE_DESCR_ID = GUID.fromHexString("B99C78879AE148218AF40A771DDCF900"); 
    public final String METADATA_PROJECTSTATE_DESCR_NAME = "ProjectState";    
    public final GUID METADATA_PROJECTSTATE_OPEN_ID = GUID.fromHexString("5BDBCC91987046918FFF1C944647268F");
    public final GUID METADATA_PROJECTSTATE_COMPLETE_ID = GUID.fromHexString("D2A0F702348F4AE99D8B30B9307412AE");
    public final GUID METADATA_PROJECTSTATE_ABORT_ID = GUID.fromHexString("7499A7B1E788472F830596B3AF823566");
    
    public final GUID METADATA_TASKTAG_CATEGORY_ID = GUID.fromHexString("6B8125E9C45A498090ACD3998388184C");
    public final GUID METADATA_TASKTAG_DESCR_ID = GUID.fromHexString("4C53783203484912881BD30D964F7D73"); 
    public final String METADATA_TASKTAG_DESCR_NAME = "TaskTag";    
    public final GUID METADATA_TASKTAG_DATAACQ_ID = GUID.fromHexString("03BC9D2116304EFAB761B04B4DB3E039");
    public final GUID METADATA_TASKTAG_DATAMAN_ID = GUID.fromHexString("C788B3CC1A824B43AABF76C37F46F9FF");
    public final GUID METADATA_TASKTAG_ANALYSIS_ID = GUID.fromHexString("2040A44925294F5D87AE084998995CC1");
    public final GUID METADATA_TASKTAG_REPORT_ID = GUID.fromHexString("74BA0B9E39164D3EB43BAE83A3FDCB18");
   
    public final GUID METADATA_PROJECTTYPE_ID = GUID.fromHexString("F4402B324F2340FFBD3937B23410525C");
    public final String METADATA_PROJECTTYPE_NAME = "ProjectType";
    
    public final GUID METADATA_PROJECT_DESCRIPTION_DESCR_ID = GUID.fromHexString("8AEB4BE87CEC4CFF8AD805599C3EE0D3"); 
    public final GUID METADATA_PROJECT_TAGS_DESCR_ID = GUID.fromHexString("A221149B4528431B9B9576A65A1EB354");
    
    public final GUID GROUP_HIDE_PUBLIC_PROJ = GUID.fromHexString("2CC0AEC82482317A84C467899B434EF9");
    public final GUID GROUP_NO_PROJ_CREATION = GUID.fromHexString("871BBE3789AB391AA924F17FB18AFE1E");
    public final GUID GROUP_NO_PROJ_OWNERSHIP = GUID.fromHexString("D3A10EB6B5E33C5087BB6CE7696C1837");
    public final GUID GROUP_NO_PROJ_COLLABORATION = GUID.fromHexString("06C08B65E66D355ABCCD6C85FAD27DAA");
    public final GUID GROUP_SHOW_PRIV_HIDDEN_PROJ = GUID.fromHexString("85CC5C4EAABD3FFABF15712C6B1C6A2F");
    public final GUID GROUP_CHANGE_RIGHTS_ON_ALL_PROJ = GUID.fromHexString("FEC1B24E30E1387D873B76BF1DA1D0C8");
    
    
    //default workflow name in sandbox project
    public final String DEFAULT_WORKFLOW = "Workflow";
    
    public final String RESOURCE_CHARACTER_PROJECT = "Project";
    
    public static int MAXLEN_METADATATEXT = 65000;
   
}
