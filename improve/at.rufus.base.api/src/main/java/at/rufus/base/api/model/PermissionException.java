package at.rufus.base.api.model;

public class PermissionException extends ModelException {
	private static final long serialVersionUID = 1L;

	public PermissionException(String message) {
		super(message);
	}
	
}
