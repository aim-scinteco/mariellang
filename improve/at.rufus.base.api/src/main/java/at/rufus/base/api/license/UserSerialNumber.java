package at.rufus.base.api.license;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class UserSerialNumber {

	private final static int BYTE_SIZE = 16;
	private final static int KEY = 3982655;
	
	private int snVersion = 1;
	private int licenseNr;
	private int licenseType;
	private int licenseUserNr;
	private int userIdHash;
	private int clientIdHash;
	private int validFromYear;
	private int validFromMonth;
	private int validFromDay;
	private int validForMonths;
	private int validForDays;
	private int tempValidForMonths;
	private int tempValidForDays;
	
	@Override
	public String toString() {
		SerialNumber sn = new SerialNumber(BYTE_SIZE, KEY);
		SerialNumber.Writer writer = sn.writer();
		writer.write(snVersion, 4);
		writer.write(licenseNr, 20);
		writer.write(licenseType, 5);
		writer.write(0x6B, 8);
		writer.write(licenseUserNr, 10);
		writer.write(userIdHash, 20);
		writer.write(clientIdHash, 12);
		writer.write(validFromYear-2000, 10);
		writer.write(validFromMonth, 4);
		writer.write(validFromDay, 5);
		writer.write(0x59, 8);
		writer.write(validForMonths, 6);
		writer.write(validForDays, 5);
		writer.write(tempValidForMonths, 6);
		writer.write(tempValidForDays, 5);
		return sn.toString();
	}
	
	public boolean set(String serialNumber) {
		SerialNumber sn = new SerialNumber(BYTE_SIZE, KEY);
		sn.set(serialNumber);
		SerialNumber.Reader reader = sn.reader();
		try {
			snVersion = reader.readInt(4);
			licenseNr = reader.readInt(20);
			licenseType = reader.readInt(5);
			int check1 = reader.readInt(8);
			licenseUserNr = reader.readInt(10);
			userIdHash = reader.readInt(20);
			clientIdHash = reader.readInt(12);
			validFromYear = 2000+reader.readInt(10);
			validFromMonth = reader.readInt(4);
			validFromDay = reader.readInt(5);
			int check2 = reader.readInt(8);
			validForMonths = reader.readInt(6);
			validForDays = reader.readInt(5);
			tempValidForMonths = reader.readInt(6);
			tempValidForDays = reader.readInt(5);
			return check1 == 0x6B && check2 == 0x59;
		} catch (Throwable t) {
			return false;
		}
	}

	public int getSnVersion() {
		return snVersion;
	}

	public int getLicenseNr() {
		return licenseNr;
	}

	public void setLicenseNr(int licenseNr) {
		this.licenseNr = licenseNr;
	}

	public int getLicenseType() {
		return licenseType;
	}
	
	public void setLicenseType(int licenseType) {
		this.licenseType = licenseType;
	}
	
	public int getLicenseUserNr() {
		return licenseUserNr;
	}
	
	public void setLicenseUserNr(int licenseUserNr) {
		this.licenseUserNr = licenseUserNr;
	}
	
	public int getUserIdHash() {
		return userIdHash;
	}
	
	public void setUserIdHash(int userIdHash) {
		this.userIdHash = userIdHash;
	}
	
	public int getClientIdHash() {
		return clientIdHash;
	}
	
	public void setClientIdHash(int clientIdHash) {
		this.clientIdHash = clientIdHash;
	}
	
	public int getValidFromYear() {
		return validFromYear;
	}

	public void setValidFromYear(int validFromYear) {
		this.validFromYear = validFromYear;
	}

	public int getValidFromMonth() {
		return validFromMonth;
	}

	public void setValidFromMonth(int validFromMonth) {
		this.validFromMonth = validFromMonth;
	}

	public int getValidFromDay() {
		return validFromDay;
	}

	public void setValidFromDay(int validFromDay) {
		this.validFromDay = validFromDay;
	}

	public int getValidForMonths() {
		return validForMonths;
	}
	
	public void setValidForMonths(int validForMonths) {
		this.validForMonths = validForMonths;
	}
	
	public int getValidForDays() {
		return validForDays;
	}
	
	public void setValidForDays(int validForDays) {
		this.validForDays = validForDays;
	}

	public int getTempValidForMonths() {
		return tempValidForMonths;
	}
	
	public void setTempValidForMonths(int tempValidForMonths) {
		this.tempValidForMonths = tempValidForMonths;
	}
	
	public int getTempValidForDays() {
		return tempValidForDays;
	}
	
	public void setTempValidForDays(int tempValidForDays) {
		this.tempValidForDays = tempValidForDays;
	}
	
	public Calendar getValidFrom() {
		Calendar calendar = new GregorianCalendar(validFromYear, validFromMonth-1, validFromDay);
		return calendar;
	}

	public Calendar getValidThrough() {
		Calendar calendar = new GregorianCalendar(validFromYear, validFromMonth-1, validFromDay);
		calendar.add(Calendar.MONTH, validForMonths);
		calendar.add(Calendar.DATE, validForDays);
		return calendar;
	}

	public Calendar getExpirationDate() {
		Calendar calendar = new GregorianCalendar(validFromYear, validFromMonth-1, validFromDay);
		calendar.add(Calendar.MONTH, validForMonths);
		calendar.add(Calendar.DATE, validForDays+1);
		return calendar;
	}

	public Calendar getTempValidThorugh() {
		Calendar calendar = new GregorianCalendar(validFromYear, validFromMonth-1, validFromDay);
		calendar.add(Calendar.MONTH, tempValidForMonths);
		calendar.add(Calendar.DATE, tempValidForDays);
		return calendar;
	}

	public Calendar getTempExpirationDate() {
		Calendar calendar = new GregorianCalendar(validFromYear, validFromMonth-1, validFromDay);
		calendar.add(Calendar.MONTH, tempValidForMonths);
		calendar.add(Calendar.DATE, tempValidForDays+1);
		return calendar;
	}
	
	public boolean updateTempTimespan() {
		Calendar validThrough = getValidThrough();
		Calendar tempValidThrough = Calendar.getInstance();
		tempValidThrough = new GregorianCalendar(tempValidThrough.get(Calendar.YEAR), tempValidThrough.get(Calendar.MONTH), tempValidThrough.get(Calendar.DAY_OF_MONTH));
		if (tempValidThrough.after(validThrough)) {
			tempValidForMonths = 0;
			tempValidForDays = 0;
			return false;
		}
		tempValidThrough.add(Calendar.DATE, 30);
		if (tempValidThrough.after(validThrough)) {
			tempValidThrough = validThrough;
		}
		Calendar validFrom = getValidFrom();
		
		int tempValidYears = tempValidThrough.get(Calendar.YEAR)-validFrom.get(Calendar.YEAR);
		this.tempValidForMonths = tempValidThrough.get(Calendar.MONTH)-validFrom.get(Calendar.MONTH)+tempValidYears*12;
		tempValidThrough.add(Calendar.MONTH, -this.tempValidForMonths);

		long dTemp = tempValidThrough.getTimeInMillis()/(1000*60*60*24);
		long dFrom = validFrom.getTimeInMillis()/(1000*60*60*24);
		int tempValidDays = (int) (dTemp-dFrom);
		
		if (tempValidDays < 0 && this.tempValidForMonths > 0) {
			this.tempValidForMonths--;
			tempValidThrough.add(Calendar.MONTH, 1);
			dTemp = tempValidThrough.getTimeInMillis()/(1000*60*60*24);
			tempValidDays = (int) (dTemp-dFrom);
		}
		
		this.tempValidForDays = tempValidDays;
		return true;
	}

	public void fillIds(String username, String clientId) {
		setUserIdHash(hashUserId(username));
		setClientIdHash(hashClientId(clientId.toUpperCase()));
	}
	
	public void clearTempLicenseFields() {
		setUserIdHash(0);
		setClientIdHash(0);
		setTempValidForDays(0);
		setTempValidForMonths(0);
	}	

	public boolean matchesUsername(String username) {
		return userIdHash == hashUserId(username);
	}
	
	public boolean matchesClientId(String clientId) {
		return clientIdHash == hashClientId(clientId.toUpperCase());
	}
	
	public static int hashUserId(String userId) {
		int hash = 113*(userId.length()+1);
		for (int i = 0; i < userId.length(); i++) {
			hash = (hash+3)*(userId.charAt(i)+7);
		}
		return hash & 0xFFFFF;
	}
	
	public static int hashClientId(String clientId) {
		int hash = 87*(clientId.length()+3);
		for (int i = 0; i < clientId.length(); i++) {
			hash = (hash+11)*(clientId.charAt(i)+5);
		}
		return hash & 0xFFF;
	}
	
//	public static void main(String[] args) {
//		UserSerialNumber sn = new UserSerialNumber();
//		sn.set("DYZUTZG3UXT1D6JLQDWTS39BX");
//		sn.setTempValidForDays(3);
//		sn.setTempValidForMonths(0);
//		System.out.println(sn.toString());
//	}

}
