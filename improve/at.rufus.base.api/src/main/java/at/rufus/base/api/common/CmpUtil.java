package at.rufus.base.api.common;

import java.math.BigDecimal;

public final class CmpUtil {

	private CmpUtil() {
	}

	public static int cmp(int v1, int v2) {
		return v1 < v2 ? -1 : (v1 == v2 ? 0 : 1);
	}
	
	public static int cmp(long v1, long v2) {
		return v1 < v2 ? -1 : (v1 == v2 ? 0 : 1);
	}
	
	public static int cmp(double v1, double v2) {
		return v1 < v2 ? -1 : (v1 == v2 ? 0 : 1);
	}

	public static <T> int cmp(Comparable<T> v1, T v2) {
		if (v1 == v2) {
			return 0;
		} else if (v1 == null) {
			return 1;
		} else if (v2 == null) {
			return -1;
		} else {
			return v1.compareTo(v2);
		}
	}

	public static <T> int cmpNumberOrSpecial(BigDecimal value1, String special1, BigDecimal value2, String special2) {
		if (value1 == value2 && special1 == special2) {
			return 0;
		} else if (special1 != null && special2 != null) {
			return special1.compareTo(special2);
		} else if (special1 != null) {
			return -1;
		} else if (special2 != null) {
			return 1;
		} else {
			return cmp(value1, value2);
		}
	}

	public static int cmpNumericString(String v1, String v2) {
		if (v1 == v2) {
			return 0;
		} else if (v1 == null) {
			return 1;
		} else if (v2 == null) {
			return -1;
		} else {
			int c = cmp(v1.length(), v2.length());
			if (c == 0) {
				c = v1.compareTo(v2);
			}
			return c;
		}
	}

	private static boolean isDigit(char c) {
		return c >= '0' && c <= '9';
	}
	
	public static int cmpNatural(String s1, String s2, boolean ignoreCase) {
		if (s1 == s2) {
			return 0;
		} else if (s1 == null) {
			return 1;
		} else if (s2 == null) {
			return -1;
		}
		int i1 = 0;
		int i2 = 0;
		while (i1 < s1.length() && i2 < s2.length()) {
			char c1 = s1.charAt(i1);
			char c2 = s2.charAt(i2);
			boolean d1 = isDigit(c1);
			boolean d2 = isDigit(c2);
			if (d1 && d2) {
				// compare nummeric parts
				long n1 = 0;
				long n2 = 0;
				while (d1 || d2) {
					if (d1) {
						n1 = n1*10+(c1-'0');
						i1++;
						if (i1 < s1.length()) {
							c1 = s1.charAt(i1);
							d1 = isDigit(c1);
						} else {
							d1 = false;
						}
					}
					if (d2) {
						n2 = n2*10+(c2-'0');
						i2++;
						if (i2 < s2.length()) {
							c2 = s2.charAt(i2);
							d2 = isDigit(c2);
						} else {
							d2 = false;
						}
					}
				}
				if (n1 != n2) {
					return cmp(n1, n2);
				}
			} else {
				// compare character
				if (ignoreCase) {
					c1 = Character.toLowerCase(c1);
					c2 = Character.toLowerCase(c2);
				}
				if (c1 != c2) {
					return cmp(c1, c2);
				}
				i1++;
				i2++;
			}
		}
		return cmp(s1.length(), s2.length());
	}
	
	public static int cmpNull(Object o1, Object o2) {
		if(o1 == null && o2 == null) {
			return 0;
		} else if (o1 == null) {
			return -1;
		} else if (o2 == null) {
			return 1;
		} else {
			return 2;
		}
	}	
	
}
















