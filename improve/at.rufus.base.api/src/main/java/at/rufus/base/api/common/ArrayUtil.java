package at.rufus.base.api.common;

import java.lang.reflect.Array;

public final class ArrayUtil {

	public static final Object[] EMPTY_ARRAY = new Object[0];
	
	private ArrayUtil() {
	}
	
	public static boolean contains(Object[] array, Object element) {
		for (Object e : array) {
			if (e == element || (e != null && e.equals(element))) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean contains(char[] array, char element) {
		for (char e : array) {
			if (e == element) {
				return true;
			}
		}
		return false;
	}

	public static boolean contains(int[] array, int element) {
		for (int e : array) {
			if (e == element) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isEmpty(Object[] array) {
		return array == null || array.length == 0;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] merge(T[]... arrays) {
		int size = 0;
		for (T[] array : arrays) {
			size += array.length;
		}
		Object result = Array.newInstance(arrays[0].getClass().getComponentType(), size);
		int pos = 0;
		for (T[] array : arrays) {
			System.arraycopy(array, 0, result, pos, array.length);
			pos += array.length;
		}
		return (T[]) result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] append(T[] array, T element) {
		T[] result = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length + 1);
		System.arraycopy(array, 0, result, 0, array.length);
		result[result.length - 1] = element;
		return result;
	}
}
