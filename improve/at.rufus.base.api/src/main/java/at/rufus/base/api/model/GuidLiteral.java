package at.rufus.base.api.model;

import org.mariella.persistence.database.Converter;
import org.mariella.persistence.query.Literal;


public class GuidLiteral extends Literal<GUID>{

	
	public GuidLiteral(Converter<GUID> converter, GUID value) {
		super(converter, value);
	}

	@Override
	public void printSql(StringBuilder b) {
		b.append("guidlit("+converter.toString(value)+")");
	}
}
