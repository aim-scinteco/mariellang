package at.rufus.base.api.license;

import java.math.BigInteger;

public class SerialNumber {
	
	public static class Writer {
		private final byte[] sn;
		private int bitPos = 0;
		private Writer(byte[] sn) {
			this.sn = sn;
		}
		private void writeBit(int bit) {
			if (bitPos >= sn.length*8) {
				throw new ArrayIndexOutOfBoundsException(bitPos);
			}
			int i = bitPos/8;
			sn[i] |= (byte) (bit << (7 - bitPos % 8));
			bitPos++;
		}
		public void write(long value, int bitCount) {
			while (bitCount > 0) {
				writeBit((int) ((value >> (bitCount-1)) & 0x1));
				bitCount--;
			}
		}
	}
	
	public static class Reader {
		private final byte[] sn;
		private int bitPos = 0;
		private Reader(byte[] sn) {
			this.sn = sn;
		}
		private int readBit() {
			if (bitPos >= sn.length*8) {
				throw new ArrayIndexOutOfBoundsException(bitPos);
			}
			int i = bitPos/8;
			int bit = (sn[i] >> (7 - bitPos % 8)) & 0x1;
			bitPos++;
			return bit;
		}
		public long readLong(int bitCount) {
			long value = 0;
			while (bitCount > 0) {
				long bit = readBit();
				value |= bit << (bitCount-1);
				bitCount--;
			}
			return value;
		}
		public int readInt(int bitCount) {
			return (int) readLong(bitCount);
		}
	}
	
	
	private final byte[] sn;
	private final int key;
	
	public SerialNumber(int sizeInBytes, int key) {
		sn = new byte[sizeInBytes];
		this.key = key;
	}
	
	public Writer writer() {
		return new Writer(sn);
	}
	
	public Reader reader() {
		return new Reader(sn);
	}
	
	private int nextKey(int key) {
		long k = key;
		return (int) (((k+3)*(k+17)) % 4294967291L);
	}
	
	private int calcBase2Digits(int base1, int base1Digits, int base2) {
		double m = base1Digits * Math.log(base1) / Math.log(base2);
		return (int) Math.ceil(m);
	}
	
	private String toHexString(byte[] bytes) {
		StringBuilder hex = new StringBuilder(bytes.length*2);
		for (int b = 0; b < sn.length; b++) {
			hex.append(Integer.toHexString((bytes[b] >> 4) & 0xF));
			hex.append(Integer.toHexString(bytes[b] & 0xF));
		}		
		return hex.toString();
	}
	
	private String encode() {
		byte[] snCoded = new byte[sn.length];
		int mask1 = nextKey(key);
		int mask2 = nextKey(mask1);
		for (int b = 0; b < sn.length; b++) {
			byte last = sn[b];
			snCoded[b] = (byte) (sn[b] ^ mask1);
			mask1 = nextKey((mask1+b)^last);
		}
		for (int b = snCoded.length-1; b >= 0; b--) {
			byte last = snCoded[b];
			snCoded[b] = (byte) (snCoded[b] ^ mask2);
			mask2 = nextKey((mask2+(snCoded.length-b))^last);
		}
		String hex = toHexString(snCoded);
		BigInteger value = new BigInteger(hex, 16);
		int base36Digits = calcBase2Digits(16, hex.length(), 36);
		String serialNumber = value.toString(36).toUpperCase();
		while (serialNumber.length() < base36Digits) {
			serialNumber = "0"+serialNumber;
		}
		return serialNumber;
	}
	
	private void decode(String serialNumber) {
		BigInteger value = new BigInteger(serialNumber.toLowerCase(), 36);
		String hex = value.toString(16);
		while (hex.length() < sn.length*2) {
			hex = "0"+hex;
		}
		for (int i = 0; i < sn.length; i++) {
			sn[i] = (byte) Integer.parseInt(hex.substring(i*2, i*2+2), 16);
		}
		int mask1 = nextKey(key);
		int mask2 = nextKey(mask1);
		for (int b = sn.length-1; b >= 0; b--) {
			sn[b] = (byte) (sn[b] ^ mask2);
			mask2 = nextKey((mask2+(sn.length-b))^sn[b]);
		}
		for (int b = 0; b < sn.length; b++) {
			sn[b] = (byte) (sn[b] ^ mask1);
			mask1 = nextKey((mask1+b)^sn[b]);
		}
	}
	
	public void set(String serialNumber) {
		decode(serialNumber);
	}
	
	@Override
	public String toString() {
		return encode();
	}
	
	
//	public static void main(String[] args) {
//		SerialNumber sn = new SerialNumber(16, 17);
//		Writer writer = sn.writer();
//		writer.write(1238, 16);
//		String nr = sn.toString();
//		System.out.println(nr);
//		System.out.println(nr.length());
//		SerialNumber sn2 = new SerialNumber(16, 17);
//		sn2.set(nr);
//		Reader reader = sn2.reader();
//		long value = reader.readLong(16);
//		System.out.println(value);
//	}
	
}
