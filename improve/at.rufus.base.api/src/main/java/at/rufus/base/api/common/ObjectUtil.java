package at.rufus.base.api.common;

public final class ObjectUtil {

	public static boolean equals(Object o1, Object o2) {
		return o1 == o2 || (o1 != null && o1.equals(o2));
	}
	
	public static int hashCode(Object... objects) {
		int hashCode = 0;
		for (Object object : objects) {
			hashCode ^= (object == null ? 0 : object.hashCode());
		}
		return hashCode;
	}
	
}
