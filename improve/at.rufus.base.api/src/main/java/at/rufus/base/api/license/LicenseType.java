package at.rufus.base.api.license;

public class LicenseType {

	public static final int FULL = 1;
	public static final int PUBLISH = 2;
	
	private static final String[] LABELS = new String[] {
		"Full",
		"Publish"
	};
	
	private LicenseType() {
	}

	public static String toLabel(int type) {
		if (type >= 1 && type <= LABELS.length) {
			return LABELS[type-1];
		} else {
			return null;
		}
	}
	
}
