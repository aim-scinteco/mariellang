package at.rufus.base.api.model;

public abstract class ModelException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ModelException(String message) {
		super(message);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends ModelException> T unwrap(Throwable root) {
		while (root != null) {
			if (root instanceof ModelException) {
				return (T) root;
			}
			root = root.getCause();
		}
		return null;
	}	
	
	@SuppressWarnings("unchecked")
	public static <T extends ModelException> T unwrap(Throwable root, Class<T> type) {
		while (root != null) {
			if (root.getClass() == type) {
				return (T) root;
			}
			root = root.getCause();
		}
		return null;
	}

}
