package at.rufus.analysis.api.model;

public interface RunStatusAware {
	
	RunStatus getRunStatus();
	
	RunResult getRunResult();
	
}
