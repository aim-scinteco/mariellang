package at.rufus.analysis.api.model;

import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="PROCESS")
public class Process extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_RUN_SERVER = "runserver";
	public static final String PROP_TOOL = "tool";
	public static final String PROP_RUNSERVER_TOOL = "runserverTool";
	public static final String PROP_STEP = "step";
	public static final String PROP_RUNS = "runs";
	public static final String PROP_PROCESS_RESOURCES = "processResources";
	public static final String PROP_POSITION = "position";
	public static final String PROP_PARAMETERS = "parameters";
	public static final String PROP_GRID_ARGUMENTS = "gridArguments";
	public static final String PROP_DELETED = "deleted";
	public static final String PROP_PARENT_PROCESS = "parentProcess";
	public static final String PROP_SUB_PROCESSES = "subProcesses";
	public static final String PROP_VARIABLES = "variables";
	public static final String PROP_MAIN = "main";
	
	public static final Comparator<Process> COMPARE_BY_POSITION_ASC = new Comparator<Process>() {
		@Override
		public int compare(Process o1, Process o2) {
			return o1.getPosition() - o2.getPosition();
		}
	};
	
	public static final Comparator<Process> COMPARE_BY_POSITION_DESC = new Comparator<Process>() {
		@Override
		public int compare(Process o1, Process o2) {
			return o2.getPosition() - o1.getPosition();
		}
	};

	@Override
	@Transient
	public String getDiscriminator() {
		return AnalysisEntityTypes.PROCESS;
	}
	
	
	@Column(name="TYPE")
	private ProcessType type;

	@Column(name="POSITION")
	private int position;

	@Column(name="NAME")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="RUN_SERVER_ID", referencedColumnName="ID")
	private Runserver runserver;
	
	@ManyToOne
	@JoinColumn(name="TOOL", referencedColumnName="ID")
	private Tool tool;
	
	@ManyToOne
	@JoinColumn(name="RUNSERVER_TOOL", referencedColumnName="ID")
	private RunserverTool runserverTool;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="SELECTED")
	private boolean selected;
	
	@Column(name="RUN_GROUP_ID")
	private Long runGroupId;
	
	@Column(name="MAIN")
	private boolean main;
	
	@ManyToOne
	@JoinColumn(name="STEP", referencedColumnName="ID")
	private Step step;
	
	@ManyToOne
	@JoinColumn(name="PARENT_PROCESS", referencedColumnName="ID")
	private Process parentProcess;
	
	@OneToMany(mappedBy=Process.PROP_PARENT_PROCESS)
	private List<Process> subProcesses = new TrackedList<Process>(propertyChangeSupport, PROP_SUB_PROCESSES);

	@ManyToOne
	@JoinColumn(name="LATEST_RUN", referencedColumnName="ID")
	private Run latestRun;

	@OneToMany(mappedBy=Run.PROP_PROCESS)
	private List<Run> runs = new TrackedList<Run>(propertyChangeSupport, PROP_RUNS);
	
	@OneToMany(mappedBy=ProcessParameter.PROP_PROCESS)
	private List<ProcessParameter> parameters = new TrackedList<ProcessParameter>(propertyChangeSupport, PROP_PARAMETERS);
	
	@OneToMany(mappedBy="process")
	private List<ProcessGridArgument> gridArguments = new TrackedList<ProcessGridArgument>(propertyChangeSupport, PROP_GRID_ARGUMENTS);
	
	@OneToMany(mappedBy=ProcessVariable.PROP_PROCESS)
	private List<ProcessVariable> variables = new TrackedList<ProcessVariable>(propertyChangeSupport, PROP_VARIABLES);

	
	public ProcessType getType() {
		return type;
	}
	
	public void setType(ProcessType type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public Tool getTool() {
		return tool;
	}
	
	public void setTool(Tool tool) {
		propertyChangeSupport.firePropertyChange(PROP_TOOL, this.tool, this.tool = tool);
	}
	
	public RunserverTool getRunserverTool() {
		return runserverTool;
	}
	
	public void setRunserverTool(RunserverTool runserverTool) {
		propertyChangeSupport.firePropertyChange(PROP_RUNSERVER_TOOL, this.runserverTool, this.runserverTool = runserverTool);
	}
	
	public Step getStep() {
		return step;
	}
	
	public void setStep(Step step) {
		propertyChangeSupport.firePropertyChange(PROP_STEP, this.step, this.step = step);
	}
	
	public Process getParentProcess() {
		return parentProcess;
	}
	
	public void setParentProcess(Process parentProcess) {
		propertyChangeSupport.firePropertyChange(PROP_PARENT_PROCESS, this.parentProcess, this.parentProcess = parentProcess);
	}
	
	public List<Process> getSubProcesses() {
		return subProcesses;
	}
	
	public List<Run> getRuns() {
		return runs;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange(PROP_DELETED, this.deleted, this.deleted = deleted);
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		propertyChangeSupport.firePropertyChange("selected", this.selected, this.selected = selected);
	}
	
	public Long getRunGroupId() {
		return runGroupId;
	}
	
	public void setRunGroupId(Long runGroupId) {
		propertyChangeSupport.firePropertyChange("runGroupId", this.runGroupId, this.runGroupId = runGroupId);
	}
	
	public Runserver getRunserver() {
		return runserver;
	}
	
	public void setRunserver(Runserver runserver) {
		propertyChangeSupport.firePropertyChange("runserver", this.runserver, this.runserver = runserver);
	}
	
	public boolean isMain() {
		return main;
	}
	
	public void setMain(boolean main) {
		propertyChangeSupport.firePropertyChange("main", this.main, this.main = main);
	}
	
	public List<ProcessParameter> getParameters() {
		return parameters;
	}
	
	@Transient
	public ProcessParameter getParameter(GUID paramLovId) {
		for (ProcessParameter parameter : parameters) {
			if (parameter.getParamLov().equalsId(paramLovId)) {
				return parameter;
			}
		}
		return null;
	}
	
	@Transient
	public int getParameterIndex(GUID paramLovId) {
		int index = 0;
		for (ProcessParameter parameter : parameters) {
			if (parameter.getParamLov().equalsId(paramLovId)) {
				return index;
			}
			index++;
		}
		return -1;
	}
	
	public List<ProcessGridArgument> getGridArguments() {
		return gridArguments;
	}
	
	public Run getLatestRun() {
		return latestRun;
	}
	
	public void setLatestRun(Run latestRun) {
		propertyChangeSupport.firePropertyChange("latestRun", this.latestRun, this.latestRun = latestRun);
	}
	
	public List<ProcessVariable> getVariables() {
		return variables;
	}
	
}
