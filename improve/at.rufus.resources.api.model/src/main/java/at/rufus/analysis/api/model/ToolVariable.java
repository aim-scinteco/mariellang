package at.rufus.analysis.api.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.common.ObjectUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;

@javax.persistence.Entity
@Table(name = "TOOL_VARIABLE")
public class ToolVariable extends Entity {
	private static final long serialVersionUID = 1L;

	// variable name by convention
	public static final String VAR_DATASET = "dataset";
	public static final String VAR_COMMAND_FILE = "command-file";

	public static final Comparator<ToolVariable> COMPARE_BY_POSITION = new Comparator<ToolVariable>() {
		@Override
		public int compare(ToolVariable o1, ToolVariable o2) {
			return o1.getPosition() - o2.getPosition();
		}
	};

	public static final String NAME_PATTERN = "[A-Za-z_][A-Za-z0-9_\\-]*";

	public ToolVariable() {
		super();
	}

	public ToolVariable(GUID id) {
		super(id);
	}

	@Column(name = "NAME")

	private String name;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "POSITION")
	private int position;

	@ManyToOne
	@JoinColumn(name = "CATEGORY", referencedColumnName = "ID")
	private ToolCategory category;

	@ManyToOne
	@JoinColumn(name = "TOOL", referencedColumnName = "ID")
	private Tool tool;

	@ManyToOne
	@JoinColumn(name = "RUNSERVER_TOOL", referencedColumnName = "ID")
	private RunserverTool runserverTool;

	@ManyToOne
	@JoinColumn(name = "APP_MEMBER", referencedColumnName = "ID")
	private Member member;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}

	public ToolCategory getCategory() {
		return category;
	}

	public void setCategory(ToolCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}

	public Tool getTool() {
		return tool;
	}

	public void setTool(Tool tool) {
		propertyChangeSupport.firePropertyChange("tool", this.tool, this.tool = tool);
	}

	public RunserverTool getRunserverTool() {
		return runserverTool;
	}

	public void setRunserverTool(RunserverTool runserverTool) {
		propertyChangeSupport.firePropertyChange("runserverTool", this.runserverTool,
				this.runserverTool = runserverTool);
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}

	public static Set<String> getNamesUsed(List<ToolVariable> selected, ToolVariable exclude) {
		Set<String> names = new HashSet<>();
		if (selected != null) {
			for (ToolVariable variable : selected) {
				if (variable != exclude) {
					names.add(variable.getName());
				}
			}
		}
		return names;
	}

	public static String getNextVariableName(List<ToolVariable> variables) {
		Set<String> usedNames = getNamesUsed(variables, null);
		String name = null;
		if (!usedNames.contains(ToolVariable.VAR_COMMAND_FILE)) {
			name = ToolVariable.VAR_COMMAND_FILE;
		} else if (!usedNames.contains(ToolVariable.VAR_DATASET)) {
			name = ToolVariable.VAR_DATASET;
		} else {
			int i = variables.size();
			do {
				name = "var" + (++i);
			} while (usedNames.contains(name));
		}
		return name;
	}

	public static boolean isExistingVariable(List<ToolVariable> variables, ToolVariable variable) {
		Collections.sort(variables, ToolVariable.COMPARE_BY_POSITION);
		Iterator<ToolVariable> iterator = variables.iterator();
		while (iterator.hasNext()) {
			ToolVariable next = iterator.next();
			if (equalVariables(next, variable)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	public static boolean equalVariables(List<ToolVariable> variables1, List<ToolVariable> variables2) {
		if (variables1.size() != variables2.size()) {
			return false;
		}
		Collections.sort(variables1, ToolVariable.COMPARE_BY_POSITION);
		Collections.sort(variables2, ToolVariable.COMPARE_BY_POSITION);
		Iterator<ToolVariable> iter1 = variables1.iterator();
		Iterator<ToolVariable> iter2 = variables2.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			ToolVariable variable1 = iter1.next();
			ToolVariable variable2 = iter2.next();
			if (!equalVariables(variable1, variable2)) {
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	public static boolean equalVariables(ToolVariable variable1, ToolVariable variable2) {
		return ObjectUtil.equals(variable1.getName(), variable2.getName());
	}	
	
}
