package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;

@javax.persistence.Entity
@Table(name = "RUNSERVER_TOOL_USER_CONFIG")
public class RunserverToolUserConfig extends Entity {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "RUNSERVER_TOOL", referencedColumnName = "ID")
	private RunserverTool runserverTool;

	@ManyToOne
	@JoinColumn(name = "APP_MEMBER", referencedColumnName = "ID")
	private Member member;

	@Column(name = "HIDDEN")
	private boolean hidden;
	
	@Column(name = "PREFERRED")
	private boolean preferred;

	@Column(name = "TOOL_PARAMS")
	private boolean toolParamsSet;

	@Column(name = "GRID_ARGS")
	private boolean gridArgsSet;

	@Column(name = "VARIABLES")
	private boolean variablesSet;

	public RunserverToolUserConfig() {
		super();
	}

	public RunserverToolUserConfig(GUID id) {
		super(id);
	}

	public RunserverTool getRunserverTool() {
		return runserverTool;
	}

	public void setRunserverTool(RunserverTool runserverTool) {
		propertyChangeSupport.firePropertyChange("runserverTool", this.runserverTool,
				this.runserverTool = runserverTool);
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		propertyChangeSupport.firePropertyChange("hidden", this.hidden, this.hidden = hidden);
	}
	
	public boolean isPreferred() {		
		return preferred;
	}

	public void setPreferred(boolean preferred) {
		propertyChangeSupport.firePropertyChange("preferred", this.preferred, this.preferred = preferred);
	}

	public boolean isToolParamsSet() {
		return toolParamsSet;
	}

	public void setToolParamsSet(boolean toolParamsSet) {
		propertyChangeSupport.firePropertyChange("toolParamsSet", this.toolParamsSet,
				this.toolParamsSet = toolParamsSet);
	}

	public boolean isGridArgsSet() {
		return gridArgsSet;
	}

	public void setGridArgsSet(boolean gridArgsSet) {
		propertyChangeSupport.firePropertyChange("gridArgsSet", this.gridArgsSet, this.gridArgsSet = gridArgsSet);
	}

	public void setVariablesSet(boolean variablesSet) {
		propertyChangeSupport.firePropertyChange("variablesSet", this.variablesSet, this.variablesSet = variablesSet);
	}

	public boolean isVariablesSet() {
		return variablesSet;
	}

}
