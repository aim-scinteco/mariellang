package at.rufus.analysis.api.model;

import java.util.ArrayList;
import java.util.List;

public class DirectoryResultEntry extends RunResultEntry {

	private static final long serialVersionUID = 1L;
	private List<RunResultEntry> resultEntries = new ArrayList<RunResultEntry>();
	private boolean partialCheckin; // is used for archive checkins. The flag prevents the cleanup of folders if they aren't checked in completely.

	
	public DirectoryResultEntry(String name) {
		super(name);
	}

	public List<RunResultEntry> getResultEntries() {
		return resultEntries;
	}

	public boolean isPartialCheckin() {
		return partialCheckin;
	}

	public void setPartialCheckin(boolean partialCheckin) {
		this.partialCheckin = partialCheckin;
	}

	public int getNumberOfChildren() {
		return 1;
	}

}