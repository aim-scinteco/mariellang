package at.rufus.analysis.api.model;

public enum PredefinedTool {
	
	NONMEM("nonmem"),
	PSN_EXECUTE("psnExecute");
	
	private final String category;
	
	private PredefinedTool(String category) {
		this.category = category;
	}
	
	public String getCategory() {
		return category;
	}
	
}
