package at.rufus.analysis.api.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class DMGStep implements Serializable{
	private static final long serialVersionUID = 1L;
	public GUID stepId;
	public GUID stepVersionId;
	public GUID parentStepId;
	public String stepName;
	
	public Timestamp revisionFromTime;
	public String entityId;
	public String entityVersionId;
	public String processType;
	public String processName;
	public GUID runserverId;
	public Timestamp lastModified;
     
	public String runserverName;
	public String runserverUrl;
	public Boolean runserverLocal;
	public Boolean runserverReproducible;
	public List<DMGInventoryNode> children = new ArrayList<DMGInventoryNode>();
	
	@Override
	public String toString() {
		return stepName + "-" + entityVersionId;
	}
}