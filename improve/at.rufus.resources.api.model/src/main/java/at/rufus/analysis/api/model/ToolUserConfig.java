package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;

@javax.persistence.Entity
@Table(name="TOOL_USER_CONFIG")
public class ToolUserConfig extends Entity {
	private static final long serialVersionUID = 1L;

	public ToolUserConfig() {
		super();
	}
	
	public ToolUserConfig(GUID id) {
		super(id);
	}
	
	@ManyToOne
	@JoinColumn(name="TOOL", referencedColumnName="ID")
	private Tool tool;
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	private Member member;
	
	@Column(name = "HIDDEN")
	private boolean hidden;
	
	
	public Tool getTool() {
		return tool;
	}
	
	public void setTool(Tool tool) {
		propertyChangeSupport.firePropertyChange("tool", this.tool, this.tool = tool);
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}
	
	public boolean isHidden() {
		return hidden;
	}
	
	public void setHidden(boolean hidden) {
		propertyChangeSupport.firePropertyChange("hidden", this.hidden, this.hidden = hidden);
	}
	
}
