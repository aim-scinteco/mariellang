package at.rufus.analysis.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.ResourceVersion;

@javax.persistence.Entity
@Table(name="ANALYSIS_TREE_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(AnalysisTreeVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class AnalysisTreeVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;
	
	public static final String DISCRIMINATOR=AnalysisEntityTypes.ANALYSIS_TREE_VERSION;
	
	@Transient
	public AnalysisTree getAnalysisTree() {
		return (AnalysisTree) getResource();
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return AnalysisTreeVersion.DISCRIMINATOR;
	}
	
}
