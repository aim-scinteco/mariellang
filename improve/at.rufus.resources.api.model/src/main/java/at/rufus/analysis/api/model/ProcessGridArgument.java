package at.rufus.analysis.api.model;

import java.sql.Timestamp;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="PROCESS_GRID_ARGUMENT")
public class ProcessGridArgument extends Entity implements IGridArgument {
private static final long serialVersionUID = 1L;

	public static final Comparator<ProcessGridArgument> DESCRIPTOR_NAME_COMPARATOR = new Comparator<ProcessGridArgument>() {
		@Override
		public int compare(ProcessGridArgument o1, ProcessGridArgument o2) {
			String n1 = o1.getDescriptor() != null ? o1.getDescriptor().getName() : null;
			String n2 = o2.getDescriptor() != null ? o2.getDescriptor().getName() : null;
			return CmpUtil.cmp(n1, n2);
		}
	};
	
	@Column(name="TEXT_VALUE")
	private String textValue;
	
	@Column(name="DATE_VALUE")
	private Timestamp dateValue;

	@ManyToOne
	@JoinColumn(name="LOV_VALUE", referencedColumnName="ID")
	private GridArgumentLov lovValue;
	
	@ManyToOne
	@JoinColumn(name="PROCESS", referencedColumnName="ID")
	private Process process;
	
	@ManyToOne
	@JoinColumn(name = "GRID_ARG_DESC", referencedColumnName = "ID")
	GridArgumentDescriptor descriptor;

	
	@Override
	@Transient
	public String getDescriptorName() {
		return descriptor != null ? descriptor.getName() : null;
	}
	
	@Override
	@Transient
	public String getGridOption() {
		return descriptor != null ? descriptor.getGridOption() : null;
	}
	
	@Override
	@Transient
	public GridArgumentType getGridArgumentType() {
		return descriptor != null ? descriptor.getType() : null;
	}
	
	@Override
	@Transient
	public GridArgumentCategory getGridArgumentCategory() {
		return descriptor != null ? descriptor.getCategory() : null;
	}
	
	public String getTextValue() {
		return textValue;
	}

	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.textValue, this.textValue = textValue);
	}

	public GridArgumentLov getLovValue() {
		return lovValue;
	}

	public void setLovValue(GridArgumentLov lovValue) {
		propertyChangeSupport.firePropertyChange("lovValue", this.lovValue, this.lovValue = lovValue);
	}

	public GridArgumentDescriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(GridArgumentDescriptor descriptor) {
		propertyChangeSupport.firePropertyChange("descriptor", this.descriptor, this.descriptor = descriptor);
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		propertyChangeSupport.firePropertyChange("process", this.process, this.process = process);
	}
	
	@Override
	public Timestamp getDateValue() {
		return dateValue;
	}

	@Override
	public void setDateValue(Timestamp dateValue) {
		propertyChangeSupport.firePropertyChange("dateValue", this.dateValue, this.dateValue = dateValue);
	}
	
}
