package at.rufus.analysis.api.model;

import java.math.BigDecimal;

import at.rufus.resources.api.model.ParameterLov;

public interface IParameter {

	ParameterLov getParamLov();
	void setParamLov(ParameterLov paramLov);
	
	String getTextValue();
	void setTextValue(String textValue);
	
	BigDecimal getNumValue();
	void setNumValue(BigDecimal numValue);
	
	Boolean getBooleanValue();
	void setBooleanValue(Boolean booleanValue);
	
}
