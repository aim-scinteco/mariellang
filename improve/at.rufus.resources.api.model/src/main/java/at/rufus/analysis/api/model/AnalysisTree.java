package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.Resource;

@javax.persistence.Entity
@Table(name="ANALYSIS_TREE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(AnalysisEntityTypes.ANALYSIS_TREE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class AnalysisTree extends Resource {
	private static final long serialVersionUID = 1L;
	
	@Column(name="NEXT_STEP_NUMBER")
	private int nextStepNumber;
	
	public int getNextStepNumber() {
		return nextStepNumber;
	}
	
	public void setNextStepNumber(int nextStepNumber) {
		propertyChangeSupport.firePropertyChange("nextStepNumber", this.nextStepNumber, this.nextStepNumber = nextStepNumber);
	}
	
	@Transient
	@Override
	public String getDiscriminator() {
		return AnalysisEntityTypes.ANALYSIS_TREE;
	}

}
