package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="STEP_LINKS")
public class StepLink extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_SOURCE_STEP = "sourceStep";
	public static final String PROP_TARGET_STEP = "targetStep";
	public static final String PROP_DESCRIPTION = "description";
	
	@ManyToOne
	@JoinColumn(name="SOURCE_STEP", referencedColumnName="ID")
	private Step sourceStep;
	
	@ManyToOne
	@JoinColumn(name="TARGET_STEP", referencedColumnName="ID")
	private Step targetStep;

	@Column(name="DESCRIPTION")
	private String description;

	
	public Step getSourceStep() {
		return sourceStep;
	}
	
	public void setSourceStep(Step sourceStep) {
		propertyChangeSupport.firePropertyChange(PROP_SOURCE_STEP, this.sourceStep, this.sourceStep = sourceStep);
	}

	public Step getTargetStep() {
		return targetStep;
	}
	
	public void setTargetStep(Step targetStep) {
		propertyChangeSupport.firePropertyChange(PROP_TARGET_STEP, this.targetStep, this.targetStep = targetStep);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange(PROP_DESCRIPTION, this.description, this.description = description);
	}
	
}
