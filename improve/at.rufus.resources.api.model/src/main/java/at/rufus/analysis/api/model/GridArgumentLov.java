package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="GRID_ARGUMENT_LOV")
public class GridArgumentLov extends Entity {

private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private GridArgumentCategory category;

	@Column(name="POSITION")
	private int position;
	
	@Column(name="TEXT")
	private String text;
	
	@Column(name="EXPIRED")
	private boolean expired;


	public GridArgumentCategory getCategory() {
		return category;
	}
	public void setCategory(GridArgumentCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		propertyChangeSupport.firePropertyChange("text", this.text, this.text = text);
	}
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		propertyChangeSupport.firePropertyChange("expired", this.expired, this.expired = expired);
	}
	
}
