package at.rufus.analysis.api.model;

import java.io.Serializable;

import at.rufus.base.api.common.ObjectUtil;

public abstract class RunResultEntry implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private final String name;
	private DirectoryResultEntry parent;
	private String path = null;
	private long lastModified;
	private boolean modified;

	public RunResultEntry(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public DirectoryResultEntry getParent() {
		return parent;
	}

	public void setParent(DirectoryResultEntry parent) {
		this.parent = parent;
	}

	public String getPath() {
		if (path == null)
			throw new IllegalStateException("RunResultEntrySet#finalInitialize call missing");
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}
	
	public boolean isModified() {
		return modified;
	}
	
	public void setModified(boolean modified) {
		this.modified = modified;
	}

	@Override
	public String toString() {
		return path;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (obj instanceof RunResultEntry) {
			RunResultEntry o = (RunResultEntry) obj;
			return ObjectUtil.equals(getPath(), o.getPath());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return ObjectUtil.hashCode(getPath());
	}
	
}
