package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;

@javax.persistence.Entity
@Table(name="RUNSERVER_USER_CONFIG")
public class RunserverUserConfig extends Entity {
	private static final long serialVersionUID = 1L;

	public RunserverUserConfig() {
		super();
	}
	
	public RunserverUserConfig(GUID id) {
		super(id);
	}
	
	@ManyToOne
	@JoinColumn(name="RUNSERVER", referencedColumnName="ID")
	private Runserver runserver;
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	private Member member;
	
	@Column(name = "HIDDEN")
	private boolean hidden;
	
	
	public Runserver getRunserver() {
		return runserver;
	}
	
	public void setRunserver(Runserver runserver) {
		propertyChangeSupport.firePropertyChange("runserver", this.runserver, this.runserver = runserver);
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}
	
	public boolean isHidden() {
		return hidden;
	}
	
	public void setHidden(boolean hidden) {
		propertyChangeSupport.firePropertyChange("hidden", this.hidden, this.hidden = hidden);
	}
	
}
