package at.rufus.analysis.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.ResourceVersion;

@javax.persistence.Entity
@Table(name = "COMPOSITE_STEP_VERSION")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(CompositeStepVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
public class CompositeStepVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;

	public static final String PROP_URL = "url";
	public static final String DISCRIMINATOR = AnalysisEntityTypes.COMPOSITE_STEP_VERSION;
	

	@Override
	public CompositeStepVersion copy() {
		CompositeStepVersion link = (CompositeStepVersion) super.copy();
		return link;
	}

	@Override
	@Transient
	public String getDiscriminator() {
		return CompositeStepVersion.DISCRIMINATOR;
	}
	
	@Transient
	public CompositeStep getCompositeStep() {
		return (CompositeStep) getResource();
	}

}
