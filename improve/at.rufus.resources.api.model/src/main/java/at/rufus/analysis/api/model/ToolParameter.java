package at.rufus.analysis.api.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.common.ObjectUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;
import at.rufus.resources.api.model.ParameterLov;

@javax.persistence.Entity
@Table(name="TOOL_PARAMETER")
public class ToolParameter extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_PARAM_VALUE = "paramValue";

	public static final Comparator<ToolParameter> NAME_COMPARATOR = new Comparator<ToolParameter>() {
		@Override
		public int compare(ToolParameter o1, ToolParameter o2) {
			String n1 = o1.getParamLov() != null ? o1.getParamLov().getName() : null;
			String n2 = o2.getParamLov() != null ? o2.getParamLov().getName() : null;
			return CmpUtil.cmp(n1, n2);
		}
	};
	
	public ToolParameter() {
		super();
	}
	
	public ToolParameter(GUID id) {
		super(id);
	}
	
	@Column(name = "PARAM_LOV")
	private ParameterLov paramLov;
	
	@Column(name = "PARAM_VALUE")
	private String paramValue;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private ToolCategory category;
	
	@ManyToOne
	@JoinColumn(name="TOOL", referencedColumnName="ID")
	private Tool tool;
	
	@ManyToOne
	@JoinColumn(name="RUNSERVER_TOOL", referencedColumnName="ID")
	private RunserverTool runserverTool;
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	private Member member;
	
	
	public ParameterLov getParamLov() {
		return paramLov;
	}
	
	public void setParamLov(ParameterLov paramLov) {
		propertyChangeSupport.firePropertyChange("paramLov", this.paramLov, this.paramLov = paramLov);
	}
	
	public String getParamValue() {
		return paramValue;
	}
	
	public void setParamValue(String paramValue) {
		propertyChangeSupport.firePropertyChange(PROP_PARAM_VALUE, this.paramValue, this.paramValue = paramValue);
	}
	
	public ToolCategory getCategory() {
		return category;
	}
	
	public void setCategory(ToolCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}
	
	public Tool getTool() {
		return tool;
	}
	
	public void setTool(Tool tool) {
		propertyChangeSupport.firePropertyChange("tool", this.tool, this.tool = tool);
	}
	
	public RunserverTool getRunserverTool() {
		return runserverTool;
	}
	
	public void setRunserverTool(RunserverTool runserverTool) {
		propertyChangeSupport.firePropertyChange("runserverTool", this.runserverTool, this.runserverTool = runserverTool);
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}

	public static boolean equalParameters(List<ToolParameter> parameters1, List<ToolParameter> parameters2) {
		if (parameters1.size() != parameters2.size()) {
			return false;
		}
		Collections.sort(parameters1, ToolParameter.NAME_COMPARATOR);
		Collections.sort(parameters2, ToolParameter.NAME_COMPARATOR);
		Iterator<ToolParameter> iter1 = parameters1.iterator();
		Iterator<ToolParameter> iter2 = parameters2.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			ToolParameter parameter1 = iter1.next();
			ToolParameter parameter2 = iter2.next();
			if (!equalParameters(parameter1, parameter2)) {
				return false;
			}
		}
		return true;
	}

	public static boolean equalParameters(ToolParameter parameter1, ToolParameter parameter2) {
		return ObjectUtil.equals(parameter1.getParamLov(), parameter2.getParamLov()) 
				&& ObjectUtil.equals(parameter1.getParamValue(), parameter2.getParamValue());
	}
	
	
}
