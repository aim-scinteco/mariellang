package at.rufus.analysis.api.model;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.common.ObjectUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;

@javax.persistence.Entity
@Table(name="TOOL_GRID_ARGUMENT")
public class ToolGridArgument extends Entity implements IGridArgument {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_TEXT_VALUE = "textValue";
	public static final String PROP_LOV_VALUE = "lovValue";

	
	public static final Comparator<ToolGridArgument> DESCRIPTOR_NAME_COMPARATOR = new Comparator<ToolGridArgument>() {
		@Override
		public int compare(ToolGridArgument o1, ToolGridArgument o2) {
			String n1 = o1.getDescriptor() != null ? o1.getDescriptor().getName() : null;
			String n2 = o2.getDescriptor() != null ? o2.getDescriptor().getName() : null;
			return CmpUtil.cmp(n1, n2);
		}
	};
	
	public ToolGridArgument() {
		super();
	}
	
	public ToolGridArgument(GUID id) {
		super(id);
	}

	@Column(name="TEXT_VALUE")
	private String textValue;
	
	@Column(name="DATE_VALUE")
	private Timestamp dateValue;
	
	@ManyToOne
	@JoinColumn(name="LOV_VALUE", referencedColumnName="ID")
	private GridArgumentLov lovValue;
	
	@ManyToOne
	@JoinColumn(name = "GRID_ARG_DESC", referencedColumnName = "ID")
	GridArgumentDescriptor descriptor;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private ToolCategory category;
	
	@ManyToOne
	@JoinColumn(name="TOOL", referencedColumnName="ID")
	private Tool tool;
	
	@ManyToOne
	@JoinColumn(name="RUNSERVER_TOOL", referencedColumnName="ID")
	private RunserverTool runserverTool;
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	private Member member;
	
	@Override
	public String getTextValue() {
		return textValue;
	}
	@Override
	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange(PROP_TEXT_VALUE, this.textValue, this.textValue = textValue);
	}
	@Override
	public GridArgumentLov getLovValue() {
		return lovValue;
	}
	@Override
	public void setLovValue(GridArgumentLov lovValue) {
		propertyChangeSupport.firePropertyChange(PROP_LOV_VALUE, this.lovValue, this.lovValue = lovValue);
	}
	public GridArgumentDescriptor getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(GridArgumentDescriptor descriptor) {
		propertyChangeSupport.firePropertyChange("descriptor", this.descriptor, this.descriptor = descriptor);
	}
	
	public ToolCategory getCategory() {
		return category;
	}
	
	public void setCategory(ToolCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}
	
	public Tool getTool() {
		return tool;
	}
	
	public void setTool(Tool tool) {
		propertyChangeSupport.firePropertyChange("tool", this.tool, this.tool = tool);
	}
	
	public RunserverTool getRunserverTool() {
		return runserverTool;
	}
	
	public void setRunserverTool(RunserverTool runserverTool) {
		propertyChangeSupport.firePropertyChange("runserverTool", this.runserverTool, this.runserverTool = runserverTool);
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}

	@Override
	public Timestamp getDateValue() {
		return dateValue;
	}

	@Override
	public void setDateValue(Timestamp dateValue) {
		propertyChangeSupport.firePropertyChange("dateValue", this.dateValue, this.dateValue = dateValue);
	}	
	
	@Override
	@Transient
	public String getDescriptorName() {
		return descriptor != null ? descriptor.getName() : null;
	}
	
	@Override
	@Transient
	public String getGridOption() {
		return descriptor != null ? descriptor.getGridOption() : null;
	}
	
	@Override
	@Transient
	public GridArgumentType getGridArgumentType() {
		return descriptor != null ? descriptor.getType() : null;
	}
	@Override
	@Transient
	public GridArgumentCategory getGridArgumentCategory() {
		return descriptor != null ? descriptor.getCategory() : null;
	}
	
	public static boolean equalGridArguments(List<ToolGridArgument> gridArguments1, List<ToolGridArgument> gridArguments2) {
		if (gridArguments1.size() != gridArguments2.size()) {
			return false;
		}
		Collections.sort(gridArguments1, ToolGridArgument.DESCRIPTOR_NAME_COMPARATOR);
		Collections.sort(gridArguments2, ToolGridArgument.DESCRIPTOR_NAME_COMPARATOR);
		Iterator<ToolGridArgument> iter1 = gridArguments1.iterator();
		Iterator<ToolGridArgument> iter2 = gridArguments2.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			ToolGridArgument gridArgument1 = iter1.next();
			ToolGridArgument gridArgument2 = iter2.next();
			if (!equalGridArguments(gridArgument1, gridArgument2)) {
				return false;
			}
		}
		return true;
	}

	public static boolean equalGridArguments(ToolGridArgument gridArgument1, ToolGridArgument gridArgument2) {
		GridProvider provider1 = gridArgument1.getDescriptor() != null ? gridArgument1.getDescriptor().getProvider() : null;
		GridProvider provider2 = gridArgument2.getDescriptor() != null ? gridArgument2.getDescriptor().getProvider() : null;
		GUID descriptorId1 = gridArgument1.getDescriptor() != null ? gridArgument1.getDescriptor().getId() : null;
		GUID descriptorId2 = gridArgument2.getDescriptor() != null ? gridArgument2.getDescriptor().getId() : null;
		GUID lovValueId1 = gridArgument1.getLovValue() != null ? gridArgument1.getLovValue().getId() : null;
		GUID lovValueId2 = gridArgument2.getLovValue() != null ? gridArgument2.getLovValue().getId() : null;
		return ObjectUtil.equals(provider1, provider2)
				&& ObjectUtil.equals(descriptorId1, descriptorId2)
				&& ObjectUtil.equals(lovValueId1, lovValueId2)
				&& ObjectUtil.equals(gridArgument1.getTextValue(), gridArgument2.getTextValue());
	}	
	
}
