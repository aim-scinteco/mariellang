package at.rufus.analysis.api.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.ParameterLov;

@javax.persistence.Entity
@Table(name="RUN_PARAMETER")
public class RunParameter extends Entity implements IParameter {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_RUN = "run";

	@ManyToOne
	@JoinColumn(name="RUN_ID", referencedColumnName="ID")
	private Run run;
	
	@Column(name = "PARAM_LOV")
	private ParameterLov paramLov;
	
	@Column(name="TEXT_VALUE")
	private String textValue;
	
	@Column(name="NUM_VALUE")
	private BigDecimal numValue;
	
	
	public ParameterLov getParamLov() {
		return paramLov;
	}
	
	public void setParamLov(ParameterLov paramLov) {
		propertyChangeSupport.firePropertyChange("paramLov", this.paramLov, this.paramLov = paramLov);
	}
	
	public Run getRun() {
		return run;
	}
	
	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange(PROP_RUN, this.run, this.run = run);
	}
	
	public String getTextValue() {
		return textValue;
	}
	
	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.textValue, this.textValue = textValue);
	}
	
	public BigDecimal getNumValue() {
		return numValue;
	}
	
	public void setNumValue(BigDecimal numValue) {
		propertyChangeSupport.firePropertyChange("numValue", this.numValue, this.numValue = numValue);
	}
	
	@Transient
	public Boolean getBooleanValue() {
		return numValue != null ? (numValue.compareTo(BigDecimal.ONE) == 0 ? Boolean.TRUE : Boolean.FALSE) : null;
	}
	
	@Transient
	public void setBooleanValue(Boolean booleanValue) {
		setNumValue(booleanValue != null ? (booleanValue.booleanValue() ? BigDecimal.ONE : BigDecimal.ZERO) : null);
	}
	
	// have to use a non-setter method name (that's why "updateEnumValue and not "setEnumValue") because "fu..ing" oxyUnitInfoBuilder doesn't care about transient annotations!
	@Transient
	public void updateEnumValue(Enum<?> enumValue) {
		setTextValue(enumValue != null ? enumValue.name() : null);
	}
	
	@Transient
	public <E extends Enum<E>> E getEnumValue(Class<E> enumType) {
		return textValue != null ? Enum.valueOf(enumType, textValue) : null;
	}	
	
}
