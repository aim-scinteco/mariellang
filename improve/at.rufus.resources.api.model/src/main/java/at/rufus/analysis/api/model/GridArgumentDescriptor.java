package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="GRID_ARGUMENT_DESC")
public class GridArgumentDescriptor extends Entity  {
private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="PROVIDER")
	private GridProvider provider;
	
	@Column(name="TYPE")
	private GridArgumentType type;
	
	@Column(name="GRID_OPTION")
	private String gridOption;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private GridArgumentCategory category;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public GridProvider getProvider() {
		return provider;
	}
	public void setProvider(GridProvider provider) {
		propertyChangeSupport.firePropertyChange("provider", this.provider, this.provider = provider);
	}
	public GridArgumentType getType() {
		return type;
	}
	public void setType(GridArgumentType type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	
	public GridArgumentCategory getCategory() {
		return category;
	}
	public void setCategory(GridArgumentCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}
	
	public String getGridOption() {
		return gridOption;
	}
	public void setGridOption(String gridOption) {
		propertyChangeSupport.firePropertyChange("gridOption", this.gridOption, this.gridOption = gridOption);
	}
	
}
