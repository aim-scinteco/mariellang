package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.ResourceVersion;

@javax.persistence.Entity
@Table(name="RUN_WS_MODIFICATION")
public class RunWsModification extends Entity {

	private static final long serialVersionUID = 1L;
	
	public static final String PROP_STEP = "step";

	@Column(name="MODIFICATION_TYPE")
	private RunWsModificationType modificationType;
	
	@Column(name="RUN_WS_PATH")
	private String runWorkspacePath;
	
	@ManyToOne
	@JoinColumn(name="STEP", referencedColumnName="ID")
	private Step step;
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE_VERSION", referencedColumnName="ID")
	private ResourceVersion resourceVersion;
	
	public RunWsModification() {
		super();
	}

	public RunWsModificationType getModificationType() {
		return modificationType;
	}
	
	public void setModificationType(RunWsModificationType modificationType) {
		propertyChangeSupport.firePropertyChange("modificationType", this.modificationType, this.modificationType = modificationType);
	}
	
	public String getRunWorkspacePath() {
		return runWorkspacePath;
	}
	
	public void setRunWorkspacePath(String runWorkspacePath) {
		propertyChangeSupport.firePropertyChange("runWorkspacePath", this.runWorkspacePath, this.runWorkspacePath = runWorkspacePath);
	}
	
	public Step getStep() {
		return step;
	}
	
	public void setStep(Step step) {
		propertyChangeSupport.firePropertyChange(PROP_STEP, this.step, this.step = step);
	}
	
	public ResourceVersion getResourceVersion() {
		return resourceVersion;
	}
	
	public void setResourceVersion(ResourceVersion resourceVersion) {
		propertyChangeSupport.firePropertyChange("resourceVersion", this.resourceVersion, this.resourceVersion = resourceVersion);
	}
	
}
