package at.rufus.analysis.api.model;

public class RunStatusFlags {
	public static final int INITIAL = 1 << 1;
	public static final int IN_PROGRESS = 1 << 2;
	public static final int END = 1 << 3;
	public static final int PRE_RUNNING = 1 << 6;
	public static final int PRE_CHECKIN = 1 << 7;
	public static final int CANCELABLE = 1 << 8;

}
