package at.rufus.analysis.api.model;

import java.sql.Timestamp;
import java.util.Comparator;

import at.rufus.base.api.common.CmpUtil;

public interface IGridArgument {

	static final Comparator<IGridArgument> DESCRIPTOR_NAME_COMPARATOR = new Comparator<IGridArgument>() {
		@Override
		public int compare(IGridArgument o1, IGridArgument o2) {
			String n1 = o1.getDescriptorName();
			String n2 = o2.getDescriptorName();
			return CmpUtil.cmp(n1, n2);
		}
	};
	

	String getDescriptorName();
	String getGridOption();
	GridArgumentType getGridArgumentType();
	GridArgumentCategory getGridArgumentCategory();
	GridArgumentDescriptor getDescriptor();

	void setLovValue(GridArgumentLov lovValue);
	GridArgumentLov getLovValue();

	void setTextValue(String textValue);
	String getTextValue();
	
	void setDateValue(Timestamp dateValue);
	Timestamp getDateValue();


}
