package at.rufus.analysis.api.model;

public enum RunResult {

	/**
	 * Error happened while running 
	 */
	ERROR(1),

	/**
	 * User canceled a run
	 */
	CANCELED(2),

	/**
	 * Warning happened while running
	 */
	WARNING(3),

	/**
	 * ???
	 */
	COMPLETED(4),

	/**
	 * ???
	 */
	OK(5),
	
	/**
	 * Currently not intended to be generated by improve but necessary for legacy data migration.
	 */
	UNKNOWN(6);
	
	
	/**
	 * When cumulating the overall run status from the different run phases, lower priorities rule. 
	 */
	private int priority;
	
	private RunResult(int priority) {
		this.priority = priority;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public boolean hasPriorityOver(RunResult result) {
		return this.priority < result.priority;
	}
	
}
