package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.ResourceVersion;

@javax.persistence.Entity
@Table(name="RUN_RESOURCE")
public class RunResource extends Entity {
	private static final long serialVersionUID = 1L;

	public static final String PROP_RUN = "run";
	
	@Column(name="ROLE")
	private String role;

	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE_VERSION", referencedColumnName="ID")
	private ResourceVersion resourceVersion;
	
	@ManyToOne
	@JoinColumn(name="RUN", referencedColumnName="ID")
	private Run run;

	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public ResourceVersion getResourceVersion() {
		return resourceVersion;
	}
	
	public void setResourceVersion(ResourceVersion resourceVersion) {
		propertyChangeSupport.firePropertyChange("resourceVersion", this.resourceVersion, this.resourceVersion = resourceVersion);
	}
	
	public Run getRun() {
		return run;
	}
	
	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange(PROP_RUN, this.run, this.run = run);
	}
	
}
