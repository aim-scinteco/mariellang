package at.rufus.analysis.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Resource;

@javax.persistence.Entity
@Table(name="COMPOSITE_STEP")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(CompositeStep.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class CompositeStep extends Resource {
	
	public static enum Status {
		INITIAL,
		RUNNING,
		STOPPED_AT_LOCAL_PROCESS,
		STOPPED_AT_MISSING_RUNSERVER,
		STOPPED_AT_WRONG_OWNERSHIP,
		STOPPED_AT_DELETED_STEP,
		STOPPED_AT_SUSPENDED_STEP,
		STOPPED_AT_UPDATE_LINKS_FAILED,
		STOPPED_AT_PROCESS_NOT_STARTABLE,
		STOPPED_AT_STEP_FINISHED_IN_ERROR,
		CANCELLED,
		FINISHED, 
		
	}

	private static final long serialVersionUID = 1L;

	public static final String DISCRIMINATOR= AnalysisEntityTypes.COMPOSITE_STEP;
	
	@Column(name="STATE")
	private Status state;
	
	@Column(name="CURRENT_STEP_POSITION")
	private int currentStepPosition;
	
	@Column(name="PROCESS_ID_STOPPED")
	private GUID processIdStopped;
	
	private List<CompositeStepAssignment> stepAssignments = new TrackedList<CompositeStepAssignment>(propertyChangeSupport, "stepAssignments");
	
	private boolean parallelExecution;

	private boolean includeLocalProcesses;
	
	@OneToMany(mappedBy = "compositeStep")
	public List<CompositeStepAssignment> getStepAssignments() {
		return this.stepAssignments;
	}
	
	public void setStepAssignments(List<CompositeStepAssignment> stepAssignments) {
		this.stepAssignments = stepAssignments;
	}

	public Status getState() {
		return state;
	}

	public void setState(Status state) {
		propertyChangeSupport.firePropertyChange("state", this.state, this.state = state);
	}

	public int getCurrentStepPosition() {
		return currentStepPosition;
	}

	public void setCurrentStepPosition(int currentStepPosition) {
		propertyChangeSupport.firePropertyChange("currentStepPosition", this.currentStepPosition, this.currentStepPosition = currentStepPosition);
	}

	public GUID getProcessIdStopped() {
		return processIdStopped;
	}

	public void setProcessIdStopped(GUID processIdStopped) {
		propertyChangeSupport.firePropertyChange("processIdStopped", 
				this.processIdStopped, this.processIdStopped = processIdStopped);
	}
	
	@Column(name="PARALLEL_EXECUTION")
	public boolean isParallelExecution() {
		return parallelExecution;
	}
	
	public void setParallelExecution(boolean parallelExecution) {
		propertyChangeSupport.firePropertyChange("parallelExecution", this.parallelExecution, this.parallelExecution = parallelExecution);
	}

	
	@Column(name="INCLUDE_LOCAL_PROCESSES")
	public boolean isIncludeLocalProcesses() {
		return includeLocalProcesses;
	}
	
	public void setIncludeLocalProcesses(boolean includeLocalProcesses) {
		propertyChangeSupport.firePropertyChange("includeLocalProcesses", this.includeLocalProcesses, this.includeLocalProcesses = includeLocalProcesses);
	}

	@Transient
	public boolean isInRunningState() {
		return state.equals(Status.RUNNING) || 
				state.equals(Status.STOPPED_AT_LOCAL_PROCESS) ||
				state.equals(Status.STOPPED_AT_MISSING_RUNSERVER) ||
				state.equals(Status.STOPPED_AT_SUSPENDED_STEP) ||
				state.equals(Status.STOPPED_AT_STEP_FINISHED_IN_ERROR) ||
				state.equals(Status.STOPPED_AT_WRONG_OWNERSHIP);
	}
	
	@Transient
	@Override
	public String getDiscriminator() {
		return DISCRIMINATOR;
	}
	
};