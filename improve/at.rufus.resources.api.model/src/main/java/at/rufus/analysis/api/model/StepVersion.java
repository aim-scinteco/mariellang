package at.rufus.analysis.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.ResourceVersion;

@javax.persistence.Entity
@Table(name="STEP_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(StepVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class StepVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;
	
	public static final String DISCRIMINATOR= AnalysisEntityTypes.STEP_VERSION;

	
	@Transient
	public Step getStep() {
		return (Step) getResource();
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return StepVersion.DISCRIMINATOR;
	}
	
}
