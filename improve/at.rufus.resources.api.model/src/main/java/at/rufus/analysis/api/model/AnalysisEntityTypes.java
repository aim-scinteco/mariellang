package at.rufus.analysis.api.model;

public class AnalysisEntityTypes {

	public static final String ANALYSIS_TREE_VERSION = "ATV";
	public static final String ANALYSIS_TREE = "AT";
	public static final String STEP_VERSION = "STV";
	public static final String STEP = "ST";
	public static final String PROCESS = "PRO";
	public static final String RUN = "RUN";
	public static final String PURGE = "PGE";
	
	public static final String TOOL = "TO";
	public static final String RUNSERVER = "RS";
	public static final String RUNSERVER_TOOL = "RST";
	
	public static final String COMPOSITE_STEP = "CST";
	public static final String COMPOSITE_STEP_VERSION = "CSTV";


	
}
