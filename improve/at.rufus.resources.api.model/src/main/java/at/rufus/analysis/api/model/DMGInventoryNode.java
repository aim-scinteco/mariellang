package at.rufus.analysis.api.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class DMGInventoryNode implements Serializable {
	private static final long serialVersionUID = 1L;
	public GUID id;
	public String name;
	public String nodeType;
	public String entityId;
	public String entityVersionId;
	public GUID targetId;
	public String targetEntityVersionId;
	public Timestamp revisionFromTime;
	public List<DMGInventoryNode> children = new ArrayList<DMGInventoryNode>();
	public List<String> parentEntityVersionIds = new ArrayList<String>();
	public List<GUID> parentResourceVersionIds = new ArrayList<GUID>();
}
