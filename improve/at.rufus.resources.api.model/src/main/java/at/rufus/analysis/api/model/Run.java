package at.rufus.analysis.api.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;
import at.rufus.resources.api.model.Revision;

@javax.persistence.Entity
@Table(name="RUN")
public class Run extends Entity implements RunStatusAware {
	
	private static final long serialVersionUID = 1L;
	public static final String DISCRIMINATOR = "RUN";

	public static final String AUTH_DOMAIN_RUN = "Run";
	
	public static final String PROP_PROCESS = "process";
	public static final String PROP_RUN_RESOURCES = "runResources";
	public static final String PROP_PARAMETERS = "parameters";
	public static final String PROP_RUN_PHASES = "phases";
	public static final String PROP_PARENT_RUN = "parentRun";
	public static final String PROP_SUB_RUN = "subRun";
	public static final String PROP_VARIABLES = "variables";
	public static final String PROP_GRID_ARGUMENTS = "gridArguments";
	
	
	@Override
	@Transient
	public String getDiscriminator() {
		return AnalysisEntityTypes.RUN;
	}
	
	
	@Column(name="RUN_GROUP_ID")
	private long runGroupId;
	
	/**
	 * Run number within step starting at 1.
	 */
	@Column(name="RUN_NR")
	private long runNr;
	
	@Column(name="RUN_STATUS")	
	private RunStatus runStatus;
	
	@Column(name="RUN_RESULT")
	private RunResult runResult;
	
	@Column(name="MANUAL_RESULT")
	private RunResult manualResult;
	
	@Column(name="GRID_PROVIDER")
	private GridProvider gridProvider;
	
	@Column(name="GRID_ARGS")
	private String gridArgs;
	
	@Column(name="GRID_COMMAND")
	private String gridCommand;
	
	@Column(name="GRID_JOBID")
	private String gridJobId;
	
	@Column(name="GRID_JOBNAME")
	private String gridJobName;
	
	@Column(name="RUN_WORKSPACE")
	private String runWorkspace;
	
	@Column(name="ISOLATED")
	private boolean isolated;
	
	@Column(name="REPRODUCIBLE")
	private boolean reproducible;
	
	@Column(name="TECHNICAL_USER")
	private String technicalUser;
	
	@Column(name="RESOLVED_TOOL_ARGS")
	private String resolvedToolArgs;
	
	@Column(name="COMMAND_LINE")
	private String commandLine;
	
	@Column(name="STDOUT_FILENAME")
	private String stdoutFileName;
	
	@Column(name="STDERR_FILENAME")
	private String stderrFileName;
	
	@Column(name="ENVREPORT_FILENAME")
	private String envReportFileName;

	@Column(name="STARTED_AT")
	private Timestamp startedAt;

	@Column(name="STOPPED_AT")
	private Timestamp stoppedAt;
	
	private Revision revisionStart;

	@Column(name="REVISION_START_TIME")
	private Timestamp revisionStartTime;
	
	private Revision revisionFinish;
	
	@Column(name="REVISION_FINISH_TIME")
	private Timestamp revisionFinishTime;
	
	@Column(name="RUNSERVER_URL")
	private String runserverUrl;
	
	@ManyToOne
	@JoinColumn(name="RUNSERVER_TOOL", referencedColumnName="ID")
	private RunserverTool runserverTool;
	
	@ManyToOne
	@JoinColumn(name="REPOSITORY_TOOL", referencedColumnName="ID")
	private Tool repositoryTool;
	
	@Column(name="GENERIC")
	private boolean generic;
	
	@Column(name="TOOL_COMMAND")
	private String toolCommand;
	
	@ManyToOne
	@JoinColumn(name="PROCESS", referencedColumnName="ID")
	private Process process;
	
	@OneToMany(mappedBy=RunResource.PROP_RUN)
	private List<RunResource> runResources = new TrackedList<RunResource>(propertyChangeSupport, PROP_RUN_RESOURCES);
	
	@ManyToOne
	@JoinColumn(name="EXECUTED_BY", referencedColumnName="ID")
	private Member executedBy;
	
	// run server local time when all resources are checked-out
	@Column(name = "CHECKOUT_TIME")
	private long checkoutTime;
	
	@Column(name="LOCAL_RUNSERVER")
	private boolean localRunserver;
	
	@Column(name="CANCELLED_AT")
	private Timestamp cancelledAt;
	
	@Column(name="CANCEL_ACTION")
	private RunCancelAction cancelAction;
	
	@Column(name="TOKEN")
	private String token;

	@Column(name="SSH_PUB_KEY_HASH")
	private String sshPublicKeyHash;
	
	@Column(name="PORT")
	private Integer port;

	@OneToMany(mappedBy=RunPhase.PROP_RUN)
	private List<RunPhase> phases = new TrackedList<RunPhase>(propertyChangeSupport, PROP_RUN_PHASES);
	
	@OneToMany(mappedBy=RunParameter.PROP_RUN)
	private List<RunParameter> parameters = new TrackedList<RunParameter>(propertyChangeSupport, PROP_PARAMETERS);
	
	@ManyToOne
	@JoinColumn(name="PARENT_RUN", referencedColumnName="ID")
	private Run parentRun;
	
	@OneToMany(mappedBy=Run.PROP_PARENT_RUN)
	private List<Run> subRuns = new TrackedList<Run>(propertyChangeSupport, PROP_SUB_RUN);
	
	@OneToMany(mappedBy=RunVariable.PROP_RUN)
	private List<RunVariable> variables = new TrackedList<RunVariable>(propertyChangeSupport, PROP_VARIABLES);
	
	@OneToMany(mappedBy="run")
	private List<RunGridArgument> gridArguments = new TrackedList<RunGridArgument>(propertyChangeSupport, PROP_GRID_ARGUMENTS);
	
	
	public Run() {
		super();
	}
	
	public Run(GUID runId) {
		super(runId);
	}

	public RunStatus getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(RunStatus runStatus) {
		propertyChangeSupport.firePropertyChange("runStatus", this.runStatus, this.runStatus = runStatus);
	}
	
	@Override
	public RunResult getRunResult() {
		return runResult;
	}
	
	public void setRunResult(RunResult runResult) {
		propertyChangeSupport.firePropertyChange("runResult", this.runResult, this.runResult = runResult);
	}
	
	public RunResult getManualResult() {
		return manualResult;
	}
	
	public void setManualResult(RunResult manualResult) {
		propertyChangeSupport.firePropertyChange("manualResult", this.manualResult, this.manualResult = manualResult);
	}
	
	public GridProvider getGridProvider() {
		return gridProvider;
	}
	
	public void setGridProvider(GridProvider gridProvider) {
		propertyChangeSupport.firePropertyChange("gridProvider", this.gridProvider, this.gridProvider = gridProvider);
	}
	
	public String getGridArgs() {
		return gridArgs;
	}
	
	public void setGridArgs(String gridArgs) {
		propertyChangeSupport.firePropertyChange("gridArgs", this.gridArgs, this.gridArgs = gridArgs);
	}
	
	public String getGridCommand() {
		return gridCommand;
	}
	
	public void setGridCommand(String gridCommand) {
		propertyChangeSupport.firePropertyChange("gridCommand", this.gridCommand, this.gridCommand = gridCommand);
	}
	
	public String getGridJobId() {
		return gridJobId;
	}

	public void setGridJobId(String gridJobId) {
		propertyChangeSupport.firePropertyChange("gridJobId", this.gridJobId, this.gridJobId = gridJobId);
	}
	
	public String getGridJobName() {
		return gridJobName;
	}
	
	public void setGridJobName(String gridJobName) {
		propertyChangeSupport.firePropertyChange("gridJobName", this.gridJobName, this.gridJobName = gridJobName);
	}
	
	public String getResolvedToolArgs() {
		return resolvedToolArgs;
	}
	
	public void setResolvedToolArgs(String resolvedToolArgs) {
		propertyChangeSupport.firePropertyChange("resolvedToolArgs", this.resolvedToolArgs, this.resolvedToolArgs = resolvedToolArgs);
	}
	
	public String getCommandLine() {
		return commandLine;
	}
	
	public void setCommandLine(String commandLine) {
		propertyChangeSupport.firePropertyChange("commandLine", this.commandLine, this.commandLine = commandLine);
	}
	
	public String getStdoutFileName() {
		return stdoutFileName;
	}
	
	public void setStdoutFileName(String stdoutFileName) {
		propertyChangeSupport.firePropertyChange("stdoutFileName", this.stdoutFileName, this.stdoutFileName = stdoutFileName);
	}
	
	public String getStderrFileName() {
		return stderrFileName;
	}
	
	public void setStderrFileName(String stderrFileName) {
		propertyChangeSupport.firePropertyChange("stderrFileName", this.stderrFileName, this.stderrFileName = stderrFileName);
	}
	
	public String getEnvReportFileName() {
		return envReportFileName;
	}
	
	public void setEnvReportFileName(String envReportFileName) {
		propertyChangeSupport.firePropertyChange("envReportFileName", this.envReportFileName, this.envReportFileName = envReportFileName);
	}
	
	@ManyToOne
	@JoinColumn(name="REVISION_START_ID", referencedColumnName="ID")
	public Revision getRevisionStart() {
		return revisionStart;
	}
	
	public void setRevisionStart(Revision revisionStart) {
		propertyChangeSupport.firePropertyChange("revisionStart", this.revisionStart, this.revisionStart = revisionStart);
	}
	
	public Timestamp getRevisionStartTime() {
		return revisionStartTime;
	}
	
	public void setRevisionStartTime(Timestamp revisionStartTime) {
		propertyChangeSupport.firePropertyChange("revisionStartTime", this.revisionStartTime, this.revisionStartTime = revisionStartTime);
	}
	
	@ManyToOne
	@JoinColumn(name="REVISION_FINISH_ID", referencedColumnName="ID")
	public Revision getRevisionFinish() {
		return revisionFinish;
	}
	
	public void setRevisionFinish(Revision revisionFinish) {
		propertyChangeSupport.firePropertyChange("revisionFinish", this.revisionFinish, this.revisionFinish = revisionFinish);
	}
	
	public Timestamp getRevisionFinishTime() {
		return revisionFinishTime;
	}
	
	public void setRevisionFinishTime(Timestamp revisionFinishTime) {
		propertyChangeSupport.firePropertyChange("revisionFinishTime", this.revisionFinishTime, this.revisionFinishTime = revisionFinishTime);
	}
	
	public Timestamp getStartedAt() {
		return startedAt;
	}
	
	public void setStartedAt(Timestamp startedAt) {
		propertyChangeSupport.firePropertyChange("startedAt", this.startedAt, this.startedAt = startedAt);
	}
	
	public Timestamp getStoppedAt() {
		return stoppedAt;
	}
	
	public void setStoppedAt(Timestamp stoppedAt) {
		propertyChangeSupport.firePropertyChange("stoppedAt", this.stoppedAt, this.stoppedAt = stoppedAt);
	}
	
	public Process getProcess() {
		return process;
	}
	
	public void setProcess(Process process) {
		propertyChangeSupport.firePropertyChange(PROP_PROCESS, this.process, this.process = process);
	}
	
	public long getRunGroupId() {
		return runGroupId;
	}
	
	public void setRunGroupId(long runGroupId) {
		propertyChangeSupport.firePropertyChange("runGroupId", this.runGroupId, this.runGroupId = runGroupId);
	}
	
	public long getRunNr() {
		return runNr;
	}
	
	public void setRunNr(long runNr) {
		propertyChangeSupport.firePropertyChange("runNr", this.runNr, this.runNr = runNr);
	}
	
	public List<RunResource> getRunResources() {
		return runResources;
	}
	
	public String getRunserverUrl() {
		return runserverUrl;
	}

	public void setRunserverUrl(String runserverUrl) {
		propertyChangeSupport.firePropertyChange("runserverUrl", this.runserverUrl, this.runserverUrl = runserverUrl);
	}
	
	public RunserverTool getRunserverTool() {
		return runserverTool;
	}
	
	public void setRunserverTool(RunserverTool runserverTool) {
		propertyChangeSupport.firePropertyChange("runserverTool", this.runserverTool, this.runserverTool = runserverTool);
	}
	
	public Tool getRepositoryTool() {
		return repositoryTool;
	}
	
	public void setRepositoryTool(Tool repositoryTool) {
		propertyChangeSupport.firePropertyChange("repositoryTool", this.repositoryTool, this.repositoryTool = repositoryTool);
	}

	public String getToolCommand() {
		return toolCommand;
	}

	public void setToolCommand(String toolCommand) {
		propertyChangeSupport.firePropertyChange("toolCommand", this.toolCommand, this.toolCommand = toolCommand);
	}
	
	public Member getExecutedBy() {
		return executedBy;
	}
	
	public void setExecutedBy(Member executedBy) {
		propertyChangeSupport.firePropertyChange("executedBy", this.executedBy, this.executedBy = executedBy);
	}
	
	public long getCheckoutTime() {
		return checkoutTime;
	}
	
	public void setCheckoutTime(long checkoutTime) {
		propertyChangeSupport.firePropertyChange("checkoutTime", this.checkoutTime, this.checkoutTime = checkoutTime);
	}
	
	public boolean isLocalRunserver() {
		return localRunserver;
	}
	
	public void setLocalRunserver(boolean localRunserver) {
		propertyChangeSupport.firePropertyChange("localRunserver", this.localRunserver, this.localRunserver = localRunserver);
	}	
	
	public String getRunWorkspace() {
		return runWorkspace;
	}
	
	public void setRunWorkspace(String runWorkspace) {
		propertyChangeSupport.firePropertyChange("runWorkspace", this.runWorkspace, this.runWorkspace = runWorkspace);
	}
	
	public boolean isIsolated() {
		return isolated;
	}
	
	public void setIsolated(boolean isolated) {
		propertyChangeSupport.firePropertyChange("isolated", this.isolated, this.isolated = isolated);
	}
	
	public boolean isReproducible() {
		return reproducible;
	}
	
	public void setReproducible(boolean reproducible) {
		propertyChangeSupport.firePropertyChange("reproducible", this.reproducible, this.reproducible = reproducible);
	}
	
	public String getTechnicalUser() {
		return technicalUser;
	}
	
	public void setTechnicalUser(String technicalUser) {
		propertyChangeSupport.firePropertyChange("technicalUser", this.technicalUser, this.technicalUser = technicalUser);
	}
	
	public Run getParentRun() {
		return parentRun;
	}
	
	public void setParentRun(Run parentRun) {
		propertyChangeSupport.firePropertyChange(PROP_PARENT_RUN, this.parentRun, this.parentRun = parentRun);
	}
	
	public List<Run> getSubRuns() {
		return subRuns;
	}
	
	public Timestamp getCancelledAt() {
		return cancelledAt;
	}
	
	public void setCancelledAt(Timestamp cancelledAt) {
		propertyChangeSupport.firePropertyChange("cancelledAt", this.cancelledAt, this.cancelledAt = cancelledAt);
	}
	
	public RunCancelAction getCancelAction() {
		return cancelAction;
	}
	
	public void setCancelAction(RunCancelAction cancelAction) {
		propertyChangeSupport.firePropertyChange("cancelAction", this.cancelAction, this.cancelAction = cancelAction);
	}
	
	public List<RunParameter> getParameters() {
		return parameters;
	}
	
	@Transient
	public RunParameter getParameter(GUID paramLovId) {
		for (RunParameter parameter : parameters) {
			if (parameter.getParamLov().equalsId(paramLovId)) {
				return parameter;
			}
		}
		return null;
	}
	
	public List<RunPhase> getPhases() {
		return phases;
	}
	
	public List<RunVariable> getVariables() {
		return variables;
	}

	public boolean isGeneric() {
		return generic;
	}

	public void setGeneric(boolean generic) {
		this.generic = generic;
	}
	
	public List<RunGridArgument> getGridArguments() {
		return gridArguments;
	}
	
	public void updateRunExecCancelled() {
		setRunStatus(RunStatus.FINISHED);
		setRunResult(RunResult.CANCELED);
		setCancelAction(RunCancelAction.CANCEL);
	}
	
	@Transient
	public boolean isRunExecCancelled() {
		return isRunExecCancelled(runStatus, runResult, cancelAction);
	}
	
	public static boolean isRunExecCancelled(RunStatus runStatus, RunResult runResult, RunCancelAction runCancelAction) {
		return runStatus == RunStatus.FINISHED && runResult == RunResult.CANCELED && runCancelAction == RunCancelAction.CANCEL;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		propertyChangeSupport.firePropertyChange("token", this.token, this.token = token);
	}
	
	public String getSshPublicKeyHash() {
		return sshPublicKeyHash;
	}
	
	public void setSshPublicKeyHash(String sshPublicKeyHash) {
		propertyChangeSupport.firePropertyChange("sshPublicKeyHash", this.sshPublicKeyHash, this.sshPublicKeyHash = sshPublicKeyHash);
	}
	
	public Integer getPort() {
		return port;
	}
	
	public void setPort(Integer port) {
		propertyChangeSupport.firePropertyChange("port", this.port, this.port = port);
	}
}
