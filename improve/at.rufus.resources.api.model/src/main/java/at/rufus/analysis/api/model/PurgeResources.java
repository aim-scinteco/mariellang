package at.rufus.analysis.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.Resource;
import at.rufus.resources.api.model.Revision;
import at.rufus.resources.api.model.UserVersion;

@javax.persistence.Entity
@Table(name="PURGE_RESOURCES")
public class PurgeResources extends Entity {

	private static final long serialVersionUID = 1L;

	public static final String PROP_PROCESS = "process";
	public static final String PROP_RUN_RESOURCES = "runResources";

	@Column(name="FOLDER_LEVEL")
	private int folderLevel;
	
	@Column(name="FILE_PATTERNS")
	private String filePatterns;
	
	@Column(name="FOLDER_PATTERNS")
	private String folderPatterns;
	
	@ManyToOne
	@JoinColumn(name="REVISION_ID", referencedColumnName="ID")
	private Revision revision;
	
	@ManyToOne
	@JoinColumn(name="PERFORM_USER", referencedColumnName="ID")
	private UserVersion performUser;
	
	@ManyToOne
	@JoinColumn(name="REVIEW_USER", referencedColumnName="ID")
	private UserVersion reviewUser;

	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	private Resource resource;
	
	
	public PurgeResources() {
		super();
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return AnalysisEntityTypes.PURGE;
	}
	
	public int getFolderLevel() {
		return folderLevel;
	}
	
	public void setFolderLevel(int folderLevel) {
		propertyChangeSupport.firePropertyChange("folderLevel", this.folderLevel, this.folderLevel = folderLevel);
	}
	
	public String getFilePatterns() {
		return filePatterns;
	}
	
	public void setFilePatterns(String filePatterns) {
		propertyChangeSupport.firePropertyChange("filePatterns", this.filePatterns, this.filePatterns = filePatterns);
	}
	
	public String getFolderPatterns() {
		return folderPatterns;
	}
	
	public void setFolderPatterns(String folderPatterns) {
		propertyChangeSupport.firePropertyChange("folderPatterns", this.folderPatterns, this.folderPatterns = folderPatterns);
	}
	
	public Revision getRevision() {
		return revision;
	}
	
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}

	public UserVersion getPerformUser() {
		return performUser;
	}
	
	public void setPerformUser(UserVersion performUser) {
		propertyChangeSupport.firePropertyChange("performUser", this.performUser, this.performUser = performUser);
	}
	
	public UserVersion getReviewUser() {
		return reviewUser;
	}
	
	public void setReviewUser(UserVersion reviewUser) {
		propertyChangeSupport.firePropertyChange("reviewUser", this.reviewUser, this.reviewUser = reviewUser);
	}
	
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}
	
}
