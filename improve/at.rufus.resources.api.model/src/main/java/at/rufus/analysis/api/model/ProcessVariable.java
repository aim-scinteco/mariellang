package at.rufus.analysis.api.model;

import java.math.BigDecimal;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.Resource;

@javax.persistence.Entity
@Table(name="PROCESS_VARIABLE")
public class ProcessVariable extends Entity {
	private static final long serialVersionUID = 1L;
	
	// variable name by convention
	public static final String VAR_DATASET = "dataset";
	public static final String VAR_COMMAND_FILE = "command-file";
	
	public static final String PROP_PROCESS = "process";
	public static final String PROP_POSITION = "position";
	public static final String PROP_NAME = "name";
	public static final String PROP_TYPE = "type";
	public static final String PROP_VALUE_RESOURCE = "valueResource";
	public static final String PROP_VALUE_TEXT = "valueText";
	public static final String PROP_VALUE_NUMBER = "valueNumber";
	
	public static final Comparator<ProcessVariable> COMPARE_BY_POSITION = new Comparator<ProcessVariable>() {
		@Override
		public int compare(ProcessVariable o1, ProcessVariable o2) {
			return o1.getPosition() - o2.getPosition();
		}
	};
	
	public static final String NAME_PATTERN = "[A-Za-z_][A-Za-z0-9_\\-]*";

	
	@ManyToOne
	@JoinColumn(name="PROCESS", referencedColumnName="ID")
	private Process process;

	@Column(name="POSITION")
	private int position;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="TYPE")
	private VariableType type;
	
	@Column(name="VALUE_TEXT")
	private String valueText;

	@Column(name="VALUE_NUMBER")
	private BigDecimal valueNumber;

	@ManyToOne
	@JoinColumn(name="VALUE_RESOURCE_NODE", referencedColumnName="ID")
	private Resource valueResource;
	
	
	
	public Process getProcess() {
		return process;
	}
	
	public void setProcess(Process process) {
		propertyChangeSupport.firePropertyChange(PROP_PROCESS, this.process, this.process = process);
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange(PROP_POSITION, this.position, this.position = position);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange(PROP_NAME, this.name, this.name = name);
	}
	
	public VariableType getType() {
		return type;
	}
	
	public void setType(VariableType type) {
		propertyChangeSupport.firePropertyChange(PROP_TYPE, this.type, this.type = type);
	}
	
	public String getValueText() {
		return valueText;
	}
	
	public void setValueText(String valueText) {
		propertyChangeSupport.firePropertyChange(PROP_VALUE_TEXT, this.valueText, this.valueText = valueText);
	}
	
	public BigDecimal getValueNumber() {
		return valueNumber;
	}
	
	public void setValueNumber(BigDecimal valueNumber) {
		propertyChangeSupport.firePropertyChange(PROP_VALUE_NUMBER, this.valueNumber, this.valueNumber = valueNumber);
	}
	
	/**
	 * Can be non-null but treated as unbound if the referenced resource is deleted.
	 */
	public Resource getValueResource() {
		return valueResource;
	}
	
	public void setValueResource(Resource valueResource) {
		propertyChangeSupport.firePropertyChange(PROP_VALUE_RESOURCE, this.valueResource, this.valueResource = valueResource);
	}
	
}
