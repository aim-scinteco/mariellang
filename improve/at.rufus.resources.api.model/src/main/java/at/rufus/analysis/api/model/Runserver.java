package at.rufus.analysis.api.model;

import java.beans.Transient;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;
import at.rufus.resources.api.model.Member;

@javax.persistence.Entity
@Table(name="RUNSERVER")
public class Runserver extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<Runserver> LABEL_COMPARATOR = new Comparator<Runserver>() {
		@Override
		public int compare(Runserver o1, Runserver o2) {
			return CmpUtil.cmpNatural(o1.getLabel(), o2.getLabel(), true);
		}
	};
	
	@Column(name="URL")
	private String url;
	
	@Column(name="SSH_PORT")
	private Integer sshPort;
	
	@Column(name="HOSTNAME")
	private String hostname;
	
	@Column(name="LABEL")
	private String label;
	
	@Column(name="POSITION")
	private Integer position;
	
	@Column(name="LOCAL")
	private boolean local;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="GENERIC")
	private boolean generic;
	
	@Column(name="REPRODUCIBLE")
	private boolean reproducible;
	
	@OneToOne
	@JoinColumn(name="OWNER", referencedColumnName="ID")
	private Member owner;
	
	@OneToMany(mappedBy="runserver")
	private List<RunserverTool> tools = new TrackedList<RunserverTool>(propertyChangeSupport, "tools");
	
	@OneToMany(mappedBy="runserver")
	private List<RunserverUserConfig> userConfigs = new TrackedList<RunserverUserConfig>(propertyChangeSupport, "userConfigs");
	
	
	public Runserver() {
		super();
	}
	
	public Runserver(GUID id) {
		super(id);
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		propertyChangeSupport.firePropertyChange("url", this.url, this.url = url);
	}
	
	public Integer getSshPort() {
		return sshPort;
	}
	
	public void setSshPort(Integer sshPort) {
		propertyChangeSupport.firePropertyChange("sshPort", this.sshPort, this.sshPort = sshPort);
	}
	
	public String getHostname() {
		return hostname;
	}
	
	public void setHostname(String hostname) {
		propertyChangeSupport.firePropertyChange("hostname", this.hostname, this.hostname = hostname);
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		propertyChangeSupport.firePropertyChange("label", this.label, this.label = label);
	}
	
	public Integer getPosition() {
		return position;
	}
	
	public void setPosition(Integer position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}
	
	public boolean isLocal() {
		return local;
	}
	
	public void setLocal(boolean local) {
		propertyChangeSupport.firePropertyChange("local", this.local, this.local = local);
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}

	public boolean isGeneric() {
		return generic;
	}

	public void setGeneric(boolean generic) {
		propertyChangeSupport.firePropertyChange("generic", this.generic, this.generic = generic);
	}
	
	public boolean isReproducible() {
		return reproducible;
	}
	
	public void setReproducible(boolean reproducible) {
		propertyChangeSupport.firePropertyChange("reproducible", this.reproducible, this.reproducible = reproducible);
	}
	
	public Member getOwner() {
		return owner;
	}
	
	public void setOwner(Member owner) {
		propertyChangeSupport.firePropertyChange("owner", this.owner, this.owner = owner);
	}

	public List<RunserverTool> getTools() {
		return tools;
	}
	
	public List<RunserverUserConfig> getUserConfigs() {
		return userConfigs;
	}
	
	@Transient
	public RunserverUserConfig getUserConfig(GUID userId) {
		for (RunserverUserConfig userConfig : userConfigs) {
			if (userConfig.getMember() != null && userId.equals(userConfig.getMember().getId())) {
				return userConfig;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getLabel());
		sb.append(" (").append(isLocal() ? "Local" : "Remote").append(")");
		return sb.toString();
	}
	
	@Transient
	public boolean isLocalRunserver(String hostname) {
		return local && this.hostname != null && this.hostname.equalsIgnoreCase(hostname) && !deleted && !generic;
	}
	
}
