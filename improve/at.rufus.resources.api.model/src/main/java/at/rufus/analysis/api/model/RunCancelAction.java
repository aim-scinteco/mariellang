package at.rufus.analysis.api.model;

/**
 * Actions to perform on run cancellation.
 */
public enum RunCancelAction {
	
	/**
	 * Clean-up run (and parent runs) after cancellation.
	 */
	CLEANUP,
	
	/**
	 * In addition, check-in run workspace after cancellation,
	 */
	CHECKIN,
	
	/**
	 * Suspend run.
	 */
	SUSPEND,
	
	/**
	 * Cancel run with no further actions (hard cancel).
	 */
	CANCEL;

}
