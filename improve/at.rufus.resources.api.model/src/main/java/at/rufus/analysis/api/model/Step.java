package at.rufus.analysis.api.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.resources.api.model.Resource;

@javax.persistence.Entity
@Table(name="STEP")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(Step.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Step extends Resource implements RunStatusAware {
	private static final long serialVersionUID = 1L;
	public static final String DISCRIMINATOR= AnalysisEntityTypes.STEP;
	
	public static final String PROP_RUN_STATUS = "runStatus";
	public static final String PROP_RUN_RESULT = "runResult";
	public static final String PROP_PARENT_STEP = "parentStep";
	public static final String PROP_CHILD_STEPS = "childSteps";
	public static final String PROP_COMPOSITE_STEPS = "compositeSteps";
	public static final String PROP_PROCESSES = "processes";
	public static final String PROP_LINK_TARGETS = "linkTargets";
	public static final String PROP_LINK_SOURCES = "linkSources";
	public static final String PROP_KEY_STEP = "keyStep";
	public static final String PROP_BASE_MODEL = "baseModel";
	public static final String PROP_FULL_MODEL = "fullModel";
	public static final String PROP_FINAL_MODEL = "finalModel";
	public static final String PROP_MAIN_RUN = "mainRun";
	public static final String PROP_RUN_WORKSPACE_MODIFICATIONS = "runWorkspaceModifications";
	
	public static final String COMMENT_TYPE_STEP_REVIEW = "STEP_REVIEW";
	
	@Column(name="RUN_STATUS")	
	private RunStatus runStatus;
	
	@Column(name="RUN_RESULT")
	private RunResult runResult;
	
	@Column(name="CHANGED")
	private boolean changed;
	
	@Column(name="KEY_STEP")
	private boolean keyStep;

	@Column(name="BASE_MODEL")
	private boolean baseModel;

	@Column(name="FULL_MODEL")
	private boolean fullModel;

	@Column(name="FINAL_MODEL")
	private boolean finalModel;
	
	@Column(name="SHOW")
	private boolean show = true;
	
	@Column(name="OFV")
	private BigDecimal ofv;
	
	@Column(name="OFV_SPECIAL")
	private String ofvSpecial;
	
	@Column(name="ISOLATED")
	private boolean isolated;
	
	@Column(name="NOTIFICATION")
	private boolean notification;
	
	@Column(name="SUSPEND_MARK_AT")
	private Timestamp suspendMarkAt;
	
	@ManyToOne
	@JoinColumn(name="PARENT_STEP", referencedColumnName="ID")
	private Step parentStep;
	
	@OneToMany(mappedBy=PROP_PARENT_STEP)
	private List<Step> childSteps = new TrackedList<Step>(propertyChangeSupport, PROP_CHILD_STEPS);
	
	@OneToMany(mappedBy=Process.PROP_STEP)
	private List<Process> processes = new TrackedList<Process>(propertyChangeSupport, PROP_PROCESSES);

	@ManyToOne
	@JoinColumn(name="CURRENT_RUN", referencedColumnName="ID")
	private Run currentRun;

	@ManyToOne
	@JoinColumn(name="MAIN_RUN", referencedColumnName="ID")
	private Run mainRun;

	@OneToMany(mappedBy=StepLink.PROP_SOURCE_STEP)
	private List<StepLink> linkSources = new TrackedList<StepLink>(propertyChangeSupport, PROP_LINK_SOURCES);

	@OneToMany(mappedBy=StepLink.PROP_TARGET_STEP)
	private List<StepLink> linkTargets = new TrackedList<StepLink>(propertyChangeSupport, PROP_LINK_TARGETS);
	
	@Column(name="STATE_TRANS_COUNT")
	private int stateTransitionCounter;

	@OneToMany(mappedBy=RunWsModification.PROP_STEP)
	private List<RunWsModification> runWorkspaceModifications = new TrackedList<RunWsModification>(propertyChangeSupport, PROP_RUN_WORKSPACE_MODIFICATIONS);
	
	
	@Transient
	private Process mainProcess;
	
	public RunStatus getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(RunStatus runStatus) {
		propertyChangeSupport.firePropertyChange(PROP_RUN_STATUS, this.runStatus, this.runStatus = runStatus);
	}
	
	@Override
	public RunResult getRunResult() {
		return runResult;
	}
	
	public void setRunResult(RunResult runResult) {
		propertyChangeSupport.firePropertyChange(PROP_RUN_RESULT, this.runResult, this.runResult = runResult);
	}
	
	public Step getParentStep() {
		return parentStep;
	}
	
	public void setParentStep(Step parentStep) {
		propertyChangeSupport.firePropertyChange(PROP_PARENT_STEP, this.parentStep, this.parentStep = parentStep);
	}
	
	public List<Step> getChildSteps() {
		return childSteps;
	}
	
	public List<Process> getProcesses() {
		return processes;
	}
	
	@Transient
	public Process getMainProcess() {
		if (mainProcess != null && mainProcess.getType() == ProcessType.main) {
			return mainProcess;
		}
		mainProcess = null;
		for (Process process : processes) {
			if (process.getType() == ProcessType.main) {
				mainProcess = process;
				break;
			}
		}
		return mainProcess;
	}
	
	public boolean isChanged() {
		return changed;
	}
	
	public void setChanged(boolean changed) {
		propertyChangeSupport.firePropertyChange("changed", this.changed, this.changed = changed);
	}
	
	public boolean isKeyStep() {
		return keyStep;
	}
	
	public void setKeyStep(boolean keyStep) {
		propertyChangeSupport.firePropertyChange(PROP_KEY_STEP, this.keyStep, this.keyStep = keyStep);
	}
	
	public boolean isBaseModel() {
		return baseModel;
	}
	
	public void setBaseModel(boolean baseModel) {
		propertyChangeSupport.firePropertyChange(PROP_BASE_MODEL, this.baseModel, this.baseModel = baseModel);
	}
	
	public boolean isFullModel() {
		return fullModel;
	}
	
	public void setFullModel(boolean fullModel) {
		propertyChangeSupport.firePropertyChange(PROP_FULL_MODEL, this.fullModel, this.fullModel = fullModel);
	}
	
	public boolean isFinalModel() {
		return finalModel;
	}
	
	public void setFinalModel(boolean finalModel) {
		propertyChangeSupport.firePropertyChange(PROP_FINAL_MODEL, this.finalModel, this.finalModel = finalModel);
	}
	
	public boolean isShow() {
		return show;
	}
	
	public void setShow(boolean show) {
		propertyChangeSupport.firePropertyChange("show", this.show, this.show = show);
	}
	
	public Run getCurrentRun() {
		return currentRun;
	}
	
	public void setCurrentRun(Run currentRun) {
		propertyChangeSupport.firePropertyChange("currentRun", this.currentRun, this.currentRun = currentRun);
	}
	
	public Run getMainRun() {
		return mainRun;
	}
	
	public void setMainRun(Run mainRun) {
		propertyChangeSupport.firePropertyChange(PROP_MAIN_RUN, this.mainRun, this.mainRun = mainRun);
	}
	
	public List<StepLink> getLinkSources() {
		return linkSources;
	}
	
	public List<StepLink> getLinkTargets() {
		return linkTargets;
	}
	
	public int getStateTransitionCounter() {
		return stateTransitionCounter;
	}

	public void setStateTransitionCounter(int stateTransitionCounter) {
		propertyChangeSupport.firePropertyChange("stateTransitionCounter", this.stateTransitionCounter, this.stateTransitionCounter = stateTransitionCounter);
	}
	
	public BigDecimal getOfv() {
		return ofv;
	}
	
	public void setOfv(BigDecimal ofv) {
		propertyChangeSupport.firePropertyChange("ofv", this.ofv, this.ofv = ofv);
	}
	
	public String getOfvSpecial() {
		return ofvSpecial;
	}
	
	public void setOfvSpecial(String ofvSpecial) {
		propertyChangeSupport.firePropertyChange("ofvSpecial", this.ofvSpecial, this.ofvSpecial = ofvSpecial);
	}
	
	public boolean isIsolated() {
		return isolated;
	}
	
	public void setIsolated(boolean isolated) {
		propertyChangeSupport.firePropertyChange("isolated", this.isolated, this.isolated = isolated);
	}
	
	public Timestamp getSuspendMarkAt() {
		return suspendMarkAt;
	}
	
	public void setSuspendMarkAt(Timestamp suspendMarkAt) {
		propertyChangeSupport.firePropertyChange("suspendMarkAt", this.suspendMarkAt, this.suspendMarkAt = suspendMarkAt);
	}
	
	public List<RunWsModification> getRunWorkspaceModifications() {
		return runWorkspaceModifications;
	}
	

	public boolean isNotification() {
		return notification;
	}

	public void setNotification(boolean notification) {
		propertyChangeSupport.firePropertyChange("notification", this.notification, this.notification = notification);
	}

	@Transient
	@Override
	public String getDiscriminator() {
		return DISCRIMINATOR;
	}

}
