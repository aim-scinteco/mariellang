package at.rufus.analysis.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="RUN_GRID_ARGUMENT")
public class RunGridArgument extends Entity {
	
private static final long serialVersionUID = 1L;
	
	@Column(name="TEXT_VALUE")
	private String textValue;
	
	@ManyToOne
	@JoinColumn(name="LOV_VALUE", referencedColumnName="ID")
	private GridArgumentLov lovValue;

	@Column(name="DATE_VALUE")
	private Timestamp dateValue;
	
	@ManyToOne
	@JoinColumn(name="RUN", referencedColumnName="ID")
	private Run run;

	@Column(name="PROVIDER")
	private GridProvider provider;
	
	@Column(name="TYPE")
	private GridArgumentType type;
	
	@Column(name="GRID_OPTION")
	private String gridOption;
	
	public Timestamp getDateValue() {
		return dateValue;
	}
	public void setDateValue(Timestamp dateValue) {
		propertyChangeSupport.firePropertyChange("dateValue", this.dateValue, this.dateValue = dateValue);
	}
	public GridProvider getProvider() {
		return provider;
	}
	public void setProvider(GridProvider provider) {
		propertyChangeSupport.firePropertyChange("provider", this.provider, this.provider = provider);
	}
	public GridArgumentType getType() {
		return type;
	}
	public void setType(GridArgumentType type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	public String getGridOption() {
		return gridOption;
	}
	public void setGridOption(String gridOption) {
		propertyChangeSupport.firePropertyChange("gridOption", this.gridOption, this.gridOption = gridOption);
	}
	public String getTextValue() {
		return textValue;
	}
	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.textValue, this.textValue = textValue);
	}
	public GridArgumentLov getLovValue() {
		return lovValue;
	}
	public void setLovValue(GridArgumentLov lovValue) {
		propertyChangeSupport.firePropertyChange("lovValue", this.lovValue, this.lovValue = lovValue);
	}
	public Run getRun() {
		return run;
	}
	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange("run", this.run, this.run = run);
	}
	
}
