package at.rufus.analysis.api.model;

import java.math.BigDecimal;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.ResourceVersion;

@javax.persistence.Entity
@Table(name="RUN_VARIABLE")
public class RunVariable extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_RUN = "run";
	public static final String PROP_POSITION = "position";
	public static final String PROP_NAME = "name";
	public static final String PROP_TYPE = "type";
	public static final String PROP_VALUE_RESOURCE_Version = "valueResourceVersion";
	public static final String PROP_VALUE_TEXT = "valueText";
	public static final String PROP_VALUE_NUMBER = "valueNumber";
	
	public static final Comparator<RunVariable> COMPARE_BY_POSITION = new Comparator<RunVariable>() {
		@Override
		public int compare(RunVariable o1, RunVariable o2) {
			return o1.getPosition() - o2.getPosition();
		}
	};

	
	@ManyToOne
	@JoinColumn(name="RUN", referencedColumnName="ID")
	private Run run;

	@Column(name="POSITION")
	private int position;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="TYPE")
	private VariableType type;
	
	@Column(name="VALUE_TEXT")
	private String valueText;

	@Column(name="VALUE_NUMBER")
	private BigDecimal valueNumber;

	@ManyToOne
	@JoinColumn(name="VALUE_RESOURCE_VERSION", referencedColumnName="ID")
	private ResourceVersion valueResourceVersion;
	

	public Run getRun() {
		return run;
	}
	
	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange(PROP_RUN, this.run, this.run = run);
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange(PROP_POSITION, this.position, this.position = position);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange(PROP_NAME, this.name, this.name = name);
	}
	
	public VariableType getType() {
		return type;
	}
	
	public void setType(VariableType type) {
		propertyChangeSupport.firePropertyChange(PROP_TYPE, this.type, this.type = type);
	}
	
	public String getValueText() {
		return valueText;
	}
	
	public void setValueText(String valueText) {
		propertyChangeSupport.firePropertyChange(PROP_VALUE_TEXT, this.valueText, this.valueText = valueText);
	}

	public BigDecimal getValueNumber() {
		return valueNumber;
	}
	
	public void setValueNumber(BigDecimal valueNumber) {
		propertyChangeSupport.firePropertyChange(PROP_VALUE_NUMBER, this.valueNumber, this.valueNumber = valueNumber);
	}
	
	public ResourceVersion getValueResourceVersion() {
		return valueResourceVersion;
	}
	
	public void setValueResourceVersion(ResourceVersion valueResourceVersion) {
		propertyChangeSupport.firePropertyChange(PROP_VALUE_RESOURCE_Version, this.valueResourceVersion, this.valueResourceVersion = valueResourceVersion);
	}
	
}
