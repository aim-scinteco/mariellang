package at.rufus.analysis.api.model;

public enum ToolScope {

	ToolCategory,
	Tool,
	RunserverTool,
	RunserverToolUser
	
}
