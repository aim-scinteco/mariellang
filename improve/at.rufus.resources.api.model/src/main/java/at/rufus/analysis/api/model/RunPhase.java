package at.rufus.analysis.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="RUN_PHASE")
public class RunPhase extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_RUN = "run";

	@ManyToOne
	@JoinColumn(name="RUN", referencedColumnName="ID")
	private Run run;
	
	@Column(name="POSITION")
	private int position;
	
	@Column(name="RUN_STATUS")
	private RunStatus runStatus;
	
	@Column(name="PHASE_RESULT")
	private RunResult phaseResult;

	@Column(name="STARTED_AT")
	private Timestamp startedAt;
	
	@Column(name="STOPPED_AT")
	private Timestamp stoppedAt;
	
	
	public Run getRun() {
		return run;
	}
	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange("run", this.run, this.run = run);
	}

	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}
	
	public RunStatus getRunStatus() {
		return runStatus;
	}
	public void setRunStatus(RunStatus runStatus) {
		propertyChangeSupport.firePropertyChange("runStatus", this.runStatus, this.runStatus = runStatus);
	}
	
	public RunResult getPhaseResult() {
		return phaseResult;
	}
	public void setPhaseResult(RunResult phaseResult) {
		propertyChangeSupport.firePropertyChange("phaseResult", this.phaseResult, this.phaseResult = phaseResult);
	}

	public Timestamp getStartedAt() {
		return startedAt;
	}
	public void setStartedAt(Timestamp startedAt) {
		propertyChangeSupport.firePropertyChange("startedAt", this.startedAt, this.startedAt = startedAt);
	}
	
	public Timestamp getStoppedAt() {
		return stoppedAt;
	}
	public void setStoppedAt(Timestamp stoppedAt) {
		propertyChangeSupport.firePropertyChange("stoppedAt", this.stoppedAt, this.stoppedAt = stoppedAt);
	}
	
}
