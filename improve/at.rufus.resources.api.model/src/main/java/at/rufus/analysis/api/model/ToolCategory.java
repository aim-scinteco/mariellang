package at.rufus.analysis.api.model;

import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="TOOL_CATEGORY")
public class ToolCategory extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<ToolCategory> POSITION_COMPARATOR = new Comparator<ToolCategory>() {
		@Override
		public int compare(ToolCategory o1, ToolCategory o2) {
			return Integer.compare(o1.getPosition(), o2.getPosition());
		}
	};
	
	public static final Comparator<ToolCategory> NAME_COMPARATOR = new Comparator<ToolCategory>() {
		@Override
		public int compare(ToolCategory o1, ToolCategory o2) {
			return CmpUtil.cmp(o1.getName(), o2.getName());
		}
	};
	
	public static final int MAX_LENGTH_NAME = 50;
	
	public ToolCategory() {
		super();
	}
	
	public ToolCategory(GUID id) {
		super(id);
	}

	@Column(name="IDENTIFIER")
	private String identifier;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="POSITION")
	private int position;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@OneToMany(mappedBy="category")
	private List<Tool> tools = new TrackedList<Tool>(propertyChangeSupport, "tools");

	@OneToMany(mappedBy="category")
	private List<ToolParameter> parameters = new TrackedList<ToolParameter>(propertyChangeSupport, "parameters");
	
	@OneToMany(mappedBy="category")
	private List<ToolGridArgument> gridArguments = new TrackedList<ToolGridArgument>(propertyChangeSupport, "gridArguments");
	
	@OneToMany(mappedBy="category")
	private List<ToolVariable> variables = new TrackedList<ToolVariable>(propertyChangeSupport, "variables");
	
	@OneToMany(mappedBy="category")
	private List<ToolCategoryUserConfig> userConfigs = new TrackedList<ToolCategoryUserConfig>(propertyChangeSupport, "userConfigs");
	
	
	public String getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(String identifier) {
		propertyChangeSupport.firePropertyChange("identifier", this.identifier, this.identifier = identifier);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	
	public List<Tool> getTools() {
		return tools;
	}
	
	public List<ToolParameter> getParameters() {
		return parameters;
	}
	
	@Transient
	public ToolParameter getParameter(GUID paramLovId) {
		for (ToolParameter parameter : parameters) {
			if (parameter.getParamLov().equalsId(paramLovId)) {
				return parameter;
			}
		}
		return null;
	}		
	
	public List<ToolGridArgument> getGridArguments() {
		return gridArguments;
	}
	
	public List<ToolVariable> getVariables() {
		return variables;
	}
	
	public List<ToolCategoryUserConfig> getUserConfigs() {
		return userConfigs;
	}
	
	@Transient
	public ToolCategoryUserConfig getUserConfig(GUID userId) {
		for (ToolCategoryUserConfig userConfig : userConfigs) {
			if (userConfig.getMember() != null && userId.equals(userConfig.getMember().getId())) {
				return userConfig;
			}
		}
		return null;
	}
	
}
