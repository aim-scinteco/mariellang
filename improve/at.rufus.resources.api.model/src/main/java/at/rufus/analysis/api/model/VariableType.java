package at.rufus.analysis.api.model;

public enum VariableType {

	fileRef("File Reference"),
	filePath("File Path");
	
	private String label;
	
	private VariableType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
