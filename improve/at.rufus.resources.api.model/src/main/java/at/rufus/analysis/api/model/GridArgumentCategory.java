package at.rufus.analysis.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="GRID_ARGUMENT_CATEGORY")
public class GridArgumentCategory extends Entity {

private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;
	
	
	@OneToMany(mappedBy="category")
	private List<GridArgumentLov> values = new TrackedList<>(propertyChangeSupport, "values");
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public List<GridArgumentLov> getValues() {
		return values;
	}
	
	
}
