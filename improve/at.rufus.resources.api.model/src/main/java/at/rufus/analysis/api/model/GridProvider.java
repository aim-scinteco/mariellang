package at.rufus.analysis.api.model;

public enum GridProvider {
	
	LSF("LSF", "lsf"),
	SLURM("Slurm", "slurm"),
	SGE("SGE", "SGE"),
	DOCKER("Docker", "docker"),
	REST("REST", "rest");
	
	private final String label;
	private final String configKey;
	
	private GridProvider(String label, String configKey) {
		this.label = label;
		this.configKey = configKey;
	}
	
	public String getLabel() {
		return label;
	}
	public String getConfigKey() {
		return configKey;
	}
	
	public static GridProvider valueOfConfigKey(String configKey) {
		if (configKey == null) return null;
		for (GridProvider p : values()) {
			if (p.getConfigKey().equalsIgnoreCase(configKey)) {
				return p;
			}
		}
		return null;
	}
}
