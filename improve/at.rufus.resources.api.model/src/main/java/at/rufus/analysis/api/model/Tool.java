package at.rufus.analysis.api.model;

import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="TOOL")
public class Tool extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<Tool> NAME_COMPARATOR = new Comparator<Tool>() {
		@Override
		public int compare(Tool o1, Tool o2) {
			return CmpUtil.cmpNatural(o1.getName(), o2.getName(), true);
		}
	};
	
	public static final Comparator<Tool> CATEGORY_POS_AND_NAME_COMPARATOR = new Comparator<Tool>() {
		@Override
		public int compare(Tool o1, Tool o2) {
			ToolCategory c1 = (ToolCategory) o1.getCategory();
			ToolCategory c2 = (ToolCategory) o2.getCategory();
			int rc = Integer.compare(c1 != null ? c1.getPosition() : 0, c2 != null ? c2.getPosition() : 0);
			if (rc == 0) {
				rc = CmpUtil.cmpNatural(o1.getName(), o2.getName(), true);
			}
			return rc;
		}
	};
	
	public static final int MAX_LENGTH_NAME = 50;
	
	public Tool() {
		super();
	}
	
	public Tool(GUID id) {
		super(id);
	}
	
	@Column(name="IDENTIFIER")
	private String identifier;
	
	@Column(name="NAME")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private ToolCategory category;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="REPO_TOOL")
	private boolean repoTool;
	
	@OneToMany(mappedBy="tool")
	private List<ToolParameter> parameters = new TrackedList<ToolParameter>(propertyChangeSupport, "parameters");
	
	@OneToMany(mappedBy="tool")
	private List<ToolGridArgument> gridArguments = new TrackedList<ToolGridArgument>(propertyChangeSupport, "gridArguments");
	
	@OneToMany(mappedBy="tool")
	private List<ToolVariable> variables = new TrackedList<ToolVariable>(propertyChangeSupport, "variables");
	
	@OneToMany(mappedBy="tool")
	private List<ToolUserConfig> userConfigs = new TrackedList<ToolUserConfig>(propertyChangeSupport, "userConfigs");
	
	
	public String getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(String identifier) {
		propertyChangeSupport.firePropertyChange("identifier", this.identifier, this.identifier = identifier);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public ToolCategory getCategory() {
		return category;
	}
	
	public void setCategory(ToolCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	
	public boolean isRepoTool() {
		return repoTool;
	}
	
	public void setRepoTool(boolean repoTool) {
		propertyChangeSupport.firePropertyChange("repoTool", this.repoTool, this.repoTool = repoTool);
	}	
	
	public List<ToolParameter> getParameters() {
		return parameters;
	}
	
	@Transient
	public ToolParameter getParameter(GUID paramLovId) {
		for (ToolParameter parameter : parameters) {
			if (parameter.getParamLov().equalsId(paramLovId)) {
				return parameter;
			}
		}
		return null;
	}
	
	public List<ToolGridArgument> getGridArguments() {
		return gridArguments;
	}
	
	public List<ToolVariable> getVariables() {
		return variables;
	}
	
	public List<ToolUserConfig> getUserConfigs() {
		return userConfigs;
	}
	
	@Transient
	public ToolUserConfig getUserConfig(GUID userId) {
		for (ToolUserConfig userConfig : userConfigs) {
			if (userConfig.getMember() != null && userId.equals(userConfig.getMember().getId())) {
				return userConfig;
			}
		}
		return null;
	}

}
