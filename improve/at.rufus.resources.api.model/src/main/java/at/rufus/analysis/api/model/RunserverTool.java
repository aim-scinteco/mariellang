package at.rufus.analysis.api.model;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="RUNSERVER_TOOL")
public class RunserverTool extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<RunserverTool> NAME_COMPARATOR = new Comparator<RunserverTool>() {
		@Override
		public int compare(RunserverTool o1, RunserverTool o2) {
			return CmpUtil.cmpNatural(o1.getName(), o2.getName(), true);
		}
	};
	
	public static final Comparator<RunserverTool> RUNSERVER_THEN_TOOL_COMPARATOR = new Comparator<RunserverTool>() {
		@Override
		public int compare(RunserverTool o1, RunserverTool o2) {
			Runserver r1 = o1.getRunserver();
			Runserver r2 = o2.getRunserver();
			int rc = CmpUtil.cmpNatural(r1 != null ? r1.getLabel() : null, r2 != null ? r2.getLabel() : null, true);
			if (rc == 0) {
				rc = CmpUtil.cmpNatural(o1.getName(), o2.getName(), true);
			}
			return rc;
		}
	};
	
	public static final int MAX_LENGTH_NAME = 50;
	
	public RunserverTool() {
		super();
	}
	
	public RunserverTool(GUID id) {
		super(id);
	}

	@ManyToOne
	@JoinColumn(name="RUNSERVER", referencedColumnName="ID")
	private Runserver runserver;
	
	@ManyToOne
	@JoinColumn(name="TOOL", referencedColumnName="ID")
	private Tool tool;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="COMMAND")
	private String command;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="GRID_PROVIDER")
	private GridProvider gridProvider;
	
	@Column(name = "PREFERRED")
	private boolean preferred;
	
	@Column(name = "QUICKLAUNCH")
	private boolean quicklaunch;
	
	@Column(name = "HIDDEN")
	private boolean hidden;
	
	@OneToMany(mappedBy="runserverTool")
	private List<ToolParameter> parameters = new TrackedList<ToolParameter>(propertyChangeSupport, "parameters");
	
	@OneToMany(mappedBy="runserverTool")
	private List<ToolGridArgument> gridArguments = new TrackedList<ToolGridArgument>(propertyChangeSupport, "gridArguments");
	
	@OneToMany(mappedBy="runserverTool")
	private List<RunserverToolUserConfig> userConfigs = new TrackedList<RunserverToolUserConfig>(propertyChangeSupport, "userConfigs");
	
	@OneToMany(mappedBy="runserverTool")
	private List<ToolVariable> variables = new TrackedList<ToolVariable>(propertyChangeSupport, "variables");
	
	public Runserver getRunserver() {
		return runserver;
	}
	
	public void setRunserver(Runserver runserver) {
		propertyChangeSupport.firePropertyChange("runserver", this.runserver, this.runserver = runserver);
	}
	
	public Tool getTool() {
		return tool;
	}
	
	public void setTool(Tool tool) {
		propertyChangeSupport.firePropertyChange("tool", this.tool, this.tool = tool);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		propertyChangeSupport.firePropertyChange("command", this.command, this.command = command);
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	
	public GridProvider getGridProvider() {
		return gridProvider;
	}
	
	public void setGridProvider(GridProvider gridProvider) {
		propertyChangeSupport.firePropertyChange("gridProvider", this.gridProvider, this.gridProvider = gridProvider);
	}
	
	public boolean isPreferred() {		
		return preferred;
	}

	public void setPreferred(boolean preferred) {
		propertyChangeSupport.firePropertyChange("preferred", this.preferred, this.preferred = preferred);
	}
	
	public boolean isQuicklaunch() {		
		return quicklaunch;
	}

	public void setQuicklaunch(boolean quicklaunch) {
		propertyChangeSupport.firePropertyChange("quicklaunch", this.quicklaunch, this.quicklaunch = quicklaunch);
	}
	
	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		propertyChangeSupport.firePropertyChange("hidden", this.hidden, this.hidden = hidden);
	}
	
	public List<ToolParameter> getParameters() {
		return parameters;
	}
	
	@Transient
	public ToolParameter getParameter(GUID paramLovId, GUID userId) {
		ToolParameter defaultParameter = null;
		ToolParameter userParameter = null;
		for (ToolParameter parameter : parameters) {
			if (parameter.getParamLov().equalsId(paramLovId)) {
				if (parameter.getMember() == null) {
					defaultParameter = parameter;
				} else if (userId != null && userId.equals(parameter.getMember().getId())) {
					userParameter = parameter;
				}
			}
		}
		return userParameter != null ? userParameter : defaultParameter;
	}
	
	public List<ToolGridArgument> getGridArguments() {
		return gridArguments;
	}
	
	@Transient
	public ToolGridArgument getGridArgument(GUID gridArgDescId, GUID userId) {
		ToolGridArgument defaultGridArg = null;
		ToolGridArgument userGridArg = null;
		for (ToolGridArgument gridArgument : gridArguments) {
			if (gridArgument.getDescriptor() != null && gridArgument.getDescriptor().getId().equals(gridArgDescId)) {
				if (gridArgument.getMember() == null) {
					defaultGridArg = gridArgument;
				} else if (userId != null && userId.equals(gridArgument.getMember().getId())) {
					userGridArg = gridArgument;
				}
			}
		}
		return userGridArg != null ? userGridArg : defaultGridArg;
	}
	
	@Transient
	public List<ToolGridArgument> getGridArguments(GUID userId) {
		RunserverToolUserConfig userConfig = getUserConfig(userId);
		if (userConfig == null) {
			return gridArguments;
		}
		if (!userConfig.isGridArgsSet()) {
			return gridArguments;
		}
		List<ToolGridArgument> filtered = new ArrayList<>(gridArguments.size());
		for (ToolGridArgument toolGridArg : gridArguments) {
			if (userId != null && toolGridArg.getMember() != null && userId.equals(toolGridArg.getMember().getId())) {
				filtered.add(toolGridArg);
			}
		}
		return filtered;
	}
	
	public List<RunserverToolUserConfig> getUserConfigs() {
		return userConfigs;
	}
	
	@Transient
	public RunserverToolUserConfig getUserConfig(GUID userId) {
		for (RunserverToolUserConfig userConfig : userConfigs) {
			if (userConfig.getMember() != null && userId.equals(userConfig.getMember().getId())) {
				return userConfig;
			}
		}
		return null;
	}
	
	public List<ToolVariable> getVariables() {
		return variables;
	}
	
	@Transient
	public ToolVariable getVariable(String name, GUID userId) {
		ToolVariable defaultVariable = null;
		ToolVariable userVariable = null;
		for (ToolVariable variable : variables) {
			if (variable.getName() != null && variable.getName().equals(name)) {
				if (variable.getMember() == null) {
					defaultVariable = variable;
				} else if (userId != null && userId.equals(variable.getMember().getId())) {
					userVariable = variable;
				}
			}
		}
		return userVariable != null ? userVariable : defaultVariable;
	}
	
	@Transient
	public List<ToolVariable> getVariables(GUID userId) {
		RunserverToolUserConfig userConfig = getUserConfig(userId);
		if (userConfig == null) {
			return variables;
		}
		if (!userConfig.isVariablesSet()) {
			return variables;
		}
		List<ToolVariable> filtered = new ArrayList<>(variables.size());
		for (ToolVariable toolVariable : variables) {
			if (userId != null && toolVariable.getMember() != null && userId.equals(toolVariable.getMember().getId())) {
				filtered.add(toolVariable);
			}
		}
		return filtered;
	}

	
}
