package at.rufus.analysis.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="COMPOSITE_STEP_RELATION")
public class CompositeStepAssignment extends Entity {
	
	public static enum StartConstraint {
		MissingLocalRunserver,
		WrongOwnership,
		LocalRunServerWithParallelExecution,
		StepDeleted, 
		UnboundVariables
	}

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="STEP", referencedColumnName="ID")
	private Step step;
	
	@ManyToOne
	@JoinColumn(name="COMPOSITE_STEP", referencedColumnName="ID")
	private CompositeStep compositeStep;
	
	@Column(name="POS")
	private int position;
	
	@Transient
	private String detailStatusLabel;
	
	@Transient
	private List<StartConstraint> startConstraints;
	
	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public CompositeStep getCompositeStep() {
		return compositeStep;
	}

	public void setCompositeStep(CompositeStep compositeStep) {
		this.compositeStep = compositeStep;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getDetailStatusLabel() {
		return detailStatusLabel;
	}

	public void setDetailStatusLabel(String detailStatusLabel) {
		this.detailStatusLabel = detailStatusLabel;
	}
	
	public List<StartConstraint> getStartConstraints() {
		if (startConstraints == null) {
			startConstraints = new ArrayList<CompositeStepAssignment.StartConstraint>();
		}
		return startConstraints;
	}
	
	public void setStartConstraints(List<StartConstraint> constraints) {
		this.startConstraints = constraints;
	}
	
	
}
