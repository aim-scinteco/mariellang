package at.rufus.analysis.api.model;

public enum GridArgumentType {

	TEXT("Text"),
	DATE_TIME("Date and Time"),
	LOV("Picklist"),
	EMPTY("Empty");
	
	private final String label;
	
	private GridArgumentType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
