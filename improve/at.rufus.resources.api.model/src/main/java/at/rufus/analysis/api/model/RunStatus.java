package at.rufus.analysis.api.model;

import java.util.ArrayList;
import java.util.List;

public enum RunStatus {
	
	
	INITIAL(RunStatusFlags.INITIAL | RunStatusFlags.PRE_RUNNING  | RunStatusFlags.PRE_CHECKIN), 
	ACCEPTED(RunStatusFlags.INITIAL | RunStatusFlags.PRE_RUNNING  | RunStatusFlags.PRE_CHECKIN | RunStatusFlags.CANCELABLE),
	QUEUED(RunStatusFlags.IN_PROGRESS | RunStatusFlags.PRE_RUNNING  | RunStatusFlags.PRE_CHECKIN | RunStatusFlags.CANCELABLE), 
	CHECKOUT_SUSPENDED(RunStatusFlags.IN_PROGRESS),
	CHECKOUT(RunStatusFlags.IN_PROGRESS | RunStatusFlags.PRE_RUNNING  | RunStatusFlags.PRE_CHECKIN | RunStatusFlags.CANCELABLE),
	RUNNING(RunStatusFlags.IN_PROGRESS  | RunStatusFlags.PRE_CHECKIN | RunStatusFlags.CANCELABLE),
	SUSPENDED(RunStatusFlags.IN_PROGRESS),
	RUN_SUB(RunStatusFlags.IN_PROGRESS | RunStatusFlags.PRE_CHECKIN),
	CHECKIN(RunStatusFlags.IN_PROGRESS),
	FINISHED(RunStatusFlags.END); 
	
	private final int flags;
	
	RunStatus(int flags) {
		this.flags = flags;
	}

	public boolean isCreateRunPhase() {
		return !isEndState() && this != INITIAL;
	}
	
	public boolean isEndState() {
		return (flags & RunStatusFlags.END) != 0;
	}
	
	public boolean isInProgress() {
		return (flags & RunStatusFlags.IN_PROGRESS) != 0;
	}

	public boolean isCancelable() {
		return (flags & RunStatusFlags.CANCELABLE) != 0;
	}

	public boolean isInitial() {
		return this == INITIAL;
	}

	public boolean isPreRunning() {
		return (flags & RunStatusFlags.PRE_RUNNING) != 0;
	}

	public boolean isPreCheckin() {
		return (flags & RunStatusFlags.PRE_CHECKIN) != 0;
	}
	
	public boolean isRunnable() {
		return this == INITIAL || this == FINISHED;
	}
	
	public static List<RunStatus> allWithFlag(int flag) {
		RunStatus[] all = RunStatus.values();
		List<RunStatus> result = new ArrayList<RunStatus>(all.length);
		for (RunStatus status : all) {
			if ((status.flags & flag) == flag) {
				result.add(status);
			}
		}
		return result;
	}
	
	public static List<RunStatus> allBut(RunStatus... excludedStatis) {
		RunStatus[] all = RunStatus.values();
		List<RunStatus> result = new ArrayList<RunStatus>(all.length);
		for (RunStatus status : all) {
			boolean excluded = false;
			for (RunStatus excludedStatus : excludedStatis) {
				if (excludedStatus == status) {
					excluded = true;
					break;
				}
			}
			if (!excluded) {
				result.add(status);
			}
		}
		return result;
	}
	
	public boolean in(RunStatus... runStatuses) {
		for (RunStatus runStatus : runStatuses) {
			if (runStatus == this) {
				return true;
			}
		}
		return false;
	}
	
}
