package at.rufus.analysis.api.model;

import java.io.Serializable;

import at.rufus.base.api.model.GUID;

public class RunserverRegistration implements Serializable {
	private static final long serialVersionUID = 1L;

	private GUID runserverId;
	/**
	 * <code>true</code> when the runserver was not registered and has now been registered. <code>false</code> when the runserver already was registered.
	 */
	private boolean newlyRegistered;
	
	public RunserverRegistration() {
	}
	
	public GUID getRunserverId() {
		return runserverId;
	}
	public void setRunserverId(GUID runserverId) {
		this.runserverId = runserverId;
	}

	public boolean isNewlyRegistered() {
		return newlyRegistered;
	}
	public void setNewlyRegistered(boolean newlyRegistered) {
		this.newlyRegistered = newlyRegistered;
	}
	
}
