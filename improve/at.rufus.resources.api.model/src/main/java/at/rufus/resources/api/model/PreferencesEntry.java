package at.rufus.resources.api.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="PREFERENCES_ENTRY")
public class PreferencesEntry extends Entity {
	private static final long serialVersionUID = 1L;
	
	@Column(name="KEY")
	private String key;
	
	@Column(name="NUMERIC_VALUE")
	private BigDecimal numericValue;
	
	@Column(name="TEXT_VALUE")
	private String textValue;
	
	@Column(name="PREF_GROUP_ID")
	private GUID preferencesGroupId;
	
	@ManyToOne
	@JoinColumn(name="MEMBER_ID", referencedColumnName="ID")
	private Member member;
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		propertyChangeSupport.firePropertyChange("key", this.key, this.key = key);
	}
	
	public String getTextValue() {
		return textValue;
	}
	
	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.key, this.textValue = textValue);
	}
	
	public BigDecimal getNumericValue() {
		return numericValue;
	}
	
	public void setNumericValue(BigDecimal numericValue) {
		propertyChangeSupport.firePropertyChange("numericValue", this.numericValue, this.numericValue = numericValue);
	}
	
	@Transient
	public Boolean getBooleanValue() {
		return numericValue != null && numericValue.compareTo(BigDecimal.ONE) == 0; 
	}
	
	@Transient
	public void setBooleanValue(Boolean value) {
		setNumericValue(value != null && value ? BigDecimal.ONE : BigDecimal.ZERO);
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}
	
	public GUID getPreferencesGroupId() {
		return preferencesGroupId;
	}
	
	public void setPreferencesGroupId(GUID preferencesGroupId) {
		propertyChangeSupport.firePropertyChange("preferencesGroupId", this.preferencesGroupId, this.preferencesGroupId = preferencesGroupId);
	}

}
