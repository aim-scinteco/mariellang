package at.rufus.resources.api.model;

/**
 * Describes the status of a "latest" file.
 */
public enum FileStatus {
	
	/**
	 * {@link FileVersion} created but no upload started yet.
	 * On server-side a working file with 0 bytes is created but 
	 * no physical file version exists yet.
	 * <br>
	 * Next possible status: <strong>Uploading</strong> or <strong>Incomplete</strong> 
	 * or <strong>Complete</strong>
	 */
	Created,
	
	/**
	 * The file upload started and is still in progress. On 
	 * server-side the working file is updated permanently 
	 * but still no physical file version exists yet.
	 * <br>
	 * Next possible status: <strong>Complete</strong> or <strong>Incomplete</strong>
	 */
	Uploading,
	
	/**
	 * The file upload was cancelled or an error occurred during 
	 * the upload.
	 * <br>
	 * Next possible status: <strong>Complete</strong> or <strong>Uploading</strong>
	 */
	Incomplete,
	
	/**
	 * The file was uploaded completely or the (previous incomplete) 
	 * file was accepted as complete.
	 * <br>
	 * Next possible status: <strong>-</strong>
	 */
	Complete;
	
	public static boolean isEndStatus(FileStatus status) {
		return status == Complete || status == Incomplete;
	}
	
}
