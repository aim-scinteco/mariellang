package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="METADATA_DESC")
public class MetadataDescriptor extends Entity  {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_NAME = "name";
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="LABEL")
	private String label;
	
	@Column(name="SCOPE")
	private String scope;
	
	@Column(name="TYPE")
	private MetadataType type;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private MetadataCategory category;
	
	@Column(name="INIT_AVAILABLE")
	private boolean initial;
	
	@Column(name="STRATEGY")
	private MetadataStrategy strategy;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		propertyChangeSupport.firePropertyChange(PROP_NAME, this.name, this.name = name);
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		propertyChangeSupport.firePropertyChange("label", this.label, this.label = label);
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		propertyChangeSupport.firePropertyChange("scope", this.scope, this.scope = scope);
	}

	public MetadataType getType() {
		return type;
	}

	public void setType(MetadataType type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}

	public MetadataCategory getCategory() {
		return category;
	}

	public void setCategory(MetadataCategory category) {
		propertyChangeSupport.firePropertyChange("category", this.category, this.category = category);
	}
	
	public boolean isInitial() {
		return initial;
	}
	
	public void setInitial(boolean initial) {
		propertyChangeSupport.firePropertyChange("initial", this.initial, this.initial = initial);
	}
	
	public MetadataStrategy getStrategy() {
		return strategy;
	}
	
	public void setStrategy(MetadataStrategy strategy) {
		propertyChangeSupport.firePropertyChange("strategy", this.strategy, this.strategy = strategy);
	}
	
}
