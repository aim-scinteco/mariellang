package at.rufus.resources.api.model;

import java.io.Serializable;

public class ExternalLinkContent implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	String externalServiceErrorMessage;
	byte[] content;
	
	public String getExternalServiceErrorMessage() {
		return externalServiceErrorMessage;
	}
	public void setExternalServiceErrorMessage(String externalServiceErrorMessage) {
		this.externalServiceErrorMessage = externalServiceErrorMessage;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	
	

}
