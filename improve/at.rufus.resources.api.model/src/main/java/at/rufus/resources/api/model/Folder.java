package at.rufus.resources.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="FOLDER")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(RepositoryEntityTypes.FOLDER)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Folder extends Resource {
	private static final long serialVersionUID = 1L;

	@Transient
	@Override
	public String getDiscriminator() {
		return RepositoryEntityTypes.FOLDER;
	}

}
