package at.rufus.resources.api.model;

public class RepositoryEntityTypes {

	public static final String RESOURCE_NODE_VERSION = "RNV";
	public static final String RESOURCE_NODE = "RN";
	public static final String FILE_VERSION = "FIV";
	public static final String FILE = "FI";
	public static final String FOLDER_VERSION = "FOV";
	public static final String FOLDER = "FO";
	public static final String RESOURCE_LINK_VERSION = "LIV";
	public static final String RESOURCE_LINK = "LI";
	public static final String REVIEW_VERSION = "RVV";
	public static final String REVIEW = "RV";
	public static final String APP_GROUP = "GR";
	public static final String APP_MEMBER = "AU";
	public static final String REVIEW_ENTRY = "RVE";
	public static final String APP_USER_VERSION = "AUV";
	public static final String SYSTEM_MESSAGE = "SME";
	public static final String METADATA = "MD";
	public static final String PROCESS_VARIABLE = "PV";
	public static final String EXT_RESOURCE_LINK = "EL";
	public static final String EXT_RESOURCE_LINK_VERSION = "ELV";
	public static final String PURGE = "PU";
	public static final String PURGE_VERSION = "PUV";
	public static final String PURGE_ENTRY = "PUE";
}

