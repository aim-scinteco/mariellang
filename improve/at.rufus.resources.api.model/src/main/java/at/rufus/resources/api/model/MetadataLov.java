package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="METADATA_LOV")
public class MetadataLov extends Entity  {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_CATEGORY = "category";
	public static final String PROP_POSITION = "position";
	public static final String PROP_TEXT = "text";
	public static final String PROP_EXPIRED = "expired";
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName="ID")
	private MetadataCategory category;

	@Column(name="POSITION")
	private int position;
	
	@Column(name="TEXT")
	private String text;
	
	@Column(name="EXPIRED")
	private boolean expired;


	public MetadataCategory getCategory() {
		return category;
	}
	public void setCategory(MetadataCategory category) {
		propertyChangeSupport.firePropertyChange(PROP_CATEGORY, this.category, this.category = category);
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange(PROP_POSITION, this.position, this.position = position);
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		propertyChangeSupport.firePropertyChange(PROP_TEXT, this.text, this.text = text);
	}
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		propertyChangeSupport.firePropertyChange(PROP_EXPIRED, this.expired, this.expired = expired);
	}
	
}
