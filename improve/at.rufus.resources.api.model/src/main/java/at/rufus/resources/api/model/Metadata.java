package at.rufus.resources.api.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="METADATA")
public class Metadata extends Entity  {
	private static final long serialVersionUID = 1L;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="NUM_VALUE")
	private BigDecimal numValue;
	
	@Column(name="TEXT_VALUE")
	private String textValue;
	
	@Column(name="DATE_VALUE")
	private Timestamp dateValue;
	
	@ManyToOne
	@JoinColumn(name="LOV_VALUE", referencedColumnName="ID")
	private MetadataLov lovValue;
	
	@ManyToOne
	@JoinColumn(name="METADATA_DESC", referencedColumnName="ID")
	private MetadataDescriptor descriptor;

	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	private Resource resource;

	@ManyToOne
	@JoinColumn(name="REVISION_FROM_ID", referencedColumnName="ID")
	private Revision revision;

	@Column(name="REVISION_FROM_TIME")
	private Timestamp revisionFromTime;
	
	@Column(name="REVISION_TO_TIME")
	private Timestamp revisionToTime;

	@Column(name="HISTORY_ID")
	private GUID historyId;
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	public BigDecimal getNumValue() {
		return numValue;
	}
	public void setNumValue(BigDecimal numValue) {
		propertyChangeSupport.firePropertyChange("numValue", this.numValue, this.numValue = numValue);
	}
	public String getTextValue() {
		return textValue;
	}
	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.textValue, this.textValue = textValue);
	}
	public Timestamp getDateValue() {
		return dateValue;
	}
	public void setDateValue(Timestamp dateValue) {
		propertyChangeSupport.firePropertyChange("dateValue", this.dateValue, this.dateValue = dateValue);
	}
	public MetadataLov getLovValue() {
		return lovValue;
	}
	public void setLovValue(MetadataLov lovValue) {
		propertyChangeSupport.firePropertyChange("lovValue", this.lovValue, this.lovValue = lovValue);
	}
	public MetadataDescriptor getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(MetadataDescriptor descriptor) {
		propertyChangeSupport.firePropertyChange("descriptor", this.descriptor, this.descriptor = descriptor);
	}
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}
	public Revision getRevision() {
		return revision;
	}
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
	public Timestamp getRevisionFromTime() {
		return revisionFromTime;
	}
	public void setRevisionFromTime(Timestamp revisionFromTime) {
		propertyChangeSupport.firePropertyChange("revisionFromTime", this.revisionFromTime, this.revisionFromTime = revisionFromTime);
	}
	public Timestamp getRevisionToTime() {
		return revisionToTime;
	}
	public void setRevisionToTime(Timestamp revisionToTime) {
		propertyChangeSupport.firePropertyChange("revisionToTime", this.revisionToTime, this.revisionToTime = revisionToTime);
	}
	public GUID getHistoryId() {
		return historyId;
	}
	public void setHistoryId(GUID historyId) {
		propertyChangeSupport.firePropertyChange("historyId", this.historyId, this.historyId = historyId);
	}
		
}
