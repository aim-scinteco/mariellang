package at.rufus.resources.api.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.AbstractLov;

@javax.persistence.Entity
@Table(name="RELATION_TYPE_LOV")
public class RelationTypeLov extends AbstractLov {

private static final long serialVersionUID = 1L;
	
	public static final Comparator<RelationTypeLov> COMPARATOR_NAME = new Comparator<RelationTypeLov>() {
		@Override
		public int compare(RelationTypeLov o1, RelationTypeLov	 o2) {
			return CmpUtil.cmpNatural(o1.name, o2.name, true);
		}
	};
	
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="REVERSE_NAME")
	private String reverseName;

	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="SYSTEM_LAYER")
	private SystemLayer systemLayer;
	
	@Column(name="EXPIRED")
	private boolean expired;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange("description", this.description, this.description = description);
	}
	
	public SystemLayer getSystemLayer() {
		return systemLayer;
	}
	
	public void setSystemLayer(SystemLayer systemLayer) {
		propertyChangeSupport.firePropertyChange("systemLayer", this.systemLayer, this.systemLayer = systemLayer);
	}
	
	public boolean isExpired() {
		return expired;
	}
	
	public void setExpired(boolean expired) {
		propertyChangeSupport.firePropertyChange("expired", this.expired, this.expired = expired);
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getReverseName() {
		return reverseName;
	}

	public void setReverseName(String reverseName) {
		propertyChangeSupport.firePropertyChange("reverseName", this.reverseName, this.reverseName = reverseName);
	}
}
