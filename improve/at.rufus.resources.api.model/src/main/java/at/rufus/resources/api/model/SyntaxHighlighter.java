package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNTAX_HIGHLIGHTER")
public class SyntaxHighlighter extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="NAME")
	private String name;
	
	@Column(name="IDENTIFIER_PATTERN")
	private String identifierPattern;

	@OneToMany(mappedBy="syntaxHighlighter")
	private List<SyntaxPattern> patterns = new TrackedList<SyntaxPattern>(propertyChangeSupport, "patterns");

	@OneToMany(mappedBy="syntaxHighlighter")
	private List<SyntaxKeywordSet> keywordSets = new TrackedList<SyntaxKeywordSet>(propertyChangeSupport, "keywordSets");
	
	@OneToMany(mappedBy="syntaxHighlighter")
	private List<SyntaxIdentifierModifier> identifierModifiers = new TrackedList<SyntaxIdentifierModifier>(propertyChangeSupport, "identifierModifiers");

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getIdentifierPattern() {
		return identifierPattern;
	}
	
	public void setIdentifierPattern(String identifierPattern) {
		propertyChangeSupport.firePropertyChange("identifierPattern", this.identifierPattern, this.identifierPattern = identifierPattern);
	}
	
	public List<SyntaxPattern> getPatterns() {
		return patterns;
	}
	
	public List<SyntaxKeywordSet> getKeywordSets() {
		return keywordSets;
	}
	
	public List<SyntaxIdentifierModifier> getIdentifierModifiers() {
		return identifierModifiers;
	}
	
}
