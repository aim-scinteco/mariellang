package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="QC_CHECK")
public class QCheck extends Entity {
    private static final long serialVersionUID = 1L;

    public static final String PROP_NAME = "name";
    public static final String PROP_ITEMS = "items";
    
    @Column(name = "CHECK_ID")
    private GUID checkId;
    
    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name="REVISION_ID", referencedColumnName="ID")
    private Revision revision;
    
    @OneToMany(mappedBy = "qc")
    private List<QCheckItem> items = new TrackedList<>(propertyChangeSupport, PROP_ITEMS);
    
    @Column(name = "DELETED")
    private boolean deleted;

    public GUID getCheckId() {
        return checkId;
    }
    
    public void setCheckId(GUID checkId) {
        this.checkId = checkId;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        propertyChangeSupport.firePropertyChange(PROP_NAME, this.name, this.name = name);
    }
    
    public Revision getRevision() {
        return revision;
    }
    
    public void setRevision(Revision revision) {
        this.revision = revision;
    }
    
    public List<QCheckItem> getItems() {
        return items;
    }
    
    public boolean isDeleted() {
        return deleted;
    }
    
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
