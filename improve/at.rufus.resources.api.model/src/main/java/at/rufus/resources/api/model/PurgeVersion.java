package at.rufus.resources.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="DPURGE_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(PurgeVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class PurgeVersion extends ResourceVersion {

	public static final String DISCRIMINATOR = RepositoryEntityTypes.PURGE_VERSION;
	
	private static final long serialVersionUID = 1L;
	
	@Override
	@Transient
	public String getDiscriminator() {
		return PurgeVersion.DISCRIMINATOR;
	}

	@Transient
	public Purge getPurge() {
		return (Purge)getResource();
	}
}
