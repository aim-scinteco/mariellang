package at.rufus.resources.api.model;

import java.util.ArrayList;
import java.util.List;

import at.rufus.base.api.model.GUID;

public class GroupNode {

	private final GUID id;
	private String systemGroupId;
	private String name;
	private final boolean user;
	private String userId;
	private boolean active = true;
	private final List<GroupNode> parents = new ArrayList<GroupNode>(3);
	private final List<GroupNode> children = new ArrayList<GroupNode>(3);
	
	public GroupNode(GUID id, boolean user) {
		this.id = id;
		this.user = user;
	}
	
	public GUID getId() {
		return id;
	}
	
	public String getSystemGroupId() {
		return systemGroupId;
	}
	
	public void setSystemGroupId(String systemGroupId) {
		this.systemGroupId = systemGroupId;
	}
	
	public boolean isUser() {
		return user;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public List<GroupNode> getParents() {
		return parents;
	}
	
	public List<GroupNode> getChildren() {
		return children;
	}
	
}
