package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SEARCH_PREFERENCES")
public class SearchPreferences extends Entity {
	private static final long serialVersionUID = 1L;

	public static final double DEFAULT_MAX_LIMIT = 100; // max. 100 MB
	
	public static final String PROP_MAX_INDEX_FILESIZE = "maxIndexFileSize";
	public static final String PROP_INCLUDE_FILETYPE_LIST = "includeFileTypeList";
	public static final String PROP_ACCEPT_NO_FILE_EXTENSION = "acceptNoFileExtension";
	public static final String PROP_FILETYPES = "fileTypes";
	
	@Column(name = "MAX_INDEX_FILESIZE")
	private Double maxIndexFileSize;
	
	@Column(name = "INCLUDE_FILETYPE_LIST")
	private boolean includeFileTypeList;
	
	@Column(name = "ACCEPT_NO_EXTENSION")
	private boolean acceptNoFileExtension;
	
	@OneToMany(mappedBy = SearchFileType.PROP_PREFERENCES)
	private final List<SearchFileType> fileTypes = new TrackedList<SearchFileType>(propertyChangeSupport, PROP_FILETYPES);
	
	
	public Double getMaxIndexFileSize() {
		return maxIndexFileSize;
	}
	
	public void setMaxIndexFileSize(Double maxIndexFileSize) {
		propertyChangeSupport.firePropertyChange(PROP_MAX_INDEX_FILESIZE, this.maxIndexFileSize, this.maxIndexFileSize = maxIndexFileSize);
	}
	
	public boolean isIncludeFileTypeList() {
		return includeFileTypeList;
	}
	
	public void setIncludeFileTypeList(boolean includeFileTypeList) {
		propertyChangeSupport.firePropertyChange(PROP_INCLUDE_FILETYPE_LIST, this.includeFileTypeList, this.includeFileTypeList = includeFileTypeList);
	}
	
	public boolean isAcceptNoFileExtension() {
		return acceptNoFileExtension;
	}
	
	public void setAcceptNoFileExtension(boolean acceptNoFileExtension) {
		propertyChangeSupport.firePropertyChange(PROP_ACCEPT_NO_FILE_EXTENSION, this.acceptNoFileExtension, this.acceptNoFileExtension = acceptNoFileExtension);
	}
	
	public List<SearchFileType> getFileTypes() {
		return fileTypes;
	}
	
}
