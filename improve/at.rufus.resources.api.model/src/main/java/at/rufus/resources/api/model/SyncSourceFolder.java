package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNC_SOURCE_FOLDER")
public class SyncSourceFolder extends Entity {
	
	private static final long serialVersionUID = 1L;
	
	private String label;
	private String mountPoint;
	private String serverPath;
	private Boolean active;
	
	private List<Member> members  = new TrackedList<Member>(propertyChangeSupport, "members");
	
	@ManyToMany
	@JoinTable(
		name="SOURCE_MEMBER_RELATION",
		joinColumns=@JoinColumn(name="SYNC_SOURCE_FOLDER", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	)
	public List<Member> getMembers() {
		return members;
	}
	
	@Column(name="LABEL")
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		propertyChangeSupport.firePropertyChange("label", this.label, this.label = label);
	}
	@Column(name="SERVER_PATH")
	public String getServerPath() {
		return serverPath;
	}
	public void setServerPath(String serverPath) {
		propertyChangeSupport.firePropertyChange("serverPath", this.serverPath, this.serverPath = serverPath);
	}
	@Column(name="MOUNT_POINT")
	public String getMountPoint() {
		return mountPoint;
	}
	public void setMountPoint(String mountPoint) {
		propertyChangeSupport.firePropertyChange("mountPoint", this.mountPoint, this.mountPoint = mountPoint);
	}
	@Column(name="ACTIVE")
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		propertyChangeSupport.firePropertyChange("active", this.active, this.active = active);
	}
	@Transient
	public boolean isFolderActive() {
		return getActive() != null && getActive();
	}

}
