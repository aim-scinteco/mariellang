package at.rufus.resources.api.model;

public interface IEntityVersionProvider {

	int getVersionCount();
	void setVersionCount(int versionCount);
	
	String getEntityId();
	
}
