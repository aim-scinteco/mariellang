package at.rufus.resources.api.model;

public enum EditorType {
	
	System("System Editor"), 
	External("External Editor"), 
	Internal("Internal Editor");
	
	private String displayName;
	
	private EditorType(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public static EditorType getByDisplayName(String displayName) {
		for(EditorType editoryType : values()) {
			if(displayName.equals(editoryType.displayName)) {
				return editoryType;
			}
		}
		return null;
	}
}
