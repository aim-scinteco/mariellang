package at.rufus.resources.api.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="FILE_REINDEX_JOB")
public class FileReindexJob extends Entity {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "INDEX_NAME")
	private String indexName;
	
	@Column(name = "STARTED_AT")
	private Timestamp startedAt;
	
	@Column(name = "FINISHED_AT")
	private Timestamp finishedAt;
	
	@Column(name = "CANCELED")
	private Boolean canceled;
	
	@Column(name = "INITIAL_HITS")
	private Long initialHits;
	
	@Column(name = "FILES_UPDATED")
	private Long filesUpdated;
	
	@Column(name = "FILES_SKIPPED")
	private Long filesSkipped;
	
	@Column(name = "FILES_CONFLICTED")
	private Long filesConflicted;
	
	@Column(name = "FILES_FAILED")
	private Long filesFailed;
	
	@Column(name = "FILES_UPDATED_MB")
	private BigDecimal filesUpdatedMb;
	
	
	public FileReindexJob() {
		super();
	}
	
	public FileReindexJob(GUID id) {
		super(id);
	}
	
	public String getIndexName() {
		return indexName;
	}
	
	public void setIndexName(String indexName) {
		propertyChangeSupport.firePropertyChange("indexName", this.indexName, this.indexName = indexName);
	}
	
	public Timestamp getStartedAt() {
		return startedAt;
	}
	
	public void setStartedAt(Timestamp startedAt) {
		propertyChangeSupport.firePropertyChange("startedAt", this.startedAt, this.startedAt = startedAt);
	}
	
	public Timestamp getFinishedAt() {
		return finishedAt;
	}
	
	public void setFinishedAt(Timestamp finishedAt) {
		propertyChangeSupport.firePropertyChange("finishedAt", this.finishedAt, this.finishedAt = finishedAt);
	}
	
	public Boolean getCanceled() {
		return canceled;
	}
	
	public void setCanceled(Boolean canceled) {
		propertyChangeSupport.firePropertyChange("canceled", this.canceled, this.canceled = canceled);
	}
	
	public Long getInitialHits() {
		return initialHits;
	}
	
	public void setInitialHits(Long initialHits) {
		propertyChangeSupport.firePropertyChange("initialHits", this.initialHits, this.initialHits = initialHits);
	}
	
	public Long getFilesUpdated() {
		return filesUpdated;
	}
	
	public void setFilesUpdated(Long filesUpdated) {
		propertyChangeSupport.firePropertyChange("filesUpdated", this.filesUpdated, this.filesUpdated = filesUpdated);
	}
	
	public Long getFilesSkipped() {
		return filesSkipped;
	}
	
	public void setFilesSkipped(Long filesSkipped) {
		propertyChangeSupport.firePropertyChange("filesSkipped", this.filesSkipped, this.filesSkipped = filesSkipped);
	}
	
	public Long getFilesConflicted() {
		return filesConflicted;
	}
	
	public void setFilesConflicted(Long filesConflicted) {
		propertyChangeSupport.firePropertyChange("filesConflicted", this.filesConflicted, this.filesConflicted = filesConflicted);
	}
	
	public Long getFilesFailed() {
		return filesFailed;
	}
	
	public void setFilesFailed(Long filesFailed) {
		propertyChangeSupport.firePropertyChange("filesFailed", this.filesFailed, this.filesFailed = filesFailed);
	}
	
	public BigDecimal getFilesUpdatedMb() {
		return filesUpdatedMb;
	}
	
	public void setFilesUpdatedMb(BigDecimal filesUpdatedMb) {
		propertyChangeSupport.firePropertyChange("filesUpdatedMb", this.filesUpdatedMb, this.filesUpdatedMb = filesUpdatedMb);
	}

}
