package at.rufus.resources.api.model;

public enum PurgeStatus {

	Planning("Plan"),
	Reviewing("Review"),
	Approved("Approved"),
	Purging("Purging"),
	Done("Done"),
	DoneErrors("Done/Errors");
	
	final String label;
	
	private PurgeStatus(String label) {
		this.label = label;
	}

	public boolean hasLaunched() {
		return this == Purging || this == Done || this == DoneErrors;
	}
	
	public String getLabel() {
		return label;
	}
	
}
