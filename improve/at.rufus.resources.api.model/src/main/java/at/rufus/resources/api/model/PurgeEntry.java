package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="DPURGE_ENTRY")
public class PurgeEntry extends Entity {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="DPURGE", referencedColumnName="ID")
	private Purge purge;
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	private Resource resource;
	
	@Column(name="ENTITY_ID")
	private String entityId;

	@Column(name="PATH")
	private String path;

	@Column(name="STATUS")
	private PurgeEntryStatus status;
	
	@Column(name="STATUS_MSG")
	private String statusMessage;

	@Override
	@Transient
	public String getDiscriminator() {
		return RepositoryEntityTypes.PURGE_ENTRY;
	}
	
	public Purge getPurge() {
		return purge;
	}

	public void setPurge(Purge purge) {
		propertyChangeSupport.firePropertyChange("purge", this.purge, this.purge = purge);
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		propertyChangeSupport.firePropertyChange("entityId", this.entityId, this.entityId = entityId);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		propertyChangeSupport.firePropertyChange("path", this.path, this.path = path);
	}

	public PurgeEntryStatus getStatus() {
		return status;
	}

	public void setStatus(PurgeEntryStatus status) {
		propertyChangeSupport.firePropertyChange("status", this.status, this.status = status);
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		propertyChangeSupport.firePropertyChange("statusMessage", this.statusMessage, this.statusMessage = statusMessage);
	}
	
}
