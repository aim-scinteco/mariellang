package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="RESOURCE_CHARACTER")
public class ResourceCharacter extends Entity  {
	private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;

	@Column(name="ICON")
	private byte[] icon;
	
	@OneToMany(mappedBy="resourceCharacter")
	private List<ResourceCharacterMetadata> metadataDescriptors = new TrackedList<>(propertyChangeSupport, "metadataDescriptors");

	@Column(name="SCOPE")
	private String scope;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public byte[] getIcon() {
		return icon;
	}
	
	public void setIcon(byte[] icon) {
		propertyChangeSupport.firePropertyChange("icon", this.icon, this.icon = icon);
	}
	
	public List<ResourceCharacterMetadata> getMetadataDescriptors() {
		return metadataDescriptors;
	}
	
	public String getScope() {
		return scope;
	}
	
	public void setScope(String scope) {
		propertyChangeSupport.firePropertyChange("scope", this.scope, this.scope = scope);
	}
	
}
