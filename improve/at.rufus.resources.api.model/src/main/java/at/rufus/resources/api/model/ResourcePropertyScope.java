package at.rufus.resources.api.model;

public enum ResourcePropertyScope {
	resource,
	user,
	application
}
