package at.rufus.resources.api.model;


import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNC_RULE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(
	name="SYNC_TYPE",
	discriminatorType=DiscriminatorType.STRING
)
public abstract class AbstractSyncRule extends Entity {

	private static final long serialVersionUID = 1L;
	
	
	private SyncSourceFolder source;
	private String sourceRelativePath;
	
	private Member owner;
	private Boolean overwrite;
	private Folder targetFolder;
	
	private Boolean active;
	
	
	@ManyToOne
	@JoinColumn(name="TARGET_FOLDER", referencedColumnName="ID")
	public Folder getTargetFolder() {
		return targetFolder;
	}
	public void setTargetFolder(Folder targetFolder) {
		propertyChangeSupport.firePropertyChange("targetFolder", this.targetFolder, this.targetFolder = targetFolder);
	}


	@ManyToOne
	@JoinColumn(name="SOURCE", referencedColumnName="ID")
	public SyncSourceFolder getSource() {
		return source;
	}

	public void setSource(SyncSourceFolder source) {
		propertyChangeSupport.firePropertyChange("source", this.source, this.source = source);
	}
	

	@ManyToOne
	@JoinColumn(name="OWNER", referencedColumnName="ID")
	public Member getOwner() {
		return owner;
	}

	public void setOwner(Member owner) {
		propertyChangeSupport.firePropertyChange("owner", this.owner, this.owner = owner);
	}

	@Column(name="OVERWRITE")
	public Boolean getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(Boolean overwrite) {
		propertyChangeSupport.firePropertyChange("overwrite", this.overwrite, this.overwrite = overwrite);
	}
	
	@Column(name="SOURCE_REL_PATH")
	public String getSourceRelativePath() {
		return sourceRelativePath;
	}

	public void setSourceRelativePath(String sourceRelativePath) {
		propertyChangeSupport.firePropertyChange("sourceRelativePath", this.sourceRelativePath, this.sourceRelativePath = sourceRelativePath);
	}
	
	@Column(name="ACTIVE")
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		propertyChangeSupport.firePropertyChange("active", this.active, this.active = active);
	}
	
	@Transient
	public boolean isRuleActive() {
		if (getSource() != null && getSource().isFolderActive()) {
			if (getSource().getMembers().contains(getOwner())) {
				return getActive() != null && getActive();
			}
		}
		return false;
	}
}
