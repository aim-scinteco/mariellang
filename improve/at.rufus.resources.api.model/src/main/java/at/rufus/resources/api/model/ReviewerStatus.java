package at.rufus.resources.api.model;

public enum ReviewerStatus {
    INVITED,
    ACCEPTED,
    DECLINED,
    REMOVED
}
