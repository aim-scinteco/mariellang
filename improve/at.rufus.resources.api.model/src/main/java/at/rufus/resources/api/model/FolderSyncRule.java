package at.rufus.resources.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="FOLDER_SYNC_RULE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue("FOLDER")
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class FolderSyncRule extends AbstractSyncRule {
	private static final long serialVersionUID = 1L;

}
