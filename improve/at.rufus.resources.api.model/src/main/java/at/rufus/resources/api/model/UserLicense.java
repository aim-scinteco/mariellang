package at.rufus.resources.api.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="USER_LICENSE")
public class UserLicense extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name = "SERIAL_NUMBER")
	private String serialNumber;

	@Column(name = "VALID_FROM")
	private Date validFrom;
	
	@Column(name = "VALID_THROUGH")
	private Date validThrough;
	
	@Column(name = "LICENSE_TYPE")
	private int licenseType;
	
//	@Column(name = "LNR")
//	private int licenseNumber;

	@ManyToOne
	@JoinColumn(name="OWNER", referencedColumnName="ID")
	private Member owner;
	
	@ManyToOne
	@JoinColumn(name="MASTER_LICENSE", referencedColumnName="ID")
	private MasterLicense masterLicense;
	
	
	public String getSerialNumber() {
		return serialNumber;
	}
	
	public void setSerialNumber(String serialNumber) {
		propertyChangeSupport.firePropertyChange("serialNumber", this.serialNumber, this.serialNumber = serialNumber);
	}
	
	public Date getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(Date validFrom) {
		propertyChangeSupport.firePropertyChange("validFrom", this.validFrom, this.validFrom = validFrom);
	}
	
	public Date getValidThrough() {
		return validThrough;
	}
	
	public void setValidThrough(Date validThrough) {
		propertyChangeSupport.firePropertyChange("validThrough", this.validThrough, this.validThrough = validThrough);
	}
	
	public int getLicenseType() {
		return licenseType;
	}
	
	public void setLicenseType(int licenseType) {
		propertyChangeSupport.firePropertyChange("licenseType", this.licenseType, this.licenseType = licenseType);
	}
	
//	public int getLicenseNumber() {
//		return licenseNumber;
//	}
//	
//	public void setLicenseNumber(int licenseNumber) {
//		propertyChangeSupport.firePropertyChange("licenseNumber", this.licenseNumber, this.licenseNumber = licenseNumber);
//	}
	
	public Member getOwner() {
		return owner;
	}
	
	public void setOwner(Member owner) {
		propertyChangeSupport.firePropertyChange("owner", this.owner, this.owner = owner);
	}
	
	public MasterLicense getMasterLicense() {
		return masterLicense;
	}
	
	public void setMasterLicense(MasterLicense masterLicense) {
		propertyChangeSupport.firePropertyChange("masterLicense", this.masterLicense, this.masterLicense = masterLicense);
	}
	
}
