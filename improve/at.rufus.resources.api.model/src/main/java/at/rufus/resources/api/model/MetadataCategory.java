package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="METADATA_CATEGORY")
public class MetadataCategory extends Entity  {
	private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="SCOPE")
	private String scope;
	
	@OneToMany(mappedBy="category")
	private List<MetadataLov> values = new TrackedList<>(propertyChangeSupport, "values");
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		propertyChangeSupport.firePropertyChange("scope", this.scope, this.scope = scope);
	}
	
	public List<MetadataLov> getValues() {
		return values;
	}
	
}
