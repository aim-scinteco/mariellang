package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="REVIEW_TEMPLATE")
public class ReviewTemplate extends Entity {
    private static final long serialVersionUID = 1L;
    
    public static final String PROP_NAME = "name";
    public static final String PROP_QC = "qc";
    public static final String PROP_RESOURCE_CHECKS = "resourceChecks";

    @Column(name = "TEMPLATE_ID")
    private GUID templateId;
    
    @Column(name = "NAME")
    private String name;
    
    @ManyToOne
    @JoinColumn(name="REVISION_ID", referencedColumnName="ID")
    private Revision revision;
    
    @ManyToOne
    @JoinColumn(name = "QC_CHECK", referencedColumnName = "ID")
    private QCheck qc;
    
    @OneToMany(mappedBy = "reviewTemplate")
    private List<ResourceQCheck> resourceChecks = new TrackedList<>(propertyChangeSupport, PROP_RESOURCE_CHECKS);
    
    @Column(name = "DELETED")
    private boolean deleted;
    
    public GUID getTemplateId() {
        return templateId;
    }
    
    public void setTemplateId(GUID templateId) {
        this.templateId = templateId;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        propertyChangeSupport.firePropertyChange(PROP_NAME, this.name, this.name = name);
    }
    
    public Revision getRevision() {
        return revision;
    }
    
    public void setRevision(Revision revision) {
        this.revision = revision;
    }
    
    public QCheck getQc() {
        return qc;
    }
    
    public void setQc(QCheck qc) {
        propertyChangeSupport.firePropertyChange(PROP_QC, this.qc, this.qc = qc);
    }
    
    public List<ResourceQCheck> getResourceChecks() {
        return resourceChecks;
    }
    
    public boolean isDeleted() {
        return deleted;
    }
    
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    @Override
    public String toString() {
        return getName();
    }
}
