package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="RESOURCE_QC_CHECK")
public class ResourceQCheck extends Entity {
    private static final long serialVersionUID = 1L;

    public static final String PROP_RESOURCE_TYPE = "resourceType";
    public static final String PROP_FILE_EXTENSION = "fileExtension";
    public static final String PROP_QC = "qc";
    
    @ManyToOne
    @JoinColumn(name = "REVIEW_TEMPLATE", referencedColumnName = "ID")
    private ReviewTemplate reviewTemplate;
    
    @Column(name = "RESOURCE_TYPE")
    private String resourceType;
    
    @Column(name = "FILE_EXTENSION")
    private String fileExtension;
    
    @ManyToOne
    @JoinColumn(name = "QC_CHECK", referencedColumnName = "ID")
    private QCheck qc;
    
    public ReviewTemplate getReviewTemplate() {
        return reviewTemplate;
    }
    
    public void setReviewTemplate(ReviewTemplate reviewTemplate) {
        this.reviewTemplate = reviewTemplate;
    }
    
    public String getResourceType() {
        return resourceType;
    }
    
    public void setResourceType(String resourceType) {
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_TYPE, this.resourceType, this.resourceType = resourceType);
    }
    
    public String getFileExtension() {
        return fileExtension;
    }
    
    public void setFileExtension(String fileExtension) {
        propertyChangeSupport.firePropertyChange(PROP_FILE_EXTENSION, this.fileExtension, this.fileExtension = fileExtension);
    }
    
    public QCheck getQc() {
        return qc;
    }
    
    public void setQc(QCheck qc) {
        propertyChangeSupport.firePropertyChange(PROP_QC, this.qc, this.qc = qc);
    }
}
