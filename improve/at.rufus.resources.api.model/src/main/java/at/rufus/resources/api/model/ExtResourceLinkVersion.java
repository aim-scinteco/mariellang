package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name = "EXTERNAL_LINK_VERSION")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(ExtResourceLinkVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
public class ExtResourceLinkVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;

	public static final String PROP_URL = "url";
	public static final String DISCRIMINATOR = RepositoryEntityTypes.EXT_RESOURCE_LINK_VERSION;

	@Column(name = "URL")
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		propertyChangeSupport.firePropertyChange(PROP_URL, this.url, this.url = url);
	}

	@Transient
	public ExtResourceLink getExternalLink() {
		return (ExtResourceLink) getResource();
	}

	@Override
	public ExtResourceLinkVersion copy() {
		ExtResourceLinkVersion link = (ExtResourceLinkVersion) super.copy();
		link.setUrl(this.getUrl());
		return link;
	}

	@Override
	@Transient
	public String getDiscriminator() {
		return ExtResourceLinkVersion.DISCRIMINATOR;
	}

}
