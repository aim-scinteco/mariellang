package at.rufus.resources.api.model;

public enum PurgeEntryStatus {
	Open,
	Purged,
	Failed
}
