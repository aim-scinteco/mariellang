package at.rufus.resources.api.model;

public enum RightsArea {

	PRIMARY(1), PUBLISH(2);

	private final int rightsArea;

	private RightsArea(int rightsArea) {
		this.rightsArea = rightsArea;
	}

	public int getRightsArea() {
		return rightsArea;
	}
	
}
