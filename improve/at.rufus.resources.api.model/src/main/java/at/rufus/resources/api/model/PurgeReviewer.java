package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="DPURGE_REVIEWER")
public class PurgeReviewer extends Entity {
    private static final long serialVersionUID = 1L;
    
    public static final String PROP_STATUS = "status";

    @ManyToOne
    @JoinColumn(name="APP_USER", referencedColumnName="ID")
    private Member user;
    
    @ManyToOne
    @JoinColumn(name="DPURGE", referencedColumnName="ID")
    private Purge purge;
    
    @Column(name="SIGNED")
    private boolean signed;

    public Member getUser() {
        return user;
    }
    
    public void setUser(Member user) {
        propertyChangeSupport.firePropertyChange("user", this.user, this.user = user);
    }

	public Purge getPurge() {
		return purge;
	}

	public void setPurge(Purge purge) {
        propertyChangeSupport.firePropertyChange("purge", this.purge, this.purge = purge);
	}

	public boolean isSigned() {
		return signed;
	}

	public void setSigned(boolean signed) {
        propertyChangeSupport.firePropertyChange("signed", this.signed, this.signed = signed);
	}
    
}
