package at.rufus.resources.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="RESOURCE_LINK")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(RepositoryEntityTypes.RESOURCE_LINK)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class ResourceLink extends Resource {
	private static final long serialVersionUID = 1L;
	
	@Transient
	@Override
	public String getDiscriminator() {
		return RepositoryEntityTypes.RESOURCE_LINK;
	}

}
