package at.rufus.resources.api.model;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="APP_USER_MEMENTO")
public class UserMemento extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="TYPE")
	private String type;
	
//	@ManyToOne
//	@JoinColumn(name="OWNER", referencedColumnName="ID")
//	private Member user;
	
	@Column(name="OWNER")
	private GUID userId;
	
	@Lob
	@Column(name="MEMENTO")
	private String memento;
	
	
//	public Member getUser() {
//		return user;
//	}
//	
//	public void setUser(Member user) {
//		propertyChangeSupport.firePropertyChange("user", this.user, this.user = user);
//	}
	
	public GUID getUserId() {
		return userId;
	}
	
	public void setUserId(GUID userId) {
		propertyChangeSupport.firePropertyChange("userId", this.userId, this.userId = userId);
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	
	public String getMemento() {
		return memento;
	}
	
	public void setMemento(String memento) {
		propertyChangeSupport.firePropertyChange("memento", this.memento, this.memento = memento);
	}
	
	public static String toMemento(Properties properties) throws IOException {
		StringWriter writer = new StringWriter();
		try {
			properties.store(writer, null);
		} finally {
			writer.close();
		}
		return writer.toString();
	}
	
	public static Properties fromMemento(String memento) throws IOException {
		Properties properties = new Properties();
		StringReader reader = new StringReader(memento);
		try {
			properties.load(reader);
		} finally {
			reader.close();
		}
		return properties;
	}
	
}
