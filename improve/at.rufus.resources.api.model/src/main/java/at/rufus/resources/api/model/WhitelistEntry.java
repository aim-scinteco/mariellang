package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name = "WHITELIST_ENTRY")
public class WhitelistEntry extends Entity {
	
	public static final String HOST_PLACEHOLDER = "<host>";
	public static final String PORT_PLACEHOLDER = "<port>";
	
	private static final long serialVersionUID = 1L;

	@Column(name = "URL")
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		propertyChangeSupport.firePropertyChange("url", this.url, this.url = url);
	}
}
