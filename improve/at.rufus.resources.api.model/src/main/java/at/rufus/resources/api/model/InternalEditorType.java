package at.rufus.resources.api.model;

public enum InternalEditorType {
	
	TextEditor("at.rufus.resources.ui.editors.text.TextEditor", "Text Editor"),
	CsvEditor("at.rufus.resources.ui.editors.csv.CsvEditor", "CSV Editor"),
	ImageEditor("at.rufus.resources.ui.editors.image.editors.ImageEditor", "Image Editor"),
	BrowserEditor("at.rufus.resources.ui.editors.browser.ReadonlyBrowserEditor", "Browser Editor");
	
	private final String editorId;
	private final String editorName;
	
	private InternalEditorType(String editorId, String editorName) {
		this.editorId = editorId;
		this.editorName = editorName;
	}
	
	public String getEditorId() {
		return editorId;
	}
	
	public String getEditorName() {
		return editorName;
	}
	
	public static InternalEditorType getByEditorName(String editorName) {
		for(InternalEditorType editor : values()) {
			if(editorName.equals(editor.editorName)) {
				return editor;
			}
		}
		return null;
	}
}
