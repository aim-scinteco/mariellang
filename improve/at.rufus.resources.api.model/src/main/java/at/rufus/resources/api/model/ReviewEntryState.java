package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="REVIEW_ENTRY_STATE")
public class ReviewEntryState extends Entity {
	
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="REVIEW_ENTRY", referencedColumnName="ID")
	private ReviewEntry reviewEntry;
	
	@ManyToOne
	@JoinColumn(name="REVIEWER", referencedColumnName="ID")
	private Member reviewer;
	
	@Column(name="STATUS")
	private ReviewEntryStatus status;

	public ReviewEntry getReviewEntry() {
		return reviewEntry;
	}

	public void setReviewEntry(ReviewEntry reviewEntry) {
		propertyChangeSupport.firePropertyChange("reviewEntry", this.reviewEntry, this.reviewEntry = reviewEntry);
	}

	public Member getReviewer() {
		return reviewer;
	}

	public void setReviewer(Member reviewer) {
		propertyChangeSupport.firePropertyChange("reviewer", this.reviewer, this.reviewer = reviewer);
	}

	public ReviewEntryStatus getStatus() {
		return status;
	}

	public void setStatus(ReviewEntryStatus status) {
		propertyChangeSupport.firePropertyChange("status", this.status, this.status = status);
	}

}
