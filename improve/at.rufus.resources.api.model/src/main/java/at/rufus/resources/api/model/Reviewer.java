package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="REVIEWER")
public class Reviewer extends Entity {
    private static final long serialVersionUID = 1L;
    
    public static final String PROP_STATUS = "status";

    @ManyToOne
    @JoinColumn(name="APP_USER", referencedColumnName="ID")
    private Member user;
    
    @ManyToOne
    @JoinColumn(name="REVIEW", referencedColumnName="ID")
    private Review review;
    
    @Column(name="STATUS")
    private ReviewerStatus status;
    
    public Reviewer() {}
    
    public Reviewer(Member user, Review review) {
        this.user = user;
        this.review = review;
        this.status = ReviewerStatus.INVITED;
    }
    
    public Member getUser() {
        return user;
    }
    
    public void setUser(Member user) {
        propertyChangeSupport.firePropertyChange("user", this.user, this.user = user);
    }
    
    public Review getReview() {
        return review;
    }
    
    public void setReview(Review review) {
        propertyChangeSupport.firePropertyChange("review", this.review, this.review = review);
    }
    
    public ReviewerStatus getStatus() {
        return status;
    }
    
    public void setStatus(ReviewerStatus status) {
        propertyChangeSupport.firePropertyChange(PROP_STATUS, this.status, this.status = status);
    }
}
