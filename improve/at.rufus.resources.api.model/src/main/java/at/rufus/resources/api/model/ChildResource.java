package at.rufus.resources.api.model;

import java.io.Serializable;

import at.rufus.base.api.model.GUID;

public class ChildResource implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private GUID resourceId;
	private GUID resourceVersionId;
	private boolean visible;
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public GUID getResourceId() {
		return resourceId;
	}
	
	public void setResourceId(GUID resourceId) {
		this.resourceId = resourceId;
	}
	
	public GUID getResourceVersionId() {
		return resourceVersionId;
	}
	
	public void setResourceVersionId(GUID resourceVersionId) {
		this.resourceVersionId = resourceVersionId;
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
}
