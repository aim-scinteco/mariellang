package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.AbstractLov;

@javax.persistence.Entity
@Table(name="SYSTEM_LAYER_LOV")
public class SystemLayer extends AbstractLov {
	private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange("description", this.description, this.description = description);
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
