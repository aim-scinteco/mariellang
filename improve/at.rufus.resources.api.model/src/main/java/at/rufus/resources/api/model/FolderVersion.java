package at.rufus.resources.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="FOLDER_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(FolderVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class FolderVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;
	
	public static final String DISCRIMINATOR=RepositoryEntityTypes.FOLDER_VERSION;
	
	@Transient
	public Folder getFolder() {
		return (Folder) getResource();
	}
	
	@Override
	public FolderVersion copy() {
		FolderVersion copy = (FolderVersion) super.copy();
		return copy;
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return FolderVersion.DISCRIMINATOR;
	}

}
