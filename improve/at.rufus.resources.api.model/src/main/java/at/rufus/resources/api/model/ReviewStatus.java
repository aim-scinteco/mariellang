package at.rufus.resources.api.model;

public enum ReviewStatus {

	Planning,
	Reviewing,
	Approved;
	
}
