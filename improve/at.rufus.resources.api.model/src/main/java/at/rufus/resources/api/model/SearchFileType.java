package at.rufus.resources.api.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SEARCH_FILETYPE")
public class SearchFileType extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<SearchFileType> FILE_TYPE_COMPARATOR = new Comparator<SearchFileType>() {
		@Override
		public int compare(SearchFileType o1, SearchFileType o2) {
			return CmpUtil.cmpNatural(o1.getFileType(), o2.getFileType(), true);
		}
	};
	
	public static final String PROP_PREFERENCES = "preferences";
	public static final String PROP_FILETYPE = "fileType";
	
	@ManyToOne
	@JoinColumn(name = "PREFERENCES", referencedColumnName = "ID")
	private SearchPreferences preferences;
	
	@Column(name = "FILETYPE")
	private String fileType;
	
	
	public SearchPreferences getPreferences() {
		return preferences;
	}
	
	public void setPreferences(SearchPreferences preferences) {
		propertyChangeSupport.firePropertyChange(PROP_PREFERENCES, this.preferences, this.preferences = preferences);
	}
	
	public String getFileType() {
		return fileType;
	}
	
	public void setFileType(String fileType) {
		propertyChangeSupport.firePropertyChange(PROP_FILETYPE, this.fileType, this.fileType = fileType);
	}
	
	@Override
	public String toString() {
		return fileType;
	}
	
	@Transient
	public static String getExtension(String filename) {
		if (filename != null && !(filename = filename.trim()).isEmpty()) {
			int idx = filename.lastIndexOf('.');
			if(idx >= 0 && idx < filename.length() - 1) {
				return filename.substring(idx + 1);
			}
		}
		return null;
	}
	
	@Transient
	public static String fromFileTypes(Collection<SearchFileType> fileTypes, String separator) {
		if (fileTypes == null || fileTypes.isEmpty()) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (SearchFileType fileType : fileTypes) {
			if (sb.length() > 0) {
				sb.append(separator);
			}
			sb.append(fileType.getFileType());
		}
		return sb.toString();
	}
	
	@Transient
	public static List<String> toFileExtensionList(String fileExtensionText, String separator, boolean lower) {
		if (fileExtensionText == null || (fileExtensionText = fileExtensionText.trim()).isEmpty()) {
			return Collections.emptyList();
		}
		String[] fileExtensionArray = fileExtensionText.split(separator);
		List<String> fileExtensions = new ArrayList<String>(fileExtensionArray.length);
		for (String fileExtension : fileExtensionArray) {
			if (!(fileExtension = fileExtension.trim()).isEmpty()) {
				fileExtensions.add(lower ? fileExtension.toLowerCase() : fileExtension);
			}
		}
		return fileExtensions;
	}
	
}
