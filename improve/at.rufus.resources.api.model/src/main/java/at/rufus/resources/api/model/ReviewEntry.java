package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="REVIEW_ENTRY")
public class ReviewEntry extends Entity {

	private static final long serialVersionUID = 1L;
	
	public static final String PROP_COMMENTS = "comments";
	
	@ManyToOne
	@JoinColumn(name="REVIEW", referencedColumnName="ID")
	private Review review;
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	private Resource resource;
	
	@OneToMany(mappedBy="reviewEntry")
	private List<ReviewEntryState> states = new TrackedList<ReviewEntryState>(propertyChangeSupport, "states");
	
	@OneToMany(mappedBy="reviewEntry")
	private List<ReviewEntryComment> comments = new TrackedList<ReviewEntryComment>(propertyChangeSupport, PROP_COMMENTS);
	
	@Column(name="STATUS")
	private ReviewEntryStatus status;

	
	@Override
	@Transient
	public String getDiscriminator() {
		return RepositoryEntityTypes.REVIEW_ENTRY;
	}
	
	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		propertyChangeSupport.firePropertyChange("review", this.review, this.review = review);
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}
	
	public ReviewEntryStatus getStatus() {
		return status;
	}
	
	public void setStatus(ReviewEntryStatus status) {
		propertyChangeSupport.firePropertyChange("status", this.status, this.status = status);
	}

	public List<ReviewEntryState> getStates() {
		return states;
	}
	
	public List<ReviewEntryComment> getComments() {
		return comments;
	}
	
	@Transient
	public ReviewEntryState getStateByReviewer(Member user) {
		for (ReviewEntryState state : states) {
			if (user.equalsId(state.getReviewer())) {
				return state;
			}
		}
		return null;
	}
	
}
