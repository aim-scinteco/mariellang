package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="FILE_SYNC_RULE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue("FILE")
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class FileSyncRule extends AbstractSyncRule {
	
	private static final long serialVersionUID = 1L;
	
	private String filename;
	

	@Column(name="FILENAME")
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		propertyChangeSupport.firePropertyChange("filename", this.filename, this.filename = filename);
	}
	
	
	
	
	
	
}
