package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNTAX_KEYWORD")
public class SyntaxKeyword extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="KEYWORD")
	private String keyword;
	
	@ManyToOne
	@JoinColumn(name="SYNTAX_KEYWORD_SET", referencedColumnName="ID")
	private SyntaxKeywordSet keywordSet;

	public String getKeyword() {
		return keyword;
	}
	
	public void setKeyword(String keyword) {
		propertyChangeSupport.firePropertyChange("keyword", this.keyword, this.keyword = keyword);
	}

	public SyntaxKeywordSet getKeywordSet() {
		return keywordSet;
	}
	
	public void setKeywordSet(SyntaxKeywordSet keywordSet) {
		propertyChangeSupport.firePropertyChange("keywordSet", this.keywordSet, this.keywordSet = keywordSet);
	}
	
}
