package at.rufus.resources.api.model;


import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;


@javax.persistence.Entity
@Table(name="RESOURCE_CONFIG_PROP")
public class ResourceConfigProperty extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String RESOURCE = "resource";
	public static final String MEMBER = "member";
	public static final String KEY = "key";
	public static final String VALUE = "value";
	
	private Resource resource;
	private Member member;
	private String key;
	private String value;

	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange(MEMBER, this.member, this.member=member);
	}
	
	@Column(name="KEY")
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		propertyChangeSupport.firePropertyChange(KEY, this.key, this.key=key);
	}
	
	@Column(name="VALUE")
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		propertyChangeSupport.firePropertyChange(VALUE, this.value, this.value=value);
	}
	
}
