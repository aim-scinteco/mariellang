package at.rufus.resources.api.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="RESOURCE_NODE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(
	name="NODE_TYPE",
	discriminatorType=DiscriminatorType.STRING
)
public class Resource extends Entity implements IEntityVersionProvider {
	
	private static final long serialVersionUID = 1L;

	public static final String PATH_DELIMITER = "/";
	
	public static final String PROP_COMMENT = "comment";
	public static final String PROP_DESCRIPTION = "description";
	public static final String PROP_RATIONALE = "rationale";
	public static final String PROP_COMMENTS = "resourceComments";
	
	private Repository repository;
	private String entityId;
	private int versionCount;
	private String nodeType;
	private Revision revision;
	private Timestamp revisionTime;
	private Member lockedBy;
	private Timestamp lockedAt;
	private String comment;
	private List<AccessControlEntry> accessControlEntries = new TrackedList<AccessControlEntry>(propertyChangeSupport, "accessControlEntries");
	private String description;
	private String rationale;
	private long touchCount;
	private long usageCount;
	private List<ResourceComment> resourceComments = new TrackedList<ResourceComment>(propertyChangeSupport, PROP_COMMENTS);
	private ResourceCharacter resourceCharacter;
	private Member finishedBy;
	private Member ownedBy;
	private List<ReviewEntry> reviewEntries = new TrackedList<ReviewEntry>(propertyChangeSupport, "reviewEntries");

	@Transient
	private boolean childrenLoaded;
	@Transient
	private List<ResourceVersion> children = new TrackedList<ResourceVersion>(propertyChangeSupport, "children");
	@Transient
	private ResourceVersion resourceVersion;
	
	
	
	
	public Resource() {
		super();
	}
	
	public Resource(GUID id) {
		super(id);
	}
	
	public List<ResourceVersion> getChildren() {
		return children;
	}	
	
	@Column(name="NODE_TYPE")
	public String getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(String nodeType) {
		propertyChangeSupport.firePropertyChange("nodeType", this.nodeType, this.nodeType = nodeType);
	}
	
	@ManyToOne
	@JoinColumn(name="REPOSITORY_ID", referencedColumnName="ID")	
	public Repository getRepository() {
		return repository;
	}
	
	public void setRepository(Repository repository) {
		propertyChangeSupport.firePropertyChange("repository", this.repository, this.repository = repository);
	}
	
	@Column(name="ENTITY_ID")
	public String getEntityId() {
		return entityId;
	}
	
	public void setEntityId(String entityId) {
		propertyChangeSupport.firePropertyChange("entityId", this.entityId, this.entityId = entityId);
	}
	
	@Column(name="VERSION_COUNT")
	public int getVersionCount() {
		return versionCount;
	}
	
	public void setVersionCount(int versionCount) {
		propertyChangeSupport.firePropertyChange("versionCount", this.versionCount, this.versionCount = versionCount);
	}
	
	@ManyToOne
	@JoinColumn(name="REVISION_ID", referencedColumnName="ID")
	public Revision getRevision() {
		return revision;
	}
	
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
	
	@Column(name="REVISION_TIME")
	public Timestamp getRevisionTime() {
		return revisionTime;
	}
	
	public void setRevisionTime(Timestamp revisionTime) {
		propertyChangeSupport.firePropertyChange("revisionTime", this.revisionTime, this.revisionTime = revisionTime);
	}	
	
	@ManyToOne
	@JoinColumn(name="LOCKED_BY", referencedColumnName="ID")
	public Member getLockedBy() {
		return lockedBy;
	}

	public void setLockedBy(Member lockedBy) {
		propertyChangeSupport.firePropertyChange("lockedBy", this.lockedBy, this.lockedBy = lockedBy);
	}
	
	@Column(name="LOCKED_AT")	
	public Timestamp getLockedAt() {
		return lockedAt;
	}

	public void setLockedAt(Timestamp lockedAt) {
		propertyChangeSupport.firePropertyChange("lockedAt", this.lockedAt, this.lockedAt = lockedAt);
	}
	
	@ManyToOne
	@JoinColumn(name="FINISHED_BY", referencedColumnName="ID")
	public Member getFinishedBy() {
		return finishedBy;
	}

	public void setFinishedBy(Member finishedBy) {
		propertyChangeSupport.firePropertyChange("finishedBy", this.finishedBy, this.finishedBy = finishedBy);
	}
	
	@ManyToOne
	@JoinColumn(name="OWNED_BY", referencedColumnName="ID")
	public Member getOwnedBy() {
		return ownedBy;
	}
	
	public void setOwnedBy(Member ownedBy) {
		propertyChangeSupport.firePropertyChange("ownedBy", this.ownedBy, this.ownedBy = ownedBy);
	}
	
	@Column(name="NODE_COMMENT")
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		propertyChangeSupport.firePropertyChange(PROP_COMMENT, this.comment, this.comment = comment);
	}
	
	public ResourceVersion getResourceVersion() {
		return resourceVersion;
	}
	
	public void setResourceVersion(ResourceVersion resourceVersion) {
		propertyChangeSupport.firePropertyChange("resourceVersion", this.resourceVersion, this.resourceVersion = resourceVersion);
	}
	
	@OneToMany(mappedBy="resource")
	public List<AccessControlEntry> getAccessControlEntries() {
		return accessControlEntries;
	}
	
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange(PROP_DESCRIPTION, this.description, this.description = description);
	}
	
	@Column(name="RATIONALE")
	public String getRationale() {
		return rationale;
	}
	
	public void setRationale(String rationale) {
		propertyChangeSupport.firePropertyChange(PROP_RATIONALE, this.rationale, this.rationale = rationale);
	}
	
	@Column(name="TOUCH_COUNT")
	public long getTouchCount() {
		return touchCount;
	}
	
	public void setTouchCount(long touchCount) {
		propertyChangeSupport.firePropertyChange("touchCount", this.touchCount, this.touchCount = touchCount);
	}	
	
	@Column(name="USAGE_COUNT")
	public long getUsageCount() {
		return usageCount;
	}
	
	public void setUsageCount(long usageCount) {
		propertyChangeSupport.firePropertyChange("usageCount", this.usageCount, this.usageCount = usageCount);
	}
	
	@OneToMany(mappedBy="resource")
	public List<ResourceComment> getResourceComments() {
		return resourceComments;
	}
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_CHARACTER", referencedColumnName="ID")
	public ResourceCharacter getResourceCharacter() {
		return resourceCharacter;
	}
	
	public void setResourceCharacter(ResourceCharacter resourceCharacter) {
		propertyChangeSupport.firePropertyChange("resourceCharacter", this.resourceCharacter, this.resourceCharacter = resourceCharacter);
	}
	
	@OneToMany(mappedBy="resource")
	public List<ReviewEntry> getReviewEntries() {
		return reviewEntries;
	}
	
	public void setReviewEntries(List<ReviewEntry> reviewEntries) {
		this.reviewEntries = reviewEntries;
	}
	
	@Transient
	public boolean hasChild(String name) {
		for (ResourceVersion child : children) {
			if (child.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	@Transient
	public boolean hasChild(String name, Class<? extends ResourceVersion> type) {
		for (ResourceVersion child : children) {
			if (name.equals(child.getName()) && type.isAssignableFrom(child.getClass())) {
				return true;
			}
		}
		return false;
	}
	
	@Transient
	public boolean hasChild(ResourceVersion resourceVersion) {
		for (ResourceVersion child : children) {
			if (child.equals(resourceVersion)) {
				return true;
			}
		}
		return false;
	}
	
	@Transient
	public ResourceVersion findChild(GUID resourceId) {
		for (ResourceVersion child : children) {
			if(resourceId.equals(child.getResource().getId())) {
				return child;
			}
			ResourceVersion childVersion = child.getResource().findChild(resourceId);
			if(childVersion != null) {
				return childVersion;
			}
		}
		return null;
	}
	
	public boolean isChildrenLoaded() {
		return childrenLoaded;
	}
	
	public void setChildrenLoaded(boolean childrenLoaded) {
		this.childrenLoaded = childrenLoaded;
	}
	

}
