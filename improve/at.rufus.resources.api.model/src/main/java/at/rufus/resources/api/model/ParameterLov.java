package at.rufus.resources.api.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.AbstractLov;


@javax.persistence.Entity
@Table(name="PARAMETER_LOV")
public class ParameterLov extends AbstractLov {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<ParameterLov> COMPARATOR_NAME = new Comparator<ParameterLov>() {
		@Override
		public int compare(ParameterLov o1, ParameterLov o2) {
			return CmpUtil.cmpNatural(o1.name, o2.name, true);
		}
	};
	
	@Column(name="TYPE")
	private ParameterType type;

	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="SYSTEM_LAYER")
	private SystemLayer systemLayer;
	
	@Column(name="ROLE")
	private ParameterRole role;
	
	@Column(name="EXPIRED")
	private boolean expired;

	
	public ParameterType getType() {
		return type;
	}
	
	public void setType(ParameterType type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange("description", this.description, this.description = description);
	}
	
	public SystemLayer getSystemLayer() {
		return systemLayer;
	}
	
	public void setSystemLayer(SystemLayer systemLayer) {
		propertyChangeSupport.firePropertyChange("systemLayer", this.systemLayer, this.systemLayer = systemLayer);
	}
	
	public ParameterRole getRole() {
		return role;
	}
	
	public void setRole(ParameterRole role) {
		propertyChangeSupport.firePropertyChange("role", this.role, this.role = role);
	}
	
	public boolean isExpired() {
		return expired;
	}
	
	public void setExpired(boolean expired) {
		propertyChangeSupport.firePropertyChange("expired", this.expired, this.expired = expired);
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public static BigDecimal toNumberValue(String paramValue) throws ArithmeticException, NumberFormatException {
		return new BigDecimal(paramValue, MathContext.DECIMAL64);
	}

}
