package at.rufus.resources.api.model;

import java.math.BigDecimal;

import at.rufus.base.api.common.Bean;
import at.rufus.base.api.model.GUID;

public class EntityParameter extends Bean {
	private static final long serialVersionUID = 1L;
	
	private GUID id;
	private GUID entityId;
	private ParameterLov paramLov;
	private String textValue;
	private BigDecimal numValue;
	
	public GUID getId() {
		return id;
	}
	
	public void setId(GUID id) {
		propertyChangeSupport.firePropertyChange("id", this.id, this.id = id);
	}
	
	public GUID getEntityId() {
		return entityId;
	}
	
	public void setEntityId(GUID entityId) {
		propertyChangeSupport.firePropertyChange("entityId", this.entityId, this.entityId = entityId);
	}
	
	public ParameterLov getParamLov() {
		return paramLov;
	}
	
	public void setParamLov(ParameterLov paramLov) {
		propertyChangeSupport.firePropertyChange("paramLov", this.paramLov, this.paramLov = paramLov);
	}
	
	public String getTextValue() {
		return textValue;
	}
	
	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.textValue, this.textValue = textValue);
	}
	
	public BigDecimal getNumValue() {
		return numValue;
	}
	
	public void setNumValue(BigDecimal numValue) {
		propertyChangeSupport.firePropertyChange("numValue", this.numValue, this.numValue = numValue);
	}
	
	public Boolean getBooleanValue() {
		return numValue != null ? (numValue.compareTo(BigDecimal.ONE) == 0 ? Boolean.TRUE : Boolean.FALSE) : null;
	}
	
	public void setBooleanValue(Boolean booleanValue) {
		setNumValue(booleanValue != null ? (booleanValue.booleanValue() ? BigDecimal.ONE : BigDecimal.ZERO) : null);
	}
	
	public void setEnumValue(Enum<?> enumValue) {
		setTextValue(enumValue != null ? enumValue.name() : null);
	}
	
	public <E extends Enum<E>> E getEnumValue(Class<E> enumType) {
		return textValue != null ? Enum.valueOf(enumType, textValue) : null;
	}

}
