package at.rufus.resources.api.model;

import java.beans.PropertyChangeListener;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="REVISION")
public class Revision extends Entity  {
	private static final long serialVersionUID = 1L;
	
//	public static final long SPECIAL_MAX = Long.MAX_VALUE;
//	public static final long LATEST = Long.MAX_VALUE-1;
	
	public static final GUID LATEST_ID = GUID.fromHexString("43F431D59DC9406BB00E17E0798B84CC");
	public static final Timestamp LATEST_TIME = new Timestamp(253402214400000l);				// 9999-12-31 00:00.000000000 UTC
	public static final Timestamp LATEST_TIME_RANGE_CMP = new Timestamp(253402214400000l-1l);	// 9999-12-30 23:59.999000000 UTC
	public static final Timestamp BEGIN_TIME = new Timestamp(0l);								// 1970-01-01 00:00.000000000 UTC
	
	public static final String CREATING_ACTION_CREATE = "create";
	public static final String CREATING_ACTION_DELETE = "delete";
	public static final String CREATING_ACTION_UNDELETE = "undelete";
	public static final String CREATING_ACTION_BULKIMPORT = "bulkimport";
	public static final String CREATING_ACTION_RESET = "reset";
	
	@Column(name="CREATED_AT")
	private Timestamp createdAt;
	
	@ManyToOne
	@JoinColumn(name="CREATED_BY", referencedColumnName="ID")
	private Member createdBy;
	
	@Column(name="CREATING_ACTION")
	private String creatingAction;

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Timestamp createdAt) {
		propertyChangeSupport.firePropertyChange("createdAt", this.createdAt, this.createdAt = createdAt);
	}
	
	public Member getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(Member createdBy) {
		propertyChangeSupport.firePropertyChange("createdBy", this.createdBy, this.createdBy = createdBy);
	}
	
	public String getCreatingAction() {
		return creatingAction;
	}
	
	public void setCreatingAction(String creatingAction) {
		propertyChangeSupport.firePropertyChange("creatingAction", this.creatingAction, this.creatingAction = creatingAction);
	}
	
	@Override
	public String toString() {
		return "Revision " + getId() == null ? "null" : getId().toString();
	}
	
}
