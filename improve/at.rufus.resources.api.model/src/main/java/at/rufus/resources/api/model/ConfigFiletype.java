package at.rufus.resources.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="CONFIG_FILETYPE")
public class ConfigFiletype extends Entity {
	private static final long serialVersionUID = 1L;

	private byte[] icon;
	private String label;
	private String pattern;
	private EditorType editorType;
	private String externalEditor;
	private InternalEditorType internalEditorType;
	private SyntaxHighlighter syntaxHighlighter;
	private Member member;
	
	public void merge(ConfigFiletype filetype) {
		if (icon == null) {
			icon = filetype.icon;
		}
		if (label == null || label.isEmpty()) {
			label = filetype.label;
		}
		if (editorType == null) {
			editorType = filetype.editorType;
		}
		if (externalEditor == null || externalEditor.isEmpty()) {
			externalEditor = filetype.externalEditor;
		}
		if (internalEditorType == null) {
			internalEditorType = filetype.internalEditorType;
		}
		if (syntaxHighlighter == null) {
			syntaxHighlighter = filetype.syntaxHighlighter;
		}
	}
	
	@Column(name="ICON")
	public byte[] getIcon() {
		return icon;
	}
	
	public void setIcon(byte[] icon) {
		propertyChangeSupport.firePropertyChange("icon", this.icon, this.icon = icon);
	}
	
	@Column(name="LABEL")
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		propertyChangeSupport.firePropertyChange("label", this.label, this.label = label);
	}
	
	@Column(name="PATTERN")
	public String getPattern() {
		return pattern;
	}
	
	public void setPattern(String pattern) {
		propertyChangeSupport.firePropertyChange("pattern", this.pattern, this.pattern = pattern);
	}
	
	@Column(name="EXTERNAL_EDITOR")
	public String getExternalEditor() {
		return externalEditor;
	}
	
	public void setExternalEditor(String externalEditor) {
		propertyChangeSupport.firePropertyChange("externalEditor", this.externalEditor, this.externalEditor = externalEditor);
	}
	
	public static List<String> splitPatterns(String patternString) {
		if(patternString == null || (patternString = patternString.trim()).isEmpty()) {
			return new ArrayList<String>(0);
		} else {
			String[] split = patternString.split(";");
			List<String> patterns = new ArrayList<String>(split.length);
			for(String pattern : split) {
				if(!(pattern = pattern.trim()).isEmpty()) {
					patterns.add(pattern);
				}
			}
			return patterns;
		}
	}

	@Column(name="EDITOR_TYPE")
	public EditorType getEditorType() {
		return editorType;
	}

	public void setEditorType(EditorType editorType) {
		propertyChangeSupport.firePropertyChange("editorType", this.editorType, this.editorType = editorType);
	}

	@Column(name="INT_EDITOR_TYPE")
	public InternalEditorType getInternalEditorType() {
		return internalEditorType;
	}

	public void setInternalEditorType(InternalEditorType internalEditorType) {
		propertyChangeSupport.firePropertyChange("internalEditorType", this.internalEditorType, this.internalEditorType = internalEditorType);
	}

	public static String getPattern(String filename) {
		if (filename == null) {
			return null;
		}
		int idx = filename.lastIndexOf('.');
		if(idx >= 0 && idx < filename.length() - 1) {
			return "*" + filename.substring(idx).toLowerCase();
		}
		return filename.toLowerCase();
	}

	@ManyToOne
	@JoinColumn(name="SYNTAX_HIGHLIGHTER", referencedColumnName="ID")
	public SyntaxHighlighter getSyntaxHighlighter() {
		return syntaxHighlighter;
	}
	
	public void setSyntaxHighlighter(SyntaxHighlighter syntaxHighlighter) {
		propertyChangeSupport.firePropertyChange("syntaxHighlighter", this.syntaxHighlighter, this.syntaxHighlighter = syntaxHighlighter);
	}

	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange("member", this.member, this.member = member);
	}

}
