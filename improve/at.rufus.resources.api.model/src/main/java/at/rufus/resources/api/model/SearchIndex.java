package at.rufus.resources.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="SEARCH_INDEX")
public class SearchIndex extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name = "INDEX_ALIAS")
	private String indexAlias;
	
	@Column(name = "ACTIVE_INDEX_NAME")
	private String activeIndexName;
	
	@Column(name="ACTIVE_CREATED_AT")
	private Timestamp activeCreatedAt;

	@Column(name="ACTIVE_REINDEXED_AT")
	private Timestamp activeReindexedAt;
	
	@Column(name="ACTIVE_ROOT_RESOURCE_ID")
	private GUID activeRootResourceId;
	
	@Column(name = "NEXT_INDEX_NAME")
	private String nextIndexName;
	
	@Column(name="NEXT_CREATED_AT")
	private Timestamp nextCreatedAt;
	
	@Column(name="NEXT_CANCELED_AT")
	private Timestamp nextCanceledAt;
	
	@Column(name="NEXT_ROOT_RESOURCE_ID")
	private GUID nextRootResourceId;
	
	@Column(name = "LAST_RESOURCE_ID")
	private GUID lastResourceId;
	
	@Column(name = "LAST_REVISION_TIME")
	private Timestamp lastRevistionTime;
	
	@Transient
	private FileReindexJob lastFileReindexJob;
	
	
	public SearchIndex() {
		super();
	}
	
	public SearchIndex(GUID id) {
		super(id);
	}
	
	public String getIndexAlias() {
		return indexAlias;
	}
	
	public void setIndexAlias(String indexAlias) {
		propertyChangeSupport.firePropertyChange("indexAlias", this.indexAlias, this.indexAlias = indexAlias);
	}
	
	public String getActiveIndexName() {
		return activeIndexName;
	}
	
	public void setActiveIndexName(String activeIndexName) {
		propertyChangeSupport.firePropertyChange("activeIndexName", this.activeIndexName, this.activeIndexName = activeIndexName);
	}
	
	public GUID getActiveRootResourceId() {
		return activeRootResourceId;
	}
	
	public void setActiveRootResourceId(GUID activeRootResourceId) {
		propertyChangeSupport.firePropertyChange("activeRootResourceId", this.activeRootResourceId, this.activeRootResourceId = activeRootResourceId);
	}
	
	public String getNextIndexName() {
		return nextIndexName;
	}
	
	public void setNextIndexName(String nextIndexName) {
		propertyChangeSupport.firePropertyChange("nextIndexName", this.nextIndexName, this.nextIndexName = nextIndexName);
	}
	
	public Timestamp getActiveCreatedAt() {
		return activeCreatedAt;
	}
	
	public void setActiveCreatedAt(Timestamp activeCreatedAt) {
		propertyChangeSupport.firePropertyChange("activeCreatedAt", this.activeCreatedAt, this.activeCreatedAt = activeCreatedAt);
	}
	
	public Timestamp getActiveReindexedAt() {
		return activeReindexedAt;
	}
	
	public void setActiveReindexedAt(Timestamp activeReindexedAt) {
		propertyChangeSupport.firePropertyChange("activeReindexedAt", this.activeReindexedAt, this.activeReindexedAt = activeReindexedAt);
	}
	
	public Timestamp getNextCreatedAt() {
		return nextCreatedAt;
	}
	
	public void setNextCreatedAt(Timestamp nextCreatedAt) {
		propertyChangeSupport.firePropertyChange("nextCreatedAt", this.nextCreatedAt, this.nextCreatedAt = nextCreatedAt);
	}
	
	public Timestamp getNextCanceledAt() {
		return nextCanceledAt;
	}
	
	public void setNextCanceledAt(Timestamp nextCanceledAt) {
		propertyChangeSupport.firePropertyChange("nextCanceledAt", this.nextCanceledAt, this.nextCanceledAt = nextCanceledAt);
	}
	
	public GUID getNextRootResourceId() {
		return nextRootResourceId;
	}
	
	public void setNextRootResourceId(GUID nextRootResourceId) {
		propertyChangeSupport.firePropertyChange("nextRootResourceId", this.nextRootResourceId, this.nextRootResourceId = nextRootResourceId);
	}
	
	public GUID getLastResourceId() {
		return lastResourceId;
	}
	
	public void setLastResourceId(GUID lastResourceId) {
		propertyChangeSupport.firePropertyChange("lastResourceId", this.lastResourceId, this.lastResourceId = lastResourceId);
	}
	
	public Timestamp getLastRevistionTime() {
		return lastRevistionTime;
	}
	
	public void setLastRevistionTime(Timestamp lastRevistionTime) {
		propertyChangeSupport.firePropertyChange("lastRevistionTime", this.lastRevistionTime, this.lastRevistionTime = lastRevistionTime);
	}
	
	public FileReindexJob getLastFileReindexJob() {
		return lastFileReindexJob;
	}
	
	public void setLastFileReindexJob(FileReindexJob lastFileReindexJob) {
		propertyChangeSupport.firePropertyChange("lastFileReindexJob", this.lastFileReindexJob, this.lastFileReindexJob = lastFileReindexJob);
	}
	
}
