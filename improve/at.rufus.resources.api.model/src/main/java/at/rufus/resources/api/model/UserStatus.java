package at.rufus.resources.api.model;

public enum UserStatus {
	active,
	inactive
}
