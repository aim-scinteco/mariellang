package at.rufus.resources.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="RESOURCE_COMMENT")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value="GENERAL_COMMENT")
public class ResourceComment extends Entity {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	private Resource resource;
	
	@ManyToOne
	@JoinColumn(name="REVISION_ID", referencedColumnName="ID")
	private Revision revision;

	@Column(name="REVISION_TIME")
	private Timestamp revisionTime;
	
	@Column(name="TEXT")
	private String comment;
	
	@Column(name="TYPE")
	private String type;

	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}

	public Revision getRevision() {
		return revision;
	}

	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}

	public Timestamp getRevisionTime() {
		return revisionTime;
	}
	
	public void setRevisionTime(Timestamp revisionTime) {
		propertyChangeSupport.firePropertyChange("revisionTime", this.revisionTime, this.revisionTime = revisionTime);
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		propertyChangeSupport.firePropertyChange("comment", this.comment, this.comment = comment);
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	
}
