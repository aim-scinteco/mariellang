package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;


@javax.persistence.Entity
@Table(name="APP_GROUP")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(Group.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Group extends Member {
	private static final long serialVersionUID = 1L;
	
	public static final String DISCRIMINATOR=RepositoryEntityTypes.APP_GROUP;
	
	public static final String GROUP_TYPE = "groupType";
	public static final String NAME = "name";
	public static final String CHILDREN = "children";
	public static final String SYSTEM_GROUP_ID = "systemGroupId";
	
	private String name;
	private String systemGroupId;
	private List<Member> children = new TrackedList<Member>(propertyChangeSupport, CHILDREN);
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange(NAME,this.name,this.name=name);
	}
	
	@Column(name="SYSTEM_GROUP_ID")
	public String getSystemGroupId() {
		return systemGroupId;
	}
	
	public void setSystemGroupId(String systemGroupId) {
		this.systemGroupId = systemGroupId;
	}
	
	@Transient
	public boolean isSystemGroup() {
		return systemGroupId != null;
	}
	
	@ManyToMany
	@JoinTable(
		name="GROUP_PARENTAL_RELATION",
		joinColumns=@JoinColumn(name="PARENT", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="CHILD", referencedColumnName="ID")
	)
	public List<Member> getChildren() {
		return children;
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return Group.DISCRIMINATOR;
	}

	
}
