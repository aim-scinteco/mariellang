package at.rufus.resources.api.model;

import java.io.Serializable;

import at.rufus.base.api.model.GUID;

public class ResourceRelationId implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private GUID sourceId;
	private GUID targetId;
	
	public ResourceRelationId() {
	}
	
	public ResourceRelationId(GUID sourceId, GUID targetId) {
		this.sourceId = sourceId;
		this.targetId = targetId;
	}

	public GUID getSourceId() {
		return sourceId;
	}
	
	public void setSourceId(GUID sourceId) {
		this.sourceId = sourceId;
	}
	
	public GUID getTargetId() {
		return targetId;
	}
	
	public void setTargetId(GUID targetId) {
		this.targetId = targetId;
	}
	
	@Override
	public int hashCode() {
		return sourceId.hashCode() ^ targetId.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return obj instanceof ResourceRelationId && ((ResourceRelationId) obj).sourceId.equals(sourceId) && ((ResourceRelationId) obj).targetId.equals(targetId);
	}
	
}
