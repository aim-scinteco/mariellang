package at.rufus.resources.api.model;

public enum MetadataType {

	TEXT("Text"),
	NUMBER("Number"),
	DATE("Date"),
	LOV("Picklist"),
	URL("URL");
	
	private final String label;
	
	private MetadataType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
