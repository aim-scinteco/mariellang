package at.rufus.resources.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="TAG")
public class Tag extends Entity  {
	private static final long serialVersionUID = 1L;
	
	@Column(name="SCOPE")
	private String scope;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="HISTORY_ID")
	private GUID historyId;

	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	private Resource resource;

	@ManyToOne
	@JoinColumn(name="REVISION_FROM_ID", referencedColumnName="ID")
	private Revision revision;

	@Column(name="REVISION_FROM_TIME")
	private Timestamp revisionFromTime;
	
	@Column(name="REVISION_TO_TIME")
	private Timestamp revisionToTime;

	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		propertyChangeSupport.firePropertyChange("scope", this.scope, this.scope = scope);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	public GUID getHistoryId() {
		return historyId;
	}
	public void setHistoryId(GUID historyId) {
		propertyChangeSupport.firePropertyChange("historyId", this.historyId, this.historyId = historyId);
	}
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}
	public Revision getRevision() {
		return revision;
	}
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
	public Timestamp getRevisionFromTime() {
		return revisionFromTime;
	}
	public void setRevisionFromTime(Timestamp revisionFromTime) {
		propertyChangeSupport.firePropertyChange("revisionFromTime", this.revisionFromTime, this.revisionFromTime = revisionFromTime);
	}
	public Timestamp getRevisionToTime() {
		return revisionToTime;
	}
	public void setRevisionToTime(Timestamp revisionToTime) {
		propertyChangeSupport.firePropertyChange("revisionToTime", this.revisionToTime, this.revisionToTime = revisionToTime);
	}
		
}
