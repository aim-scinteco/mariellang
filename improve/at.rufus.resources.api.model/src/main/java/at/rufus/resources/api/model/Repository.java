package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="REPOSITORY")
public class Repository extends Entity {
	private static final long serialVersionUID = 1L;

	public final static String SEPARATOR = ":";
	
	private String name;
	private String alias;

	@Column(name="NAME")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	@Column(name="ALIAS")
	public String getAlias() {
		return alias;
	}
	
	public void setAlias(String alias) {
		propertyChangeSupport.firePropertyChange("alias", this.alias, this.alias = alias);
	}
	
}
