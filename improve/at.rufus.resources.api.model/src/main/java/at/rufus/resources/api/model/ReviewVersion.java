package at.rufus.resources.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="REVIEW_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(ReviewVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class ReviewVersion extends ResourceVersion{

	public static final String DISCRIMINATOR=RepositoryEntityTypes.REVIEW_VERSION;
	
	private static final long serialVersionUID = 1L;

    @Column(name="DUE_DATE")
    private Date dueDate;
	
	@ManyToOne
	@JoinColumn(name = "REVIEW_TEMPLATE", referencedColumnName = "ID")
	private ReviewTemplate template;
	
	
	public Date getDueDate() {
        return dueDate;
    }
	
	public void setDueDate(Date dueDate) {
	    propertyChangeSupport.firePropertyChange("dueDate", this.dueDate, this.dueDate = dueDate);
    }
	
	public ReviewTemplate getTemplate() {
        return template;
    }
	
	public void setTemplate(ReviewTemplate template) {
        propertyChangeSupport.firePropertyChange("template", this.template, this.template = template);
    }
	
	@Transient
	public Review getReview() {
		return (Review) getResource();
	}
	
	
	@Override
	@Transient
	public String getDiscriminator() {
		return ReviewVersion.DISCRIMINATOR;
	}
	
	@Override
	public ReviewVersion copy() {
	     ReviewVersion copy = (ReviewVersion) super.copy();
	     copy.setTemplate(getTemplate());
	     copy.setDueDate(getDueDate());
	     return copy;
	}
}
