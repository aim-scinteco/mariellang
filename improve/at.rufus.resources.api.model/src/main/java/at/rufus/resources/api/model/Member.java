package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="APP_MEMBER")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(
	name="MEMBER_TYPE",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(Member.DISCRIMINATOR)
public class Member extends Entity  {
	
	private static final long serialVersionUID = 1L;
	public static final String DISCRIMINATOR=RepositoryEntityTypes.APP_MEMBER;
	
	public static final String PARENTS = "parents";
	
	private List<Group> parents = new TrackedList<Group>(propertyChangeSupport, PARENTS);
	
	public Member() {
		super();
	}
	
	public Member(GUID userId) {
		super(userId);
	}
	
	@ManyToMany
	@JoinTable(
		name="GROUP_PARENTAL_RELATION",
		joinColumns=@JoinColumn(name="CHILD", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="PARENT", referencedColumnName="ID")
	)
	public List<Group> getParents() {
		return parents;
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return Member.DISCRIMINATOR;
	}
}
