package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNTAX_IDENTIFIER_MODIFIER")
public class SyntaxIdentifierModifier extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="POST_IDENTIFIER_PATTERN")
	private String postIdentifierPattern;
	
	@Column(name="IDENTIFIER_COLOR")
	private int identifierColor;
	
	@Column(name="IDENTIFIER_FONT_STYLE")
	private int identifierFontStyle;
	
	@ManyToOne
	@JoinColumn(name="SYNTAX_HIGHLIGHTER", referencedColumnName="ID")
	private SyntaxHighlighter syntaxHighlighter;

	public String getPostIdentifierPattern() {
		return postIdentifierPattern;
	}
	
	public void setPostIdentifierPattern(String postIdentifierPattern) {
		propertyChangeSupport.firePropertyChange("postIdentifierPattern", this.postIdentifierPattern, this.postIdentifierPattern = postIdentifierPattern);
	}

	public int getIdentifierColor() {
		return identifierColor;
	}
	
	public void setIdentifierColor(int identifierColor) {
		propertyChangeSupport.firePropertyChange("identifierColor", this.identifierColor, this.identifierColor = identifierColor);
	}
	
	public int getIdentifierFontStyle() {
		return identifierFontStyle;
	}
	
	public void setIdentifierFontStyle(int identifierFontStyle) {
		propertyChangeSupport.firePropertyChange("identifierFontStyle", this.identifierFontStyle, this.identifierFontStyle = identifierFontStyle);
	}
	
	public SyntaxHighlighter getSyntaxHighlighter() {
		return syntaxHighlighter;
	}
	
	public void setSyntaxHighlighter(SyntaxHighlighter syntaxHighlighter) {
		propertyChangeSupport.firePropertyChange("syntaxHighlighter", this.syntaxHighlighter, this.syntaxHighlighter = syntaxHighlighter);
	}

}
