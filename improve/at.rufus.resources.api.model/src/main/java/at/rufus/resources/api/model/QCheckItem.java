package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="QC_CHECK_ITEM")
public class QCheckItem extends Entity {
    private static final long serialVersionUID = 1L;
    
    public static final String PROP_DESCRIPTION = "description";
    public static final String PROP_SEQ_NO = "seqNo";
    
    @ManyToOne
    @JoinColumn(name = "QC_CHECK", referencedColumnName = "ID")
    private QCheck qc;
    
    @Column(name = "SEQ_NO")
    private int seqNo;
    
    @Column(name = "DESCRIPTION")
    private String description;

    public QCheck getQc() {
        return qc;
    }
    
    public void setQc(QCheck qc) {
        this.qc = qc;
    }
    
    public int getSeqNo() {
        return seqNo;
    }
    
    public void setSeqNo(int seqNo) {
        propertyChangeSupport.firePropertyChange(PROP_SEQ_NO, this.seqNo, this.seqNo = seqNo);
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        propertyChangeSupport.firePropertyChange(PROP_DESCRIPTION, this.description, this.description = description);
    }
}
