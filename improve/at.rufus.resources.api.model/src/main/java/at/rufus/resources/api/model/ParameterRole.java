package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.AbstractLov;

@javax.persistence.Entity
@Table(name="PARAMETER_ROLE")
public class ParameterRole extends AbstractLov {
	private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="SYSTEM_LAYER")
	private SystemLayer systemLayer;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange("description", this.description, this.description = description);
	}
	
	public SystemLayer getSystemLayer() {
		return systemLayer;
	}
	
	public void setSystemLayer(SystemLayer systemLayer) {
		this.systemLayer = systemLayer;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
