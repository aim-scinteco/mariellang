package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="RESOURCE_CHARACTER_METADATA")
public class ResourceCharacterMetadata extends Entity  {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="METADATA_DESC", referencedColumnName="ID")
	private MetadataDescriptor metadataDescriptor;

	@ManyToOne
	@JoinColumn(name="RESOURCE_CHARACTER", referencedColumnName="ID")
	private ResourceCharacter resourceCharacter;

	@Column(name="MANDATORY")
	private boolean mandatory;

	
	public MetadataDescriptor getMetadataDescriptor() {
		return metadataDescriptor;
	}
	
	public void setMetadataDescriptor(MetadataDescriptor metadataDescriptor) {
		propertyChangeSupport.firePropertyChange("metadataDescriptor", this.metadataDescriptor, this.metadataDescriptor = metadataDescriptor);
	}
	
	public ResourceCharacter getResourceCharacter() {
		return resourceCharacter;
	}
	
	public void setResourceCharacter(ResourceCharacter resourceCharacter) {
		propertyChangeSupport.firePropertyChange("resourceCharacter", this.resourceCharacter, this.resourceCharacter = resourceCharacter);
	}
	
	public boolean isMandatory() {
		return mandatory;
	}
	
	public void setMandatory(boolean mandatory) {
		propertyChangeSupport.firePropertyChange("mandatory", this.mandatory, this.mandatory = mandatory);
	}
	
}
