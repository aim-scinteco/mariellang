package at.rufus.resources.api.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import at.rufus.base.api.model.GUID;

public class GroupTree {

	private final Map<GUID, GroupNode> nodeMap = new HashMap<GUID, GroupNode>();
	private final List<GroupNode> rootNodes = new ArrayList<GroupNode>(3);
	
	public List<GroupNode> getRootNodes() {
		return rootNodes;
	}
	
	public Map<GUID, GroupNode> getNodeMap() {
		return nodeMap;
	}
	
	public GroupNode findParent(GUID nodeId, GUID parentId) {
		GroupNode node = nodeMap.get(nodeId);
		if (node != null) {
			return findParent(node, parentId);
		}
		return null;
	}
	
	private GroupNode findParent(GroupNode node, GUID parentId) {
		GroupNode match = null;
		for (GroupNode parent : node.getParents()) {
			if (parentId.equals(parent.getId())) {
				match = parent;
				break;
			}
		}
		if (match == null) {
			for (GroupNode parent : node.getParents()) {
				match = findParent(parent, parentId);
				if (match != null) {
					break;
				}
			}
		}
		return match;
	}
	
	public Set<GUID> resolveGroupToUsers(GUID memberId) {
		GroupNode node = nodeMap.get(memberId);
		if (node != null) {
			Set<GUID> userIds = new HashSet<GUID>();
			collectUsers(node, userIds);
			return userIds;
		} else {
			return null;
		}
	}
	
	public Set<GUID> getAllUsers() {
		Set<GUID> userIds = new HashSet<GUID>();
		for (GroupNode rootNode : rootNodes) {
			collectUsers(rootNode, userIds);
		}
		return userIds;
	}
	
	private void collectUsers(GroupNode node, Set<GUID> userIds) {
		if (node.isUser()) {
			userIds.add(node.getId());
		} else {
			for (GroupNode childNode : node.getChildren()) {
				collectUsers(childNode, userIds);
			}
		}
	}
	
	public String getUserSystemGroup(GUID userId) {
		GroupNode node = nodeMap.get(userId);
		if (node != null && node.isUser()) {
			for (GroupNode parent : node.getParents()) {
				if (parent.getSystemGroupId() != null) {
					return parent.getSystemGroupId();
				}
			}
		}
		return null;
	}

	
}
