package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="FILE_NODE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(RepositoryEntityTypes.FILE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class File extends Resource {
	private static final long serialVersionUID = 1L;
	
	public static final String PROP_STATUS = "status";
	
//	private long workingFileId;
//	private boolean workingFileModified;
//	private Timestamp workingFileLastModifiedAt;
	private FileStatus status;
	private String workingFileFStorePath;
	private Long workingFileSize;
	private byte[] workingFileHash;
	
	
	@Column(name="WFILE_FILE_STORE_PATH")
	public String getWorkingFileFStorePath() {
		return workingFileFStorePath;
	}
	
	public void setWorkingFileFStorePath(String workingFileFStorePath) {
		propertyChangeSupport.firePropertyChange("workingFileFStorePath", this.workingFileFStorePath, this.workingFileFStorePath = workingFileFStorePath);
	}
	
	@Column(name="WFILE_FILESIZE")
	public Long getWorkingFileSize() {
		return workingFileSize;
	}
	
	public void setWorkingFileSize(Long workingFileSize) {
		propertyChangeSupport.firePropertyChange("workingFileSize", this.workingFileSize, this.workingFileSize = workingFileSize);
	}
	
	@Column(name="WFILE_HASH")
	public byte[] getWorkingFileHash() {
		return workingFileHash;
	}
	
	public void setWorkingFileHash(byte[] workingFileHash) {
		propertyChangeSupport.firePropertyChange("workingFileHash", this.workingFileHash, this.workingFileHash = workingFileHash);
	}
	
//	@Column(name="WFILE_ID")
//	public long getWorkingFileId() {
//		return workingFileId;
//	}
//	
//	public void setWorkingFileId(long workingFileId) {
//		propertyChangeSupport.firePropertyChange("workingFileId", this.workingFileId, this.workingFileId = workingFileId);
//	}
	
//	@Column(name="WFILE_MODIFIED")
//	public boolean isWorkingFileModified() {
//		return workingFileModified;
//	}
//	
//	public void setWorkingFileModified(boolean workingFileModified) {
//		propertyChangeSupport.firePropertyChange("workingFileModified", this.workingFileModified, this.workingFileModified = workingFileModified);
//	}
	
//	@Column(name="WFILE_LAST_MODIFIED_AT")
//	public Timestamp getWorkingFileLastModifiedAt() {
//		return workingFileLastModifiedAt;
//	}
//
//	public void setWorkingFileLastModifiedAt(Timestamp workingFileLastModifiedAt) {
//		this.workingFileLastModifiedAt = workingFileLastModifiedAt;
//	}
	
	@Column(name="FILE_STATUS")
	public FileStatus getStatus() {
		return status;
	}
	
	public void setStatus(FileStatus status) {
		propertyChangeSupport.firePropertyChange(PROP_STATUS, this.status, this.status = status);
	}

	@Transient
	@Override
	public String getDiscriminator() {
		return RepositoryEntityTypes.FILE;
	}

}
