package at.rufus.resources.api.model;

import java.io.Serializable;

import at.rufus.base.api.model.GUID;


public interface IAccessControlEntry extends Serializable {

	public static final int RIGHT_VISIBLE = 1;
	public static final int RIGHT_READ_CONTENT = 2;
	public static final int RIGHT_TRAVERSE_LEAF = 3;
	public static final int RIGHT_TRAVERSE_CONTAINER = 4;
	public static final int RIGHT_MODIFY = 5;
	public static final int RIGHT_CHANGE_RIGHTS = 6;

	public static final int RIGHTS_CHAR_COUNT = 6;

	String getRights();
	RightValue calcRightValue(int right);
	int getOrderNr();
	GUID getMemberId();
	boolean isInherit();
	int getRightsArea();	
}
