package at.rufus.resources.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="RESOURCE_LINK_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(ResourceLinkVersion.DISCRIMINATOR)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class ResourceLinkVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;
	
	public static final String DISCRIMINATOR=RepositoryEntityTypes.RESOURCE_LINK_VERSION;
	
	private ResourceVersion target;
	
	@ManyToOne
	@JoinColumn(name="TARGET", referencedColumnName="ID")
	public ResourceVersion getTarget() {
		return target;
	}
	
	public void setTarget(ResourceVersion target) {
		propertyChangeSupport.firePropertyChange("target", this.target, this.target = target);
	}

	@Transient
	public ResourceLink getLink() {
		return (ResourceLink) getResource();
	}
	
	@Override
	public ResourceLinkVersion copy() {
		ResourceLinkVersion link = (ResourceLinkVersion) super.copy();
		link.setTarget(getTarget());
		return link;
	}
	

	@Override
	@Transient
	public String getDiscriminator() {
		return ResourceLinkVersion.DISCRIMINATOR;
	}
	
}
