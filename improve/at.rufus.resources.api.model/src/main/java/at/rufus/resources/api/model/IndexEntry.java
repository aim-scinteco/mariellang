package at.rufus.resources.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="INDEX_ENTRY")
public class IndexEntry extends Entity {

	private static final long serialVersionUID = 1L;
	public static final String PROPERTY_DELIMITER = ",";
	public static final String PROPERTY_QUALIFIER = ":";
	public static final String PROPERTY_ASSIGNMENT = "=";
	
	public static final String PROPERTY_USERNAME = "username";
	public static final String PROPERTY_RESOURCE_ID = "resourceId";
	public static final String PROPERTY_RESOURCE_CLASS = "resourceClass";
	
	public static final String PROPERTY_METADATA_DESC_ID = "mdDescId";
	public static final String PROPERTY_METADATA_DESC_NAME = "mdDescName";
	public static final String PROPERTY_METADATA_LOV_ID = "mdLovId";
	public static final String PROPERTY_METADATA_LOV_TEXT = "mdLovText";
	
	public IndexEntry() {
	}
	
	public IndexEntry(IndexingType entryType, long entryNr) {
		this.entryType = entryType;
		this.entryNr = entryNr;
	}
	
	public IndexEntry(IndexingType entryType, long entryNr, Resource resource, Member user) {
		this.entryType = entryType;
		this.entryNr = entryNr;
		this.resource = resource;
		this.user = user;
	}
	
	public IndexEntry(IndexingType entryType, long entryNr, ResourceVersion resourceVersion, Member user) {
		this.entryType = entryType;
		this.entryNr = entryNr;
		this.resourceVersion = resourceVersion;
		this.user = user;
	}
	
	@Column(name = "ENTRY_NR")
	private long entryNr;
	
	@Column(name = "ENTRY_TYPE")
	private IndexingType entryType;
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_ID", referencedColumnName="ID")
	private Resource resource;
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_VERSION_ID", referencedColumnName="ID")
	private ResourceVersion resourceVersion;
	
	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="ID")
	private Member user;
	
	@Column(name = "UPDATE_TIME")
	private Timestamp updateTime;
	
	@Column(name = "PROPERTIES")
	private String properties;
	
	
	public long getEntryNr() {
		return entryNr;
	}
	
	public void setEntryNr(long entryNr) {
		propertyChangeSupport.firePropertyChange("entryNr", this.entryNr , this.entryNr = entryNr);
	}
	
	public IndexingType getEntryType() {
		return entryType;
	}

	public void setEntryType(IndexingType entryType) {
		propertyChangeSupport.firePropertyChange("entryType", this.entryType , this.entryType = entryType);
	}
	
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceVersion getResourceVersion() {
		return resourceVersion;
	}
	
	public void setResourceVersion(ResourceVersion resourceVersion) {
		propertyChangeSupport.firePropertyChange("resourceVersion", this.resourceVersion , this.resourceVersion = resourceVersion);
	}
	
	public Member getUser() {
		return user;
	}
	
	public void setUser(Member user) {
		propertyChangeSupport.firePropertyChange("user", this.user , this.user = user);
	}
	
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		propertyChangeSupport.firePropertyChange("properties", this.properties , this.properties = properties);
	}
	
	@Transient
	public void addProperties(String... properties) {
		if(properties.length > 0) {
			String value = toProperties(properties);
			if(this.properties != null && !this.properties.isEmpty()) {
				value = this.properties + PROPERTY_DELIMITER + value;
			}
			setProperties(value);
		}
	}
	
	@Transient
	public static String toProperties(String... properties) {
		StringBuilder sb = new StringBuilder();
		for(String property : properties) {
			if(sb.length() > 0) {
				sb.append(PROPERTY_DELIMITER);
			}
			sb.append(property);
		}
		return sb.toString();
	}
	
	@Transient
	public static String[] fromProperties(String properties) {
		return properties.split(PROPERTY_DELIMITER);
	}	
	
}
