package at.rufus.resources.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="RESOURCE_RELATION")
public class ResourceRelation extends Entity  {
	private static final long serialVersionUID = 1L;
	
	@Column(name="DELETED")
	private boolean deleted;
	
	@Column(name="DESCRIPTION")
	private String description;

	@Column(name = "RELATION_TYPE")
	private RelationTypeLov relationType;
	
	@ManyToOne
	@JoinColumn(name="SOURCE_RESOURCE_NODE", referencedColumnName="ID")
	private Resource source;

	@ManyToOne
	@JoinColumn(name="TARGET_RESOURCE_NODE", referencedColumnName="ID")
	private Resource target;

	@ManyToOne
	@JoinColumn(name="REVISION_FROM_ID", referencedColumnName="ID")
	private Revision revision;

	@Column(name="REVISION_FROM_TIME")
	private Timestamp revisionFromTime;
	
	@Column(name="REVISION_TO_TIME")
	private Timestamp revisionToTime;

	@Column(name="HISTORY_ID")
	private GUID historyId;

	public ResourceRelation() {
		super();
	}
	
	public RelationTypeLov getRelationType() {
		return relationType;
	}

	public void setRelationType(RelationTypeLov relationType) {
		propertyChangeSupport.firePropertyChange("relationType", this.relationType, this.relationType = relationType);
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange("description", this.description, this.description = description);
	}
	public Resource getSource() {
		return source;
	}
	public void setSource(Resource source) {
		propertyChangeSupport.firePropertyChange("source", this.source, this.source = source);
	}
	public Resource getTarget() {
		return target;
	}
	public void setTarget(Resource target) {
		propertyChangeSupport.firePropertyChange("target", this.target, this.target = target);
	}
	public Revision getRevision() {
		return revision;
	}
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
	public Timestamp getRevisionFromTime() {
		return revisionFromTime;
	}
	public void setRevisionFromTime(Timestamp revisionFromTime) {
		propertyChangeSupport.firePropertyChange("revisionFromTime", this.revisionFromTime, this.revisionFromTime = revisionFromTime);
	}
	public Timestamp getRevisionToTime() {
		return revisionToTime;
	}
	public void setRevisionToTime(Timestamp revisionToTime) {
		propertyChangeSupport.firePropertyChange("revisionToTime", this.revisionToTime, this.revisionToTime = revisionToTime);
	}
	
	public GUID getHistoryId() {
		return historyId;
	}
	public void setHistoryId(GUID historyId) {
		propertyChangeSupport.firePropertyChange("historyId", this.historyId, this.historyId = historyId);
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	
	@Transient
	public ResourceRelation copy() {
		ResourceRelation copy = new ResourceRelation();
		copy.setDeleted(isDeleted());
		copy.setHistoryId(getHistoryId());
		copy.setRelationType(getRelationType());
		copy.setSource(getSource());
		copy.setTarget(getTarget());
		return copy;
	}
}
