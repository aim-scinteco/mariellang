package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="PARENTAL_RELATION")
public class ParentalRelation extends Entity  {
	private static final long serialVersionUID = 1L;

	private List<ResourceVersion> inputs = new TrackedList<ResourceVersion>(propertyChangeSupport, "inputs");
	private List<ResourceVersion> outputs = new TrackedList<ResourceVersion>(propertyChangeSupport, "outputs");

	@ManyToMany
	@JoinTable(
		name="PARENTAL_INPUT",
		joinColumns=@JoinColumn(name="PARENTAL_RELATION", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="RESOURCE_NODE_VERSION", referencedColumnName="ID")
	)
	public List<ResourceVersion> getInputs() {
		return inputs;
	}

	@ManyToMany
	@JoinTable(
		name="PARENTAL_OUTPUT",
		joinColumns=@JoinColumn(name="PARENTAL_RELATION", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="RESOURCE_NODE_VERSION", referencedColumnName="ID")
	)
	public List<ResourceVersion> getOutputs() {
		return outputs;
	}
	
}
