package at.rufus.resources.api.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.BusinessException;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="RESOURCE_NODE_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(
	name="NODE_TYPE",
	discriminatorType=DiscriminatorType.STRING
)
public class ResourceVersion extends Entity {
	private static final long serialVersionUID = 1L;
	
	public static final String ROLE_INPUT = "INPUT";
	public static final String ROLE_OUTPUT = "OUTPUT";
	
	public static final String PARENTAL_SOURCES = "parentalSources";
	public static final String PARENTAL_TARGETS = "parentalTargets";
	
	public static final String METADATA = "metadata";

	private String nodeType;
	private String entityVersionId;
	private String name;
	private Resource parent;
	private boolean deleted;
	private Revision revision;
	private Timestamp revisionFromTime;
//	private Revision revisionTo;
	private Timestamp revisionToTime;
	private Resource resource;
//	private String role;
	private String comment;
	private List<ParentalRelation> parentalSources = new TrackedList<ParentalRelation>(propertyChangeSupport, PARENTAL_SOURCES);
	private List<ParentalRelation> parentalTargets = new TrackedList<ParentalRelation>(propertyChangeSupport, PARENTAL_TARGETS);
	
	@Column(name="NODE_TYPE")	
	public String getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(String nodeType) {
		propertyChangeSupport.firePropertyChange("nodeType", this.nodeType, this.nodeType = nodeType);
	}
	
	@Column(name="ENTITY_VERSION_ID")
	public String getEntityVersionId() {
		return entityVersionId;
	}
	
	public void setEntityVersionId(String entityVersionId) {
		propertyChangeSupport.firePropertyChange("entityVersionId", this.entityVersionId, this.entityVersionId = entityVersionId);
	}

	@Column(name="NAME")	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}

	@ManyToOne
	@JoinColumn(name="PARENT", referencedColumnName="ID")
	public Resource getParent() {
		return parent;
	}

	public void setParent(Resource parent) {
		propertyChangeSupport.firePropertyChange("parent", this.parent, this.parent = parent);
	}

	@Column(name="DELETED")	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		propertyChangeSupport.firePropertyChange("deleted", this.deleted, this.deleted = deleted);
	}
	
	@ManyToOne
	@JoinColumn(name="REVISION_FROM_ID", referencedColumnName="ID")
	public Revision getRevision() {
		return revision;
	}
	
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
	
	@Column(name="REVISION_FROM_TIME")	
	public Timestamp getRevisionFromTime() {
		return revisionFromTime;
	}
	
	public void setRevisionFromTime(Timestamp revisionFromTime) {
		propertyChangeSupport.firePropertyChange("revisionFromTime", this.revisionFromTime, this.revisionFromTime = revisionFromTime);
	}
	
//	@ManyToOne
//	@JoinColumn(name="REVISION_TO_ID", referencedColumnName="ID")
//	public Revision getRevisionTo() {
//		return revisionTo;
//	}
//	
//	public void setRevisionTo(Revision revisionTo) {
//		propertyChangeSupport.firePropertyChange("revisionTo", this.revisionTo, this.revisionTo = revisionTo);
//	}
	
	// ATTENTION: This method should never be used with the bean model but always inside a query.
	@Column(name="REVISION_TO_TIME")
	public Timestamp getRevisionToTime() {
		return revisionToTime;
	}
	
	public void setRevisionToTime(Timestamp revisionToTime) {
		propertyChangeSupport.firePropertyChange("revisionToTime", this.revisionToTime, this.revisionToTime = revisionToTime);
	}
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange("resource", this.resource, this.resource = resource);
	}
	
	@ManyToMany(mappedBy="outputs")
	public List<ParentalRelation> getParentalSources() {
		return parentalSources;
	}

	@ManyToMany(mappedBy="inputs")
	public List<ParentalRelation> getParentalTargets() {
		return parentalTargets;
	}
	
//	@Column(name="ROLE")
//	public String getRole() {
//		return role;
//	}
//
//	public void setRole(String role) {
//		propertyChangeSupport.firePropertyChange("role", this.role, this.role = role);
//	}
//	
	@Column(name="VERSION_COMMENT")
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		propertyChangeSupport.firePropertyChange("comment", this.comment, this.comment = comment);
	}	
	
	@Transient
	public ResourceVersion getParentVersion() {
		if(parent != null) {
			return parent.getResourceVersion();
		}
		return null;
	}
	
	@Transient
	public ResourceVersion getRootVersion() {
		ResourceVersion resourceVersion = this;
		ResourceVersion parentVersion = getParentVersion();
		while(parentVersion != null) {
			resourceVersion = parentVersion;
			parentVersion = parentVersion.getParentVersion();
		}
		return resourceVersion;
	}
	
	@Transient
	public boolean isChildOf(Resource parent) {
		ResourceVersion parentVersion = getParentVersion();
		while(parentVersion != null) {
			if(parent.equals(parentVersion.getResource())) {
				return true;
			}
			parentVersion = parentVersion.getParentVersion();
		}
		return false;
	}
	
	@Transient
	public boolean isChildOf(GUID parentId) {
		return findParent(parentId) != null;
	}
	
	@Transient
	public boolean isChildOf(Class<? extends Resource> parentClass) {
		return findParent(parentClass) != null;
	}
	
	@SuppressWarnings("unchecked")
	@Transient
	public <T extends Resource> T findParent(Class<T> parentClass) {
		ResourceVersion parentVersion = getParentVersion();
		while(parentVersion != null) {
			if(parentClass.equals(parentVersion.getResource().getClass())) {
				return (T) parentVersion.getResource();
			}
			parentVersion = parentVersion.getParentVersion();
		}
		return null;
	}
	
	@Transient
	public Resource findParent(GUID parentId) {
		ResourceVersion parentVersion = getParentVersion();
		while(parentVersion != null) {
			if(parentId.equals(parentVersion.getResource().getId())) {
				return parentVersion.getResource();
			}
			parentVersion = parentVersion.getParentVersion();
		}
		return null;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Transient
	public ResourceVersion copy() {
		try {
			ResourceVersion copy = getClass().newInstance();
			copy.setNodeType(getNodeType());
			copy.setName(getName());
			copy.setResource(getResource());
			copy.setParent(getParent());
//			copy.setVersionNumber(getVersionNumber());
//			copy.setRole(role);
			return copy;
		} catch (ReflectiveOperationException ex) {
			throw new BusinessException("Error creating copy.", ex);
		}
	}
	
	/**
	 * @return The path to the resource.
	 * 
	 * Example for file "foo.txt" in folder /a/b/c:
	 * -> path = /a/b/c/
	 * -> absolute path = /a/b/c/foo.xml
	 */
	@Transient
	public static String getPath(ResourceVersion resourceVersion, boolean absolutePath) {
		StringBuilder path = new StringBuilder();
		ResourceVersion node = absolutePath ? resourceVersion : resourceVersion.getParentVersion();
		while(node != null) {
			path.insert(0, node.getName() == null ? "?" : node.getName()).insert(0, Resource.PATH_DELIMITER);
			node = node.getParentVersion();
		}
		return path.toString();
	}	
	
	@Transient
	public static Collection<GUID> toResourceIds(Collection<? extends ResourceVersion> resourceVersions) {
		Collection<GUID> resourceIds = new ArrayList<GUID>(resourceVersions.size());
		for(ResourceVersion resourceVersion : resourceVersions) {
			resourceIds.add(resourceVersion.getResource().getId());
		}
		return resourceIds;
	}
	
	@Transient
	public static Map<GUID, ResourceVersion> toResourceIdMap(Collection<? extends ResourceVersion> resourceVersions) {
		Map<GUID, ResourceVersion> resourceIdMap = new HashMap<GUID, ResourceVersion>();
		for(ResourceVersion resourceVersion : resourceVersions) {
			resourceIdMap.put(resourceVersion.getResource().getId(), resourceVersion);
		}
		return resourceIdMap;
	}
	
	@Transient
	public static Set<GUID> toParentIdsAsSet(Collection<? extends ResourceVersion> resourceVersions) {
		Set<GUID> parentIds = new HashSet<GUID>();
		for(ResourceVersion resourceVersion : resourceVersions) {
			if (resourceVersion.getParent() != null) {
				parentIds.add(resourceVersion.getParent().getId());
			}
		}
		return parentIds;
	}
	
	@Transient
	public boolean isLatest() {
		return revisionToTime != null && revisionToTime.compareTo(Revision.LATEST_TIME) == 0;
	}
	
	
	@Override
	@Transient
	public String getDiscriminator() {
		return RepositoryEntityTypes.RESOURCE_NODE_VERSION;
	};
	
}
