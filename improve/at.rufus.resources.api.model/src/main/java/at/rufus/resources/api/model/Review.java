package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="REVIEW")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(RepositoryEntityTypes.REVIEW)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Review extends Resource {

	private static final long serialVersionUID = 1L;

	public static final String COMMENT_TYPE_REVIEW_COMMENT = "REVIEW_COMMENT";
	
	@Column(name="STATUS")
	private ReviewStatus status;
	
	@OneToMany(mappedBy="review")
	private List<ReviewEntry> entries = new TrackedList<ReviewEntry>(propertyChangeSupport, "entries");
	
	@OneToMany(mappedBy = "review")
	private List<Reviewer> reviewers = new TrackedList<>(propertyChangeSupport, "reviewers");
	
	public ReviewStatus getStatus() {
		return status;
	}
	
	public void setStatus(ReviewStatus status) {
		propertyChangeSupport.firePropertyChange("status", this.status, this.status = status);
	}
	
	public List<ReviewEntry> getEntries() {
		return entries;
	}
	
	public List<Reviewer> getReviewers() {
		return reviewers;
	}
	
	@Transient
	public ReviewEntry findReviewEntry(GUID resourceId) {
		for (ReviewEntry reviewEntry : entries) {
			if (resourceId.equals(reviewEntry.getResource().getId())) {
				return reviewEntry;
			}
		}
		return null;
	}

	@Transient
	@Override
	public String getDiscriminator() {
		return RepositoryEntityTypes.REVIEW;
	}

}
