package at.rufus.resources.api.model;

public enum ReviewEntryStatus {
	
	Reviewing,
	Approved,
	Rejected;

}
