package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNTAX_PATTERN")
public class SyntaxPattern extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="PATTERN")
	private String pattern;
	
	@Column(name="COLOR")
	private int color;
	
	@Column(name="FONT_STYLE")
	private int fontStyle;
	
	@ManyToOne
	@JoinColumn(name="SYNTAX_HIGHLIGHTER", referencedColumnName="ID")
	private SyntaxHighlighter syntaxHighlighter;

	public String getPattern() {
		return pattern;
	}
	
	public void setPattern(String pattern) {
		propertyChangeSupport.firePropertyChange("pattern", this.pattern, this.pattern = pattern);
	}
	
	public int getColor() {
		return color;
	}
	
	public void setColor(int color) {
		propertyChangeSupport.firePropertyChange("color", this.color, this.color = color);
	}
	
	public int getFontStyle() {
		return fontStyle;
	}
	
	public void setFontStyle(int fontStyle) {
		propertyChangeSupport.firePropertyChange("fontStyle", this.fontStyle, this.fontStyle = fontStyle);
	}
	
	public SyntaxHighlighter getSyntaxHighlighter() {
		return syntaxHighlighter;
	}
	
	public void setSyntaxHighlighter(SyntaxHighlighter syntaxHighlighter) {
		propertyChangeSupport.firePropertyChange("syntaxHighlighter", this.syntaxHighlighter, this.syntaxHighlighter = syntaxHighlighter);
	}
	
}
