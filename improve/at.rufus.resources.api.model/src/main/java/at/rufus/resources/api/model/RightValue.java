package at.rufus.resources.api.model;

public enum RightValue {
	
	allow('A'),
	deny('D'),
	noStatement(' ');
	
	private final static RightValue[] enumValues = values();
	
	private char charValue;
	
	private RightValue(char charValue) {
		this.charValue = charValue;
	}
	
	public char getCharValue() {
		return charValue;
	}

	public static RightValue fromCharValue(char charValue) {
		for (RightValue enumValue : enumValues) {
			if (enumValue.charValue == charValue) {
				return enumValue;
			}
		}
		return null;
	}

	public static RightValue[] emptyRights() {
		RightValue[] rightValues = new RightValue[IAccessControlEntry.RIGHTS_CHAR_COUNT];
		for (int i = 0; i < rightValues.length; i++) {
			rightValues[i] = RightValue.noStatement;
		}
		return rightValues;
	}

	
}
