package at.rufus.resources.api.model;

public enum MetadataStrategy {
	
	None("None", "No effect on child resources"),
	Inherit("Inherit", "Inherits metadata to child resources recursively"),
	Copy("Copy", "Metadata is copied initially to direct child resources");
	
	private final String label;
	private final String description;
	
	private MetadataStrategy(String label, String description) {
		this.label = label;
		this.description = description;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getDescription() {
		return description;
	}

}
