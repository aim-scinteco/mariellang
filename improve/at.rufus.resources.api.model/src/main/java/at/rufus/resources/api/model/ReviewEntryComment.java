package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="REVIEW_ENTRY_COMMENT")
public class ReviewEntryComment extends Entity {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="REVIEW_ENTRY", referencedColumnName="ID")
	private ReviewEntry reviewEntry;
	
	@ManyToOne
	@JoinColumn(name="REVISION_ID", referencedColumnName="ID")
	private Revision revision;
	
	@Column(name="TEXT")
	private String comment;

	public ReviewEntry getReviewEntry() {
		return reviewEntry;
	}
	
	public void setReviewEntry(ReviewEntry reviewEntry) {
		propertyChangeSupport.firePropertyChange("reviewEntry", this.reviewEntry, this.reviewEntry = reviewEntry);
	}

	public Revision getRevision() {
		return revision;
	}

	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		propertyChangeSupport.firePropertyChange("comment", this.comment, this.comment = comment);
	}
	
}
