package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="FILE_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(RepositoryEntityTypes.FILE_VERSION)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class FileVersion extends ResourceVersion {
	private static final long serialVersionUID = 1L;
	
	public static final String DISCRIMINATOR="FIV";
	
	public static final String PROP_FILE_SIZE = "fileSize";
//	public static final String PROP_VERSION_ID = "versionId";
	public static final String PROP_FILE_STORE_PATH = "fileStorePath";
	public static final String PROP_FILE_HASH = "fileHash";

	@Column(name="FILESIZE")
	private Long fileSize;
	
	@Column(name="EXPECTED_FILESIZE")
	private Long expectedFileSize;
	
//	@Column(name="VERSION_ID")
//	private long versionId;
	
	@Column(name="FILE_STORE_PATH")
	private String fileStorePath;
	
	@Column(name="FILE_HASH")
	private byte[] fileHash;
	
	
	@Override
	@Transient
	public String getDiscriminator() {
		return RepositoryEntityTypes.FILE_VERSION;
	}
	
	
	public Long getFileSize() {
		return fileSize;
	}
	
	public void setFileSize(Long fileSize) {
		propertyChangeSupport.firePropertyChange(PROP_FILE_SIZE, this.fileSize, this.fileSize = fileSize);
	}
	
	public Long getExpectedFileSize() {
		return expectedFileSize;
	}
	
	public void setExpectedFileSize(Long expectedFileSize) {
		this.expectedFileSize = expectedFileSize;
	}
	
//	public long getVersionId() {
//		return versionId;
//	}
//	
//	public void setVersionId(long versionId) {
//		propertyChangeSupport.firePropertyChange(PROP_VERSION_ID, this.versionId, this.versionId = versionId);
//	}
	
	public String getFileStorePath() {
		return fileStorePath;
	}
	
	public void setFileStorePath(String fileStorePath) {
		propertyChangeSupport.firePropertyChange(PROP_FILE_STORE_PATH, this.fileStorePath, this.fileStorePath = fileStorePath);
	}
	
	public byte[] getFileHash() {
		return fileHash;
	}
	
	public void setFileHash(byte[] fileHash) {
		propertyChangeSupport.firePropertyChange(PROP_FILE_HASH, this.fileHash, this.fileHash = fileHash);
	}
	
	@Transient
	public File getFile() {
		return (File) getResource();
	}
	
		@Override
	public FileVersion copy() {
		FileVersion copy = (FileVersion) super.copy();
//		copy.setVersionId(getVersionId());
		copy.setFileSize(getFileSize());
		copy.setExpectedFileSize(getExpectedFileSize());
		copy.setFileStorePath(getFileStorePath());
		copy.setFileHash(getFileHash());
		return copy;
	}
	
}
