package at.rufus.resources.api.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.AbstractLov;


@javax.persistence.Entity
@Table(name="ACTOR_LOV")
public class Actor extends AbstractLov {
	private static final long serialVersionUID = 1L;
	
	public static final Comparator<Actor> COMPARATOR_NAME = new Comparator<Actor>() {
		@Override
		public int compare(Actor o1, Actor o2) {
			return CmpUtil.cmpNatural(o1.name, o2.name, true);
		}
	};

	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="SYSTEM_LAYER")
	private SystemLayer systemLayer;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		propertyChangeSupport.firePropertyChange("description", this.description, this.description = description);
	}
	
	public SystemLayer getSystemLayer() {
		return systemLayer;
	}
	public void setSystemLayer(SystemLayer systemLayer) {
		propertyChangeSupport.firePropertyChange("systemLayer", this.systemLayer, this.systemLayer = systemLayer);
	}
	
	@Override
	public String toString() {
		return name;
	}

}
