package at.rufus.resources.api.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="APP_USER_VERSION")
public class UserVersion extends Entity {
	
	private static final long serialVersionUID = 1L;
	
	public static final String USER_NAME = "username";
	public static final String DISPLAY_NAME = "displayName";
	public static final String ROLE = "role";
	public static final String STATUS = "status";

	private Member member;

	private String displayName;
	private String username;
	private String email;
	private UserRole role;
	private UserStatus status;
	private Revision revision;
	private Timestamp revisionFromTime;
	private Timestamp revisionToTime;

	
	@Column(name="USERNAME")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@Column(name="DISPLAY_NAME")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
        return email;
    }
	
	public void setEmail(String email) {
        this.email = email;
    }

	@Column(name="ROLE")
	public UserRole getRole() {
		return role;
	}
	
	public void setRole(UserRole role) {
		this.role = role;
	}
	
	@Column(name="STATUS")
	public UserStatus getStatus() {
		return status;
	}
	
	public void setStatus(UserStatus status) {
		this.status = status;
	}
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ManyToOne
	@JoinColumn(name="REVISION_FROM_ID", referencedColumnName="ID")
	public Revision getRevision() {
		return revision;
	}
	
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}

	@Column(name="REVISION_FROM_TIME")
	public Timestamp getRevisionFromTime() {
		return revisionFromTime;
	}
	
	public void setRevisionFromTime(Timestamp revisionFromTime) {
		propertyChangeSupport.firePropertyChange("revisionFromTime", this.revisionFromTime, this.revisionFromTime = revisionFromTime);
	}
	
	@Column(name="REVISION_TO_TIME")
	public Timestamp getRevisionToTime() {
		return revisionToTime;
	}
	
	public void setRevisionToTime(Timestamp revisionToTime) {
		propertyChangeSupport.firePropertyChange("revisionToTime", this.revisionToTime, this.revisionToTime = revisionToTime);
	}
	
	public UserVersion copy() {
		UserVersion copy = new UserVersion();
		copy.setDisplayName(getDisplayName());
		copy.setMember(getMember());
		copy.setEmail(getEmail());
		copy.setRole(getRole());
		copy.setStatus(getStatus());
		copy.setUsername(getUsername());
		return copy;
	}
	
	@Override
	@Transient
	public String getDiscriminator() {
		return RepositoryEntityTypes.APP_USER_VERSION;
	}

}
