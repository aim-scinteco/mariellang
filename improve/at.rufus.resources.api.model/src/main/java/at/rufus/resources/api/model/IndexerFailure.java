package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="INDEXER_FAILURES")
public class IndexerFailure extends Entity {
    private static final long serialVersionUID = 1L;
    
    @Column(name = "MESSAGE")
    private String message;
    
    public IndexerFailure(GUID id, String message) {
        super(id);
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
}
