package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="SYNTAX_KEYWORD_SET")
public class SyntaxKeywordSet extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="NAME")
	private String name;
	
	@Column(name="CASE_SENSITIVE")
	private boolean caseSensitive;
	
	@Column(name="COLOR")
	private int color;
	
	@Column(name="FONT_STYLE")
	private int fontStyle;
	
	@ManyToOne
	@JoinColumn(name="SYNTAX_HIGHLIGHTER", referencedColumnName="ID")
	private SyntaxHighlighter syntaxHighlighter;
	
	@OneToMany(mappedBy="keywordSet")
	private List<SyntaxKeyword> keywords = new TrackedList<SyntaxKeyword>(propertyChangeSupport, "keywords");

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public boolean isCaseSensitive() {
		return caseSensitive;
	}
	
	public void setCaseSensitive(boolean caseSensitive) {
		propertyChangeSupport.firePropertyChange("caseSensitive", this.caseSensitive, this.caseSensitive = caseSensitive);
	}
	
	public int getColor() {
		return color;
	}
	
	public void setColor(int color) {
		propertyChangeSupport.firePropertyChange("color", this.color, this.color = color);
	}
	
	public int getFontStyle() {
		return fontStyle;
	}
	
	public void setFontStyle(int fontStyle) {
		propertyChangeSupport.firePropertyChange("fontStyle", this.fontStyle, this.fontStyle = fontStyle);
	}
	
	public SyntaxHighlighter getSyntaxHighlighter() {
		return syntaxHighlighter;
	}
	
	public void setSyntaxHighlighter(SyntaxHighlighter syntaxHighlighter) {
		propertyChangeSupport.firePropertyChange("syntaxHighlighter", this.syntaxHighlighter, this.syntaxHighlighter = syntaxHighlighter);
	}
	
	public List<SyntaxKeyword> getKeywords() {
		return keywords;
	}
}
