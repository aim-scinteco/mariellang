package at.rufus.resources.api.model;

import javax.persistence.Column;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="MASTER_LICENSE")
public class MasterLicense extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name = "SERIAL_NUMBER")
	private String serialNumber;

	public String getSerialNumber() {
		return serialNumber;
	}
	
	public void setSerialNumber(String serialNumber) {
		propertyChangeSupport.firePropertyChange("serialNumber", this.serialNumber, this.serialNumber = serialNumber);
	}
	
}
