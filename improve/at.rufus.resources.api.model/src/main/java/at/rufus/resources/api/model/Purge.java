package at.rufus.resources.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

@javax.persistence.Entity
@Table(name="DPURGE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(RepositoryEntityTypes.PURGE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Purge extends Resource {

	private static final long serialVersionUID = 1L;
	
	@Column(name="STATUS")
	private PurgeStatus status;

	@ManyToOne
    @JoinColumn(name="REQUESTOR", referencedColumnName="ID")
    private Member requestor;
	
	@OneToMany(mappedBy="purge")
	private List<PurgeEntry> purgeEntries = new TrackedList<PurgeEntry>(propertyChangeSupport, "purgeEntries");

	@OneToMany(mappedBy="purge")
	private List<PurgeReviewer> purgeReviewers = new TrackedList<PurgeReviewer>(propertyChangeSupport, "purgeReviewers");

	public PurgeStatus getStatus() {
		return status;
	}
	
	public void setStatus(PurgeStatus status) {
		propertyChangeSupport.firePropertyChange("status", this.status, this.status = status);
	}
	
	public List<PurgeEntry> getPurgeEntries() {
		return purgeEntries;
	}

	public List<PurgeReviewer> getPurgeReviewers() {
		return purgeReviewers;
	}

	public Member getRequestor() {
		return requestor;
	}

	public void setRequestor(Member requestor) {
		propertyChangeSupport.firePropertyChange("requestor", this.requestor, this.requestor = requestor);
	}

	@Transient
	@Override
	public String getDiscriminator() {
		return RepositoryEntityTypes.PURGE;
	}

}
