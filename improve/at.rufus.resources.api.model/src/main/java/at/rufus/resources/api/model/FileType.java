package at.rufus.resources.api.model;

public enum FileType {
	WorkingFile, 
	FileVersion;
}
