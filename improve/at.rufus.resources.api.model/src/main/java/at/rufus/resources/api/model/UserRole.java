package at.rufus.resources.api.model;

public enum UserRole {

	// order most rights first, least rights last
	admin("Administrator", "admins"),
	user("Full User", "users"),
	publish("Publish User", "publish");
	
	private final String label;
	private final String systemGroupId;
	
	private UserRole(String label, String systemGroupId) {
		this.label = label;
		this.systemGroupId = systemGroupId;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getSystemGroupId() {
		return systemGroupId;
	}
	
}
