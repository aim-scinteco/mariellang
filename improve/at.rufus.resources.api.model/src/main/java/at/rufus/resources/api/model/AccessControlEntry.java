package at.rufus.resources.api.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.common.CmpUtil;
import at.rufus.base.api.model.Entity;
import at.rufus.base.api.model.GUID;

@javax.persistence.Entity
@Table(name="ACCESS_CONTROL_ENTRY")
public class AccessControlEntry extends Entity implements IAccessControlEntry {
	private static final long serialVersionUID = 1L;
	
	public static final String ORDER_NR = "orderNr";
	public static final String RESOURCE = "resource";
	public static final String MEMBER = "member";
	public static final String RIGHTS = "rights";
	public static final String INHERIT = "inherit";
	public static final String RIGHTS_AREA = "area";
	
	public static final Comparator<AccessControlEntry> ACE_NATURAL_ORDER_COMPARATOR = new Comparator<AccessControlEntry>() {
		@Override
		public int compare(AccessControlEntry o1, AccessControlEntry o2) {
			int r1 = o1.getRightsArea();
			int r2 = o2.getRightsArea();
			if (r1 == r2) {
				int order1 = o1.getOrderNr();
				int order2 = o2.getOrderNr();
				return CmpUtil.cmp(order1, order2);
			}
			return CmpUtil.cmp(r1, r2);
		}
	};

	private int orderNr;
	private int rightsArea;
	private Resource resource;
	private Member member;
	private String rights = "          ";
	private boolean inherit = false;
	private Revision revision;

	
	@Column(name="ORDER_NR")
	public int getOrderNr() {
		return orderNr;
	}
	
	public void setOrderNr(int orderNr) {
		propertyChangeSupport.firePropertyChange(ORDER_NR, this.orderNr, this.orderNr = orderNr);
	}
	
	@ManyToOne
	@JoinColumn(name="RESOURCE_NODE", referencedColumnName="ID")
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		propertyChangeSupport.firePropertyChange(RESOURCE, this.resource, this.resource = resource);
	}
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	public Member getMember() {
		return member;
	}
	
	@Transient
	@Override
	public GUID getMemberId() {
		return member == null ? null : member.getId();
	}
	
	public void setMember(Member member) {
		propertyChangeSupport.firePropertyChange(MEMBER, this.member, this.member = member);
	}
	
	@Column(name="RIGHTS")
	public String getRights() {
		return rights;
	}
	
	public void setRights(String rights) {
		propertyChangeSupport.firePropertyChange(RIGHTS, this.rights, this.rights = rights);
	}
	
	@Transient
	@Override
	public RightValue calcRightValue(int right) {
		return RightValue.fromCharValue(rights.charAt(right-1));
	}
	
	public static RightValue calcRightValue(String rights, int right) {
		return RightValue.fromCharValue(rights.charAt(right-1));
	}

	@Column(name="INHERIT")
	public boolean isInherit() {
		return inherit;
	}
	
	public void setInherit(boolean inherit) {
		propertyChangeSupport.firePropertyChange(INHERIT, this.inherit, this.inherit = inherit);
	}
	
	@ManyToOne
	@JoinColumn(name="REVISION_ID", referencedColumnName="ID")
	public Revision getRevision() {
		return revision;
	}
	
	public void setRevision(Revision revision) {
		propertyChangeSupport.firePropertyChange("revision", this.revision, this.revision = revision);
	}
		
	@Transient
	public void putRightValue(int right, RightValue value) {
		char[] rightChars = rights.toCharArray();
		rightChars[right - 1] = value.getCharValue();
		setRights(new String(rightChars));
	}
	
	@Column(name="RIGHTS_AREA")
	public int getRightsArea() {
		return rightsArea;
	}
	
	public void setRightsArea(int rightsArea) {
		propertyChangeSupport.firePropertyChange(RIGHTS_AREA, this.rightsArea, this.rightsArea = rightsArea);
	}
}
