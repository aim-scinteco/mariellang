package at.rufus.resources.api.model;

public enum ParameterType {

	TEXT("Text"),
	NUMBER("Number"),
	BOOLEAN("Boolean");
	
	private final String label;
	
	private ParameterType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
