package com.scinteco.improve.discussion.api.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.resources.api.model.Member;
import at.rufus.resources.api.model.Resource;


@javax.persistence.Entity
@Table(name="DISCUSSION_NODE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(DiscussionItem.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public abstract class DiscussionItem extends Resource implements Rateable, Readable {

	public static final String TYPE = "X";
	
	private static final long serialVersionUID = -1568147224742057665L;
	
	private List<Member> readBy = new TrackedList<Member>(propertyChangeSupport, "readBy");
	private List<Rating> ratings = new TrackedList<>(propertyChangeSupport, "ratings");
	
	@ManyToMany
	@JoinTable(
		name="READ_BY",
		joinColumns=@JoinColumn(name="RESOURCE_ID", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	)
	@Override
	public List<Member> getReadBy() {
		return this.readBy;
	}

	public void setReadBy(List<Member> readBy) {
		this.readBy = readBy;
	}
			
	@Override
	@OneToMany(mappedBy="resource")
	public List<Rating> getRatings() {
		return this.ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}
	
}
