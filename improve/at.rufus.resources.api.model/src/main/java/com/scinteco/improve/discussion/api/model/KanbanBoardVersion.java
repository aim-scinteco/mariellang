package com.scinteco.improve.discussion.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.MetadataDescriptor;
import at.rufus.resources.api.model.Resource;
import at.rufus.resources.api.model.ResourceCharacter;
import at.rufus.resources.api.model.ResourceVersion;

@Entity
@Table(name="KANBAN_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(KanbanBoardVersion.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class KanbanBoardVersion extends ResourceVersion {

	public static final String TYPE = "KBV";
	/**
	 * 
	 */
	private static final long serialVersionUID = 7429244985524985756L;

	@Transient
	@Override
	public String getDiscriminator() {
		return TYPE;
	}

	@ManyToOne
	@JoinColumn(name="FILTER_RESOURCE_ID", referencedColumnName="ID")
	private Resource filter;

	@ManyToOne
	@JoinColumn(name="X_AXIS_META", referencedColumnName="ID")
	private MetadataDescriptor xAxisDescriptor;

	@ManyToOne
	@JoinColumn(name="Y_AXIS_META", referencedColumnName="ID")
	private MetadataDescriptor yAxisDescriptor;

	@ManyToOne
	@JoinColumn(name="RESOURCE_CHARACTER_ID", referencedColumnName="ID")
	private ResourceCharacter boardCharacter;
		
	public Resource getFilter() {
		return filter;
	}

	public void setFilter(Resource filter) {
		this.filter = filter;
	}

	public MetadataDescriptor getxAxisDescriptor() {
		return xAxisDescriptor;
	}

	public void setxAxisDescriptor(MetadataDescriptor xAxisDescriptor) {
		this.xAxisDescriptor = xAxisDescriptor;
	}

	public MetadataDescriptor getyAxisDescriptor() {
		return yAxisDescriptor;
	}

	public void setyAxisDescriptor(MetadataDescriptor yAxisDescriptor) {
		this.yAxisDescriptor = yAxisDescriptor;
	}

	public ResourceCharacter getBoardCharacter() {
		return boardCharacter;
	}

	public void setBoardCharacter(ResourceCharacter resourceCharacter) {
		this.boardCharacter = resourceCharacter;
	}
	
	@Override
	public KanbanBoardVersion copy() {
		KanbanBoardVersion copy = (KanbanBoardVersion) super.copy();
		copy.setFilter(getFilter());
		copy.setxAxisDescriptor(getxAxisDescriptor());
		copy.setyAxisDescriptor(getyAxisDescriptor());
		copy.setBoardCharacter(getBoardCharacter());
		return copy;
	}

}
