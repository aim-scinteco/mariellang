package com.scinteco.improve.discussion.api.model;

import java.util.List;

public interface Rateable {
	
	List<Rating> getRatings();
	
}
