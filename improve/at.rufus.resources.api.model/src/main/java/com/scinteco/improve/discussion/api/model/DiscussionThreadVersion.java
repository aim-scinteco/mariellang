package com.scinteco.improve.discussion.api.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.ResourceVersion;

@Entity
@Table(name="DISCUSSION_NODE_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(DiscussionThreadVersion.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class DiscussionThreadVersion extends ResourceVersion {

	public static final String TYPE = "DIV";
	
	private static final long serialVersionUID = -3884494138488928066L;

	@Column(name="TITLE")
	private String topic;
	
	@Column(name="SPACE_NAME")
	private String space;
	
	@Column(name="CLOSED")
	private boolean closed;
	
	@Column(name="PROJECT")
	private String project;
	
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		propertyChangeSupport.firePropertyChange("topic", this.topic, this.topic = topic);
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		propertyChangeSupport.firePropertyChange("space", this.space, this.space = space);
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		propertyChangeSupport.firePropertyChange("closed", this.closed, this.closed = closed);
	}

	@Transient
	public DiscussionThread getDiscussionThread() {
		return (DiscussionThread) getResource();
	}

	@Transient
	@Override
	public String getDiscriminator() {
		return TYPE;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		propertyChangeSupport.firePropertyChange("project", this.project, this.project = project);
	}
	
	@Override
	public DiscussionThreadVersion copy() {
		DiscussionThreadVersion copy = (DiscussionThreadVersion) super.copy();
		copy.setTopic(getTopic());
		copy.setSpace(getSpace());
		copy.setProject(getProject());
		copy.setClosed(isClosed());
		return copy;
	}
	
}
