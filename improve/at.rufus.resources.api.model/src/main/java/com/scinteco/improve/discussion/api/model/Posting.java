package com.scinteco.improve.discussion.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name="POSTING_NODE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(Posting.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class Posting extends DiscussionItem {

	public static final String TYPE = "PO";
	
	private static final long serialVersionUID = -5568147224742057665L;

	@Transient
	@Override
	public String getDiscriminator() {
		return Posting.TYPE;
	}
}
