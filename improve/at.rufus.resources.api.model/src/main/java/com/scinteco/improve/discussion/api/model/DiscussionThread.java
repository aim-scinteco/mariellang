package com.scinteco.improve.discussion.api.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.resources.api.model.Member;


@javax.persistence.Entity
@Table(name="DISCUSSION_NODE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(DiscussionThread.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class DiscussionThread extends DiscussionItem implements Viewable, Subscribeable {

	public static final String TYPE = "DI";
	
	private static final long serialVersionUID = -5568147224742057665L;
	
	private List<Member> viewedBy = new TrackedList<Member>(propertyChangeSupport, "viewedBy");
	private List<Member> subscribedBy = new TrackedList<Member>(propertyChangeSupport, "subscribedBy");
	
	@ManyToMany
	@JoinTable(
		name="VIEWED_BY",
		joinColumns=@JoinColumn(name="RESOURCE_ID", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	)
	@Override
	public List<Member> getViewedBy() {
		return this.viewedBy;
	}

	public void setViewedBy(List<Member> viewedBy) {
		this.viewedBy = viewedBy;
	}
	
	@ManyToMany
	@JoinTable(
		name="SUBSCRIBED_BY",
		joinColumns=@JoinColumn(name="RESOURCE_ID", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	)
	@Override
	public List<Member> getSubscribedBy() {
		return this.subscribedBy;
	}

	public void setSubscribedBy(List<Member> subscribedBy) {
		this.subscribedBy = subscribedBy;
	}

	@Transient
	@Override
	public String getDiscriminator() {
		return DiscussionThread.TYPE;
	}
}
