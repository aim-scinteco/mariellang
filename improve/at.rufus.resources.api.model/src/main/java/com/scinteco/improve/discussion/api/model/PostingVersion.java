package com.scinteco.improve.discussion.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.ResourceVersion;

@Entity
@Table(name="POSTING_NODE_VERSION")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(PostingVersion.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class PostingVersion extends ResourceVersion {

	public static final String TYPE = "POV";
	
	private static final long serialVersionUID = -3884494138488928066L;

	@Transient
	public Posting getPosting() {
		return (Posting) getResource();
	}
	
	@Transient
	@Override
	public String getDiscriminator() {
		return TYPE;
	}

}
