package com.scinteco.improve.analysis.matrix.api.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="DCELL")
public final class DataCell extends Entity {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="MATRIX_ID", referencedColumnName="ID")
	private DataMatrix matrix; 
	
	@Column(name="ROW_IDX")
	private int rowIndex;
	
	@Column(name="COLUMN_IDX")
	private int columnIndex;
	
	@Column(name="CELL_IDX")
	int cellIndex;
	
	@Column(name="NUM_VALUE")
	private BigDecimal numberValue;

	@Column(name="TEXT_VALUE")
	private String textValue;

	public DataMatrix getMatrix() {
		return matrix;
	}

	public void setMatrix(DataMatrix matrix) {
		propertyChangeSupport.firePropertyChange("matrix", this.matrix, this.matrix = matrix);
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		propertyChangeSupport.firePropertyChange("rowIndex", this.rowIndex, this.rowIndex = rowIndex);
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public void setColumnIndex(int columnIndex) {
		propertyChangeSupport.firePropertyChange("columnIndex", this.columnIndex, this.columnIndex = columnIndex);
	}

	public BigDecimal getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(BigDecimal numberValue) {
		propertyChangeSupport.firePropertyChange("numberValue", this.numberValue, this.numberValue = numberValue);
	}

	public String getTextValue() {
		return textValue;
	}

	public void setTextValue(String textValue) {
		propertyChangeSupport.firePropertyChange("textValue", this.textValue, this.textValue = textValue);
	}

	public int getCellIndex() {
		return cellIndex;
	}

	public void setCellIndex(int cellIndex) {
		propertyChangeSupport.firePropertyChange("cellIndex", this.cellIndex, this.cellIndex = cellIndex);
	}

	@Transient
	final static NumberFormat DEBUG_NUM_FORMAT = new DecimalFormat("#0.00");

	@Override
	@Transient
	public String toString() {
		String s = "cell:" + cellIndex + "/row:" + rowIndex + "/col:" + columnIndex;
		if (numberValue != null) {
			s += "/value:" + DEBUG_NUM_FORMAT.format(numberValue);
		}
		if (textValue != null) {
			s += "/text:" + textValue;
		}
		return s;
	}
}
