package com.scinteco.improve.discussion.api.model;

import java.util.List;

import at.rufus.resources.api.model.Member;

public interface Subscribeable {

	List<Member> getSubscribedBy();
	
}
