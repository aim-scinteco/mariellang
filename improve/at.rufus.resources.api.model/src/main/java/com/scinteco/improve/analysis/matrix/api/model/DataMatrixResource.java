package com.scinteco.improve.analysis.matrix.api.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.FileVersion;

@javax.persistence.Entity
@Table(name="DMATRIX_RES")
public class DataMatrixResource extends Entity {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="MATRIX_ID", referencedColumnName="ID")
	private DataMatrix matrix;
	
	@ManyToOne
	@JoinColumn(name="SRC_FILE_VERSION_ID", referencedColumnName="ID")
	private FileVersion sourceFileVersion;

	public FileVersion getSourceFileVersion() {
		return sourceFileVersion;
	}
	
	public void setSourceFileVersion(FileVersion sourceFileVersion) {
		propertyChangeSupport.firePropertyChange("sourceFileVersion", this.sourceFileVersion, this.sourceFileVersion = sourceFileVersion);
	}

	public DataMatrix getMatrix() {
		return matrix;
	}

	public void setMatrix(DataMatrix matrix) {
		propertyChangeSupport.firePropertyChange("matrix", this.matrix, this.matrix = matrix);
	}

}
