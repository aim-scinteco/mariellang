package com.scinteco.improve.analysis.matrix.api.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.analysis.api.model.Run;
import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="DANALYSIS")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(
		name="TYPE",
		discriminatorType=DiscriminatorType.STRING
	)
@DiscriminatorValue(DataAnalysis.TYPE)
public class DataAnalysis extends Entity {
	private static final long serialVersionUID = 1L;

	public final static String TYPE = "DA";
	
	@ManyToOne
	@JoinColumn(name="RUN_ID", referencedColumnName = "ID")
	private Run run;

	@Column(name="SCHEMA_NAME")
	private String schemaName;

	@Column(name="title")
	private String title;
	
	@OneToMany(mappedBy = "analysis")
	private List<DataMatrix> matrixes = new TrackedList<DataMatrix>(propertyChangeSupport, "matrixes");

	@Transient
	private Map<String,DataMatrix> matrixMap = null;
	
	public List<DataMatrix> getMatrixes() {
		return matrixes;
	}

	public Run getRun() {
		return run;
	}

	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange("run", this.run, this.run=run);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		propertyChangeSupport.firePropertyChange("title", this.title, this.title=title);
	}
	
	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		propertyChangeSupport.firePropertyChange("schemaName", this.schemaName, this.schemaName = schemaName);
	}

	public DataMatrix getMatrix(String schemaName) {
		if (matrixMap == null) {
			matrixMap = new HashMap<>();
			for (DataMatrix m : matrixes) {
				matrixMap.put(m.getSchemaName(), m);
			}
		}
		return matrixMap.get(schemaName);
	}

}
