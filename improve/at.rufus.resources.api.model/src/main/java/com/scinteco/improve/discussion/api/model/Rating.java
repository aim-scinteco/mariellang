package com.scinteco.improve.discussion.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.Member;
import at.rufus.resources.api.model.Resource;

@javax.persistence.Entity
@Table(name="RATING")
public class Rating extends Entity{

	private static final long serialVersionUID = 2189861312174158832L;

	@Column(name="rating")
	private int rating;
	
	@ManyToOne
	@JoinColumn(name="APP_MEMBER", referencedColumnName="ID")
	private Member member;

	@ManyToOne
	@JoinColumn(name="RESOURCE_ID", referencedColumnName="ID")
	private Resource resource;
	
	
	public int getRating() {
		return this.rating;
	}
	
	public void setRating(int rating) {
		propertyChangeSupport.firePropertyChange("rating", this.rating, this.rating = rating);
	}
	
	public Member getMember() {
		return this.member;
	}
	
	public void setMember(Member member) {
		this.member = member;
	}
	
	public Resource getResource() {
		return this.resource;
	}
	
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
}
