package com.scinteco.improve.discussion.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import at.rufus.resources.api.model.Resource;

@javax.persistence.Entity
@Table(name="KANBAN_NODE")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue(KanbanBoard.TYPE)
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class KanbanBoard extends Resource {

	public static final String TYPE = "KB";

	/**
	 * 
	 */
	private static final long serialVersionUID = 6592580393023682905L;
	
	@Transient
	@Override
	public String getDiscriminator() {
		return KanbanBoard.TYPE;
	}

}
