package com.scinteco.improve.analysis.matrix.api.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="DMATRIX")
public final class DataMatrix extends Entity {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="ANALYSIS_ID", referencedColumnName="ID")
	private DataAnalysis analysis;

	@Column(name="SCHEMA_NAME")
	private String schemaName;

	@Column(name="title")
	private String title;
	
	@Column(name="row_count")
	private int rowCount;
	
	@Column(name="column_count")
	private int columnCount;
	

	@OneToMany(mappedBy = "matrix")
	@OrderBy("CELL_IDX")
	private List<DataCell> cells = new TrackedList<>(propertyChangeSupport, "cells");
	
	@OneToMany(mappedBy = "matrix")
	private List<DataMatrixResource> resources = new TrackedList<>(propertyChangeSupport, "resources");

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		propertyChangeSupport.firePropertyChange("title", this.title, this.title = title);
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		propertyChangeSupport.firePropertyChange("rowCount", this.rowCount, this.rowCount = rowCount);
	}

	public int getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(int columnCount) {
		propertyChangeSupport.firePropertyChange("columnCount", this.columnCount, this.columnCount = columnCount);
	}

	public List<DataCell> getCells() {
		return cells;
	}
	
	public DataAnalysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(DataAnalysis analysis) {
		propertyChangeSupport.firePropertyChange("analysis", this.analysis, this.analysis = analysis);
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		propertyChangeSupport.firePropertyChange("schemaName", this.schemaName, this.schemaName = schemaName);
	}

	/**
	 * DataCells can be omitted when no value is given (null-values)
	 * The offsetMap keeps track of those "gaps".
	 * 
	 */
	@Transient
	private Map<Integer,Integer> offsetMap = null; 
	@Transient
	private List<Integer> rowIndexes = null;
	
	@Transient
	public DataCell getCell(int rowIndex, int colIndex) {
		if (offsetMap == null) {
			buildOffsetMap();
		}

		int offset = offsetMap.get(rowIndex);
		int cellIdx = offset + colIndex;
		DataCell cell = cells.get(cellIdx); // assume cells are ordered
		assert(cell.getRowIndex() == rowIndex);
		assert(cell.getColumnIndex() == colIndex);
		return cell;
	}

	@Transient
	private void buildOffsetMap() {
		offsetMap = new HashMap<>();
		int curRow=-1;
		int curOffset=0;
		int lastCellIdx=-1;
		for (DataCell cell : cells) { // assume cells are ordered
			int row = cell.getRowIndex();
			if (row != curRow) {
				offsetMap.put(row, curOffset);
				curRow = row;
			}
			int cellIdx = cell.getCellIndex();
			curOffset += cell.getCellIndex()-lastCellIdx;
			lastCellIdx = cellIdx;
		}
	}

	public List<DataMatrixResource> getResources() {
		return resources;
	}

	public List<Integer> getRowIndexes() {
		if (rowIndexes == null) {
			if (offsetMap == null) {
				buildOffsetMap();
			}
			rowIndexes = offsetMap.keySet().stream().sorted().collect(Collectors.toList());
		}
		return rowIndexes;
	}
	
}
