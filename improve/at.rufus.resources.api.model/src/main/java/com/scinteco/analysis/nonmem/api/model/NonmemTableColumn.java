package com.scinteco.analysis.nonmem.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="NONMEM_TABLE_COLUMN")
public class NonmemTableColumn extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="NAME")
	private String name;

	@Column(name="COLUMN_NR")
	private int columnNr;

	@ManyToOne
	@JoinColumn(name="NONMEM_TABLE", referencedColumnName="ID")
	private NonmemTable table;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public int getColumnNr() {
		return columnNr;
	}
	
	public void setColumnNr(int columnNr) {
		propertyChangeSupport.firePropertyChange("columnNr", this.columnNr, this.columnNr = columnNr);
	}

	public NonmemTable getTable() {
		return table;
	}
	
	public void setTable(NonmemTable table) {
		propertyChangeSupport.firePropertyChange("table", this.table, this.table = table);
	}
	
}
