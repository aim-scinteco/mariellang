package com.scinteco.analysis.nonmem.api.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="NONMEM_TABLE_CELL")
public class NonmemTableCell extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="VALUE")
	private BigDecimal value;

	@Column(name="VALUE_SPECIAL")
	private String valueSpecial;

	@Column(name="ROW_NR")
	private int rowNr;

	@Column(name="COLUMN_NR")
	private int columnNr;

	@Column(name="FIXED")
	private boolean fixed;

	@Column(name="SAME")
	private boolean same;

	@ManyToOne
	@JoinColumn(name="NONMEM_TABLE", referencedColumnName="ID")
	private NonmemTable table;
	
	public BigDecimal getValue() {
		return value;
	}
	
	public void setValue(BigDecimal value) {
		propertyChangeSupport.firePropertyChange("value", this.value, this.value = value);
	}
	
	public String getValueSpecial() {
		return valueSpecial;
	}
	
	public void setValueSpecial(String valueSpecial) {
		propertyChangeSupport.firePropertyChange("valueSpecial", this.valueSpecial, this.valueSpecial = valueSpecial);
	}
	
	public int getRowNr() {
		return rowNr;
	}
	
	public void setRowNr(int rowNr) {
		propertyChangeSupport.firePropertyChange("rowNr", this.rowNr, this.rowNr = rowNr);
	}
	
	public int getColumnNr() {
		return columnNr;
	}
	
	public void setColumnNr(int columnNr) {
		propertyChangeSupport.firePropertyChange("columnNr", this.columnNr, this.columnNr = columnNr);
	}
	
	public boolean isFixed() {
		return fixed;
	}
	
	public void setFixed(boolean fixed) {
		propertyChangeSupport.firePropertyChange("fixed", this.fixed, this.fixed = fixed);
	}
	
	public boolean isSame() {
		return same;
	}
	
	public void setSame(boolean same) {
		propertyChangeSupport.firePropertyChange("same", this.same, this.same = same);
	}
	
	public NonmemTable getTable() {
		return table;
	}
	
	public void setTable(NonmemTable table) {
		propertyChangeSupport.firePropertyChange("table", this.table, this.table = table);
	}
	
}
