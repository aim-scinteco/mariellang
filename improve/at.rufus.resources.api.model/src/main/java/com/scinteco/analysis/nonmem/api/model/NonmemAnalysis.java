package com.scinteco.analysis.nonmem.api.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.analysis.api.model.Run;
import at.rufus.base.api.model.Entity;
import at.rufus.resources.api.model.FileVersion;

@javax.persistence.Entity
@Table(name="NONMEM_ANALYSIS")
public class NonmemAnalysis extends Entity {
	private static final long serialVersionUID = 1L;

	public static final Comparator<NonmemAnalysis> COMPARE_BY_POSITION = new Comparator<NonmemAnalysis>() {
		@Override
		public int compare(NonmemAnalysis o1, NonmemAnalysis o2) {
			return o1.getPosition() - o2.getPosition();
		}
	};
	
	@Column(name="OBJECTIVE_FUNCTION_VALUE")
	private BigDecimal objectiveFunctionValue;

	@Column(name="NAME")
	private String name;

	@Column(name="PROBLEM_NUMBER")
	private int problemNumber;

	@Column(name="OBSERVED_RECORDS_NUMBER")
	private Integer numberOfObservedRecords;
	
	@Column(name="INDIVIDUALS_NUMBER")
	private Integer numberOfIndividuals;
	
	@Column(name="POSITION")
	private int position;

	@Column(name="SELECTED")
	private boolean selected;

	@ManyToOne
	@JoinColumn(name="NONMEM_RUN", referencedColumnName="ID")
	private Run run;
	
	@ManyToOne
	@JoinColumn(name="COMMAND_FILE_VERSION", referencedColumnName="ID")
	private FileVersion commandFileVersion;

	@ManyToOne
	@JoinColumn(name="OUTPUT_FILE_VERSION", referencedColumnName="ID")
	private FileVersion outputFileVersion;
	
	@OneToMany(mappedBy="analysis")
	private List<NonmemTable> tables = new TrackedList<>(propertyChangeSupport, "tables");
	
	public BigDecimal getObjectiveFunctionValue() {
		return objectiveFunctionValue;
	}
	
	public void setObjectiveFunctionValue(BigDecimal objectiveFunctionValue) {
		this.objectiveFunctionValue = objectiveFunctionValue;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public int getProblemNumber() {
		return problemNumber;
	}
	
	public void setProblemNumber(int problemNumber) {
		propertyChangeSupport.firePropertyChange("problemNumber", this.problemNumber, this.problemNumber = problemNumber);
	}

	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		propertyChangeSupport.firePropertyChange("position", this.position, this.position = position);
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		propertyChangeSupport.firePropertyChange("selected", this.selected, this.selected = selected);
	}
	
	public Run getRun() {
		return run;
	}
	
	public void setRun(Run run) {
		propertyChangeSupport.firePropertyChange("run", this.run, this.run = run);
	}
	
	public FileVersion getCommandFileVersion() {
		return commandFileVersion;
	}
	
	public void setCommandFileVersion(FileVersion commandFileVersion) {
		propertyChangeSupport.firePropertyChange("commandFileVersion", this.commandFileVersion, this.commandFileVersion = commandFileVersion);
	}
	
	public FileVersion getOutputFileVersion() {
		return outputFileVersion;
	}
	
	public void setOutputFileVersion(FileVersion outputFileVersion) {
		propertyChangeSupport.firePropertyChange("outputFileVersion", this.outputFileVersion, this.outputFileVersion = outputFileVersion);
	}
	
	public List<NonmemTable> getTables() {
		return tables;
	}

	public Integer getNumberOfObservedRecords() {
		return numberOfObservedRecords;
	}

	public void setNumberOfObservedRecords(Integer numberOfObservedRecords) {
		propertyChangeSupport.firePropertyChange("numberOfObservedRecords", this.numberOfObservedRecords, this.numberOfObservedRecords = numberOfObservedRecords);
	}

	public Integer getNumberOfIndividuals() {
		return numberOfIndividuals;
	}

	public void setNumberOfIndividuals(Integer numberOfIndividuals) {
		propertyChangeSupport.firePropertyChange("numberOfIndividuals", this.numberOfIndividuals, this.numberOfIndividuals = numberOfIndividuals);
	}
	
	@Transient
	public BigDecimal getConditionNumber() {
		for (NonmemTable nonmemTable : tables) {
			if (nonmemTable.getName() == NonmemTableName.EIGENVALUES && nonmemTable.getNumberOfRows() == 2) {
				BigDecimal min = null;
				BigDecimal max = null;
				for (int i = 0; i < nonmemTable.getNumberOfColumns(); i++) {
					NonmemTableCell cell = nonmemTable.findCell(2, i+1);
					if (cell != null && cell.getValue() != null) {
						if (min == null) {
							min = cell.getValue();
						} else if (min.compareTo(cell.getValue()) >= 0) {
							min = cell.getValue();
						}
						
						if (max == null) {
							max = cell.getValue();
						} else if (max.compareTo(cell.getValue()) <= 0) {
							max = cell.getValue();
						}
					}
				}
				if (min != null && max != null) {
					return max.divide(min, MathContext.DECIMAL32);
				}
			} 
		}
		return null;
	}

}
