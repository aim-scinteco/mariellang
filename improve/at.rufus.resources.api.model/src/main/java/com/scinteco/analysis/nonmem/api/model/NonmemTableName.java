package com.scinteco.analysis.nonmem.api.model;

public enum NonmemTableName {

	THETA,
	OMEGA,
	SIGMA,
	ETABAR,
	P_VAL,
	SHRINKAGE,
	EIGENVALUES
	
	
}
