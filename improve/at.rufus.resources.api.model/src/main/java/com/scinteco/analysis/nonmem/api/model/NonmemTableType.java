package com.scinteco.analysis.nonmem.api.model;

public enum NonmemTableType {

	// used for command file parsed table
	initial("Initial"),				
	
	// used for output file parsed table
	finalparam("Final"),	
	stderr("Stderr");
	
	private String label;
	
	private NonmemTableType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
