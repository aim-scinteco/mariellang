package com.scinteco.analysis.nonmem.api.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.mariella.persistence.runtime.TrackedList;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="NONMEM_TABLE")
public class NonmemTable extends Entity {
	private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private NonmemTableName name;

	@Column(name="TYPE")
	private NonmemTableType type;

	@ManyToOne
	@JoinColumn(name="NONMEM_ANALYSIS", referencedColumnName="ID")
	private NonmemAnalysis analysis;

	@OneToMany(mappedBy="table")
	private List<NonmemTableColumn> columns = new TrackedList<>(propertyChangeSupport, "columns");
	
	@OneToMany(mappedBy="table")
	private List<NonmemTableRow> rows = new TrackedList<>(propertyChangeSupport, "rows");
	
	@OneToMany(mappedBy="table")
	private List<NonmemTableCell> cells = new TrackedList<>(propertyChangeSupport, "cells");
	
	public NonmemTableName getName() {
		return name;
	}
	
	public void setName(NonmemTableName name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public NonmemTableType getType() {
		return type;
	}
	
	public void setType(NonmemTableType type) {
		propertyChangeSupport.firePropertyChange("type", this.type, this.type = type);
	}
	
	public NonmemAnalysis getAnalysis() {
		return analysis;
	}
	
	public void setAnalysis(NonmemAnalysis analysis) {
		propertyChangeSupport.firePropertyChange("analysis", this.analysis, this.analysis = analysis);
	}
	
	public List<NonmemTableColumn> getColumns() {
		return columns;
	}
	
	public List<NonmemTableRow> getRows() {
		return rows;
	}
	
	public List<NonmemTableCell> getCells() {
		return cells;
	}
	
	@Transient
	public int getNumberOfColumns() {
		int nrOfColumns = 0;
		for (NonmemTableCell cell : cells) {
			nrOfColumns = Math.max(nrOfColumns, cell.getColumnNr());
		}
		for (NonmemTableColumn column : columns) {
			nrOfColumns = Math.max(nrOfColumns, column.getColumnNr());
		}
		return nrOfColumns;
	}
	
	@Transient
	public int getNumberOfRows() {
		int nrOfRows = 0;
		for (NonmemTableCell cell : cells) {
			nrOfRows = Math.max(nrOfRows, cell.getRowNr());
		}
		for (NonmemTableRow row : rows) {
			nrOfRows = Math.max(nrOfRows, row.getRowNr());
		}
		return nrOfRows;
	}
	
	public NonmemTableRow findRow(int rowNr) {
		for (NonmemTableRow row : rows) {
			if (row.getRowNr() == rowNr) {
				return row;
			}
		}
		return null;
	}

	public NonmemTableColumn findColumn(int colNr) {
		for (NonmemTableColumn col : columns) {
			if (col.getColumnNr() == colNr) {
				return col;
			}
		}
		return null;
	}

	public NonmemTableCell findCell(int rowNr, int colNr) {
		for (NonmemTableCell cell : cells) {
			if (cell.getColumnNr() == colNr && cell.getRowNr() == rowNr) {
				return cell;
			}
		}
		return null;
	}

}
