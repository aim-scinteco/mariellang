package com.scinteco.analysis.nonmem.api.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.rufus.base.api.model.Entity;

@javax.persistence.Entity
@Table(name="NONMEM_TABLE_ROW")
public class NonmemTableRow extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(name="NAME")
	private String name;

	@Column(name="ROW_NR")
	private int rowNr;

	@ManyToOne
	@JoinColumn(name="NONMEM_TABLE", referencedColumnName="ID")
	private NonmemTable table;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange("name", this.name, this.name = name);
	}
	
	public int getRowNr() {
		return rowNr;
	}
	
	public void setRowNr(int rowNr) {
		propertyChangeSupport.firePropertyChange("rowNr", this.rowNr, this.rowNr = rowNr);
	}

	public NonmemTable getTable() {
		return table;
	}
	
	public void setTable(NonmemTable table) {
		propertyChangeSupport.firePropertyChange("table", this.table, this.table = table);
	}
	
}
